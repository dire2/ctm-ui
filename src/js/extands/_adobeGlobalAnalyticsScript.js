(function(Core){
	var md = null;
	var queryString = '';
	var language = (navigator.language != null) ? navigator.language : 'ko-KR';
	var currencyType = 'KRW';
	var isMobile = _GLOBAL.DEVICE.IS_MOBILE();
	var isSnkrs = _GLOBAL.SITE.IS_SNKRS || false;
	var experienceType = isSnkrs ? 'snkrs' : 'nikecom';
	var pageType = '';
	var eventName = '';
	var classification = '';
	var filtered = {};
	var dl = {};
	var schemaIds = {}
	var isStaging = false;
	var isProd = false;
	var eventQueueList = [];
	var isEventCallReady = false;
	
	var eventNames = {
		CLICK: 'Content Clicked',
		NAVIGATION_CLICK: 'Navigation Clicked',
		BANNER_CLICK: 'Banner Clicked',
		PAGE_VIEWED: 'Page Viewed',
		PRODUCT_LIST_VIEWED: 'Product List Viewed',
		PRODUCT_LIST_FILTERED: 'Product List Filtered',
		PRODUCT_VIEWED: 'Product Viewed',
		PRODUCT_REVIEWED: 'Product Reviewed',
		PRODUCT_REMOVED : 'Product Removed',
		PRODUCT_ADDED : 'Product Added',
		PRODUCT_ADDED_ERROR_VIEWED : 'Add To Cart Error Viewed',
		PRODUCT_CLICKED : 'Product Clicked',
		PRODUCT_SAVED : 'Product Saved',
		PRODUCT_UNSAVED : 'Products Unsaved',
		PRODUCTS_SEARCHED : 'Products Searched',
		PRODUCT_SIZE_SELECTED : 'Product Size Selected',
		CART_VIEWED: 'Cart Viewed',
		CHECKOUT_STARTED:'Checkout Started',
		ORDER_INFO_VIEWED: 'Order Info Viewed',
		SHIPPING_INFO_VIEWED: 'Shipping Info Viewed',
		PAYMENT_INFO_VIEWED: 'Payment Info Viewed',
		ORDER_CONFIRMATION_VIEWED: 'Order Confirmation Viewed',
		ORDER_COMPLETED: 'Order Completed',
		ACCOUNT_AUTHENTICATED: 'Account Authenticated',
		ACCOUNT_CREATED: 'Account Created',
		WISHLIST_VIEWED : 'Wishlist Viewed',
		SEARCH_POPULAR_SUGGESTION_CLICKED : 'Search Popular Suggestion Clicked',
		SEARCH_SUBMITTED : 'Search Submitted',
		SEARCH_TYPEAHEAD_CLICKED : 'Search Typeahead Clicked',
		FILTER_APPLIED : 'Filter Applied',
		FILTER_MENU_TOGGLED : 'Filter Menu Toggled',
		ELEVATED_CONTENT_VIEWED : 'Elevated Content Viewed',
		NOTIFY_ME_DISMISSED: 'Notify Me Dismissed',
		NOTIFY_ME_SUBMITTED: 'Notify Me Submitted',
		PICKUP_OFFERINGS_VIEWED: 'Pickup Offerings Viewed',
		PRODUCT_ZOOMED: 'Product Zoomed',
		PRODUCT_ZOOM_CLOSED: 'Product Zoom Closed',
		RECOMMENDED_PRODUCT_SELECTED: 'Recommended Product Selected',
		RECOMMENDED_PRODUCT_CLICKED: 'Recommended Product Clicked',
		PROMO_CODE_APPLIED: 'Promo Code Applied',
		PROMO_CODE_REJECTED: 'Promo Code Rejected',
		RECOMMENDED_PRODUCTS_CAROUSEL_SHOWN: 'Recommended Products Carousel Shown',
		CHECKOUT_INTENT_START: 'Checkout Intent Start',
		ERROR_MODAL_VIEWED: 'Error Modal Viewed',
		FULFILLMENT_LOCATION_ENTERED: 'Fulfillment Location Entered',
		LOGIN_PROMPT_VIEWED: 'Login Prompt Viewed',
		ADDRESS_SUGGESTION_SELECTED: 'Address Suggestion Selected',
		PAYMENT_METHOD_SELECTED: 'Payment Method Selected',
		SHIPPING_METHOD_SELECTED: 'Shipping Method Selected',
		SHIPPING_ADDRESS_SELECTED: 'Shipping Address Selected',
		SHIPPING_OPTIONS_AVAILABLE: 'Shipping Options Available',
		SHIPPING_OPTION_SELECTED: 'Shipping Option Selected',
		ORDER_DETAILS_VIEWED: 'Order Details Viewed',
		ORDER_HISTORY_VIEWED: 'Order History Viewed',
		RETURN_ITEMS_VIEWED: 'Return Items Viewed',
		RETURN_ITEM_CLICKED: 'Return Item Clicked',
		RETURN_REASON_CLICKED: 'Return Reason Clicked',
		ORDER_CANCELLATION_SUCCESS_VIEWED: 'Order Cancellation Success Viewed',
		ORDER_CANCELLATION_FAILURE_VIEWED: 'Order Cancellation Failure Viewed',
		LOCATOR_VIEWED: 'Locator Viewed',
		NOT_FOUND_VIEWED: 'Not Found Viewed',
		MAP_MARKER_CLICKED: 'Map Marker Clicked',
		STORY_CLICKED: 'Story Clicked',
		SIDE_STORY_CLICKED: 'Side Story Clicked',
		IMPRESSION: 'Impression Tracked',
		VIDEO_PAUSED: 'Video Paused',
		VIDEO_ENTERED_FULLSCREEN: 'Video Entered Fullscreen',
		VIDEO_PLAYED: 'Video Played',
		VIDEO_REWOUND: 'Video Rewound',
		VIDEO_EXITED_FULLSCREEN: 'Video Exited Fullscreen',
		VIDEO_MUTED: 'Video Muted',
		VIDEO_UNMUTED: 'Video Unmuted',
		VIDEO_SUBTITLES_OFF: 'Video Subtitles Turned Off',
		VIDEO_SUBTITLES_ON: 'Video Subtitles Turned On',
		VIDEO_RESTARTED: 'Video Restarted',
		VIDEO_STARTED: 'Video Started',
		VIDEO_ENDED: 'Video Ended',
		BUY_NOW_CLICKED: 'Buy Now Clicked',
		PRODUCT_SHOWN : 'Product Shown'
	};

	var eventTypes = {
		PAGE: 'page',
		TRACK: 'track',
		CHANGE_VIEW: 'changeView'
	};
	var classifications = {
		EXPERIENCE : 'experience event',
		CORE_BUY_FLOW : 'core buy flow'
	}
	var shippingOptions = [ '1:x:x:0:' + currencyType, '2:x:x:5000:' + currencyType];
	function getSchemaUrl(eventName, eventType, experience, isCoreBuyFlow){
		var url = '';
		url += 'https://www.nike.com/assets/measure/schemas/digital-product/dotcom/platform/web/classification/' +
				( isCoreBuyFlow ? 'core-buy-flow' : 'experience-event') + 
				'/experience/'+experience+'/event-type/'+eventType+'/event-name/'+eventName+'/version/LATEST/schema.json';
		return url;
	}
	function createSchemaIdAttrbute(eventName, experience, isCoreBuyFlow){
		var experienceType = experience;
		if(isSnkrs && experience != 'stories'){
			experienceType = 'snkrs';
		}
		return { eventName: eventName, experience: experienceType, isCoreBuyFlow: isCoreBuyFlow };
	}
	function setSchemaId(eventName, experience, isCoreBuyFlow){
		schemaIds[eventName] = createSchemaIdAttrbute(Core.utils.string.toLower(eventName.split(' ').join('-')), experience, isCoreBuyFlow);
	}
	function createSchemaIds(){
		schemaIds = {
			[eventNames.CLICK]: createSchemaIdAttrbute('content-clicked', getSchemaExperienceType(), false),
			[eventNames.PAGE_VIEWED]: createSchemaIdAttrbute('page-viewed', getSchemaExperienceType(), true),
			[eventNames.PRODUCT_LIST_VIEWED]: createSchemaIdAttrbute('product-list-viewed', 'pw', true),
			[eventNames.PRODUCT_LIST_FILTERED]: createSchemaIdAttrbute('product-list-filtered', 'pw', true),
			[eventNames.PRODUCT_VIEWED]: createSchemaIdAttrbute('product-viewed', 'pdp', true),
			[eventNames.CART_VIEWED]: createSchemaIdAttrbute('cart-viewed', 'cart', true),
			[eventNames.CHECKOUT_STARTED]: createSchemaIdAttrbute('checkout-started', 'checkout', true),
			[eventNames.ORDER_INFO_VIEWED]: createSchemaIdAttrbute('order-info-viewed', 'checkout', false),
			[eventNames.SHIPPING_INFO_VIEWED]: createSchemaIdAttrbute('shipping-info-viewed', 'checkout', false),
			[eventNames.PAYMENT_INFO_VIEWED]: createSchemaIdAttrbute('payment-info-viewed', 'checkout', false),
			[eventNames.ORDER_CONFIRMATION_VIEWED]: createSchemaIdAttrbute('order-confirmation-viewed', 'checkout', true),
			[eventNames.ORDER_COMPLETED]: createSchemaIdAttrbute('order-completed', 'checkout', true),
			[eventNames.ACCOUNT_AUTHENTICATED]: createSchemaIdAttrbute('account-authenticated', getSchemaExperienceType(), true),
			[eventNames.ACCOUNT_CREATED]: createSchemaIdAttrbute('account-created', getSchemaExperienceType(), true),
			[eventNames.PRODUCT_REMOVED]: createSchemaIdAttrbute('product-removed', 'cart', true),
			[eventNames.PRODUCT_ADDED]: createSchemaIdAttrbute('product-added', getSchemaExperienceType(), true),
			[eventNames.PRODUCT_ADDED_ERROR_VIEWED]: createSchemaIdAttrbute('add-to-cart-error-viewed', getSchemaExperienceType(), true),
			[eventNames.PRODUCT_CLICKED]: createSchemaIdAttrbute('product-clicked', 'pw', true),
			[eventNames.PRODUCTS_SEARCHED]: createSchemaIdAttrbute('products-searched', 'pw', true),
			[eventNames.WISHLIST_VIEWED]: createSchemaIdAttrbute('wishlist-viewed', 'wishlist', false),
			[eventNames.SEARCH_POPULAR_SUGGESTION_CLICKED]: createSchemaIdAttrbute('search-popular-suggestion-clicked', 'global-nav', false),
			[eventNames.SEARCH_SUBMITTED]: createSchemaIdAttrbute('search-submitted', 'global-nav', false),
			[eventNames.SEARCH_TYPEAHEAD_CLICKED]: createSchemaIdAttrbute('search-typeahead-clicked', 'global-nav', false),
			[eventNames.FILTER_APPLIED]: createSchemaIdAttrbute('filter-applied', 'pw', false),
			[eventNames.FILTER_MENU_TOGGLED]: createSchemaIdAttrbute('filter-menu-toggled', 'pw', false),
			[eventNames.ELEVATED_CONTENT_VIEWED]: createSchemaIdAttrbute('elevated-content-viewed', 'pdp', false),
			[eventNames.NOTIFY_ME_DISMISSED]: createSchemaIdAttrbute('notify-me-dismissed', 'pdp', false),
			[eventNames.NOTIFY_ME_SUBMITTED]: createSchemaIdAttrbute('notify-me-submitted', 'pdp', false),
			[eventNames.PICKUP_OFFERINGS_VIEWED]: createSchemaIdAttrbute('pickup-offerings-viewed', 'pdp', false),
			[eventNames.PRODUCT_REVIEWED]: createSchemaIdAttrbute('product-reviewed', 'pdp', false),
			[eventNames.PRODUCT_SAVED]: createSchemaIdAttrbute('product-saved', getSchemaExperienceType(), false),
			[eventNames.PRODUCT_SIZE_SELECTED]: createSchemaIdAttrbute('product-size-selected', 'pdp', false),
			[eventNames.PRODUCT_UNSAVED]: createSchemaIdAttrbute('product-unsaved', getSchemaExperienceType(), false),
			[eventNames.PRODUCT_ZOOMED]: createSchemaIdAttrbute('product-zoomed', 'pdp', false),
			[eventNames.PRODUCT_ZOOM_CLOSED]: createSchemaIdAttrbute('product-zoom-closed', 'pdp', false),
			[eventNames.RECOMMENDED_PRODUCT_SELECTED]: createSchemaIdAttrbute('recommended-product-selected', 'pdp', false),
			[eventNames.PROMO_CODE_APPLIED]: createSchemaIdAttrbute('promo-code-applied', getSchemaExperienceType(), false),
			[eventNames.PROMO_CODE_REJECTED]: createSchemaIdAttrbute('promo-code-rejected', getSchemaExperienceType(), false),
			[eventNames.RECOMMENDED_PRODUCT_CLICKED]: createSchemaIdAttrbute('recommended-product-clicked', getSchemaExperienceType(), false),
			[eventNames.RECOMMENDED_PRODUCTS_CAROUSEL_SHOWN]: createSchemaIdAttrbute('recommended-products-carousel-shown', getSchemaExperienceType(), false),
			[eventNames.CHECKOUT_INTENT_START]: createSchemaIdAttrbute('checkout-intent-start', 'cart', false),
			[eventNames.ERROR_MODAL_VIEWED]: createSchemaIdAttrbute('error-modal-viewed', 'checkout', false),
			[eventNames.FULFILLMENT_LOCATION_ENTERED]: createSchemaIdAttrbute('fulfillment-location-entered', 'checkout', false),
			[eventNames.LOGIN_PROMPT_VIEWED]: createSchemaIdAttrbute('login-prompt-viewed', 'checkout', false),
			[eventNames.ADDRESS_SUGGESTION_SELECTED]: createSchemaIdAttrbute('address-suggestion-selected', 'checkout', false),
			[eventNames.PAYMENT_METHOD_SELECTED]: createSchemaIdAttrbute('payment-method-selected', 'checkout', false),
			[eventNames.SHIPPING_METHOD_SELECTED]: createSchemaIdAttrbute('shipping-method-selected', 'checkout', false),
			[eventNames.SHIPPING_ADDRESS_SELECTED]: createSchemaIdAttrbute('shipping-address-selected', 'checkout', false),
			[eventNames.SHIPPING_OPTIONS_AVAILABLE]: createSchemaIdAttrbute('shipping-options-available', 'checkout', false),
			[eventNames.SHIPPING_OPTION_SELECTED]: createSchemaIdAttrbute('shipping-option-selected', 'checkout', false),
			[eventNames.ORDER_DETAILS_VIEWED]: createSchemaIdAttrbute('order-details-viewed', 'myorders', false),
			[eventNames.ORDER_HISTORY_VIEWED]: createSchemaIdAttrbute('order-history-viewed', 'myorders', false),
			[eventNames.RETURN_ITEMS_VIEWED]: createSchemaIdAttrbute('return-items-viewed', 'myorders', false),
			[eventNames.RETURN_ITEM_CLICKED]: createSchemaIdAttrbute('return-item-clicked', 'myorders', false),
			[eventNames.RETURN_REASON_CLICKED]: createSchemaIdAttrbute('return-reason-clicked', 'myorders', false),
			[eventNames.ORDER_CANCELLATION_SUCCESS_VIEWED]: createSchemaIdAttrbute('order-cancellation-success-viewed', 'myorders', false),
			[eventNames.ORDER_CANCELLATION_FAILURE_VIEWED]: createSchemaIdAttrbute('order-cancellation-failure-viewed', 'myorders', false),
			[eventNames.LOCATOR_VIEWED]: createSchemaIdAttrbute('locator-viewed', 'store-locator', false),
			[eventNames.NOT_FOUND_VIEWED]: createSchemaIdAttrbute('not-found-viewed', 'store-locator', false),
			[eventNames.MAP_MARKER_CLICKED]: createSchemaIdAttrbute('map-marker-clicked', 'store-locator', false),
			[eventNames.STORY_CLICKED]: createSchemaIdAttrbute('story-clicked', 'stories', false),
			[eventNames.SIDE_STORY_CLICKED]: createSchemaIdAttrbute('side-story-clicked', 'stories', false),
			[eventNames.VIDEO_PAUSED]: createSchemaIdAttrbute('video-paused', 'landing-page', false),	
			[eventNames.VIDEO_PLAYED]: createSchemaIdAttrbute('video-played', 'landing-page', false),	
			[eventNames.VIDEO_REWOUND]: createSchemaIdAttrbute('video-rewound', 'landing-page', false),	
			[eventNames.VIDEO_ENTERED_FULLSCREEN]: createSchemaIdAttrbute('video-entered-fullscreen', 'landing-page', false),	
			[eventNames.VIDEO_EXITED_FULLSCREEN]: createSchemaIdAttrbute('video-exited-fullscreen', 'landing-page', false),	
			[eventNames.VIDEO_MUTED]: createSchemaIdAttrbute('video-muted', 'landing-page', false),	
			[eventNames.VIDEO_UNMUTED]: createSchemaIdAttrbute('video-unmuted', 'landing-page', false),	
			[eventNames.VIDEO_SUBTITLES_OFF]: createSchemaIdAttrbute('video-subtitles-turned-off', 'landing-page', false),	
			[eventNames.VIDEO_SUBTITLES_ON]: createSchemaIdAttrbute('video-subtitles-turned-on', 'landing-page', false),	
			[eventNames.VIDEO_RESTARTED]: createSchemaIdAttrbute('video-restarted', 'landing-page', false),	
			[eventNames.VIDEO_STARTED]: createSchemaIdAttrbute('video-started', 'landing-page', false),	
			[eventNames.VIDEO_ENDED]: createSchemaIdAttrbute('video-ended', 'landing-page', false),	
			[eventNames.BUY_NOW_CLICKED]: createSchemaIdAttrbute('buy-now-clicked', 'pdp', false),
			[eventNames.PRODUCT_SHOWN]: createSchemaIdAttrbute('product-shown', 'pw', false),
		}
		return;
	}
	var WRITE_KEY = {
		STG : 'POwa4r8vBBSw7xdQZ0dqGlNuyaT7Y7pZ',
		PROD : '2iv4Qa7rFEipAs4iE850BkBpTYQvlAFZ'
	}
	$(window).bind("pageshow", function (event) {
		if (event.originalEvent.persisted) {
			try {
				if( _GLOBAL.MARKETING_DATA().pageType != 'checkout'){
					sessionStorage.removeItem('checkoutStarted');	
				}
			} catch (error) {}
			// 뒤로가기로 페이지 로드 시
		}
	});
	function init(){
		var currnetUrl = Core.utils.url.getCurrentUrl();
		isStaging = currnetUrl.indexOf('stg-www') > -1;
		isProd = currnetUrl.indexOf('nike.com') > -1;
		try {
			var isEnableDryRun = false;	
			// 로컬 일 때 isEnableDryRun 활성화
			if( !isStaging && !isProd ){
				isEnableDryRun = true;
			}
			if( !isProd ) {
				analyticsClient.debug();
			}
			analyticsClient.load({ enableDryRun: isEnableDryRun, countryCode : 'KR', uniteTimeout : 1 });
		} catch (error) {
			console.warn("Can't not found AnalyticsClient!");
			return;
		}

		queryString = Core.utils.url.getQueryStringParams( Core.utils.url.getCurrentUrl());
		md = _GLOBAL.MARKETING_DATA();

		$(document).ready( function(){
			// korea adobe 설정해놓은 값 중 캐치해서 사용해야 하는 상황에서 사용
			$('body').on('click', '[data-click-name]', function(e){
				/*
				var clickArea = $(this).data('click-area');
				// 메인이나 PW Landing
				
				if( clickArea == 'hp' || clickArea == 'glp'){
					contentClickEvent.call( this );
				}
				*/
				if( $(this).is('[data-global-click-name]') == false ){
					contentClickEvent.call( this );
				}
				/*
				var name = $(this).data('click-name') || '';
				if( Core.utils.string.startsWith(name, 'in wall content:')){
					var content = name.split(':')[1];
					endPoint.call('clickEventHandler', {area : 'pw', name : 'Content Clicked', activity : 'con:1:gridwall:' + content});
				}
				*/
			})

			$('body').on('click', '[data-global-click-name]', function(e){
				if ($(this).data("click-enable") == false) {
					return;
				}
				var name = $(this).data('global-click-name');
				var area = $(this).data('global-click-area') || null;
				var activity = $(this).data('global-click-activity') || null;
				var endPoint = Core.getComponents('component_endpoint');

				// toggle-on attribute 가 있으면 off 일 때, 즉 닫혀있어서 열리는 상황에 전송한다.
				if (!_.isUndefined($(this).attr('data-global-click-toggle-on'))) {
					if ($(this).data('click-toggle-on') == 'on'){
						$(this).data('click-toggle-on', 'off');
						return;
					}else{
						$(this).data('click-toggle-on', 'on');
					}
				}

				endPoint.call('clickEventHandler', {area : area, name : name, activity : activity});
			})
		})

		if( md.pageType != 'checkout'){
			sessionStorage.removeItem('checkoutStarted');	
		}

		callPageEvent();
		callTrackEvent();
	}
	
	function callPageEvent(){
		log('callPageEvent');
		var eventType = eventTypes.PAGE;
		var isLoginSuccessCheck = false;
		var isCall = true;
		switch( md.pageType ){
			case 'home':
				pageType = 'homepage';
				eventName = eventNames.PAGE_VIEWED;
				classification = classifications.CORE_BUY_FLOW;
			break;
			case 'category' :
				$.extend( dl, getCategoryData());
			break;
			case 'search' :
				$.extend( dl, getSearchData());
			break;
			case 'product' :
				$.extend( dl, getProductData());
			break; 
			case 'cart' :
				isLoginSuccessCheck = true;
				$.extend( dl, getCartData());
				/*
				if(Core.utils.is.isEmpty(md.itemList)){
					isCall = false;
				}
				*/
			break;
			case 'checkout' :
				isLoginSuccessCheck = true;
				$.extend( dl, getCheckoutData());
			break;
			case 'confirmation' :
				$.extend( dl, getOrderConfirmationData());
			break;
			case 'register':
				eventType = eventTypes.CHANGE_VIEW;
				$.extend( dl, getRegisterData());
			break;
			case 'registerSuccess':
				isCall = false;
				var startPageType = sessionStorage.getItem('checkRegistStartPage');
				if( startPageType == 'cart' ){
					$.extend( dl, getRegisterSuccessData('cart'));
					sessionStorage.removeItem('checkRegistStartPage');
				}else{
					$.extend( dl, getRegisterSuccessData('registration'));
				}
			break;
			case 'wishlist':
				if(Core.utils.is.isEmpty(md.itemList)){
					isCall = false;
				}
				$.extend( dl, getWishListData());
			break;
			case 'orderDetail':
				pageType = 'myorders';
				eventName = eventNames.ORDER_DETAILS_VIEWED;
			break;
			case 'orderHistory':
				pageType = 'myorders';
				eventName = eventNames.ORDER_HISTORY_VIEWED;
			break;
			case 'mypage':
				pageType = 'mypage';
				eventName = eventNames.ORDER_HISTORY_VIEWED;
			break;
			case 'orderReturnable':
				pageType = 'myorders';
				eventName = eventNames.PAGE_VIEWED;
			break;
			case 'orderReturned':
				pageType = 'myorders';
				eventName = eventNames.RETURN_ITEMS_VIEWED;
			break;
			case 'store':
				pageType = 'store locator';
				eventName = eventNames.LOCATOR_VIEWED;
				if(md.storeInfo.totalItemCount==0){
					eventName = eventNames.NOT_FOUND_VIEWED;
				}
			break;
			case 'content':	
				if( Core.utils.string.startsWith(md.pathName, '/l/')){
					pageType = String(md.pathName).replace('/l/', '') + ':land';
					classification = classifications.CORE_BUY_FLOW;
				}else if( Core.utils.string.startsWith(md.pathName, '/c/') ){
					pageType = String(md.pathName).replace('/c/', '') + ':land';
				}else if(md.pathName == '/login'){
					pageType = 'login';
					if(_GLOBAL.CUSTOMER.ISSIGNIN == false){
						pageType = 'guest';
					}
				}else{
					pageType = String(md.pathName).replace('/account/', '/member/').substr(1).replaceAll('/','>');
				}
				eventName = eventNames.PAGE_VIEWED;
			break;
		}

		if( pageType == '') return;
		createSchemaIds();
		$.extend( dl, getPageData(eventType, eventName)); // 기본 페이지 데이터 생성;

		if(isCall) submitDataLayer(dl);

		// 로그인 당시 콜백으로 이벤트를 받아 처리 가능하지만 스크립트가 호출되기전에 화면이 리프레시 되는 상황 때문에 타이밍 변경
		if( isLoginSuccessCheck ) loginSuccessTrackEvent();
	}
	function callTrackEvent(){
		log('callTrackEvent');
		var data = null;
		switch( md.pageType ){
			case 'category' :
				submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.PRODUCT_LIST_VIEWED, null, classifications.CORE_BUY_FLOW)));
			break;
			case 'product' :
				// 하단 컨텐츠 있을 시
				if( md.productInfo.is1on1 == true ){
					submitDataLayer(getPageData(eventTypes.TRACK, eventNames.ELEVATED_CONTENT_VIEWED, 'pdp:elevated content viewed', classifications.EXPERIENCE));
				}

				// 픽업 가능한 상품
				if( md.productInfo.isAvailablePickup == true ){
					data = getPageData(eventTypes.TRACK, eventNames.PICKUP_OFFERINGS_VIEWED, null, classifications.EXPERIENCE);
					data.fulfillment = {
						pickupOfferingStatus : 'offerings' // no stores, stores but no offerings, offerings, no location detected
					}
					submitDataLayer(data);
				}
			break
			case 'search':
				submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.PRODUCTS_SEARCHED, null, classifications.CORE_BUY_FLOW)));
			break;
			case 'cart' :
				//if( md.itemList != null ){
					submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.CART_VIEWED, null, classifications.CORE_BUY_FLOW)));
				//}

				if( md.productRelatedProducts == null ){
					data = getPageData(eventTypes.TRACK, eventNames.RECOMMENDED_PRODUCTS_CAROUSEL_SHOWN, null, classifications.EXPERIENCE );
					data.cartId = md.cartId;
					submitDataLayer(data);
				}
			break
			case 'registerSuccess':
				submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.ACCOUNT_CREATED, null, classifications.CORE_BUY_FLOW )));
			break;
			case 'wishlist':
				if( Core.utils.is.isEmpty(md.itemList) == false ){
					submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.WISHLIST_VIEWED)));
				}
			break;
			case 'checkout' :
				if(md.marketingData.checkoutInfo != null) {
					var checkoutInfo = md.marketingData.checkoutInfo;
					if(checkoutInfo.step == 'shipping' || checkoutInfo.step == 'payment'){
						callCheckoutTrackEvent(eventNames.SHIPPING_ADDRESS_SELECTED, 'shipping methods');
						callCheckoutTrackEvent(eventNames.SHIPPING_OPTIONS_AVAILABLE);
						// TODO 옵션 선택된것 확인해서 처리 해야함
						callCheckoutTrackEvent(eventNames.SHIPPING_OPTION_SELECTED, null, { shipping : { selectedOption : shippingOptions[0] }});
						if(checkoutInfo.alreadyShippingAddress == true){
							callCheckoutTrackEvent(eventNames.FULFILLMENT_LOCATION_ENTERED);
						}
					}

					var isCheckoutStartedCall = sessionStorage.getItem('checkoutStarted');
					if(isCheckoutStartedCall != 'true'){
						sessionStorage.setItem('checkoutStarted', true);
						var data = getPageData(eventTypes.TRACK, eventNames.CHECKOUT_STARTED, null, classifications.CORE_BUY_FLOW);
						data.checkoutType = getCheckoutType();
						submitDataLayer($.extend( _.cloneDeep(dl), data));
					}
				}
			break;
			case 'confirmation' :
				submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.ORDER_COMPLETED, null, classifications.CORE_BUY_FLOW)));
			break;
			case 'store' :
				if( queryString._search != null ){
					submitDataLayer(getPageData(eventTypes.TRACK, eventNames.SEARCH_SUBMITTED, 'sl:search submitted'));
				}
			break;
		}
	}
	function loginSuccessTrackEvent(){
		if( sessionStorage.getItem('loginSuccess') != 'true' ){
			return;
		}
		sessionStorage.removeItem('loginSuccess');
		submitDataLayer( $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.ACCOUNT_AUTHENTICATED, null, classifications.CORE_BUY_FLOW)));
	}
	function getCategoryData(){
		pageType = 'pw';
		if( isSnkrs ){
			pageType = queryString.type == null ? 'feed' : queryString.type;
			// TODO 노출되는 상품정보다 다시 맞춰야함
		}
		eventName = eventNames.PRODUCT_LIST_VIEWED;
		classification = classifications.CORE_BUY_FLOW;

		var data = {};
		data.isRedirect = false;
		data.products = makeProducts(md.itemList, 'pw');

		if( !isSnkrs ){
			if( md.categoryInfo != null ){
				// data.category = md.categoryInfo.name;
				data.searchResultsCount = md.categoryInfo.totalCount;
				data.selectedConcepts = (function(){
					return String(md.categoryInfo.breadcrumbs).replace('Home||','').split('||');
				}());
			}
			data.selectedConceptsIds = [];
			$.each( data.selectedConcepts, function(index, selectedConcept){
				data.selectedConceptsIds.push('none-' + (index+1));
			})
		}
		return data;
	}
	function getSearchData(){
		pageType =  'pw';
		eventName = eventNames.PRODUCT_LIST_VIEWED; // track 호출 할 떄 이벤트 네임 변경하여 호출
		classification = classifications.CORE_BUY_FLOW;
		
		var isResultsfound = ( md.searchInfo.totalCount > 0 );
		var data = {};
		data.searchResultPageType = isResultsfound ? 'onsite search results' : 'no results found';
		data.searchResultsCount = md.searchInfo.totalCount;
		
		if( queryString.pl !=  't'){
			data.searchTerm = md.searchInfo.keyword;  //내가 검색어를 입력했을떄만 들어옴
		}
		data.searchText = md.searchInfo.keyword;
		data.products = makeProducts(md.itemList, 'pw');
		return data;
	}
	function getCategoryFilteredData(){
		var data ={};
		var search = location.search;
		var filters = [];
		var sorts = [];
		search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value){
			( key == 'sort') ? sorts.push({ type : value.split('+')[0], value : value.split('+')[1] }) : filters.push({ type : key, value : value });
		})
		data.filters = filters;
		data.sorts = sorts;
		data.content = {
			pwFacetList : (function(){
				var facetList = [];
				$.each( filters, function(index ,data){
					facetList.push(data.type + ':' + data.value);
				})
				return facetList;
			}()),
			pwSearchFacet : (function(){
				return (filtered.type == undefined) ? {} : (filtered.type + ':' + filtered.item.key +':' + filtered.item.value);
			}()),
			sortCriteria : (function(){
				var sort = '';
				$.each( sorts, function(index, data){
					sort = 'sort:' + data.type + '|' + data.value;
				})
				return sort;
			}())
		}
		return data;
	}
	function getProductData(){
		pageType = 'pdp';
		eventName = eventNames.PRODUCT_VIEWED;
		classification = classifications.CORE_BUY_FLOW;
		
		var data = {};
		var productData = makeProducts([md.productInfo], 'pdp');
		$.extend( data, productData[0]);
		data.products = productData;
		return data;
	}
	function getCartData(){
		pageType = 'cart';
		eventName = eventNames.CART_VIEWED;
		classification = classifications.CORE_BUY_FLOW;
		
		var data = {};
		data.products = [];
		data.cartItemCount = 0;
		data.cartUnitCount = 0;
		data.cartValue = 0;
		if( md.itemList != null ){
			data.products = makeProducts(md.itemList, 'cart');
			data.cartItemCount = md.totalItemCount;
			data.cartUnitCount = md.itemList.length || 0;
			data.cartValue = md.cartTotalAmount;
		}
		return data;
	}
	function getCheckoutData(){
		pageType = 'checkout';
		if( Core.utils.is.isEmpty( md.marketingData )){
			pageType = '';
			return;
		}
		var checkoutInfo = md.marketingData.checkoutInfo;
		if(checkoutInfo.step == 'order') eventName = eventNames.ORDER_INFO_VIEWED;
		if(checkoutInfo.step == 'shipping') eventName = eventNames.SHIPPING_INFO_VIEWED;
		if(checkoutInfo.step == 'payment') eventName = eventNames.PAYMENT_INFO_VIEWED;

		var data = {};
		data.checkoutId = String(checkoutInfo.cartId);
		var productData = makeProducts(md.itemList, 'checkout');
		if( isSnkrs ){
			$.extend( data, productData[0]);
		} else {
			data.products = productData;
		}

		return data;
	}
	function getOrderConfirmationData(){
		pageType = 'checkout';
		eventName = eventNames.ORDER_CONFIRMATION_VIEWED;
		classification = classifications.CORE_BUY_FLOW;
		var data = {};
		data.discount = md.orderDiscount;
		data.checkoutId = String(md.orderId);
		
		if( _GLOBAL.CUSTOMER.IS_CONSENT_TRANSFER_OVERSEA ){
			data.orderId = md.orderNumber;
		}
		$.extend(data, getPaymentInfo( md.payment_method || null, md.payment_method_detail || null, true ));
		var productData = makeProducts(md.itemList, 'checkout');
		if( isSnkrs ){
			data.isExclusiveAccess = false; // TODO
			$.extend( data, productData[0]);
		} else {
			data.products = productData;
		}
		if( md.promoList != null ){
			data.promoCode = String($.map(md.promoList, function(item){ return  item.name } ).join('|'));
		}
		var isSameDayShipping = ($('[data-sameday-price]').length !== 0);
		data.shipping = getShippingInfo(isSameDayShipping);
		data.tax = md.orderTaxAmount;
		data.total = md.orderTotalAmount;

		return data;
	}
	function getWishListData(){
		pageType = 'favorites';
		eventName = eventNames.WISHLIST_VIEWED;
		classification = classifications.EXPERIENCE;
		var data = {};
		data.wishlistId = 'none';
		data.wishlistName = 'Default List';
		data.products = makeProducts(md.itemList, 'cart');
		return data;
	}
	function getRegisterData(){
		pageType = 'landing page';
		eventName = eventNames.PAGE_VIEWED;
		classification = classifications.EXPERIENCE;
		var data = {};
		data.event = experienceType + '>guest>register';
		data.interactionType = 'load';
		return data;
	}
	function getRegisterSuccessData(type){
		pageType = type;
		eventName = eventNames.ACCOUNT_CREATED;
		classification = classifications.CORE_BUY_FLOW;
	}
	function makeProducts( itemList, type ){
		var products = [];
		try{
			$.each( itemList, function( index, productData ){
				var isPw = (type == 'pw');
				var isPdp = (type == 'pdp');
				var isCart = (type == 'cart');
				var isCheackout = (type == 'checkout');

				if( isPw && productData.bannerImage == true){
					products.push({ position : index+1 });
					return;
				}

				var gid = String(productData.gid || null);
				if( gid == '-99999999' || gid == 'null') gid = null;
				var product = {
					cloudProductId: null,
					name: String(productData.name),
					price: Number(productData.price) || Number(productData.retailPrice),
					prodigyProductId: gid,
					productId: gid
				}
				product.priceStatus = ( product.price < productData.retailPrice ) ? 'clearance' : 'regular';
				
				if( productData.bu != null){
					product.productType = (function(){
						switch (productData.bu || '') {
							case 'FW':
								return 'FOOTWEAR';
							case 'AP':
								return 'APPAREL';
							case 'EQ': case 'AW':
								return 'EQUIPMENT';
							default:
								return '';
						}
					}());
				}
				
				if( isPw || isCheackout ) {
					if( productData.hasOwnProperty('position') == true ){
						product.position = productData.position;
					}else{
						product.position = index+1;
					}
				}

				if( isPdp || isCart || isCheackout ){
					product.brand = 'Nike'; // TODO NikeNike Sportswear, Jordan
				}

				if( isPdp || isCheackout ){
					product.isMembershipExclusive = productData.isMemberOnly;
				}
				
				if( isPdp ){
					product.quantity = 1; // TODO 구매 제한 수량을 봐야함
					if( !isSnkrs ){
						// TODO low, medium, high, out of stock
						product.inventoryLevel = productData.isSoldOut ? 'out of stock' : 'low';
						// TODO in stock, out of stock, coming soon, preorder
						product.inventoryStatus = productData.isSoldOut ? 'out of stock' : ( productData.isUpcoming ? 'coming soon' : 'in stock' ); 
						product.isReserveNow = false;
						product.publishType = 'flow'; //TODO launch, flow
						try {
							product.reviewAverage = (function(){
								var average = Number(productData.reviewInfo.totalRatingAvg)/20;
								return Number(average.toFixed(1));
							}())
							product.reviewCount = Number(productData.reviewInfo.totalRatingCount);
						} catch (error) {
							product.reviewAverage = 0.0;
							product.reviewCount = 0;
						}
						
					}else{
						launchType = 'buy'  //TODO buy, line
					}

				}

				if( isCart || isCheackout ){
					product.sku = productData.skuId || 'none';
					product.quantity = Number(productData.quantity || 1);
					product.styleColor = productData.model;
				}

				if( isCheackout ) {
					product.size = (function(){
						var size = '';
						if(productData.option != null && productData.option.length > 0){
							try{
								for( var key in productData.option[0] ){
									size = productData.option[0][key];
								}
							}catch(e){console.log(e);}
						}
						return size;
					}())
					product.styleType = "INLINE" // TODO
					product.subtitle = 'none' // TODO
				} 
				products.push( product );
			});
		}catch (error) { 
			console.log(error);
		}
		return products;
	}
	function getPaymentMethod( type ){
		switch( type ){
			case 'GIFT_CARD' : return 'giftcertificate'; break;
			case 'CUSTOMER_CREDIT': return 'customercredit'; break;
			case 'CREDIT_CARD' : case 'credit card' : return 'creditcard'; break;
			case 'WIRE': return 'wire'; break;
			case 'BANK_ACCOUNT': case 'bankaccount': case 'bank transfer': return 'banktransfer'; break;
			case 'MOBILE': return 'cellphonepay'; break;
			case 'NAVER_PAY': case 'NaverPay': return 'naverpay'; break;
			case 'KAKAO_POINT': case 'KakaoPay': case 'KAKAO': return 'kakaopay'; break;
			case 'PAYCO': case 'Payco': return 'payco'; break;
			case '기타': return 'none'; break;
			default : return 'none'; break;
		}
	}
	function getCreditCartDetailInfo(type){
		switch( type ){
			case '현대카드' : return 'Hyundai Card'; break;
			case '현대비자개인' : return 'Hyundai Card'; break;
			case '삼성카드': return 'Samsung Card'; break;
			case 'BC카드': return 'BC Card' ;break;
			case '국민KB카드': case 'KB은련카드': case '국민카드': return 'KB Card' ;break;
			case '신한카드': case '신한은련': return 'Shinhan Card' ;break;
			case '롯데카드': return 'Lotte Card' ;break;
			case 'NH카드': case 'NH농협비씨카드': return 'NH Card' ;break;
			case '하나카드': case 'KEB하나카드': case '외환카드': return 'Hana Card' ;break;
			case '씨티카드': return 'City Card' ;break;
			case '유니온페이': case '해외은련카드': return 'UnionPay' ;break;
			case '이니포인트': return 'Inipoint' ;break;
			case '케이뱅크': return 'K Bank' ;break;
			case '우리카드': case '우리비씨카드': case '우리체크': return 'Woori Card' ;break;
			case '광주카드': return 'GwangJoo Card' ;break;
			case '전북카드': return 'JeonBuk Card' ;break;
			case '수협카드': return 'SuHyup Card' ;break;
			case 'KDB카드': return 'KDB Card' ;break;
			case '조흥카드': return 'CHB Card' ;break;
			case '제주카드': return 'JeJu Card' ;break;
			case '신협카드': return 'CU Card' ;break;
			case '우체국카드': return 'Epost BC Card' ;break;
			case '하나BC카드': return 'Hana Bc Card' ;break;
			case '농협BC카드': return 'NH BC Card' ;break;
			case '카카오뱅크': return 'Kakao Bank' ;break;
			case 'KB증권': return 'KB INVESTMENT & SECURITIES' ;break;
			default : return type; break;
		}
	}
	function getPaymentInfo(type, detail, isConfirmation){
		/*
		대분류: kakaopay, payco, bankTransfer,cellphonePay, creditCard 이렇게  크게 구분 부탁드립니다.
		카드결제(creditCard)에 대한 부분은 디테일한 카드사를 뒤에 포함해서 다음 예시와 같이 전달 부탁드립니다.  
		(ex; creditCard_kb card:not stored, creditCard_BC card:stored, creditCard_shinhan card:not stored)
		*/
		var typeName = 'none';
		if( isConfirmation ){
			typeName = getPaymentMethod(sessionStorage.getItem('payment_method'));
			if( typeName == 'creditcard' ){
				if( detail != null && detail != '기타' ) typeName += '_' + getCreditCartDetailInfo(detail);
			}
		}else{
			typeName = getPaymentMethod(type);
		}
		
		return {
			paymentsConcatenated : typeName + (( typeName=='none' ) ? '' : ':not stored'),
			payments : [{
				paymentType : typeName,
				//md.payment_method_detail != '기타' ? md.payment_method_detail : 'etc',
				"stored": false // 저장하는 것이 없기 때문에 무조건 false
			}]
		}
	}
	function getShippingInfo(isSameDay){
		var isBopis = ((_GLOBAL.MARKETING_DATA().ctm_order_type || '') == 'BOPIS');
		return {
			availableOptions: shippingOptions,
			cost: (isSameDay==true) ? 5000 :  0,
			method : (isSameDay==true) ? 'SAMEDAY' : (isBopis ? 'BOPIS' : 'SHIP'),
			selectedOption: (isSameDay==true) ? shippingOptions[1] : shippingOptions[0]
		};
	}
	function getPageOptions(dataLayer){
		log('eventName: ', dataLayer.eventName)
		log('eventType: ', dataLayer.eventType)
		var schemaId = schemaIds[dataLayer.eventName];
		var experience = schemaId.experience;

		// 같은 이름의 이벤트 네임을 사용하는 경우가 있어 억지로 분기 처리...
		if (dataLayer.eventName == eventNames.SEARCH_SUBMITTED && dataLayer.searchType == null) {
			experience = 'store-locator';
		}
		var schemaUrl = getSchemaUrl(schemaId.eventName, dataLayer.eventType, experience, schemaId.isCoreBuyFlow);
		return {'writeKey' : _GLOBAL.SITE.GLOBAL_ADOBE_WRITEKEY || WRITE_KEY.STG, '$schema' : schemaUrl}
	}
	function getPageName(type, detail){
		// pageNameSelector
		// experience type>page type>page detail
		var name = experienceType + '>' + type + ((detail != '') ? '>' + detail : '');
		if( md.pageType == 'register' && type == 'landing page'){
			name = experienceType + '>membership:land'; 
		}
		return name;
	}
	function getPageType(){
		// analyticsPageTypeSelector
		// homepage, pdp, pw, landing page
		if(pageType == 'pw'){
			if(md.pageType == 'search'){
				return 'onsite search'; //( dl.searchResultsCount > 0) ? 'onsite search' : 'no onsite search';
			}
		}else if(pageType.indexOf(':land') > -1){
			return 'landing page';
		}
		return pageType;
	}
	function getSchemaExperienceType(){
		// 같은 이벤트 타입인데 experience 값이 변경되야 하는 상황에서 사용
		switch (pageType) {
			case 'homepage':
				return 'landing-page'
			break;
			case 'pw':
				if(md.pageType == 'search'){
					return 'onsite search'; //( dl.searchResultsCount > 0) ? 'onsite search' : 'no onsite search';
				}
			break;
			case 'favorites':
				return 'wishlist';
			break;
			case 'registration':
				return 'account';
			break;
		}
		return pageType;
	}
	function getPageDetail(event){
		/*
			pageDetail: "men_shoes_new releases"
			pageName: "nikecom>pw>men_shoes_new releases"
		*/
		var detail = '';
		switch (pageType) {
			case 'pw':
				if( md.pageType == 'category'){
					detail = dl.selectedConcepts.join('_');
				}else if(md.pageType == 'search'){
					detail = ( dl.searchResultsCount > 0 ) ? 'results found' : 'no search results';
				}
			break;
			case 'pdp':
				detail = dl.name;
			break;
			case 'cart': case 'favorites':
				detail = 'view';
			break;
			case 'checkout':
				if( md.pageType == 'confirmation'){
					detail = 'order confirmation';
				}else{
					detail = md.marketingData.checkoutInfo.step;
				}
			case 'myorders':
				switch (md.pageType) {
					case 'orderDetail':
						detail = 'order details';
					break;
					case 'orderHistory':
						detail = 'order history';
					break;
					case 'orderReturnable':
						detail = 'return items';
					break;
					case 'orderReturned':
						detail = 'return items';
					break;
				}
				if( event == eventNames.ORDER_CANCELLATION_SUCCESS_VIEWED ){
					detail = 'cancel order>success';
				}
				if( event == eventNames.ORDER_CANCELLATION_FAILURE_VIEWED ){
					detail = 'cancel order>failed';
				}
				if( event == eventNames.RETURN_ITEM_CLICKED ){
					detail = 'order details';
				}
			break;
			case 'store locator':
				if( event == eventNames.NOT_FOUND_VIEWED ){
					detail = 'not found';
				}
			break;
			case 'story':
				detail = 'story';
			break;
			case 'guest':
				detail = 'login';
			break;
		}
		return detail;
	}
	function getClassification(){
		return classification == '' ? classifications.EXPERIENCE : classification;
	}
	function getClickActivity(){
		//pw:add:category:jordan
		return '';
	}
	function getCheckoutType(){
		//registered|guest|member|paypal
		return _GLOBAL.CUSTOMER.ISSIGNIN ? 'registered' : 'guest';
	}
	function getPreviousView(){
		return JSON.parse(sessionStorage.getItem('previousViewData')) || undefined;
	}
	function getPageData(eventType, eventName, clickActivity, classification){
		var data = {};
		data.app = {
			'backendPlatform': 'cloud',
			'domain': 'www.nike.com',
			'version': '1.0.0'
		},
		data.consumer = {
			koreaId : 'guest',
			loginStatus : _GLOBAL.CUSTOMER.ISSIGNIN ? 'logged in' : 'not logged in',
			upmId : 'guest' //_GLOBAL.CUSTOMER.ISSIGNIN ? 'none' : 'guest'
		}
		if( _GLOBAL.CUSTOMER.ISSIGNIN == true ){
			if( _GLOBAL.CUSTOMER.IS_CONSENT_TRANSFER_OVERSEA == true ){
				data.consumer.koreaId = String(_GLOBAL.CUSTOMER.ID);
			}else{
				data.consumer.koreaId = 'nonconsent';
			}
		}
		data.view = {
			experienceType: experienceType,
			pageType: getPageType(),
			pageDetail : getPageDetail(eventName)			
		},
		data.view.pageName = getPageName(data.view.pageType, data.view.pageType != 'checkout' ? data.view.pageDetail : '');
		data.currency = currencyType;
		data.eventName = eventName;
		data.eventType = eventType,
		data.language = language;
		data.locale = { language: 'ko-KR', country: 'kr'},
		data.classification = (classification != null) ? classification : getClassification();
		data.video = undefined;
		data.abTest = undefined;
		data.clickActivity = (clickActivity != null) ? clickActivity : getClickActivity();
		data.previousView = getPreviousView();

		sessionStorage.setItem('previousViewData', JSON.stringify({ pageType: data.view.pageType, pageName: data.view.pageName }));
		
		return data;
	}
	function submitDataLayer(dataLayer){
		log('name: ', dataLayer.eventName);
		log('dataLayer: ', dataLayer);
		try {
			var options = getPageOptions(dataLayer);
			if(dataLayer.eventType == eventTypes.PAGE){
				analyticsClient.page(dataLayer.view.pageName, dataLayer, options);
			}else{
				analyticsClient.track(dataLayer.eventName, dataLayer, options);
			}
		} catch (error) {
			console.warn(error);
		}
		log('------------');
	}
	function addEvent(){
		var endPoint = Core.getComponents('component_endpoint');
		// 기본 클릭 이벤트
		endPoint.addEvent('clickEventHandler', clickEvent );
		// 필터 선택하여 상품 리스트 변경 시
		endPoint.addEvent('updateCategoryListByFiltered', updateCategoryListByFilteredEvent );
		// 필터 적용
		endPoint.addEvent('applyFilter', function(param){ 
			filtered = { type : 'add', item : param };
			applyFilter(param);
		})
		// 필터 삭제
		endPoint.addEvent('removeFilter', function(param){ 
			filtered = { type : 'remove', item : param };
			applyFilter(param);
		})
		// 로그인 성공
		endPoint.addEvent('loginSuccess', function(){
			var type = getPageType();
			if( type == 'cart' || type == 'checkout' ){
				sessionStorage.setItem('loginSuccess', true);
			}
		});
		// 장바구니 삭제
		endPoint.addEvent('removeFromCart', function(param){
			cartProductUpdateEvent(param);
		});
		//장바구니 수정
		endPoint.addEvent('cartAddQuantity', function(param){
			cartProductUpdateEvent(param, true);
		});
		// 장바구니 담기
		endPoint.addEvent('addToCart', function(param){
			pdpAddToCartEvent( param );
		} );
		// 장바구니 담기 실패
		endPoint.addEvent('addToCartFail', function(param){
			pdpAddToCartEvent( param, true );
		} );
		// 그리드 상품 클릭
		endPoint.addEvent('pwProductClick', function(param){
			if(param.isSnkrsStory == true){
				snkrsStoryTrackEvent(eventNames.STORY_CLICKED, 'snkrs:feed:stories tap');
			}else{
				pwProductClickEvent(param);
			}
		});
		// 추천검색어 클릭
		endPoint.addEvent('searchPopularSuggestionClick', function(param){
			callSearchEvent(eventNames.SEARCH_POPULAR_SUGGESTION_CLICKED, 'popular term', param.searchText);
		} );
		// 검색어 자동완성 클릭
		endPoint.addEvent('searchTypeaheadClick', function(param){
			callSearchEvent(eventNames.SEARCH_TYPEAHEAD_CLICKED, 'typeahead', param.searchText);
		} );
		// 검색어 직접 입력
		endPoint.addEvent('searchSubmitted', function(param){
			callSearchEvent(eventNames.SEARCH_SUBMITTED, 'user typed', param.searchText);
		} );
		//카테고리 필터on/off(와이드뷰) 클릭시
		endPoint.addEvent('wideToggleClick', filterMenuToggleEvent);
		// 재입고 알림 완료
		endPoint.addEvent('notifymeSubmitted', function(){
			callNodifymeEvent(eventNames.NOTIFY_ME_SUBMITTED, 'pdp:notify me submitted');
		});
		// 신청 후 재입고 알림 창 닫침
		endPoint.addEvent('notifymeModalClose', function(){
			callNodifymeEvent(eventNames.NOTIFY_ME_DISMISSED, 'pdp:notify me dismissed');
		});
		// 리뷰 작성시
		endPoint.addEvent('writeReview', function(param){
			callPdpTrackEvent( eventNames.PRODUCT_SAVED, 'pdp:review submitted', { rating : param.starCount });
		});
		// wishlist 등록시
		endPoint.addEvent('addToWishlist', function(data){
			//callPdpTrackEvent( eventNames.PRODUCT_SAVED, getPageType()+':product saved');
			callWishlistTrackEvent(eventNames.PRODUCT_SAVED, 'product saved', data.index);
		});
		// wishlist 제거시
		endPoint.addEvent('removeToWishlist', function(data){
			//callPdpTrackEvent( eventNames.PRODUCT_UNSAVED, getPageType()+':product unsaved');
			callWishlistTrackEvent(eventNames.PRODUCT_UNSAVED, 'product unsaved', data.index);
		});
		// 상품 옵션 선택시
		endPoint.addEvent('pdpOptionClick', function(param){
			if( md.pageType == 'wishlist') return;
			callPdpTrackEvent( eventNames.PRODUCT_SIZE_SELECTED, 'size selection: ' + param.value);
		});
		// 상품 이미지 줌
		endPoint.addEvent('pdpImageZoom', function(){
			callPdpTrackEvent( eventNames.PRODUCT_ZOOMED, 'product zoom');
		});
		// 상품 이미지 줌
		endPoint.addEvent('pdpImageZoomClose', function(){
			callPdpTrackEvent( eventNames.PRODUCT_ZOOM_CLOSED, 'pdp:product zoom closed');
		});
		// 연관 상품 클릭
		endPoint.addEvent('crossSaleClick', function(param){
			switch( md.pageType ){
				case 'product' :
					callPdpTrackEvent( eventNames.RECOMMENDED_PRODUCT_SELECTED, 'pdp: recommended product selected:' + param.index);
				break; 
				case 'cart' :
					var data = getPageData(eventTypes.TRACK, eventNames.RECOMMENDED_PRODUCT_CLICKED);
					data.modelVariant = 'none';
					data.cartId = md.cartId;
					submitDataLayer(data);;
				break;
			}
		});
		// 프로모코드 등록
		endPoint.addEvent('applyPromoCode', callPromoEvent);
		// 결제하기 클릭
		endPoint.addEvent('checkoutSubmit', checkoutSubmitEvent);
		// 결제 에러시
		endPoint.addEvent('paymentError', function(param){
			callCheckoutTrackEvent(eventNames.ERROR_MODAL_VIEWED, null, { errorCode : param.message });
		});
		// 비회원 구매시 기존 회원과 동일한 정보일 때 노출되는 팝업
		endPoint.addEvent('openAlreadyRegistered', function(){
			callCheckoutTrackEvent(eventNames.LOGIN_PROMPT_VIEWED);
		});
		// 배송주소 검색하여 선택시
		endPoint.addEvent('shippingAddressSelect', function(){
			callCheckoutTrackEvent(eventNames.ADDRESS_SUGGESTION_SELECTED);
		});
		// 결제 수단 선택 시
		endPoint.addEvent('changePaymentMethod', function(data){
			// var type = Core.utils.string.toLower(data.paymentType).split('_').join('');
			var type = getPaymentMethod(data.paymentType);
			var paymentMethod = getPaymentInfo(data.paymentType, null, false);
			callCheckoutTrackEvent(eventNames.PAYMENT_METHOD_SELECTED,'payment method :'+ type, paymentMethod );
		});
		// 주문서 배송지 목록에서 주소 선택시
		endPoint.addEvent('changeShippingAddress', function(data){
			callCheckoutTrackEvent(eventNames.SHIPPING_ADDRESS_SELECTED, 'shipping methods');
		});
		// ShippingInfoForm 작성 완료시
		endPoint.addEvent('submitShippingInfoForm', function(data){
			callCheckoutTrackEvent(eventNames.SHIPPING_METHOD_SELECTED, null, { shipping : getShippingInfo(data.isSameDay) });
		});
		// 배송지 주소 변경시
		endPoint.addEvent('changeShippingType', function(data){
			callCheckoutTrackEvent(eventNames.SHIPPING_OPTION_SELECTED, null, { 
				shipping : { 
					selectedOption : (data.shippingType == 'default') ? shippingOptions[0] : shippingOptions[1]
				}
			});
		});
		// 반품 신청 클릭하여 주문 오픈시
		endPoint.addEvent('openReturnRequestOrder', function(data){
			submitDataLayer(getPageData(eventTypes.PAGE, eventNames.RETURN_ITEM_CLICKED));
		});
		// 반품 이유 변경시
		endPoint.addEvent('changeReturnReason', function(data){
			submitDataLayer(getPageData(eventTypes.TRACK, eventNames.RETURN_REASON_CLICKED));
		});
		// 주문 취소 완료시
		endPoint.addEvent('orderCancelSuccess', function(data){
			submitDataLayer(getPageData(eventTypes.PAGE, eventNames.ORDER_CANCELLATION_SUCCESS_VIEWED));
		});
		// 주문 취소 실패시
		endPoint.addEvent('orderCancelFail', function(data){
			submitDataLayer(getPageData(eventTypes.PAGE, eventNames.ORDER_CANCELLATION_FAILURE_VIEWED));
		});
		// 검색된 store 클릭시
		endPoint.addEvent('selectStoreItem', function(data){
			var markerName = 'sl:map marker:'+data.name;
			submitDataLayer(getPageData(eventTypes.TRACK, eventNames.MAP_MARKER_CLICKED, markerName));
		});
		// store 검색
		// 검색 후 페이지 이동 된 후 호출하는 것으로 타이밍 변경
		/*
		endPoint.addEvent('searchStore', function(data){
			submitDataLayer(getPageData(eventTypes.TRACK, eventNames.SEARCH_SUBMITTED, 'sl:search submitted'));
		});
		*/
		endPoint.addEvent('snkrsMobileSwipeIndex', function(param){
			if(param.SNKRS_mobile_swipe_type == 'swipe'){
				snkrsStoryTrackEvent(eventNames.SIDE_STORY_CLICKED, 'snkrs:stories:tap' + param.swipe_direction);
			}
		})
		// PDP 에서 바로구매 시
		endPoint.addEvent('buyNow', function(param){
			var data = getPageData(eventTypes.TRACK, eventNames.BUY_NOW_CLICKED, 'pdp: buy now', classifications.EXPERIENCE);
			var productData = makeProducts([md.productInfo], 'pdp');
			data.isReserveNow = false;
			data.estimatedDeliveryDate = 'none';
			productData[0].sku = param.skuId;
			productData[0].quantity = param.quantityAdded;
			data.products = productData;
			submitDataLayer(data);
		})
		// Category 에서 스크롤에 따라 노출 되는 상품
		endPoint.addEvent('productItemShown', function(param){
			var data = $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.PRODUCT_SHOWN, null, classifications.EXPERIENCE));
			$.extend( data, getCategoryFilteredData());
			delete data.content;
			delete data.products;
			var productData = makeProducts([param], 'pw');
			var badgeLabel = getBadgeLabel(param.badge);
			if( badgeLabel != ''){
				data.badgeLabel = badgeLabel;
			}
			data.landmarkX = 50;
			data.landmarkY = 50;
			$.extend(data, productData[0]);
			submitDataLayer(data);
		})

		try {
			$.each(videojs.getAllPlayers(), function(index, player){
				player.ready(function() {
					if( player.tagAttributes['data-video-id'] == null ){
						return;
					}
					var targetPlayer = this.player();
					targetPlayer.videoId = player.tagAttributes['data-video-id'];
					targetPlayer._isShowTextTrack = false;
					targetPlayer.isShowTextTrack = (function(){ return targetPlayer._isShowTextTrack; });
					targetPlayer.on('loadeddata', function() {
						targetPlayer.on('texttrackchange', function(event) {
							this._isShowTextTrack = !this._isShowTextTrack;
							callVideoEvent(this, 'texttrackchange');
						});
					});
					targetPlayer.on('fullscreenchange', function() {
						callVideoEvent(this, 'fullscreenchange');
					});
					targetPlayer.on('play', function() {
						callVideoEvent(this, 'play');
					});
					targetPlayer.on('pause', function() {
						callVideoEvent(this, 'pause');
					});
					targetPlayer.on('seeking', function() {
						callVideoEvent(this, 'seeking');
					});
					targetPlayer.on('volumechange', function() {
						callVideoEvent(this, 'volumechange');
					});
				});
			})
		} catch (error) {}
		
		var $videos = $('video');
		$videos.each(function(index, video){
			if( $(this).find('source').length == 0 || $(this).data('video-id') == null ){
				return;
			}
			var targetPlayer = video;
			targetPlayer.videoId = String($(this).data('video-id'));
			targetPlayer.isShowTextTrack = false;
			targetPlayer.isFullscreen = false;
			targetPlayer.status = 'pause';
			targetPlayer.addEventListener('playing', function(){
				callVideoEventByMp4Type(this, 'play');
			})
			targetPlayer.addEventListener('volumechange', function(){
				callVideoEventByMp4Type(this, 'volumechange');
			})
			targetPlayer.addEventListener('seeking', function(){
				callVideoEventByMp4Type(this, 'seeking');
			})
			targetPlayer.addEventListener('pause', function(){
				callVideoEventByMp4Type(this, 'pause');
			})
			targetPlayer.addEventListener('timeupdate', function(){
				if(this.loop == true && this.status != 'ended'){
					if( Math.floor(this.currentTime) === Math.floor(this.duration) ){
						callVideoEventByMp4Type(this, 'ended');
					}
				}
			})
			setInterval(function(){
				if( document.fullscreenElement == null ){
					if(targetPlayer.isFullscreen == true ){
						// 풀스크린 해제
						targetPlayer.isFullscreen = false;
						callVideoEventByMp4Type(targetPlayer, 'fullscreenchange');
					}
				}else{
					if( targetPlayer.videoId == $(document.fullscreenElement).data('video-id')){
						if(targetPlayer.isFullscreen == false ){
							// 풀스크린
							targetPlayer.isFullscreen = true;
							callVideoEventByMp4Type(targetPlayer, 'fullscreenchange');
						}
					}
				}
			}, 300)
		})
	}
	function callVideoEvent(player, eventType){
		var targetPlayer = player;
		var videoData = {
			videoId : player.videoId,
			autoplay : !!player.autoplay(),
			currentTime : Math.floor(player.currentTime()),
			duration : Math.floor(player?.mediainfo?.duration),	
			loop : player.loop(),
			sound : !player.muted(),
			fullScreen : player.isFullscreen(), 
			subtitles : player.isShowTextTrack()
		}
		var eventName = getVideoEventName(targetPlayer, eventType);
		var data = getPageData(eventTypes.TRACK, eventName, null, classifications.EXPERIENCE);
		data.video = videoData;
		submitDataLayer(data);
	}
	function getVideoEventName(player, eventType){
		switch (eventType) {
			case 'volumechange':
				return player.muted()
					? eventNames.VIDEO_MUTED
					: eventNames.VIDEO_UNMUTED;
			case 'play':
				return player.currentTime() < 0.5
					? eventNames.VIDEO_STARTED
					: eventNames.VIDEO_PLAYED;
			case 'pause':
				return player.currentTime() === player.duration()
					? eventNames.VIDEO_ENDED
					: eventNames.VIDEO_PAUSED;
			case 'seeking':
				return player.currentTime() === 0
					? eventNames.VIDEO_RESTARTED
					: eventNames.VIDEO_REWOUND;
			case 'fullscreenchange':
				return player.isFullscreen()
					? eventNames.VIDEO_ENTERED_FULLSCREEN
					: eventNames.VIDEO_EXITED_FULLSCREEN;
			case 'texttrackchange':
				return player.isShowTextTrack()
					? eventNames.VIDEO_SUBTITLES_ON
					: eventNames.VIDEO_SUBTITLES_OFF;
			default:
			return false;
		}
	}
	function callVideoEventByMp4Type(player, eventType){
		var targetPlayer = player;
		player.status = eventType;
		var videoData = {
			videoId : player.videoId,
			autoplay : !!player.autoplay,
			currentTime : Math.floor(player.currentTime),
			duration : Math.floor(player.duration),	
			loop : player.loop,
			sound : !player.muted,
			fullScreen : player.isFullscreen, 
			subtitles : player.isShowTextTrack
		}
		var eventName = getVideoEventNameByMp4Type(targetPlayer, eventType);
		var data = getPageData(eventTypes.TRACK, eventName, null, classifications.EXPERIENCE);
		data.video = videoData;
		submitDataLayer(data);
	}
	function getVideoEventNameByMp4Type(player, eventType){
		switch (eventType) {
			case 'volumechange':
				return player.muted
					? eventNames.VIDEO_MUTED
					: eventNames.VIDEO_UNMUTED;
			case 'play':
				return player.currentTime < 0.5
					? eventNames.VIDEO_STARTED
					: eventNames.VIDEO_PLAYED;
			case 'pause':
				return Math.floor(player.currentTime) === Math.floor(player.duration)
					? eventNames.VIDEO_ENDED
					: eventNames.VIDEO_PAUSED;
			case 'ended':
				return eventNames.VIDEO_ENDED;
			case 'seeking':
				return player.currentTime === 0
					? eventNames.VIDEO_RESTARTED
					: eventNames.VIDEO_REWOUND;
			case 'fullscreenchange':
				return player.isFullscreen
					? eventNames.VIDEO_ENTERED_FULLSCREEN
					: eventNames.VIDEO_EXITED_FULLSCREEN;
			case 'texttrackchange':
				return player.isShowTextTrack
					? eventNames.VIDEO_SUBTITLES_ON
					: eventNames.VIDEO_SUBTITLES_OFF;
			default:
			return false;
		}
	}
	function clickEvent(param){
		var experience = param.area;
		var eventName = param.name;
		var clickActivity = param.activity;
		var eventType = eventTypes.TRACK;
		var activeDetail = '';
		classification = classifications.EXPERIENCE;
		var customData = null;
		switch (param.area) {
			case 'global-nav':
				eventName = 'Nav ' + param.name + ' Clicked';
				if( param.name == 'Footer' ){
					activeDetail = 'footer:'
				}
				if( param.name == 'Shopping Menu' ){
					if( clickActivity != 'menu open' ){
						activeDetail = 'shop>';
					}
				}
				if( param.name == 'Membership' && param.activity == 'join'){
					if( md.pageType == 'cart' ){
						sessionStorage.setItem('checkRegistStartPage', 'cart');
					}
				}
				if( param.name == 'Account' ){
					activeDetail = 'myAccount:'
				}
				clickActivity = 'nav:' + activeDetail + param.activity;
			break;
			case 'global-banner':
				eventName = param.name + ' Clicked';
				clickActivity = getPageType()+':banner clicked:' + param.activity;
				experience = getSchemaExperienceType();
			break;
			case 'cms-content':
				eventName = param.name + ' Clicked';
				experience = getSchemaExperienceType();
			break;
		}
		
		setSchemaId(eventName, experience, false);
		submitDataLayer((customData == null ) ? getPageData(eventType, eventName, clickActivity) : customData);
		/*
		var schemaData = {
			'eventName' : eventName, 
			'experience' : experience,
			'isCoreBuyFlow' : false
		}
		submitDataLayer($.extend( getPageData(eventType, eventName, clickActivity), {schemaData: schemaData}));
		*/
	}
	function callPromoEvent(param){
		var eventName = (param.promoAdded == true) ? eventNames.PROMO_CODE_APPLIED : eventNames.PROMO_CODE_REJECTED;
		var data = getPageData(eventTypes.TRACK, eventName);
		if( md.pageType == 'cart'){
			data.cartId = md.cartId;
		}
		if(param.promoAdded == true){
			data.discount = 0; //TODO 할인된 가격은 없음
		}else{
			data.errers = [{
				code : param.exceptionKey
			}]
			data.reason = data.exception;
		}
		data.promoCode = param.promoCode;
		data.products = makeProducts(md.itemList, 'cart');
		submitDataLayer(data);
	}
	function callSearchEvent(eventName, searchType, searchTerm){
		var data = getPageData(eventTypes.TRACK, eventName, null, classifications.EXPERIENCE);
		data.searchTerm = searchTerm;
		data.searchType = searchType;
		data.searchTermType = searchType + ':' + searchTerm;
		submitDataLayer(data);
	}
	function callNodifymeEvent(eventName, clickActivity){
		submitDataLayer( getPageData(eventTypes.TRACK, eventName, clickActivity, classifications.EXPERIENCE) );
	}
	function callPdpTrackEvent(eventName, clickActivity, options){
		var data = getPageData(eventTypes.TRACK, eventName, clickActivity, classifications.EXPERIENCE);
		if( options != null ){
			$.extend(data, options);
		} 
		var product = null;
		if( md.isProduct){
			product = md.productInfo;
		}else{
			var index = sessionStorage.getItem('selectedUpdateProductIndex');
			try {
				product = md.itemList[index-1];
			} catch (error) {}
		}
		data.products = makeProducts([product], 'pdp');
		submitDataLayer(data);
	}
	function callWishlistTrackEvent(eventName, clickActivity, index){
		var type = getPageType();
		var data = getPageData(eventTypes.TRACK, eventName, type + ':' + clickActivity, null, classifications.EXPERIENCE);
		if( md.pageType == 'product'){
			data.products = makeProducts([md.productInfo], 'pdp');
		}else{
			if( index != null ){
				var products = makeProducts(md.itemList, 'cart');
				data.wishlistId = 'none';
				data.wishlistItemId = 'none';
				data.wishlistName = 'Default List';
				$.extend( data, products[Number(index)-1]);
			}
		}
		submitDataLayer(data);
	}
	function callCheckoutTrackEvent(eventName, clickActivity, options){
		var data = $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventName, clickActivity, classifications.EXPERIENCE));
		if( options != null ){
			$.extend(data, options);
		} 
		submitDataLayer(data);
	}
	function checkoutSubmitEvent(param){
		var data = getPageData(eventTypes.TRACK, eventNames.CHECKOUT_INTENT_START, null, classifications.CORE_BUY_FLOW);
		data.checkoutType = getCheckoutType();
		submitDataLayer(data);
	}
	function filterMenuToggleEvent(param){
		var data = getPageData(eventTypes.TRACK, eventNames.FILTER_MENU_TOGGLED, 'filter-menu:' + ((param=='on') ? 'hidden' : 'shown'), classifications.EXPERIENCE);
		submitDataLayer(data);
	}
	function applyFilter(param){
		var data = $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.FILTER_APPLIED, 'pw:filter-applied:' + param.key + ':' + param.value, classifications.EXPERIENCE));
		data.filter = {
			detail: param.value,
			group: param.key
		}
		submitDataLayer(data);
	}
	function updateCategoryListByFilteredEvent(param){
		// if( param.isPageLoad ) return;
		md = _GLOBAL.MARKETING_DATA();
		$.extend(dl, getCategoryData()); // 필터로 인하여 바뀐 상품 정보를 다시 적용
		var data = $.extend( _.cloneDeep(dl), getCategoryFilteredData());
		submitDataLayer( $.extend( data, getPageData(eventTypes.PAGE, eventNames.PRODUCT_LIST_FILTERED, null, classifications.CORE_BUY_FLOW)));
		submitDataLayer( $.extend( data, getPageData(eventTypes.TRACK, eventNames.PRODUCT_LIST_FILTERED, null, classifications.CORE_BUY_FLOW)));
	}
	function cartProductUpdateEvent(param, isUpdate){
		// selectedUpdateProductIndex 옵션 변경 하기 위해 선택한 상품 index 번호
		md = _GLOBAL.MARKETING_DATA();
		pageType = 'cart';
		var data = getPageData(eventTypes.TRACK, eventNames.PRODUCT_REMOVED, null, classifications.CORE_BUY_FLOW);
		var products = makeProducts(md.itemList, 'cart');
		var index = (isUpdate == true) ? sessionStorage.getItem('selectedUpdateProductIndex') : param.index;
		if( index != null ){
			index = Number(index) - 1;
			data.products = [products[index]];
			$.extend(data, products[index]);
		}
		data.cartId = md.cartId;
		data.quantity = 1;
		submitDataLayer(data);

		// 수정하는 상황이면 삭제 이후에 add도 바로 실행
		if( isUpdate == true ) {
			eventName = eventNames.PRODUCT_ADDED;
			data.eventName = eventName;
			submitDataLayer(data);
			sessionStorage.removeItem('selectedUpdateProductIndex');
		}
	}
	function pdpAddToCartEvent(param, isFailed){
		// 상품을 담고 바로 장바구니 삭제를 하면 pageType이 바뀌는 상황이 생겨 다시 한번 적용
		var isWishlist = md.pageType == 'wishlist';
		var index = sessionStorage.getItem('selectedAddProductIndex');
		var eventName = eventNames.PRODUCT_ADDED;
		var clickActivity = null;
		classification = classifications.CORE_BUY_FLOW;

		if( isFailed ){
			eventName = eventNames.PRODUCT_ADDED_ERROR_VIEWED;
			classification = classifications.EXPERIENCE;
			clickActivity = 'failed to add to cart';
		}

		if( isWishlist ){ pageType = 'favorites' };
		var data = $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventName, clickActivity));

		if( isWishlist ){
			if( index != null ){
				var products = makeProducts(md.itemList, 'cart');
				index = Number(index) - 1;
				data.products = products[index];
				$.extend(data, data.products);
				sessionStorage.removeItem('selectedAddProductIndex');
			}
		}else{
			data.sku =  param.skuId;
			data.products[0]['sku'] = param.skuId;
			data.sizeSuggestion = 'none';
			data.estimatedDeliveryDate = 'none';
		}
		submitDataLayer(data);
	}
	function pwProductClickEvent(param){
		md = _GLOBAL.MARKETING_DATA();
		var data = $.extend( _.cloneDeep(dl), getPageData(eventTypes.TRACK, eventNames.PRODUCT_CLICKED, 'pw:product clicked', classifications.CORE_BUY_FLOW));
		var badgeLabel = getBadgeLabel(param.badge);
		if( badgeLabel != ''){
			data.badgeLabel = badgeLabel;
		}
		delete data.product;
		var products = makeProducts(md.itemList, 'pw');
		$.extend(data, products[param.grid_wall_rank-1]);
		submitDataLayer(data);
	}
	function getBadgeLabel(label){
		if( label == null ) return '';
		var badgeLabel = Core.utils.string.toUpper(Core.utils.string.trim(label).split(' ').join('_'));
		switch(Core.utils.string.trim(label)){
			case '출시 예정':
				badgeLabel = 'COMING_SOON';
			break;
			case 'Launching in SNKRS':
				badgeLabel = 'SNKRS_COMING_SOON';
			break;
			case 'Customize':
				badgeLabel = 'CUSTOMIZE';
			break;
			case '품절':
				badgeLabel = 'SOLDOUT';
			break;
			case '친환경 소재':
				badgeLabel = 'SUSTAINABLE_MATERIALS';
			break;
		}
		return badgeLabel;
	}
	function snkrsStoryTrackEvent(eventName, clickActivity){
		pageType = 'story';
		var data = getPageData(eventTypes.TRACK, eventName, clickActivity, classifications.EXPERIENCE);
		submitDataLayer(data);
	}
	function contentClickEvent(dataArr, index){
		var data = null;
		var clickArea = String( $(this).data('click-area') || '' );
		var clickName = String( $(this).data('click-name') || '' );
		var dataArr = clickName.split('_');
		if( dataArr.length > 4) {
			var conId = $('[data-click-name^="' + dataArr[0] + '_' + dataArr[1] + '"]:visible').index( this );
			var positionId = dataArr[1].substr(1,1);
			var placementId = dataArr[2];
			var ctaTitle = (function(){
				var title = '';
				for (var i=5; i<dataArr.length; i++) {
					title += dataArr[i];
					if( i<dataArr.length-1){ title += '_';}
				}
				return title;
			})();
			// if( dataArr.length > 6){ ctaTitle = ctaTitle + '_' + dataArr[6] }
			var url = $(this).attr('href');
			var type = (function(){
				if( url.indexOf('/t/') > -1){
					return 'nocta';
				}else if( url.indexOf('/w/') > -1){
					return 'gridwall';
				}else if( url.indexOf('/c/') > -1){
					return 'page';
				}
				return 'url';
			})()
			var contentClickActivity = 'con:' + conId + ':' + type + ':' + ctaTitle;
			/*
				// /t 로 시작하면 nocta
				// 타입 gridwall(/w), page(/c), url, 
				con:순서:타입:타이틀
				"contentClickActivity": "con:2:url:sign in", con:1:url:join us
			*/
			data = getPageData(eventTypes.TRACK, eventNames.CLICK, contentClickActivity, classifications.EXPERIENCE)
			data.content = {
				ctaTitle : ctaTitle,
				positionId: positionId,
				placementId : placementId,
				pageOfClick : data.view.pageName
			}
		}else{
			data = getPageData(eventTypes.TRACK, eventNames.CLICK, clickArea+':'+clickName, classifications.EXPERIENCE)
		}
		submitDataLayer(data);
	}
	function log(msg, data){
		if(!isProd){
			if( data ){
				if( typeof data != 'object'){
					console.log(msg + data);
				}else{
					console.log(msg);
					console.log(data);
				}
			}else{
				console.log(msg);
			}
		}
	}
	Core.gaa = {
		init : function(){
			init();
			addEvent();
		}
	}

})(Core);