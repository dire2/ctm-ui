(function (Core) {
	var ProductQuickView = function () {
		'use strict';
		var _self, $this, args;
		var setting = {
			url: Core.Utils.contextPath + '/processor/execute/product'
		};

		function open(url, option){
			if (!_.isEmpty(args.showActionButton)) {
				option.showActionButton = args.showActionButton;
			}

			// 상품 팝업 호출
			Core.ui.modal.ajax(url,{
				param: option,
				selector: '#quickview-wrap',
				fullscreen: false,
				show : function(){
					$(this).addClass('quickview');
					if (_.isEqual(args.actionType, 'confirm')) {
						// 장바구니 add 되었을 때 미니카트가 나오지 않고 confirm 창 오픈
						$('[data-btn-add-cart]').attr('action-type', 'confirm');
					}
				}
			})

			/*  
			var modal = UIkit.modal('#common-modal');
			Core.Utils.ajax(url, 'GET', option, function (data) {
				var $modal = $('#common-modal');
				var domObject = $(data.responseText).find('#quickview-wrap');
				if (domObject.length < 1){
					UIkit.modal.alert('상품 정보를 불러올 수 없습니다.');
					return;
				}
				$modal.find('.contents').empty().append(domObject[0].outerHTML);
				$modal.addClass('quickview');
				Core.moduleEventInjection(domObject[0].outerHTML);

				if (_.isEqual(args.actionType,'confirm')){
					$('[data-btn-add-cart]').attr('action-type', 'confirm');
				}
				modal.show();
			}, true, false, 1500)
			*/
		}
		function openByProductId(productId) {
			var obj = {
				'id': productId,
				'mode': 'template',
				'accepted': true,
				'quickview': true,
				'templatePath': '/catalog/product',
			}
			open(setting.url, obj);
		}
		function openByProductUrl(url) {
			var obj = {
				'accepted': true,
				'quickview': true,
			}
			open(url, obj);
		}
		var Closure = function () { }
		Closure.prototype = {
			setting: function () {
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init: function () {
				_self = this;
				args = arguments[0];
				$this = $(setting.selector);

				if (!_.isEmpty(args.productId)) {
					$this.on('click', function (){
						openByProductId(args.productId);
					});
					return this;
				} else if (!_.isEmpty(args.productUrl)) {
					$this.on('click', function () {
						openByProductUrl(args.productUrl);
					});
					return this;
				}
				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_product_quick_view'] = {
		constructor: ProductQuickView,
		reInit: true,
		attrName: 'data-component-product-quick-view'
	}
})(Core);
