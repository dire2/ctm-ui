(function (Core, utils) {
	function set( name, value, hours, path ){
        if( hours ){
            var date = new Date();
            date.setTime(date.getTime()+(hours*3600*1000));
            var opt = {
                expires: date,
            }
            if( path != null ){
                opt.path = path;
            }
            $.cookie(name, value, opt);
        }
    }
    function get( name ){
        return $.cookie( name );
    }
    function remove( name, path ){
        var opt = {};
        if( path != null ){
            opt.path = path;
        }
        $.removeCookie(name, opt);
    }
	utils.cookie = {
		set: set,
		get: get,
		remove: remove
	}
})(Core = window.Core || {}, Core.Utils = window.Core.Utils || {});

