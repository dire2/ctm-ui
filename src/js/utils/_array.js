(function (Core, utils) {
	function filter(arr, fn){
		return _.filter(arr, fn);
	}
	utils.array = {
		filter: filter
	}
})(Core = window.Core || {}, Core.Utils = window.Core.Utils || {});
