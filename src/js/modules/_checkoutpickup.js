(function(Core){
	Core.register('module_order_pickup', function(sandbox){
		var args;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				args = arguments[0];

				//주문자 인풋 컴포넌트
				var inputComponent = sandbox.getComponents('component_textfield', {context:$this});

				//주문자와 동일
				var checkoutComponent = sandbox.getComponents('component_checkbox', {context:$this}, function(i){
					this.addEvent('change', function(isChecked){
						if(isChecked){
							var customerInfo = sandbox.getModule('module_order_customer').getOrderCustomerInfo();
							$this.find('#fullname').val(customerInfo.name).focusout();
							$this.find('#phonenumber').val(customerInfo.phoneNum).focusout();
						}else{
							$this.find('#fullname').val('').focusout();
							$this.find('#phonenumber').val('').focusout();
						}
					});
				});


				$this.find('[data-order-pickup-submit-btn]').click(function(e){
					if(!inputComponent[0].getValidateChk('errorLabel') || !inputComponent[1].getValidateChk('errorLabel')){
						e.preventDefault();
					}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-pickup]',
					attrName:'data-module-order-pickup',
					moduleName:'module_order_pickup',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
