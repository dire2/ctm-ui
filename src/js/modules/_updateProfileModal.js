(function(Core){
	'use strict';

	Core.register('module_update_profile_modal', function(sandbox){
		var $this, $modal, dontShowCookieId, endPoint, isConfirmation = false;
		var Method = {
			moduleInit:function(){
				var showUrl = String(_GLOBAL.SITE.CONSENT_TRANSFER_OVERSEA_SHOW_URL || '/').split(',');
				var isShow = false;
				showUrl.forEach(function(url){
					if( url == _GLOBAL.MARKETING_DATA().pathName ){
						isShow = true;
					}
					// 주문 완료 페이지에서는 회원일 때만 노출
					if( url == '/confirmation'){
						// 주문번호가 뒤에 붙는 경우 때문에 따로 처리함
						if( _GLOBAL.MARKETING_DATA().pathName.indexOf( '/confirmation') > -1 ){
							isShow = false;
							if( _GLOBAL.CUSTOMER.ISSIGNIN == true ){
								isShow = true;
								isConfirmation = true;
							}
						}
					}
				});
				if( isShow == false ) return;
			 
				/*
				if( _.indexOf(showUrl, _GLOBAL.MARKETING_DATA().pathName) == -1 ){
					return;
				}
				*/

				$this = $(this);
				endPoint = Core.getComponents('component_endpoint');
				$modal = UIkit.modal('#update-profile', {center:true, keyboard:false, bgclose:false});
				$modal.on({
					'hide.uk.modal': function(){
						Method.setDontShowCookie();
					}
				});
				dontShowCookieId = 'dontShowUpdateProfileModal_' + _GLOBAL.CUSTOMER.ID;
				var isDontShowCookieValue = Core.utils.cookie.get(dontShowCookieId);
				if( isDontShowCookieValue != 'Y' || isConfirmation == true ){
					// 비회원은 무조건 노출
					if( _GLOBAL.CUSTOMER.ISSIGNIN == false ){
						Method.show();
					}else{
						// 회원은 아직 동의 안한 고객만 노출
						if( _GLOBAL.CUSTOMER.IS_CONSENT_TRANSFER_OVERSEA == false ){
							Method.show();
						}
					}
				}

				$('[data-btn-ask-me-later]').on('click', function(){
					endPoint.call('updateProfileModalAskMeLater', Method.getDontShowAgainValue())
				})

				$('[data-btn-update-profile]').on('click', function(){
					endPoint.call('updateProfileModalLinkRedirect', Method.getDontShowAgainValue())
				})
			},
			getDontShowAgainValue:function(){
				return $('input[name="dontShowAgain"]').is(':checked');
			},
			show:function(){
				$modal.show();
			},
			setDontShowCookie:function(){
				if( Method.getDontShowAgainValue() == true ){
					Core.utils.cookie.set(dontShowCookieId, 'Y', 24, '/');
				}else{
					Core.utils.cookie.remove(dontShowCookieId, '/');
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-update-profile-modal]',
					attrName:'data-module-update-profile-modal',
					moduleName:'module_update_profile_modal',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
