(function(Core){
	Core.register('module_reservation_product', function(sandbox){
		var $this, $btn, args, serviceMenData={}, reservationData={}, arrInventoryList, itemRequest, confirmData;
		var loadStore = function(){
			if(!serviceMenData.hasOwnProperty('goodsCode')) return;
			//serviceMenData['mode'] = 'template';
			//serviceMenData['templatePath'] = '/page/partials/reservedInventory';
			//serviceMenData['cache'] = new Date().getTime();

			sandbox.utils.promise({
				//url:'/processor/execute/reserved_inventory',
				url:sandbox.utils.contextPath + '/reservedInventory',
				type:'GET',
				cache:false,
				data:serviceMenData
			}).then(function(data){
				// 은정 배포 후 적용 라인
				var inventoryList = data; //sandbox.utils.strToJson(data.replace(/&quot;/g, '"'));
				arrInventoryList = [];
				inventoryList.forEach(function(a,i){
				    if(a.quantity > 0){
				        arrInventoryList.push(a);
				    }
				});

				$this.find('.location-search').empty().append(
					Handlebars.compile($("#store-list").html())({
						result:(arrInventoryList.length > 0)?true:false,
						list:arrInventoryList
					})
				);
			}).fail(function(msg){
				defer = null;
				UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
			});
		}

		var Method = {
			moduleInit:function(){
				args = arguments[0];
				$this = $(this);
				$btn = $this.find('.reservation-btn');

				var currentDate = new Date();
				var reservationModal = UIkit.modal('#reservation-modal', {center:true});
				var disabledDays = [];
				var radioComponent = sandbox.getComponents('component_radio', {context:$this}, function(i){
					var _self=this;
					var INDEX = i;
					var skuData = sandbox.utils.strToJson(this.getThis().attr('data-sku'));

					this.addEvent('change', function(val){
						switch(INDEX){
							case 0 :
								for(var i=0; i<skuData.length; i++){
									for(var j=0; j<skuData[i].selectedOptions.length; j++){
										if($(this).attr('data-id') == skuData[i].selectedOptions[j]){
											serviceMenData['goodsCode'] = escape(skuData[i].externalId);
											loadStore();
											return;
										}
									}
								}
								break;
							case 1 :
								serviceMenData['localNo'] = val;
								$this.find('.location').text(val);
								$this.find('.dim').removeClass('active');
								$this.find('.location-code-wrap').removeClass('active');
								loadStore();
								break;
						}
					});
				});

				$this.on('click', '.location-select', function(e){
					e.preventDefault();
					$this.find('.location-code-wrap').addClass('active');
					$this.find('.dim').addClass('active');
				});

				$this.on('click', '.reservation-apply', function(e){
					e.preventDefault();
					var $form = $(this).closest('form');
					var INDEX = $(this).closest('.shipping-list').index();

					confirmData = {};
					itemRequest = BLC.serializeObject($form);

					/* 예약주문 확인 */
					confirmData.customer = {name:_GLOBAL.CUSTOMER.FIRSTNAME, phone:_GLOBAL.CUSTOMER.PHONE}
					confirmData.storeInfo = arrInventoryList[INDEX];
					confirmData.product = itemRequest;
					disabledDays = arrInventoryList[INDEX].fulfillmentLocationCloseDates;
					$this.find('.datepicker').datepicker('refresh');
					$this.find('.datepicker-wrap').addClass('active');
					$this.find('.dim').addClass('active');
					$this.find('input[name=fulfillmentLocationId]').val($(this).attr('data-locationid'));
				});


				$this.on('click', '.reservation-confirm-btn', function(e){
					e.preventDefault();

					var $form = $(this).closest('form');
					itemRequest = BLC.serializeObject($form);

					/* 예약주문 필수값 처리 */
					if(!itemRequest.reservedDate || itemRequest.reservedDate === ''){
						UIkit.notify('방문 날짜/시간를 선택해주세요', {timeout:3000,pos:'top-center',status:'danger'});
						return false;
					}

					sandbox.setLoadingBarState(true);
					BLC.ajax({
						url:$form.attr('action'),
						type:"POST",
						dataType:"json",
						data:itemRequest
					},function(data){
						if(data.error){
							sandbox.setLoadingBarState(false);
							UIkit.modal.alert(data.error);
						}else{
							/*var reservationComplateTemplate = Handlebars.compile($("#store-complate").html())();
							$('#reservation-modal').find('.contents').empty().append(reservationComplateTemplate);*/
							_.delay(function(){
								window.location.assign( sandbox.utils.contextPath + '/checkout' );
							}, 500);
						}
					});
				});

				$this.on('click', '.cencel-btn', function(e){
					e.preventDefault();
					$this.find('.reservation-confirm-wrap').removeClass('active');
					$this.find('.datepicker').removeClass('active');
					$this.find('.dim').removeClass('active');
				});

				//datapicker
				$this.find('.datepicker').datepicker({
					dateFormat: "yy-mm-dd",
					minDate:new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()),
					onSelect:function(date){
						confirmData['reservedDate'] = date;
						$this.find('.timepicker').focus();
					},
					beforeShowDay:function(date){
						var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
						return [ disabledDays.indexOf(string) == -1 ];
					}
				});

				//timepicker
				$this.find('.timepicker').focusout(function(e){
					var _self = $(this);
					setTimeout(function(){
						var time = _self.val();
						confirmData['reservedDate'] += ' ' + time + ':00';
					},200);
				});

				$this.find('.btn-time-submit').click(function(e){
					e.preventDefault();

					if(!confirmData['reservedDate']){
						UIkit.notify('방문 날짜와 시간을 선택해 주세요.', {timeout:3000,pos:'top-center',status:'warning'});
						return;
					}

					$this.find('.datepicker-wrap').removeClass('active');
					$this.find('.dim').removeClass('active');

					var reservationConfirmTemplate = Handlebars.compile($("#store-confirm").html())(confirmData);
					$this.find('.reservation-confirm-wrap').empty().append(reservationConfirmTemplate);
					$this.find('.reservation-confirm-wrap').addClass('active');
					$this.find('input[name=reservedDate]').val(confirmData['reservedDate']);
					itemRequest['reservedDate'] = confirmData['reservedDate'];
				});

				//dim click addEvent
				$this.find('.dim').off().on('click', function(e){
					$this.find('.reservation-confirm-wrap').removeClass('active');
					$this.find('.datepicker').removeClass('active');
					$this.find('.dim').removeClass('active');
					$this.find('.location-code-wrap').removeClass('active');
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-reservation-product]',
					attrName:'data-module-reservation-product',
					moduleName:'module_reservation_product',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
