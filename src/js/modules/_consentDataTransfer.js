(function(Core){
	'use strict';

	Core.register('module_consent_transfer', function(sandbox){
		var $this, dontShowCookieId, endPoint, redirectContext;
		var Method = {
			moduleInit:function(){
				$this = $(this);
				endPoint = Core.getComponents('component_endpoint');
				dontShowCookieId = 'dontShowUpdateProfileModal_' + _GLOBAL.CUSTOMER.ID;
				$this.find('[data-btn-cancel]').on('click', Method.cancel);
				$this.find('[data-btn-save]').on('click', Method.save);
				redirectContext = $('[data-redirect-context]').val() || Core.utils.contextPath;
				sandbox.getComponents('component_radio', {context:$this}, function(i){
					this.addEvent('change', function(){
						Method.agreeRadioChangeHanlder();
					});
				});
			},
			agreeRadioChangeHanlder:function(){
				$this.find('[data-btn-save]').removeClass('disabled').attr('disabled', false);
			},
			cancel:function(){
				Core.ui.modal.open('#not-identity-confirm-modal', {
					center:true, 
					show:function(){
						$('[data-btn-not-consent]').off('click').on('click', function(){
							$('[data-btn-go-back]').trigger('click');
							Method.setDontShowCookie();
							Method.redirectHome();
						})
					}
				});
			},
			save:function(){
				var isAgree = $("input[name='agree']:checked").val() == 'true';
				endPoint.call('consentDataTransferClick', isAgree );
				if(isAgree){
					Method.submit(true);
				}else{
					Core.ui.modal.open('#not-consent-confirm-modal', {
						center:true, 
						show:function(){
							$('[data-btn-not-consent]').off('click').on('click', function(){
								endPoint.call('noConsentSureClick', Method.getDontShowAgainValue());
								$('[data-btn-go-back]').trigger('click');
								Method.submit(false);
							})
						}
					});
				}
			},
			submit:function(isAgree){
				var $form = $this.find('form');
				var param = sandbox.utils.getQueryParams($form.serialize());
				param.cache = false;
				sandbox.utils.ajax($form.attr('action'), $form.attr('method'), param, function(data){
					var resultData = data.responseJSON;
					if( resultData.result == true ){
						if( isAgree == true ){
							Core.ui.modal.open('#consent-success-alert',{
								center:true,
								keyboard:false,
								bgclose:false,
								hide:function(){
									Method.redirectHome();
								}
							});
						}else{
							Method.setDontShowCookie();
							Method.redirectHome();
						}
					}else{
						endPoint.call('errorEvent', 'decision processing error' );
						Core.ui.modal.alert(resultData.errorMessage);
					}
				})
			},
			getDontShowAgainValue:function(){
				return $('input[name="dontShowAgain"]').is(':checked');
			},
			redirectHome:function(){
				Core.ui.loader.show();
				location.assign(redirectContext + '/');
			},
			setDontShowCookie:function(){
				if( Method.getDontShowAgainValue() == true ){
					Core.utils.cookie.set(dontShowCookieId, 'Y', 24, '/');
				}else{
					Core.utils.cookie.remove(dontShowCookieId, '/');
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-consent-transfer]',
					attrName:'data-module-consent-transfer',
					moduleName:'module_consent_transfer',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
