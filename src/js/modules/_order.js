(function(Core){

	Core.register('module_order', function(sandbox){
		var $this = null,
			$allCheck = null, 					// 팝업 전체 선택 체크박스
			$itemList = null, 					// 선택 해서 popup에 노출되는 아이템 리스트
			$itemListObj = null, 				// addon 이 제거된 아이템들
			$popModal = null, 					// 취소 팝업
			cancelOrderId = null, 				// 취소할 주문 id
			$popSubmitBtn = null, 				// 취소 submit 버튼
			$previewContainer = null, 			// 사용안함
			isAllFg = null, 					// 취소 선택시 모든 fulfilment가 취소 가능했는지 여부
			isAblePartialVoid = null, 			// 부분 취소 가능여부
			beforeSelectedOrder = null, 		// 사용안함
			$refundAccountInfo = null, 			//환불정보 입력 폼
			oldOrderUrl = null, 				//이전 사이트 주문 정보 URL
			args = null,
			objStore = {store:[]};

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0] || {};

				//modal init
				var modal = UIkit.modal('#common-modal', {center:true});
				// 오늘 날짜 location 대입
		        var today = new Date(),
		            yyyy = today.getFullYear(),
		            mm = today.getMonth() + 1,
		            dd = today.getDate();

		        if (dd < 10) dd = '0' + dd
		        else if (mm < 10) mm = '0' + mm
		        today = yyyy + mm + dd;

				//주문취소 시작
				//상점정보 가져오기
				var orderHistoryContainer = new Vue({
					el:'[data-vue-orderhistory]',
					data:objStore,
					created:function(){
						objStore['store'] = 1;
						sandbox.utils.promise({
							url:sandbox.utils.contextPath +'/processor/execute/store',
							method:'GET',
							data:{
								'mode':'template',
								'templatePath':'/page/partials/storeList',
								'isShowMapLocation':false,
								'ignoreSharing':true
							},
							isLoadingBar:false
						}).then(function(data){
							objStore['store'] = sandbox.utils.strToJson(data.replace(/&quot;/g, '"'));
						}).fail(function(data){
							UIkit.notify(data, {timeout:3000,pos:'top-center',status:'danger'});
						});
					},
					components:{
						'location':{
							props:['store']
						},
						'order-cencel-button':{
							props:['orderId', 'isJustReservation'],
							template:'<button class="btn-link width-fix cancel-order" v-on:click="orderCencel">{{rtnLabel}}</button>',
							computed:{
								rtnLabel:function(){
									return (this.isJustReservation) ? '예약취소' : '주문취소';
								}
							},
							methods:{
								orderCencel:function(e){
									e.preventDefault();
									var orderId = this.orderId;

									sandbox.utils.promise({
										url:sandbox.utils.contextPath + '/account/order/cancel/' + orderId,
										method:'GET'
									}).then(function(data){
										var defer = $.Deferred();
										$('#common-modal').find('.contents').empty().append(data);
										sandbox.moduleEventInjection(data, defer);
										modal.show();
										return defer.promise();
									}).then(function(data){
										UIkit.modal.alert("취소 되었습니다.").on('hide.uk.modal', function() {
											window.location.reload();
										});
									}).fail(function(error){
										if(error){
											UIkit.modal.alert(error).on('hide.uk.modal', function() {
												window.location.reload();
											});
										}else{
											window.location.reload();
										}
									});
								}
							}
						}
					},
					methods:{
						findLocation:function(locationId){
							try{
								for(var i=0; i<this.store.length; i++){
									if(this.store[i]['id'] == locationId){
										return this.store[i]['name'];
									}
								}
							}catch(e){
								console.log(e);
							}
						},
						shipType:function(locationId){
							try{
								var rtnState = '온라인 물류센터';
								for(var i=0; i<this.store.length; i++){
									if(this.store[i]['id'] == locationId){
										rtnState = this.store[i]['isDefault'] ? '온라인 물류센터':'매장';
										break;
									}
								}
								return rtnState;
							}catch(e){
								console.log(e);
							}
						}
					}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order]',
					attrName:'data-module-order',
					moduleName:'module_order',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
