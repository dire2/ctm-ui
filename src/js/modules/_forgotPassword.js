(function(Core){
	Core.register('module_forgot_password', function(sandbox){
		var $this, $form, $stepContainer, $errorAlert, $identifier, $otpSubmitWrap, $otp, identifierVal, messageType;
		var Method = {
			moduleInit:function(){
				// listSize = 검색 결과 한번에 보여질 리스트 수
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);
				$this = $(this);
				$form = $this.find("form");
				$stepContainer = $this.find(".step-container");
				$errorAlert = $this.find('[data-error-alert]');
				$identifier = $this.find('#identifier');
				$otpSubmitWrap = $this.find('[data-wrap-submit-otp]');
				$otp = $this.find('#otp');

				// 검색버튼 클릭시
				$form.submit(function(e){
					e.preventDefault();
					Method.hideAlert();
					Method.submit();
					return false;
				});

				// 검색된 리스트중 선택시
				// $this.on('click', '[data-customer-select-btn]', Method.selectCutomer );
				$this.on('click', '[data-certify-btn]', Method.guestCertify );
				$this.on('click', '[data-back-btn]', function(){
					$('[data-component-forgotpasswordmodal]').trigger('click');
				});
				$this.on('click', '[data-btn-request-otp]', Method.requestOtpCode );
				$this.on('click', '[data-btn-submit-otp]', Method.submitOtpCode );
			},

			requestOtpCode:function(e){
				e.preventDefault();
				var _self = this;
				identifierVal = Core.utils.string.trim($identifier.val());

				if(Core.utils.string.trim(identifierVal)==''){
					Core.ui.modal.alert('이메일 또는 전화번호를 입력하세요.');
					return;
				}

				// 체크 로직 추가 해야함
				console.log($identifier.val());
				if(identifierVal.indexOf('@') > -1){
					var emailRule = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z_])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/;
					if(!emailRule.test(identifierVal)){
						Core.ui.modal.alert('정확한 이메일을 입력하세요.');
						return;
					}
					messageType = 'EMAIL';
				}else{
					// 숫자와 '-' 만 있으면 전화번호 입력
					var phoneType = /^[0-9]([0-9||-])+$/;
					if(phoneType.test(identifierVal)){
						var phoneRule = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
						if(!phoneRule.test(identifierVal)){
							Core.ui.modal.alert('정확한 전화번호를 입력하세요.');
							return;
						}
						messageType = 'KAKAO';
					}else{
						Core.ui.modal.alert('정확한 정보를 입력하세요.');
						return;
					}
					
				}
				
				var param = {
					'messageType' : messageType,
					'userName': identifierVal,
					'mode': 'customer',
					'csrfToken' : $form.find('input[name="csrfToken"]').val()
				}

				Core.utils.ajax(Core.utils.contextPath+'/otp/request', 'POST', param, function(data) {
					var jsonData = Core.Utils.strToJson( data.responseText, true );
					if(jsonData.result==true){
						Core.ui.modal.alert(jsonData.messages || '인증번호를 발송했습니다. 인증번호가 오지 않으면<br />입력하신 정보가 회원정보와 일치하는지 확인해 주세요.');
						$(_self).text('다시받기');
						$otpSubmitWrap.removeClass('uk-hidden');
					}else{
						if( jsonData.errors == null){
							Core.ui.modal.alert('인증번호를 발송하지 못하였습니다.<br/>다시 시도해주세요.', {
								hide:function(){
									Core.ui.modal.close('#common-modal');
								}
							})
						}else{
							Core.ui.modal.alert(jsonData.errors);
						}
					}
				}, true, false, 1500);
			},

			submitOtpCode:function(e){
				e.preventDefault();
				var $otp = $this.find('#otp');
				var otpVal = Core.utils.string.trim($otp.val());

				if(Core.utils.string.trim(otpVal)==''){
					Core.ui.modal.alert('인증번호를 입력하세요.');
					return;
				}

				var param = {
					'messageType' : messageType,
					'userName': identifierVal,
					'otp': otpVal,
					'mode': 'customer',
					'csrfToken' : $form.find('input[name="csrfToken"]').val()
				}

				Core.utils.ajax(Core.utils.contextPath+'/otp/confirm', 'POST', param, function(data) {
					var jsonData = Core.Utils.strToJson( data.responseText, true );
					if(jsonData.result==true){
						Method.submit();
					}else{
						if( jsonData.errors == null){
							Core.ui.modal.alert('인증에 실패 하였습니다.<br/>다시 시도해주세요.', {
								hide:function(){
									Core.ui.modal.close('#common-modal');
								}
							})
						}else{
							Core.ui.modal.alert(jsonData.errors);
						}
					}
				}, true, false, 1000);
			},
			
			updateSelectOrder:function(e){
				e.preventDefault();
				// 자신 버튼 숨기기
				$(this).parent().hide();
				// 자신 컨텐츠 켜기
				$(this).closest('li').find('[data-certify-content]').slideDown('300');
				// 다른 버튼 보이기
				$(this).closest('li').siblings().find('[data-customer-select-btn]').parent().show();
				// 다른 컨텐츠 숨기기
				$(this).closest('li').siblings().find('[data-certify-content]').hide();
			},

			submit:function(){
				// 인증번호 받은 이후에 input 에 있는 번호를 수정할 경우를 대비하여 다시 설정
				$identifier.val(identifierVal);
				sandbox.utils.ajax($form.attr("action"), 'POST', $form.serialize(), function(data){
					Method.createCustomerList(JSON.parse( data.responseText ));
				});
			},
			viewStep:function(num){
				// 현재 2번으로 넘어갈때 만 사용중
				$stepContainer.addClass('uk-hidden');
				$otpSubmitWrap.addClass('uk-hidden');
				$identifier.val('');
				$otp.val('');
				$this.find('.step-' + num ).removeClass('uk-hidden');
			},
			showAlert:function(msg){
				Core.ui.modal.alert(msg);
			},
			hideAlert:function(){
				$errorAlert.addClass('uk-hidden');
			},
			createCustomerList:function(data){
				var result = data['result'];
				var $listContainer = $this.find('.list-container');
				var list = data['ro'];
				var html = '';

				if( result == true ){
					if( list.length == 0 ){
						Method.showAlert('검색하신 내용을 찾을 수 없습니다. 다른 정보를 이용해 다시 검색해 주십시오.');
					}else{
						$.each( list, function( index, li ){
							li.useName = (li.fullName!=null && $.trim(li.fullName)!='');
							li.dateCreated = li.dateCreated.slice(0, 10).split("-").join(".");
						});

						html = Handlebars.compile($("#forgot-password-list").html())(list);

						$listContainer.html( html );
						//console.log( list );
						sandbox.moduleEventInjection( html );

						$this.on('click', '[data-customer-select-btn]',  Method.updateSelectOrder );

						Method.viewStep(2);
					}
				}else{
					Method.showAlert('검색하신 내용을 찾을 수 없습니다. 다른 정보를 이용해 다시 검색해 주십시오.');
				}
				///customer/requestPasswordChangeUrl?successUrl=/recover&customer=
			},

			// 비회원 인증 처리
			guestCertify:function(){
				var type = $(this).attr('data-type');
				var customerId = $(this).closest('li').find('input[name="customerId"]').val();
				var email = $(this).closest('li').find('input[name="email"]').val();
				var phoneNum = $(this).closest('li').find('input[name="phonenum"]').val();
				var url = sandbox.utils.contextPath + "/login/requestPasswordChangeUrl?customer=" + customerId;

				if( type === 'email'){
					url += '&messageType=EMAIL';
				}else if( type === 'kakao'){
					url += '&messageType=KAKAO';
				}

				sandbox.utils.ajax(url, 'GET', {}, function(data){
					var responseData = sandbox.rtnJson(data.responseText);
					if(responseData.result == true){
						if(type === 'email'){
							Method.viewStep(3);
						}else if(type === 'kakao'){
							Method.viewStep(4);
						}
					}else{
						Method.showAlert(responseData['errorMsg']);
					}

				}, true );

				return;
			}

		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[ data-module-forgot-password ]',
					attrName:'data-module-forgot-password',
					moduleName:'module_forgot_password',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
