(function(Core){
	Core.register('module_category', function(sandbox){
		var $that;
		var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				$this = $(this);

				// assist category 리스트 판별
				$(this).closest('body').addClass('module_category');

				//uk-width-medium-1-3 uk-width-large-1-3
				//view Length 2:maxLen
				$this.find('.select-view > button').click(function(e){
					e.preventDefault();

					if(!$(this).hasClass('active')){
						$(this).addClass('active').siblings().removeClass('active');

						var value = $(this).attr('data-value');

						$this.find('[data-component-categoryitem]').parent()
						.removeClass(arrViewLineClass.join(' '))
						.addClass('uk-width-large-1-'+value);

						//category lineSize
						sandbox.getModule('module_pagination').setLineSize(value);

						var $customBanner = $(this).closest('section').find('.item-list-wrap').find('.product-item.customBanner');

						if( $customBanner.length > 0){
							if( value <= 2 ){
								$customBanner.addClass('uk-hidden');
							}else{
								$customBanner.removeClass('uk-hidden');
							}
						}
					}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-category]',
					attrName:'data-module-category',
					moduleName:'module_category',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
