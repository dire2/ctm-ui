(function(Core){
	'use strict';

	Core.register('module_global_popup', function(sandbox){
		var pop, endPoint;
		var Method = {
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				endPoint = Core.getComponents('component_endpoint');
				$.extend(Method, args);

				var options = {
					id : Method.id,
					width : Method.width,
					height : Method.height,
					marginLeft : Method.marginLeft || 0,
					marginTop : Method.marginTop || 0,
					marginBottom : Method.marginBottom || 0,
					layoutType : Method.layoutType,
					backgroundColor : Method.backgroundColor,
					borderWidth : Method.borderWidth || 0,
					boxPosition : Method.boxPosition,
					triggerActionType : Method.triggerActionType,
					triggerActionValue : Method.triggerActionValue,
					animationType : Method.animationType,
					closeExpireTime : Method.closeExpireTime,
					useCloseMessage : Method.useCloseMessage,
					closeType : Method.closeType,
					closePosition : Method.closePosition,
					closeMarginTop : Method.closeMarginTop || 0,
					closeMarginLeft : Method.closeMarginLeft || 0,
					closePaddingTop : Method.closePaddingTop || 0,
					closePaddingRight : Method.closePaddingRight || 0,
					closePaddingBottom : Method.closePaddingBottom || 0,
					closePaddingLeft : Method.closePaddingLeft || 0,
					closeBackgroundHeight : Method.closeBackgroundHeight || 0
				}
				pop = $("#global_popup_" + options.id);
				// productList 있을시에는 상품 정보가 있는 페이지( cart, checkout, confirmation ) 에서만 팝업이 동작한다.
				// 팝업 role 설정에서 위 3페이지에 대한 설정을 해야 정상적으로 동작할수 있다.
				if( Method.productList != 'none' ){
					var itemList = _GLOBAL.MARKETING_DATA().itemList;
					var productList = Method.productList;

					if( itemList != null ){
						productList = String(productList).replace(/\s/gi, "");
						productList = productList.split(',');
						itemList = $.map(itemList, function(item){ return item.model });

						if(_.intersection( productList, itemList ).length > 0){
							Method.openPopup(options);
						}else{
							pop.remove();
						}
					}else{
						pop.remove();
					}
				}else if( Method.cartItem != 'none' ){
					if( Core.utils.is.isEmpty(Method.cartItem) == false){
						var rules = '==';
						var result = false;
						var cartItemLen = Number(Method.cartItem.replace('>','').replace('<',''));
						var curCartItemLen = _GLOBAL.CUSTOMER.CART_ITEM;
						if( Core.utils.string.startsWith(Method.cartItem, '>')) rules = '>';
						if( Core.utils.string.startsWith(Method.cartItem, '<'))	rules = '<';
						switch (rules) {
							case '==':
								result = curCartItemLen == cartItemLen;
								break;
							case '<':
								result = curCartItemLen < cartItemLen;
								break;
							case '>':
								result = curCartItemLen > cartItemLen;
								break;
						}
						result ? Method.openPopup(options) : pop.remove();
					}else{
						pop.remove();
					}
				}else{
					Method.openPopup(options);
				}
			},
			openPopup:function(options){
				pop.brzPopup($.extend(options, {
					show:function(){
						endPoint.call('openGlobalPopup',{name : options.id});
					}
				}))
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-global-popup]',
					attrName:'data-module-global-popup',
					moduleName:'module_global_popup',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
