;(function($){
	var brzPopup = function( $el, opts ){
		this.element = $el;
		this.content = this.element.find(".gp-content-container");
		this.close = this.element.find(".gp-close");
		//this.closeBtn = this.element.find(".gp-close .btn.close");
		this.closeBtn = this.element.find(".gp-close .close-container");
		this.options = opts;
		this.style = {}; // 레이아웃 잡아놓은 스타일 저장
		this.isOver = null; // 현재 지정된 사이즈보다 화면 사이즈가 큰지 여부
		this.isFixHeader = this.options.layoutType == "FIXHEADER"; // 상단 고정 여부
		this.isCreatCloseArea = ( this.options.closeType == "DEFAULT" ); // 하단에 영역을 만들어 닫기 버튼을 노출 할지 여부
		//this.target = ( this.options.layoutType != 'LAYER' ) ? this.content : this.content // 모션처리와 리사이즈가 될 객체
		this.target = this.content // 모션처리와 리사이즈가 될 객체
		this.init();
	}
	$.extend(brzPopup.prototype, {
		init : function(){
			if( cookie.get( "pop_cookie_"+this.options.id ) != "Y"){
				this.setLayout();
				this.setEvent();

				if(this.options.triggerActionType == "SECONDS" ){
					var _this = this;
					var time = ( this.options.triggerActionValue == null ) ? 100 : (this.options.triggerActionValue * 1000);
					setTimeout(function(){
						_this.show();
						_this.setCloseLayout();
					}, time);
				}else{
					this.show();

					// 화면에 그려진 후에 높이값이 체크 되는 것 때문에 우선 show를 하고 처리
					this.setCloseLayout();
				}
			}else{
				this.hide();
			}
		},

		setLayout : function(){
			var css = {"top":0, "left":0, "marginTop":0, "marginLeft":0, "maxWidth": this.options.width, "maxHeight": this.options.height };

			// 좌우 여백 처리
			css.marginLeft = this.options.marginLeft;

			// 상하 여백 처리
			css.marginTop = this.options.marginTop;

			// 닫기 영역 높이값만큼 컨텐츠 여백 처리
			if( this.isCreatCloseArea ){
				css.paddingBottom = this.options.closeBackgroundHeight;
			}

			if( this.isFixHeader ){
				css.top = 0;
				css.left = 0;
				css.height = this.options.height;
				css.marginBottom = this.options.marginBottom;
			}else{
				// 세로 위치 값 처리
				switch( this.options.boxPosition.split("_")[0] ){
					case "MIDDLE":
						css.top = "50%";
						// + Number(this.options.borderWidth*2)
						css.marginTop -= (Number(this.options.height))/2;
						if( this.isCreatCloseArea == true ){
							css.marginTop -= Number(this.options.closeBackgroundHeight)/2;
						}
						break;
					case "BOTTOM":
						css.top = "100%";
						css.marginTop -= (Number(this.options.height));

						// 닫기 버튼이 하단에 있으면 높이만큼 marginTop을 올려준다
						if( this.isCreatCloseArea == true ) css.marginTop -= Number(this.options.closeBackgroundHeight);
						break;
				}
			}

			// 가로 위치값 처리
			switch( this.options.boxPosition.split("_")[1] ){
				case "CENTER":
					css.left = "50%";
					// + Number(this.options.borderWidth*2)
					css.marginLeft -= ( Number(this.options.width))/2
					break;
				case "RIGHT":
					css.left = "100%";
					// + Number(this.options.borderWidth*2)
					css.marginLeft -= (Number(this.options.width));
					break;
			}

			// layout 타입에 따른 위치값 조절 처리
			if( this.options.layoutType == "MODAL" ){
				css.position = "absolute";
				this.target.css( css );
			}else if( this.options.layoutType == "LAYER" ){
				css.position = "fixed";
				this.target.css( css );
			}else{
				css.position = "relative";
			}

			this.style = css;
			this.resize();
		},

		setCloseLayout : function(){
			/*
				overlay 타입
				 - 상하 간격 적용
				 - padding 적용
				 - 그 사이즈에 따라 배경색 적용

				footer 타입
				 - 높이값 적용
				 - 높이값 만큼 lineheight 적용
			*/

			var css = {};
			var btnCss ={};

			// overlay로 처리 될 때
			if( !this.isCreatCloseArea ){
				css.paddingLeft = this.options.closePaddingLeft;
				css.paddingRight = this.options.closePaddingRight;
				css.paddingTop = this.options.closePaddingTop;
				css.paddingBottom = this.options.closePaddingBottom;

				// 세로 위치 값 처리
				switch( this.options.closePosition.split("_")[0] ){
					case "TOP":
						css.top = 0;
						css.marginTop = this.options.closeMarginTop;
						break;
					case "MIDDLE":
						css.top = "50%";
						css.marginTop = this.options.closeMarginTop;
						css.marginTop -= Number(this.close.height()) /2 + (( this.options.closePaddingTop + this.options.closePaddingBottom ) / 2);
						break;
					case "BOTTOM":
						css.bottom = 0;
						css.marginBottom = this.options.closeMarginTop;
						break;
				}

				// 가로 위치값 처리
				switch( String(this.options.closePosition.split("_")[1] )){
					case "LEFT":
						css.left = 0;
						css.marginLeft = this.options.closeMarginLeft;
						break;
					case "CENTER":
						css.left = "50%";
						css.marginLeft = 0;
						css.marginLeft -= Number(this.close.width()) /2 + (( this.options.closePaddingLeft + this.options.closePaddingRight ) / 2);
						break;
					case "RIGHT":
						css.right = 0;
						css.marginRight = this.options.closeMarginLeft;
						break;
				}

			}else{
				css.height = this.options.closeBackgroundHeight;
				css.lineHeight = this.options.closeBackgroundHeight +"px";

				// 가로 위치값 처리
				switch( String(this.options.closePosition.split("_")[1] )){
					case "LEFT":
						btnCss.marginLeft = this.options.closeMarginLeft;
						break;
					case "CENTER":
						css.textAlign = "center";
						//btnCss.marginLeft = -Number(this.closeBtn.width()) /2;
						break;
					case "RIGHT":
						css.textAlign = "right";
						btnCss.marginRight = this.options.closeMarginLeft;
						break;
				}
			}
			this.close.css( css );
			this.closeBtn.css ( btnCss );
		},

		setEvent : function(){
			var _this = this;
			var $el = _this.element;
			// 닫기 버튼을 누르거나 배경을 눌렀을때 닫침
			//var $close = $el.find(".btn.close, >.gp-overlay-container");

			var $closeBtn = $el.find(".btn.close");
			var $bg = $el.find(">.gp-overlay-container");
			var $expiresChk = $el.find("input[name='checkClose']");

			/*
			$bg.off("click").on("click", function(e){
				e.preventDefault();
				_this.hide();
			});
			*/
			$expiresChk.off('click').on('click', function(e){
				// 하루동안 보지 않기 체크박스를 숨겨놓을 경우 바로 닫기 버튼을 클릭 되도록 처리
				if( $(this).closest('[data-component-checkbox]').find('[data-checkbox-close]').is(':visible') == false ){
					_this.popupClose();
				}
			})
			$closeBtn.off("click").on("click", function(e){
				e.preventDefault();
				_this.popupClose();
				
				/*
				// 닫기버튼에 쿠키 제어 할수 있는 class가 있으면 처리
				if( $(this).hasClass("expires")){
					cookie.set( "pop_cookie_"+  _this.options.id, "Y", _this.options.closeExpireTime );
				}
				*/

				
			});

			$(window).resize(function() {
			   _this.resize();
			});
		},
		
		resize : function(){
			var screenWidth = window.innerWidth;
			var popWidth = Number(this.options.width);
			var popHeight = Number(this.options.height);
			var borderWidth = Number(this.options.borderWidth);
			var css = {};

			// 화면 보다 작을때
			//+borderWidth*2
			if( screenWidth <= popWidth){
				if( this.isOver || this.isOver == null){
					this.isOver = false;
					//this.target.css({"marginLeft":0, "left":0});
					css.marginLeft = 0;
					css.left = 0;
				}

				// 가로 사이즈 비율에 따른 세로 사이즈 처리
				//var maxHeight = popHeight * (screenWidth-(borderWidth*2)) / popWidth;
				var maxHeight = popHeight * screenWidth / popWidth;
				var marginTop = 0;

				if( this.isFixHeader ){
					// 상단 고정 배너라면 marginBottom 값 처리
					css.marginBottom = this.options.marginBottom;
				}else{
					// 세로 사이즈 변경에 따른 높이값 처리
					if( this.options.boxPosition.indexOf( "MIDDLE" ) > -1){
						marginTop = (popHeight - maxHeight ) / 2;
					}else if(this.options.boxPosition.indexOf( "BOTTOM" ) > -1){
						marginTop = popHeight - maxHeight;
					}
				}
				//borderWidth*2
				css.maxWidth = screenWidth;
				css.maxHeight = maxHeight;
				css.marginTop = this.style.marginTop + marginTop;

				this.target.css( css );

			}else{
				if( !this.isOver || this.isOver == null){
					this.isOver = true;
					this.target.css(this.style);
				}
			}
		},

		show : function(){
			// 화면 스크롤 막기
			if( this.options.layoutType == 'MODAL' ){
				$("html").css({ "overflow" : "hidden" });
			}

			this.element.show();

			if( this.options.animationType == "FADE_IN" ){
				this.target.css({"opacity":0, "filter":"alpha(opacity=0)", "display" : "block" });
				this.target.animate({ opacity : 1 }, 500 );
			}else if( this.options.animationType == "SLIDE_IN" ){
				this.target.css({"opacity":0, "filter":"alpha(opacity=0)", "display" : "block", "marginTop": "+=30px"});
				this.target.animate({ opacity : 1, marginTop : "-=30px"	}, 500 );
			}else{
				this.target.show();
			}

			if( Core.utils.is.isFunction(this.options.show)){
				this.options.show();
			}
		},

		hide : function(){
			if( this.options.layoutType == 'MODAL' ){
				$("html").removeAttr('style');
			}

			if( this.isFixHeader ){
				this.element.slideUp("normal", function(){
					//this.element.remove();
				});
			}else{
				this.element.remove();
			}

			if( Core.utils.is.isFunction(this.options.hide)){
				this.options.hide();
			}
		},

		popupClose : function(){
			var _this = this;
			var $expiresChk = _this.element.find("input[name='checkClose']");

			setTimeout(function(){
				_this.hide();
			}, 100);

			if( $expiresChk.is(":checked") ){
				cookie.set( "pop_cookie_"+  _this.options.id, "Y", _this.options.closeExpireTime );
			}
		}

	});

	var cookie = {
		set : function( name, value, hours ){
			if( hours ){
				var date = new Date();
				date.setTime(date.getTime()+(hours*3600*1000));
				$.cookie(name, value, { expires: date });
			}
		},
		get : function( name ){
			return $.cookie( name );
		},
		remove : function( name ){
			$.cookie( name, null );
		}
	}

	$.fn.brzPopup = function( opts ){
		return this.each(function(){
			new brzPopup( $(this), opts );
		});
	}
})($);
