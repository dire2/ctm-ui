Handlebars.registerHelper('isStoreType', function(key, option) {
	if(key !== 'storeType' && key !== 'icon'){
		return option.fn(this);
	}
});


//handlebars helper
Handlebars.registerHelper('addClass', function(index, option){
   if(index == 0){
	  return 'active';
   }else{
	  return ''
   }
});

Handlebars.registerHelper('isMobile', function(key, option) {
	if(Core.Utils.mobileChk){
		return option.fn(this);
	} else {
		return option.inverse(this);
	}
});
