(function(Core){
	Core.register('module_fulfillmentfilter', function(sandbox){
		'use strict';

		var $filterForm, $filter, args, currentMinPrice, currentMaxPrice, minPrice, maxPrice, limit, arrInputPrice = [], arrQuery = [], currentRangePrice = '', endPoint;
		var pricePattern = 'price=range[{{minPrice}}:{{maxPrice}}]';

		var limitPrice = function(price){
			if(price < minPrice) return minPrice;
			else if(price > maxPrice) return maxPrice;
			else return price;
		}

		var replaceComma = function(str){
			return str.replace(/,|\.+[0-9]*/g, '');
		}

		var getPriceByPercent = function(price){
			return (price-minPrice) / (maxPrice-minPrice) * 100;
		}

		var getPercentByPrice = function(per){
			return Math.round(minPrice+(limit * per)/ 100);
		}


		var callEndPoint = function( option ){
			var temp = option.split("=");
			if( temp.length > 1){
				var opt = {
					key : temp[0],
					value : temp[1]
				}
				endPoint.call( 'applyFilter', opt );
			}
		}

		var Method = {
			moduleInit:function(){
				args = arguments[0];
				$filter = $(this);
				$filterForm = $filter.find('#fillterForm');
				endPoint = Core.getComponents('component_endpoint');

				//$('input[type=checkbox]').prop('checked', false);

				$filter.find('[data-initialization-btn]').on("click", Method.reset );
				$filter.find('[data-submit-btn]').on("click", Method.submitFilter );

				//초기 query 분류
				//arrQuery = sandbox.utils.getQueryParams(location.href, 'array');
                var query = sandbox.utils.getQueryParams(location.href);
                var initYn = true;

                // 초기에 셋팅된 검색 필터가 없다면 search 영역 삭제
                var $searchOptions = $("#search_type option");
                if(_.isEmpty( $searchOptions ) ){
                	$("[data-search-content]").remove();
                }

                /*
					term 검색기간 param : day, threeday, week, month, setting( 설정으로 stdDate, etdDate=20180712 )
                */
				for(var key in query){
					if(key !== 'page'){
						if(typeof query[key] === 'string'){
							arrQuery.push(key+'='+query[key]);
						}else if(typeof query[key] === 'object'){
							for(var i=0; i < query[key].length; i++){
								arrQuery.push(key+'='+query[key][i]);
							}
						}
					}

					if(key.indexOf("term") !== -1){
						initYn = false;
					}
				}

				if(initYn){
					Method.initialization();
				}else{
					for(var key in query){
						//console.log( key + ' : ' + query[key] )
						if(key === 'stdDate' || key === 'etdDate' || key === 'searchType' || key === 'searchText'){
							if(key === 'stdDate' || key === 'etdDate'){
								//console.log("key" + decodeURI(query[key]));
								if(query[key] != ''){
									var strDate = query[key].substring(0,4) + "/" + query[key].substring(4,6) + "/" + query[key].substring(6,8);
									$("input[name='"+key+"']").val(strDate);
								}
							}else if(key === 'searchType'){
								$("select[name='"+key+"']").val(query[key]).attr('selected', 'selected');

								// 검색어가 있을 때
								var searchTypeName = query[key];
								if( query[ searchTypeName ] != null && query[ searchTypeName ] != null ){
									$("input[name='searchText']").val(decodeURIComponent(query[searchTypeName]).split("+").join(" "));
								}
							}else{
								/*
								if(query[key] != ''){
									$('.search-input').addClass('focus');
								}
								$("input[name='"+key+"']").val(decodeURIComponent(query[key]).split("+").join(" "));
								*/
							}
						}else{
							if(key === 'term' && query[key] === 'setting'){
								$("input:radio[name='"+key+"'][value='"+query[key]+"']").attr('id');
								$("label[for='"+$("input:radio[name='"+key+"'][value='"+query[key]+"']").attr('id')+"']").addClass('on');
							}

							if( $('[data-filter-option]'+'[name="'+key+'"]').length > 0){
								$("input:text[name='"+key+"']").val(decodeURIComponent(query[key]).split("+").join(" "));
							}
							$("input:radio[name='"+key+"'][value='"+query[key]+"']").attr('checked', true);
						}
					}
				}


				var filterType, pType, pStatus, rStatus, paramTerm,  paramText,  termLabel, paramDelivery, label;

				for (var key in query) {

					if (key === 'term') {
						paramTerm = query[key];
					}

					if (key === 'pStatus' || key === 'rStatus' || key === 'pType' || key === 'filterType') {
						console.log( key );
                        paramDelivery = query[key];
                    }

					if (key === 'searchText') {
						paramText = decodeURIComponent(query[key]);
					}
				}

				if(initYn){
					termLabel = $('input[name=term]').eq(0).attr('id');
					pType = $('input[name="pType"]').eq(0).attr('id');
					pStatus = $('input[name="pStatus"]').eq(0).attr('id');
					rStatus = $('input[name="rStatus"]').eq(0).attr('id');
					filterType = $('input[name="filterType"]').eq(0).attr('id');
				}else{
					termLabel = $('input:radio[name=term][value="' + paramTerm + '"]').attr('id');
					pType = $('input:radio[name="pType"][value="'+paramDelivery+'"]').attr('id');
					pStatus = $('input:radio[name="pStatus"][value="'+paramDelivery+'"]').attr('id');
					rStatus = $('input:radio[name="rStatus"][value="'+paramDelivery+'"]').attr('id');
					filterType = $('input:radio[name="filterType"][value="'+paramDelivery+'"]').attr('id');

					if(paramText == null || paramText == 'undefined' || paramText == '') {
						$('.keyword').html("<span></span>");
					} else {
						$('.keyword').html("#<span>" +  paramText.split("+").join(" ") + "</span>");
					}
				}

				if(paramTerm=='setting'){
					$('.state_btn_area').html("<a>" + $('#cal_inp01').val() + "~" + $('#cal_inp02').val() + "</a>");
				}else{
					$('.state_btn_area').html("<a>" + $("label[for='" + termLabel + "']").text() + "</a>");
				}

				if( pStatus != null ){
					label = pStatus;
				}
				if( rStatus != null){
					label = rStatus;
				}
				if( pType != null){
					label = pType;
				}
				if( filterType != null){
					label = filterType;
				}

				if( label != null ){
					$('.state_btn_area').append( '<a>' + $('label[for="'+label+'"]').text() + '</a>' );
				}

				$("input:radio").click(function(){
					//console.log($(this).val());
					//console.log($(this).attr("name"));

					// 기간 설정 클릭시
					if($(this).attr("name") == "term" && $(this).val() == "setting"){
						var stdDate = $('input[name=stdDate]').attr('id');
						var etdDate = $('input[name=etdDate]').attr('id');

						// 날짜 입력 필드 설정 추가
						$('.inp').attr('autocomplete', 'on');

						if($("label[for='"+$(this).attr('id')+"']").hasClass('on')){
							Method.settingDate();
						}
					}
				});

				//filter price range
				var priceRange = sandbox.getComponents('component_range', {context:$filter}, function(){
					this.addEvent('change', function(per){
						if($(this).hasClass('min')){
							currentMinPrice = getPercentByPrice(per);
							arrInputPrice[0].setValue(sandbox.rtnPrice(currentMinPrice));
						}else if($(this).hasClass('max')){
							currentMaxPrice = getPercentByPrice(per);
							arrInputPrice[1].setValue(sandbox.rtnPrice(currentMaxPrice));
						}
					});

					this.addEvent('touchEnd', function(per){
						var val = sandbox.utils.replaceTemplate(pricePattern, function(pattern){
							switch(pattern){
								case 'minPrice' :
									return currentMinPrice;
									break;
								case 'maxPrice' :
									return currentMaxPrice;
									break;
							}
						});

						if(arrQuery.indexOf(currentRangePrice) > -1){
							arrQuery.splice(arrQuery.indexOf(currentRangePrice), 1);
						}

						callEndPoint( val );
						arrQuery.push(val);
						currentRangePrice = val;

						Method.appendCateItemList();
					});
				});

				var textfield = sandbox.getComponents('component_textfield', {context:$filter}, function(){
					this.addEvent('focusout', function(e){
						var type = $(this).attr('data-name');
						var per = getPriceByPercent(limitPrice(replaceComma($(this).val())));

						if(type === 'min'){
							priceRange.getSlide(0).setPercent(per);
						}else if(type === 'max'){
							priceRange.getSlide(1).setPercent(per);
						}
					});

					arrInputPrice.push(this);
				});

				if(priceRange){
					var objPrice = (priceRange) ? priceRange.getArgs() : {min:0, max:1};
					minPrice = (objPrice.min == 'null') ? 0:parseInt(objPrice.min);
					maxPrice = (objPrice.max == 'null') ? 1:parseInt(objPrice.max);
					limit = maxPrice - minPrice;
					currentMinPrice = replaceComma(arrInputPrice[0].getValue());
					currentMaxPrice = replaceComma(arrInputPrice[1].getValue());
					priceRange.getSlide(0).setPercent(getPriceByPercent(currentMinPrice));
					priceRange.getSlide(1).setPercent(getPriceByPercent(currentMaxPrice));

					currentRangePrice = sandbox.utils.replaceTemplate(pricePattern, function(pattern){
						switch(pattern){
							case 'minPrice' :
								return currentMinPrice;
								break;
							case 'maxPrice' :
								return currentMaxPrice;
								break;
						}
					});
				}


				// 필터 클릭 처리
				sandbox.getComponents('component_radio', {context:$filter, unlock:true}, function(i){
					var currentValue = '';

					//처음 라디오 박스에 체크 되었을때만 이벤트 발생
					this.addEvent('init', function(){
						var val = this.attr('name') +'='+ encodeURIComponent($(this).val());
						currentValue = val;
					});

					this.addEvent('click', function(input){
						var val = $(input).attr('name') +'='+ encodeURIComponent($(input).val());
						if($(this).parent().hasClass('checked')){
							arrQuery.splice(arrQuery.indexOf(val), 1);
						}else{
							if(currentValue !== '') arrQuery.splice(arrQuery.indexOf(currentValue), 1);
							callEndPoint( val );
							arrQuery.push(val);
							currentValue = val;
						}

						Method.appendCateItemList();
					});
				});

				sandbox.getComponents('component_checkbox', {context:$filter}, function(){
					this.addEvent('change', function(){
						var val = $(this).attr('name') +'='+ encodeURIComponent($(this).val());

						if(arrQuery.indexOf(val) !== -1){
							arrQuery.splice(arrQuery.indexOf(val), 1);
						}else{
							callEndPoint( val );
							arrQuery.push(val);
						}

						Method.appendCateItemList();
					});
				});

				//필터 동작
				$(document).on('click', '.filter-remove-btn', function(e){
					e.preventDefault();

					var query = encodeURI($(this).attr('href'));

					/*if(href.match(/[가-힣]/g)){
						query = encodeURI(href);
					}else{
						query = href;
					}*/

					arrQuery.splice(arrQuery.indexOf(query), 1);
					Method.appendCateItemList();
				});

				$(document).on('click', args['data-module-fulfillmentfilter'].filterOpenBtn, function(e){
					e.preventDefault();

					var query = sandbox.utils.getQueryParams(location.href);
					//console.log(Object.keys(query).length);
					//console.log(Object.keys(query));
					$("label[for='"+$("input:radio[name='"+key+"'][value='"+query[key]+"']").attr('id')+"']").removeClass('on');
					$("label[for='"+$('input[name=stdDate]').attr('id')+"']").css('display', 'none');
					$("label[for='"+$('input[name=etdDate]').attr('id')+"']").css('display', 'none');

					/* webpos 수정 */
					$filter.stop().animate({opacity:1, left:0}, 300,function(){
						$(this).addClass('active');
					});
					$('.dim').addClass('active');
					$('html').addClass('uk-modal-page');
					//$('body').css('paddingRight', 15);
					/* //webpos 수정 */
				});

				$(document).find('.dim').click(function(e){
					/* webpos 수정 */
					// $filter.stop().removeClass('active').animate({opacity:0, left:-300}, 300);
					$filter.stop().animate({opacity:0, left:-300}, 300,function(){
						$(this).removeClass('active');
					});
					$('.dim').removeClass('active');
					$('html').removeClass('uk-modal-page');
					$('body').removeAttr('style');
				});

				$('.locationInventories').delegate('.dim', 'click', function(e){
					/* webpos 수정 */
					// $filter.stop().removeClass('active').animate({opacity:0, left:-300}, 300);
					$filter.stop().animate({opacity:0, left:-300}, 300,function(){
						$(this).removeClass('active');
					});
					$('.dim').removeClass('active');
					$('html').removeClass('uk-modal-page');
					$('body').removeAttr('style');
				});


				//필터 더보기 버튼
				$filter.find('.more-btn').each(function(){
					var $this = $(this);
					var $target = $this.prev();
					var minHeight = $target.height();
					var maxHeight = $target.children().height();

					$(this).click(function(e){
						e.preventDefault();

						if($this.hasClass('active')){
							$this.removeClass('active');
							$target.stop().animate({'height':minHeight}, 300);
						}else{
							$this.addClass('active');
							$target.stop().animate({'height':maxHeight}, 300);
						}
					});
				});


				$("label[for='productSportAll']").on('click', function() {
					var len = 0;
					var attrId = $(this).attr('id');
					if($('#' + attrId + len).is(":checked") == true) {
						$('#' + attrId + len).attr("checked", false);
						for (var i = 0; i < $("input[name=" + attrId + "]").length; i++) {
							$("input[name=" + attrId + "]").eq(len).attr("checked", false);
							len ++;
						}
					} else {
						$('#' + attrId + len).attr("checked", true);
						for (var i = 0; i < $("input[name=" + attrId + "]").length; i++) {
							$("input[name=" + attrId + "]").eq(len).attr("checked", true);
							len ++;
						}
					}
				});

				$('.calender_wrap > .calender_box input[type="tel"]')
				.on('focus', function(){
					Method.setDateFocus( $(this).attr("name") )
				})
				.on('blur', function(){
					Method.setDateBlur( $(this).attr("name") )
				})

			},
			reset:function(){
				var type = $(this).closest('[data-section-key]').data('section-key');
				var menuList = $('body').find('[data-module-fulfillmobilegnb]');
				// 현재 페이지의 key와 같은 링크를 메뉴에서 찾아 클릭 이벤트 실행
				var menu = menuList.find('.mobile-menu_onedepth').find('a[href^="/'+type+'"]');
				if( menu.length > 0 ){
					$(menu)[0].click();
				}
			},
			initialization:function(){
				//첫번째 radio버튼 checked처리
				for(var i = 0 ; i < $('.f-radiobox').length ; i++){
					$('.f-radiobox').eq(i).find('input:radio').eq(0).attr('checked',true)
					//console.log($('.f-radiobox').eq(i).find('input:radio').eq(0).attr('checked',true));
				}
				$("select[name=searchType] option:eq(0)").attr("selected", "selected");
				$('input[name=searchText]').val('');
				$("label[for='"+$("input:radio[name=term][value=setting]").attr('id')+"']").removeClass('on');
				//달력세팅(당일~당일)
				//Method.settingDate();




				// osun 기본적으로 필터 값을 맞추기 위해  redirect 처리 하던 부분은 사용하지 않는다.
				/*
				var filterReset = window.location.pathname.split('/')[4];
				switch(filterReset){
					case 'orderList' 			:	// 일반주문확인
						return location.href="orderList?fgType=PHYSICAL_SHIP&sort=last&term=day";
					case 'packingList' 			:	// 배송관리
						return location.href="packingList?fgType=PHYSICAL_SHIP&delivery_stat=picking&type=pack&sort=last&term=threeday";
					case 'bopisOrder' 			:	// BOPIS주문확인
						return location.href="bopisOrder?fgType=PHYSICAL_PICKUP&sort=last&term=day";
					case 'pickedOrReadyCmtList' :	// 픽업관리
						return location.href="pickedOrReadyCmtList?type=delivery&sort=last&term=threeday&delivery_stat=product";
					case 'seamlessOrderList' 	:	// seamless주문관리
						return location.href="seamlessOrderList?type=order&fgType=PHYSICAL_SHIP&sort=last&term=day&orderStore=store&delivery_stat=submitted";
				}
				*/
			},
			settingDate:function(){
				/*
				var now = new Date();

				var year = now.getFullYear();       //년
				var month = now.getMonth() + 1;     //월
				var day = now.getDate();            //일
				var currentDate;
				if(month < 10){
					month = '0' + month;
				}

				if(day < 10){
					day = '0' + day;
				}
				currentDate = year + "/" + month + "/" + day;
				//currentDate = year + month + day;
				*/

				var currentDate = moment().format('YYYY/MM/DD');
				var stdDate = $('input[name=stdDate]').attr('id');
				var etdDate = $('input[name=etdDate]').attr('id');
				$('input[name=stdDate]').val(currentDate);
				$('input[name=etdDate]').val(currentDate);
				$("label[for='"+stdDate+"']").text(currentDate);
				$("label[for='"+etdDate+"']").text(currentDate);
				$("label[for='"+stdDate+"']").css('display', 'none');
				$("label[for='"+etdDate+"']").css('display', 'none');
				//return currentDate;
			},

			setDateBlur:function(dt){
				if($("input[name='"+dt+"']").val() != '' && $("input[name='"+dt+"']").val().length == 8){
					var strdt = $("input[name='"+dt+"']").val().substring(0,4) + "/" + $("input[name='"+dt+"']").val().substring(4,6) + "/" + $("input[name='"+dt+"']").val().substring(6,8);
					$("input[name='"+dt+"']").val(strdt);
					console.log("strdt : " + strdt);
				}
			}
			,
			setDateFocus:function(dt){
				$("input[name='"+dt+"']").val($("input[name='"+dt+"']").val().split("/").join(""));
				console.log("dt : " + $("input[name='"+dt+"']").val());
			},

			getTermValidateResult:function(){
				// console.log("stdDate : " + $('input[name=stdDate]').val());
					// console.log("etdDate2222 : " + $('input[name=etdDate]').val());
					// console.log("stdDate1111 : " + ($('input[name=stdDate]').val() == ''));
				if(($('input[name=stdDate]').val() == '') || ($('input[name=etdDate]').val() == '')){
						UIkit.modal.alert('조회기간을 입력하세요.');
						return false;
				}
				//alert($('input[name=etdDate]').val().split("/").join("").length);
				if($('input[name=stdDate]').val().length != 10 || $('input[name=etdDate]').val().length != 10
					|| $('input[name=stdDate]').val().split("/").join("").length != 8 || $('input[name=etdDate]').val().split("/").join("").length != 8){
					UIkit.modal.alert('날짜형식에 맞지 않습니다.');
					return false;
				}

				var date1 = new Date($('input[name=stdDate]').val());
				var date2 = new Date($('input[name=etdDate]').val());
				var diffDay = (date2.getTime() - date1.getTime()) / (1000*60*60*24);
				var today = new Date();

				// console.log("date2222 : " + isNaN(date1));
				// console.log("date3333 : " + isNaN(date2));
				if(isNaN(date1) || isNaN(date2)){
					UIkit.modal.alert('날짜형식에 맞지 않습니다.');
					return false;
				}
				if(today < date1 || today < date2){
					UIkit.modal.alert('오늘 이후의 날짜는 선택할 수 없습니다.');
					return false;
				}
				console.log(diffDay);
				if(diffDay < 0){
					UIkit.modal.alert('조회 시작일이 종료일보다 이후 일 수 없습니다.');
					return false;
				}
				if(diffDay > 31){
					UIkit.modal.alert('조회기간은 최대 1개월 입니다.');
					return false;
				}

				return true;
			},
			getSubmitDate:function( type, startDate, endDate ){
				var now = $("#now").val();
				var date = new Date( now );
				var submitDate = '';
				var startTime = ' 00:00:00';
				var endTime = ' 23:59:59';
				var day, month, year;

				switch( type ){
					case 'day':
						return now + startTime + '|' + now + endTime;
					break;

					case 'threeday':
						//date.setDate(date.getDate()-3);
						submitDate = moment(now, 'YYYY.MM.DD').subtract(3, 'day').format('YYYY.MM.DD');
					break;

					case 'week':
						//date.setDate(date.getDate()-7);
						submitDate = moment(now, 'YYYY.MM.DD').subtract(1, 'week').format('YYYY.MM.DD');
					break;

					case 'month':
						//date.setMonth(date.getMonth()-1);
						submitDate = moment(now, 'YYYY.MM.DD').subtract(1, 'months').format('YYYY.MM.DD')
					break;

					case 'setting':
						return startDate.split('/').join('.') + startTime + '|' + endDate.split('/').join('.') + endTime
					break;
				}

				return submitDate  + startTime +  '|' + now + endTime ;
				
				/*
				if( type != 'day' && type != 'setting'){
					day = date.getDate();
					month = date.getMonth() + 1;
					year = date.getFullYear();
				}
				return year + '.' + month + '.' + day  + startTime +  '|' + now + endTime ;
				*/

			},

			getMinLenByFilterType:function(type){
				var query = sandbox.utils.getQueryParams(location.href);
				var isTest = (query.test != null) ? true : false;

				if( isTest ){
					return 0;
				}else{
					switch( type ){
						case 'orderNumber': case 'order.orderNumber': case 'pickingNumber': case 'returnNumber': case 'PARENT_ORDER_NUMBER': case 'fulfillmentGroupItem.orderItem.order.returnNumber':
							return 8;
						break;

						case 'address.fullName': case 'name': case 'assignedAdminUser': case 'fulfillmentGroupItem.fulfillmentGroup.address.fullName':
							return 2;
						break;
						
						case 'address.phonePrimary.phoneNumber': case 'fulfillmentGroups.address.phonePrimary.phoneNumber':
							return 7;
						break;

						case 'emailAddress': case 'fulfillmentGroupItem.orderItem.product.model': case 'fulfillmentGroupItem.orderItem.sku.upc': case 'fulfillmentGroupItem.fulfillmentGroup':
							return 4;
						break;

						default:
							return 2; 
						break;
					}
				}
			},

			getFgStatusByFtStatus:function( ftStatus ){

				/*
							상품 준비중           		   상품 준비완료             배송중            배송완료(인도완료)     	  취소
				FG 		   PROCESSING				   PROCESSING			FULFILLED			 DELIVERED			CANCELLED
				FT 		   REQUEST_PROCESSING          REQUESTED        	 FULFILL			 DELIVERED      	 CANCEL

				취소는 확인 해야함 - FG가 CANCELLED 이지만 FT는 CANCEL 이 아닌 경우가 있음
				*/

				switch( ftStatus ){
					case 'REQUEST_PROCESSING':
						return 'PROCESSING';
					break;
					case 'REQUESTED':
						return 'PROCESSING';
					break;
					case 'SCHEDULED':
						return 'PROCESSING';
					break;
					case 'FULFILL':
						return 'FULFILLED';
					break;
					case 'DELIVERED':
						return 'DELIVERED';
					break;
					case 'CANCEL':
						return 'CANCELLED';
					break;
				}
			},
			getFmatByFtStatus:function( ftStatus ){
				return ftStatus.split('PROCESSING_').join('');
			},
			submitFilter:function(e){
				/*
				console.log( Method.getSubmitDate('day'));
				console.log( Method.getSubmitDate('threeDay'));
				console.log( Method.getSubmitDate('week'));
				console.log( Method.getSubmitDate('month'));
				console.log( Method.getSubmitDate('setting', ));
				*/
				var dateType = $("input:radio[name=term]:checked").attr("value");
				var startDate = "";
				var endDate = "";

				// 기간 설정 사용시
				if(dateType == 'setting'){
					// 정상적인 날짜 체크가 되면
					if( Method.getTermValidateResult() ){
						startDate = $('input[name=stdDate]').val();
						endDate = $('input[name=etdDate]').val();

						// order.submitDate 값 설정
						$('input[id="submitDate"]').val( Method.getSubmitDate('setting', startDate, endDate ) );
					}else{
						// submit 되는것을 막음
						return false;
					}
				}else{
					$('input[id="submitDate"]').val( Method.getSubmitDate( dateType ));
				}
				
				$('input[name=stdDate]').val( startDate.split("/").join("") );
				$('input[name=etdDate]').val( endDate.split("/").join("") );

				// 검색어를 입력 해놨다면
				if( $.trim($('#search_text').val()) != '' ){
					var name = $("#search_type option:selected").val();
					var value = $('#search_text').val();
					var minLen =  Method.getMinLenByFilterType( name );
					if( value.length < minLen){
						UIkit.modal.alert('최소 ' + minLen + '자 이상을 입력하세요.');
						return false;
					}else{
						$('#search_text_hidden').attr('name', name );
						$('#search_text_hidden').val(value);
					}
				}

				// 배송관리에서 상태값 처리
				// FULFILL_PREPARE, FULFILL_COMPLETE,  CUSTOMER_SERVICE_REQUEST, CUSTOMER_SERVICE_PROCESSING
				var pStatus = $("input:radio[name='pStatus']:checked").attr("value");
				if( pStatus != null && pStatus != "ALL"){
					$('input[id="pStatus"]').val( pStatus );
					$('input[id="delivery_fg_state"]').val( Method.getFgStatusByFtStatus(pStatus) );
					
					if( pStatus == "CANCEL"){
						$('input[id="pStatus"]').remove();
					}
					if( pStatus.indexOf('PROCESSING_') > -1 ){
						$('input[id="pStatus"]').remove();
						$('input[id="delivery_fg_state"]').val( 'PROCESSING' );
						$('input[id="fma_type"]').val( Method.getFmatByFtStatus(pStatus) );
					}else{
						$('input[id="fma_type"]').remove();
					}
				}else{
					// 8.24 all이 없어져서 현재 사용안함
					$('input[id="pStatus"]').remove();
					$('input[id="delivery_fg_state"]').remove();
					$('input[id="fma_type"]').remove();
				}

				// 반품에서 상태값 처리
				var rStatus = $("input:radio[name='rStatus']:checked").attr("value");
				if( rStatus != null && rStatus != "ALL"){
					$('input[id="rStatus"]').val( rStatus );
				}else{
					$('input[id="rStatus"]').remove();
				}

				// 주문확인에서 배송 타입 처리
				var pType = $("input:radio[name='pType']:checked").attr("value");
				if( pType != null ){
					var rType = '';
					var isJustReservation = '';
	 				switch( pType ){
						case "SHIPPING":
							rType = 'PHYSICAL_SHIP';
						break;
						case "BOPIS":
							rType = 'PHYSICAL_PICKUP';
							isJustReservation = false;
						break;
						case "ROPIS":
							rType = 'PHYSICAL_PICKUP';
							isJustReservation = true;
						break;
						case "SAMEDAY":								
							rType = 'PHYSICAL_SHIP';		
						break;
					}

					if(rType == ''){
						$('input[name="type"]').remove();
					}else{
						$('input[name="type"]').val(rType);
					}

					if(isJustReservation === ''){
						$('input[name="isJustReservation"]').remove();
					}else{
						$('input[name="isJustReservation"]').val(isJustReservation);
					}
				}
				$(document).find('.dim').trigger('click');
			},
			appendCateItemList:function(){
				//console.log(getPagingType);
				var query = arrQuery.join('&');
				query += sandbox.getModule('module_pagination') ? (sandbox.getModule('module_pagination').getPagingType() === 'number' ? '&page=1&' : '') : ''
				window.location.assign(location.pathname + '?' + query);

				//$(args.form).serialize();
				//$(args.form).submit();
				//console.log($('[' + args['data-module-fulfillmentfilter'].form + ']').serialize());
				//$('[' + args['data-module-fulfillmentfilter'].form + ']').submit();

				/*sandbox.utils.ajax(url, 'GET', queryString, function(data){
					var responseText = $(data.responseText).find(args['data-module-fulfillmentfilter'].target)[0].innerHTML;
					$(args['data-module-fulfillmentfilter'].target).empty().append(responseText);
					sandbox.moduleEventInjection(responseText);
				});*/
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-fulfillmentfilter]',
					attrName:['data-module-fulfillmentfilter'],
					moduleName:'module_fulfillmentfilter',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			getMinLenByFilterType:Method.getMinLenByFilterType
		}
	});
})(Core);