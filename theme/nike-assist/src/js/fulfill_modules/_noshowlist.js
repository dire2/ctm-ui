(function(Core){
	Core.register('module_noshow_list', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0] || {};
				var queryParams = sandbox.utils.getQueryParams($(this).find('form').serialize());

				$this.on('click', '[data-pickup-btn]', function(e){
					e.preventDefault();
					var $form = $(this).closest('#paramForm');
					var params = sandbox.utils.getQueryParams($form.serialize());

					var phoneNumber = params.phoneNumber;
					var orderNumber = params.orderNumber;
					var userName = decodeURIComponent(params.userName);

					if( _.isEmpty(phoneNumber) || _.isEmpty(orderNumber) || _.isEmpty(userName) ){
						UIkit.modal.alert('정보가 올바르지 않습니다.<br/>확인 후 다시 시도해 주세요.').on('hide.uk.modal', function() {
							location.reload();
						});
						return;
					}
					UIkit.modal.confirm('상품 인도는 본인 인증 후 가능합니다.<br />' + phoneNumber + ' 번호로 인증 하시겠습니까? ', function(){
						var otpModal = UIkit.modal('#popup-layer-order-custom');
						otpModal.show();
						var otp = sandbox.getModule('module_authenticate');

						otp.reset({
							isDirectAuth : true,
							customText : '<h6 style="margin:15px 0px;">이름 : '+ userName + '</h6><h6 style="margin:15px 0px;">연락처 : ' + phoneNumber + '</h6>',
							authData : {
								orderNumber : orderNumber,
								messageType : 'KAKAO'
							}
						});

						otp.success(function(){
							if(params.isJustReservation === 'true'){
								UIkit.modal.confirm('매장예약 주문입니다. 매장 내 POS 로 결제해야 합니다.<br /> 진행하시겠습니까?', function(){
									Method.pickupSubmit(params);
								}, function(){
									UIkit.modal.alert('매장 내 POS로 결제 후 다시 시도해 주세요').on('hide.uk.modal', function() {
										sandbox.setLoadingBarState(true);
										location.reload();
									});
								})
							}else{
								Method.pickupSubmit(params);
							}

						}).fail( function(){
							UIkit.modal.alert('본인 인증에 실패 하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function() {
								location.reload();
							});
						});
					});
				});

				$this.on('click', '[data-cancel-btn]', function(e){
					e.preventDefault();
					var defer = $.Deferred();
					var $form = $(this).closest('#paramForm');
					var params = sandbox.utils.getQueryParams($form.serialize());

					UIkit.modal.confirm("'No Show'로 인해 주문을 취소합니다.<br/> 진행하시겠습니까?", function(){
						sandbox.utils.promise({
							url:decodeURIComponent(params.submitUrl),
							method:'POST',
							data:{fgId:params.fgId, csrfToken:params.csrfToken, cancelledReasonType:'CANCELLED_NO_SHOW'}
						}).then(function(data){
							if(!_.isEmpty(data.errors)){
								//defer.reject(data.errors);
								defer.reject('주문 취소 처리 중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.');
							}else{
								UIkit.modal.alert('정상적으로 처리되었습니다.').on('hide.uk.modal', function() {
									sandbox.setLoadingBarState(true);
									location.reload();
								});
							}
						}).fail(function(msg){
							UIkit.modal.alert(msg).on('hide.uk.modal', function() {
								//sandbox.setLoadingBarState(true);
								//location.reload();
							});
						});
					});
				});
			},
			pickupSubmit:function(queryParams){
				var defer = $.Deferred();
				var params = queryParams;

				sandbox.utils.ajax('/order-action/pickup-delivery', 'POST', {fgId:params.fgId, csrfToken:params.csrfToken}, function(data){
					var data = $.parseJSON( data.responseText );
					if(!_.isEmpty(data.errors)){
						UIkit.modal.alert(data.errors.message).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						})
					}else{
						UIkit.modal.alert('정상적으로 처리되었습니다.').on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}
				});

				/*
				sandbox.utils.promise({
					url: '/order-action/fulfill',
					data : {fgId:params.fgId, csrfToken:params.csrfToken},
					method:'POST'
				}).then(function(data){
					if(!_.isEmpty(data.errors)){
						defer.reject('상품 인도 처리 중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.(배송처리)');
					}else{
						defer.resolve();
					}
					return defer.promise();
				}).then(function(data){
					return sandbox.utils.promise({
						url:'/order-action/delivery',
						data:{fgId:params.fgId, csrfToken:params.csrfToken},
						method:'POST',
					});
				}).then(function(data){
					if(!_.isEmpty(data.errors)){
						defer.reject('상품 인도 처리 중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.(배송완료처리)');
					}else{
						UIkit.modal.alert('상품 인도 처리가 정상적으로 완료되었습니다.').on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}
				}).fail(function(msg){
					UIkit.modal.alert(msg).on('hide.uk.modal', function() {
						sandbox.setLoadingBarState(true);
						location.reload();
					});
				});

				*/
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-noshow-list]',
					attrName:'data-module-noshow-list',
					moduleName:'module_noshow_list',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
