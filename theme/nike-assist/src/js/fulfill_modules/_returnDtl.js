(function(Core){
	Core.register('module_return_detail', function(sandbox){
		var $popAddressModal, $itemList, $quantityList, $retrunOrderForm, isAblePartialVoid, customerId, locationId, btnArea, paymentInfo, storeAddress, qtyComponent;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				$retrunOrderForm = $this.find('#returnOrderForm');
				$itemList = $retrunOrderForm.find('[data-return-order-item]');
				$quantityList = $retrunOrderForm.find('[data-return-order-item] [name="quantity"]');
				isAblePartialVoid = $this.find('input[name="ablePartialReturn"]'); //부분 반품 가능 여부
				locationId = $this.find('input[name="locationId"]').val();
				customerId = $this.find('input[name="customerId"]').val();
				$btnArea = $this.find('.btn_area');
				$popAddressModal = UIkit.modal("#popup-customer-address", {modal: false});

				// 반품 신청
				$this.find('[data-return-submit-btn]').on('click', function(){
					if(!$(this).hasClass('disabled')){
						Method.returnOrderSubmit();
					}
				})

				// 금액 계산
				$this.find('[data-return-calculator-btn]').on('click', function(){
					if(!$(this).hasClass('disabled')){
						Method.runPaymnentCalculator();
					}
				});

				// 수량 변경
				qtyComponent = sandbox.getComponents('component_quantity', {context:$this}, function(){
					this.addEvent('change', function(val){
						Method.resetInfo();
					});
				})

				// 전체 반품
				$this.find('[data-return-all-btn]').on('click', Method.returnAll );
				if( qtyComponent == null ){
					$this.find('[data-return-all-btn]').remove();
				}
				//$itemList.find('select').on("change", Method.resetInfo );
				//$('[data-use-store-address-btn]').on('click', Method.updateAddressByStore );
				//$('[data-customer-address-btn]').on('click', Method.showCustomerAddressListModal );

				paymentInfo = new Vue({
					el : '#refundPaymentInfo',
					data : {
						isShowRefundAccount : false,
						isShowPaymentInfo : false,
						isShowReturnAddressInfo : false,
						isShowCustomerPhoneNumber:false,
						paymentInfo : {},
						addressInfo : {
							name : '정보 없음',
							address1 : '정보 없음',
							address2 : '정보 없음',
							phoneNumber : '정보 없음',
							zipCode : '정보 없음',
							addressCity : '정보 없음',
						},
						isStoreReturn : true,
						fType : 'pickup'
					},
					created(){
						this.$nextTick( function(){
							Core.moduleEventInjection($('#refundPaymentInfo').html());
						})
					},
					watch:{
						// 환불 계좌 금액 영역 노출될 때 이벤트 재등록
						isShowRefundAccount:function( data ){
							this.$nextTick( function(){
								if( data == true ){
									Method.resetElementEvent($('#returnAddressInfo'));
									Method.resetElementEvent($('#refundAccount'));
								}
							});
						},
						addressInfo:function( data ){
							this.$nextTick( function(){
								this.isShowReturnAddressInfo = true;
							});
						},
						isStoreReturn:function( data ){
							//매장 반품 접수 값 변화에 따른 type update
							if( data ){
								this.fType = 'pickup';
							}else{
								this.fType = 'shpping';
							}
						}
					},
					methods:{
						updateAddressByStore:function(){
							Method.updateAddressByStore();
						},
						showCustomerAddressListModal:function(){
							Method.showCustomerAddressListModal();
						},
						resetInfo:function(){
							console.log( 'resetInfo' );
							Method.resetInfo();
						}
					}
				});

				var customerAddressComponent = sandbox.getComponents('component_customer_address', {context:$this}, function(){
					// 배송지 선택
					this.addEvent('select', function(address){
						Method.updateCustomerAddress({
							name : address.fullName,
							address1 : address.addressLine1,
							address2 : address.addressLine2,
							phoneNumber : address.phoneNumber,
							zipCode : address.postalCode,
							addressCity : address.city,
						});
						paymentInfo.$data.isShowCustomerPhoneNumber = false;
						if( $popAddressModal.isActive()){
							$popAddressModal.hide();
						}
					});
				});

				/*
				var addressList = $popAddressModal.find('[data-customer-address-select-btn]');
				// 등록되어있는 배송지가 없다면
				if( addressList.length == 0 ){
					Method.updateAddressByStore( true );
				}else{
					// 첫번째 주소 선택
					addressList.eq(0).trigger('click');
				}
				*/
				if( !_.isEmpty(locationId) ){
					Method.updateAddressByStore( true );
				}else{
					var addressList = $popAddressModal.find('[data-customer-address-select-btn]');
					// 등록되어있는 배송지가 없다면
					if( addressList.length == 0 ){
						Method.updateAddressByStore( true );
					}else{
						// 첫번째 주소 선택
						addressList.eq(0).trigger('click');
					}
				}
				sandbox.validation.init( $retrunOrderForm );
			},

			// 수량이나 사유 변경시 정보 초기화
			resetInfo:function(){
				console.log( 'resetInfo' );
				Method.updateBtnStatus('calc');
				paymentInfo.$data.isShowRefundAccount = false;
				paymentInfo.$data.isShowPaymentInfo = false;
			},
			// 매장 주소 사용 선택시
			updateAddressByStore:function( backLoading ){
				console.log('updateAddressByStore');
				// 이미 받아온 주소가 있다면
				if( storeAddress != null ){
					Method.updateCustomerAddress(storeAddress);
					paymentInfo.$data.isShowCustomerPhoneNumber = true;
				}else{
					sandbox.utils.ajax('/return-action/storeAddress', 'POST', { 'locationId' : locationId } ,function(data){
						var data = $.parseJSON( data.responseText );
						if( data.result == false ){
							// 여기가 에러임 ;;
							UIkit.modal.alert('매장 주소 정보가 올바르지 않습니다.').on('hide.uk.modal', function(){
								//sandbox.setLoadingBarState(true);
								//location.reload();
							})
						}else{
							storeAddress = {
								name : data.storeAddress.addressFirstName,
								address1 : data.storeAddress.addressLine1,
								address2 : data.storeAddress.addressLine2,
								phoneNumber : data.storeAddress.addressPhone,
								zipCode : data.storeAddress.addressPostalCode,
								addressCity : data.storeAddress.addressCityLocality
							}
							Method.updateCustomerAddress(storeAddress);
							paymentInfo.$data.isShowCustomerPhoneNumber = true;
						}
					}, false, backLoading );
				}
			},
			// 주소지 선택 팝업
			showCustomerAddressListModal:function(){
				console.log('showCustomerAddressListModal');
				/*
				paymentInfo.$data.addressInfo = {
					name : '정보 없음',
					address1 : '정보 없음',
					address2 : '정보 없음',
					phoneNumber : '정보 없음',
					zipCode : '정보 없음',
					addressCity : '정보 없음'
				}
				*/
				$popAddressModal.show();
			},
			// Vue 로 인하여 변화 되는 element에 다시 이벤트 등록
			resetElementEvent:function( $el ){
				Core.moduleEventInjection($el.html());
			},
			// 주소지 정보 업데이트
			updateCustomerAddress:function( data ){
				paymentInfo.$data.addressInfo = data;
			},
			// 전체 반품 선택시
			returnAll:function(){
				/*
				$.each($quantityList, function( index, data ){
					var maxIndex = $(this).find('option').length - 1;
					$(this).find('option').eq(maxIndex).attr("selected", "selected").trigger('update');
				})
				*/
				$.each(qtyComponent, function( index, data ){
					if( data.setQuantity != null ){
						data.setQuantity(100);
					}else{
						qtyComponent.setQuantity(100);
					}
				});
				Method.resetInfo();
			},
			// 반품 요청한 수량이 0보다 큰것이 있는지
			getIsAvaliableSubmitQuantity:function(){
				var isOk = false;
				$.each( $quantityList, function( index, data ){
					if( $(data).val() > 0 ){
						isOk = true;
						return false;
					}
				});
				return isOk;
			},
			/*
			getIsAvailablePartialReturn:function(){
				var itemList = {};
				$itemList.each( function(){
					isAvailable = false;
					var orderItemSize = $(this).find('input[name="orderItemSize"]').val();
					var ableEntireReturn = $(this).find('input[name="ableEntireReturn"]').val();
					var returnableQuantity = $(this).find('input[name="returnableQuantity"]').val();
					var returnedQuantity = $(this).find('input[name="returnedQuantity"]').val();
					var orderId = $(this).find('input[name="orderId"]').val();
					var quantity = $(this).find('[name="quantity"]:not(:disabled)').val();

					if( itemList[orderId] == undefined ){
						itemList[orderId] = {};
						itemList[orderId].ableEntireReturn = ableEntireReturn;
						itemList[orderId].orderItemSize = orderItemSize;
						itemList[orderId].items = [];
					}

					var itemObj = {
						returnedQuantity : Number(returnedQuantity),
						returnableQuantity : Number(returnableQuantity),
						quantity : Number(quantity)
					}

					itemList[orderId].items.push( itemObj );
				})

				console.log( itemList );
				var isAvailable = false;
				$.each( itemList, function(){
					// 전체 반품이 불가능하거나 현재 주문의 전체 아이템 사이즈와 선택된 아이템 사이즈가 같지 않다면
					console.log( this.orderItemSize + ' : ' + this.items.length );
					if( this.ableEntireReturn == "false" || this.orderItemSize != this.items.length ){
						//console.log( '전체 반품이 불가능하거나 현재 주문의 전체 아이템 사이즈와 선택된 아이템 사이즈가 같지 않다면')
						isAvailable = true;
					}else{
						$.each( this.items, function(){
							//console.log( this.returnableQuantity + ' : ' + this.quantity);
							if( this.returnedQuantity != 0 || this.returnableQuantity != this.quantity ){
								//console.log('반품된 수량이 있거나 전체 수량이 아닌것 있음');
								isAvailable = true;
								return;
							}
						})
					}

					if( isAvailable == false ){
						//console.log('부분반품 불가능한 order가 있음');
						return false;
					}
				})
				return isAvailable;
			},
			*/
			updateReturnPaymentInfo:function( payment ){
				var infoObj = [];
				/*
				$.each( payment.orderPaymentDTOs, function(){
					infoObj.push({
						name : _GLOBAL.PAYMENT_TYPE[ this.gatewayType ] + '(원결제금액)', price : )
					})
				})
				infoObj.push({
					name : '환불 가능 금액 ', price : sandbox.utils.price(payment.totalRefundablePayment.amount)
				})
				*/
				infoObj.push({
					name : '상품금액 합계 ', price : sandbox.utils.price(payment.totalItemAmount.amount)
				})
				infoObj.push({
					name : '주문 배송비', price : sandbox.utils.price(payment.originRefundableFulfillmentFee.amount)
				})
				infoObj.push({
					name : '반품 배송비', price : sandbox.utils.price(payment.returnFulfillmentFee.amount)
				})
				if( payment.feeChargeable == true ){
					infoObj.push({
						name : '추가 배송비', price : sandbox.utils.price(payment.originFulfillmentChargeFee.amount)
					})
				}
				infoObj.push({
					name : '총 환불 예정 금액', price : sandbox.utils.price(payment.totalRefundableAmount.amount)
				})
				paymentInfo.$data.paymentInfo = infoObj;

				// 환불 금액 정보 내용 변경 후 노출
				paymentInfo.$data.isShowPaymentInfo = true;

				// 환불 정보가 필요 하면
				//paymentInfo.$data.isShowRefundAccount = true;

				// 새로 노출 되는 정보들이 하단에 몰려 있기 때문에 스크롤을 밑으로 내린다.
				$("body, html").animate({easing: "easeIn", scrollTop: $(document).height()}, 800);
			},
			// 하단 버튼 상태값 변경 calc 이후에 반품 신청 가능하도록
			updateBtnStatus:function( type ){
				$btnArea.find('a').addClass('disabled');
				if( type == 'calc' ){
					$btnArea.find('[data-return-calculator-btn]').removeClass('disabled');
				}else{
					$btnArea.find('[data-return-submit-btn]').removeClass('disabled');
				}
			},

			// 수량이 0 인것만 체크해서 전달
			getFormDataByValid:function(){
				var params = sandbox.utils.getQueryParams($retrunOrderForm.serialize());
				var result = null;
				$.each( params.quantity, function( index, data ){
					if( data == 0 ){
						var qty = $($quantityList[index]);
						var orderId = qty.closest('li').find('input[name="orderId"]');
						var orderItemId = qty.closest('li').find('input[name="orderItemId"]');
						qty.attr('disabled', true );
						orderId.attr('disabled', true );
						orderItemId.attr('disabled', true );
					}
				})
				result = $retrunOrderForm.serialize();

				// 값을 만든 후에는 다시 상태값 원복
				$.each( params.quantity, function( index, data ){
					if( data == 0 ){
						var qty = $($quantityList[index]);
						var orderId = qty.closest('li').find('input[name="orderId"]');
						var orderItemId = qty.closest('li').find('input[name="orderItemId"]');
						qty.removeAttr('disabled');
						orderId.removeAttr('disabled');
						orderItemId.removeAttr('disabled');
					}
				})
				return result;
			},
			runPaymnentCalculator:function(){
				sandbox.validation.validate( $retrunOrderForm );
				if(!sandbox.validation.isValid( $retrunOrderForm )){
					return false;
				}

				if( !Method.getIsAvaliableSubmitQuantity() ){
					UIkit.modal.alert('반품 요청 수량이 없습니다.');
					return false;
				}

				var param = Method.getFormDataByValid() + '&calculateOnly=true';
				sandbox.utils.ajax('/return-action/returnable', 'POST', param , function(data){
					var data = $.parseJSON( data.responseText );
					console.log( data );
					if( data.result != false ){
						//통신 성공 후
						Method.updateReturnPaymentInfo( data.returnRequestDTO );
						Method.updateBtnStatus('submit');
					}else{
						UIKit.modal.alert('반품 금액 정보가 올바르지 않습니다.<br/>관리자에게 문의 하세요.').on('hide.uk.modal', function(){
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}
				});
			},
			returnOrderSubmit:function(){
				sandbox.validation.validate( $retrunOrderForm );
				if(!sandbox.validation.isValid( $retrunOrderForm )){
					return false;
				}

				UIkit.modal.confirm('반품 하시겠습니까?', function(){
					if( paymentInfo.$data.isShowCustomerPhoneNumber == true ){
						$retrunOrderForm.find("input[name='addressPhone']").val( $retrunOrderForm.find("input[name='customerPhoneNumber']").val());

						//환불 계좌 이름을 hidden 에  넣어준다
						var str_accountName = $retrunOrderForm.find('select[name="accountCode"] option:selected').text();
											  $retrunOrderForm.find('input[name="accountName"]').val(str_accountName);												
					}

					var action = $retrunOrderForm.attr('action');
					var param = Method.getFormDataByValid() + '&calculateOnly=false';
					// 부분 반품이 가능 하다면
					/*
					if( isAblePartialVoid ){
						// 부분 반품이 가능한 상황이지만 모든 상품의 모든 수량을 반품 할때는 entireReturn 값을 넘겨준다.
						if( !Method.getIsAvailablePartialReturn()){
							param += '&entireReturn=true';
						}
					}else{
						param += '&entireReturn=true';
					}
					*/

					console.log( sandbox.utils.getQueryParams(param));

					sandbox.utils.ajax(action, 'POST', param, function(data){
						var data = $.parseJSON( data.responseText );
						if(!data.result || !_.isEmpty(data.message)){
							UIkit.modal.alert(data.message).on("hide.uk.modal", function(){
								sandbox.setLoadingBarState(true);
								location.reload();
							});
						}else{
							UIkit.modal.alert("반품이 정상적으로 처리 되었습니다.").on("hide.uk.modal", function(){
								sandbox.setLoadingBarState(true);
								location.href=$("#fulfill-mobile-menu #return-condition").attr('href');
							});
						}
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-return-detail]',
					attrName:'data-module-return-detail',
					moduleName:'module_return_detail',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});

})(Core);
