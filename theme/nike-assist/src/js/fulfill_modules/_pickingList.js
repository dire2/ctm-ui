(function(Core){
	Core.register('module_picking_list', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				$this.find("[data-excel-btn]").on('click', Method.downExcel);
			},

			downExcel:function(){
				var url = $(this).closest('#pickingList').find('a').attr('href') +'/download';

				UIkit.modal.confirm('동일한 아이템에 대해 수량을 합쳐서<br />다운로드 하시겠습니까?', function(){
					location.href = url += '?isConsolidateItem=true';
				}, function(){
					location.href = url += '?isConsolidateItem=false'
				}, 
				{
					labels: {'Ok': '예', 'Cancel': '아니오'}
				})
				
				//console.log( url );
				/*
				var param = $('input[name="processConfirmNumber"]').val();
				location.href = sandbox.utils.contextPath + '/fulfillment/pickingDetailListExcel?processConfirmNumber=' + param;
				*/
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-picking-list]',
					attrName:'data-module-picking-list',
					moduleName:'module_picking_list',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);