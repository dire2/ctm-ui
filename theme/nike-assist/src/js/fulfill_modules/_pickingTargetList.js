(function(Core){
	Core.register('module_picking_target_list', function(sandbox){
		var totalCount=0;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;

				modal = UIkit.modal('#common-modal', {center:true});
				totalCount = $this.find('input[name="totalRecords"]').val();

				if (totalCount > 0) {
					$("footer").css('bottom', "0");
					$("footer").css('width', "100%");
					$('footer').css('position', "fixed");
				}

				$(this).on('click', '.list_check', function() {
					if ($(this).find("span").attr("class") == "webpos-ns-check") {
						$(this).removeClass("active");
						$(this).find("span").removeClass("webpos-ns-check");
						$(this).find("input[name='recordId']").attr('checked', false);
					} else {
						$(this).addClass("active");
						$(this).find("span").addClass("webpos-ns-check");
						$(this).find("input[name='recordId']").attr('checked', true);
					}
				});

				$this.find("a[id='pickingTargetCheck']").on("click", function(){ Method.targetCheck() });
			},
			targetCheck:function(){
				var ftiIds=[], conf, ftiId, url, param, selectedList;
				var selectedList = $('.fcontents_wrap').find(".webpos-ns-check");
				var checkedLength = selectedList.length;
				if (checkedLength == 0) {
					UIkit.modal.alert('선택된 항목이 없습니다.');
					return;
				} else {
					$.each( selectedList, function( index, data ){
						ftiIds[index] = 'ftiId=' + $(this).closest('.list_check').find('[name="recordId"]').val();
					})
					conf="총 " + checkedLength + " 건을 Picking List로 생성하시겠습니까?";
				}

				ftiId = ftiIds.join('&');
				UIkit.modal.confirm(conf,function(){
					var adminUserList = sandbox.getComponents('component_admin_userlist', {context:Method.$this});
					if( adminUserList ){
						var adminUserModal = UIkit.modal('#admin-user-modal', {center:true});
						if( adminUserList.isLoaded() ){
							adminUserModal.show();
						}else{
							// 이미 로드 되었다면 이벤트 중복 등록을 하지 않는다.
							// 담당자 리스트 생성
							adminUserList.create();
							// 생성 완료시 modal 창 오픈
							adminUserList.addEvent('createComplete', function(){
								adminUserModal.show();
							});
							// 담당자 선택시
							adminUserList.addEvent('selectAdminUser', function( adminUser ){
								adminUserModal.hide();
								// 담당지 정보가 정상이라면
								if( adminUser != null ){
									var assignMyOwn = false;
									if( adminUser == 'assignMyOwn'){
										adminUser = '';
										assignMyOwn = true ;
									}
									Method.submitTargetCheck( ftiId, adminUser, assignMyOwn );
								}else{
									UIkit.modal.alert('담당자 정보가 잘못 되었습니다. 다시 시도해 주세요.').on('hide.uk.modal', function(){
										location.reload();
									})
								}
							});
						}
					}else{
						UIkit.modal.alert('담당자 정보를 불러오지 못하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function(){
							location.reload();
						});
					}
				},
				function(){

				},{ modal : false });

			},
			submitTargetCheck:function( ftiId, adminUser, assignMyOwn ){
				var action = '/fms-action/create-picking-list';
				var param = ftiId + '&adminUserId='+adminUser+'&assignMyOwn='+assignMyOwn;
				sandbox.utils.ajax(action, 'GET', param, function(data){
					console.log(data);
					var data = $.parseJSON( data.responseText );
					var msg = "";
					if(!_.isEmpty(data.errors)){
						//TODO 에러 타입과 메시지가 있으니 노출 처리 해야함
						msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
						//UIkit.modal.alert("Picking 확인 처리를 정상적으로 완료하지 못했습니다. 다시 시도해주세요.");
					}else{
						msg = "정상적으로 처리되었습니다.";
					}

					UIkit.modal.alert(msg).on("hide.uk.modal", function(){
						sandbox.setLoadingBarState(true);
						location.reload();
					});

				});
			}

		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-picking-target-list]',
					attrName:'data-module-picking-target-list',
					moduleName:'module_picking_target_list',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);