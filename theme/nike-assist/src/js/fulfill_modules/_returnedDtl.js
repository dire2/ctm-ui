(function(Core){
	Core.register('module_returned_detail', function(sandbox){
		var orderId, queryParams;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				//sandbox.validation.init( $this.find('#order-success-form') );

				var $form = $(this).find('#paramForm');
				queryParams = sandbox.utils.getQueryParams($form.serialize());
                if( typeof(queryParams['item.productName'])=='string' ){
					queryParams['item.productName'] = [queryParams['item.productName']];
					queryParams['item.productModel'] = [queryParams['item.productModel']];
					queryParams['item.productOpt'] = [queryParams['item.productOpt']];
				}

				 console.log( queryParams );
				orderId = $('input[id="orderId"]').val();
				// 반품 취소
				$this.find('[data-return-cancel-btn]').on("click", Method.returnCancel);
				// 반품라벨 인쇄
				$this.find('[data-returnlabel-btn]').on("click", Method.setReturnLabel);
				// 팩킹라벨 인쇄
				$this.find('[data-packinglabel-btn]').on("click", Method.setPackingLabel);
				/*
					csrfToken: "YYI8-MV2T-EV16-T2Y6-LPWM-Z86J-W6OS-018P"
					fulfillmentGroupId: "5151"
					fulfillmentGroupItemIds: (2) ["5351", "5352"]
					productName: (2) ["SB+%EB%8D%A9%ED%81%AC+%ED%94%84%EB%A1%9C+%ED%95%98%EC%9D%B4", "SB+%EB%8D%A9%ED%81%AC+%ED%94%84%EB%A1%9C+%ED%95%98%EC%9D%B4"]
					productsize: (2) ["260", "250"]
					productstyle: (2) ["881758-400", "881758-400"]
					quantity: (2) ["1", "1"]
					returnnum: "201808132126421895051"
					submitDate: "2018-08-13+21%3A26%3A42.0"
				*/
				

			},
			fillZero:function(type, id, len ){
				var obj= '000000000000000'+id;
				return type + obj.substring(obj.length-len);
			},
			setReturnLabel : function() {
				var _this = this;
				var url = 'seamless://print=ok&mode=rlabel';
				/*
				- 반품 라벨(아이템 수만큼 반복 )
					returnnum  // 반품번호                          orderDto.orderNumber
					collectionname  // 반품지명                     없음
					returnshippbid  //반품배송 ID                   orderDto.orderGroup.fulfillmentGroupId
					returnshippprodbid  반품 상품 ID 				  orderDto.orderGroup.orderGroupItems.fulfillmentGroupItemId
					productName  // 상품명						  orderDto.orderGroup.orderGroupItems.name
					productstyle  //스타일						  orderDto.orderGroup.orderGroupItems.model
					productsize  // 사이즈						  orderDto.orderGroup.orderGroupItems.skuId -> brz:sku_option_display
				*/
				//var param = encodeURI($(_this).attr("data-param"));
				var param = '';
				var parse = '';
				var obj = {
					returnnum : '',
					collectionname : '',
					returnshippbid : '',
					returnshippprodbid : '',
					productName : '',
					productstyle : '',
					productsize : ''
				};
				$.each( queryParams['item.productName'], function(idx, data){
					parse = ( (queryParams['item.productName'].length-1) != idx ) ? '|' : '';
					
					obj.returnnum += queryParams['returnNumber'] + parse,
					obj.collectionname += decodeURIComponent(queryParams['returnLocationName']) + parse,
					obj.returnshippbid += Method.fillZero('D', queryParams['fulfillmentGroupId'], 10 ) + parse,
					obj.returnshippprodbid += Method.fillZero('P', queryParams['item.fulfillmentGroupItemId'][idx], 10 ) + parse,
					obj.productName += decodeURIComponent(queryParams['item.productName'][idx]) + parse,
					obj.productstyle += decodeURIComponent(queryParams['item.productModel'][idx]) + parse,
					obj.productsize += decodeURIComponent(queryParams['item.productOpt'][idx]) + parse
					
					param = '&' + $.param( obj );
					/*
					if( (queryParams.productName.length-1) != idx ){
					     param += "|";
					}
					*/
				})
				console.log(param.replace(/%2B/g, '%20').replace(/%26/gi, '%20') );
				location.href = url + param.replace(/%2B/g, '%20').replace(/%26/gi, '%20');

			},
			setPackingLabel : function() {
				var _this = this;
				var url = 'seamless://print=ok&mode=rplabel&labeltype=packing';

				/*
					- 팩킹 라벨(반복 없음) 반품번호
					returnnum //
					returndate //반품일시 						  orderDto.submitDate
					returnpackingname // 반품지명
					returnaddress 반품지 주소
					prodinfo //상품명
				*/

				//var param = encodeURI($(_this).attr("data-param"));

				var param = "";
				var parse = '';
				var obj = {
					returnnum : Method.fillZero('D', queryParams['fulfillmentGroupId'], 10 ),
					returndate : decodeURIComponent(queryParams['submitDate']),
					returnpackingname : decodeURIComponent(queryParams['returnLocationName']),
					returnaddress : decodeURIComponent(queryParams['returnAddress1']) + decodeURIComponent(queryParams['returnAddress2'])
				}
				//param += '&' + $.param( obj );
				var str = "";
				$.each( queryParams['item.productName'], function(idx, data){
					parse = ( (queryParams['item.productName'].length-1) != idx ) ? '|' : '';
					str += decodeURIComponent(queryParams['item.productName'][idx]) + '/' + decodeURIComponent(queryParams['item.productModel'][idx]) + '/' + decodeURIComponent(queryParams['item.productOpt'][idx]) + parse
				})
				obj.prodinfo = str;
				param = '&' + $.param( obj );
				console.log( param );

				location.href = url + param.replace(/%2B/g, '%20').replace(/%26/gi, '%20');
			},

			returnCancel:function(){
				var action = sandbox.utils.contextPath + '/return-action/cancel';
				UIkit.modal.confirm("반품을 취소 하시겠습니까?", function(){
					sandbox.utils.ajax( '/return-action/cancel/'+orderId, 'GET', '', function(data){
						var data = $.parseJSON( data.responseText );
						var msg = "";
						if(!_.isEmpty(data.message)){
							msg = data.message;
						}else{
							msg = "반품 취소가 정상적으로 처리 되었습니다.";
						}
						UIkit.modal.alert(msg).on("hide.uk.modal", function(){
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					});
				})
			}
		}


		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-returned-detail]',
					attrName:'data-module-returned-detail',
					moduleName:'module_returned_detail',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);