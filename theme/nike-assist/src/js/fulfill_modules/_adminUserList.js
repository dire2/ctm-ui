(function(Core){
	var AdminUserList = function(){
		'use strict';

		var _self, $this, args, isDirectShow, isLoaded=false, locationId='', currentUserId=null;
		var setting = {
			selector:'[data-component-admin-userlist]',
			selectBtn: '[data-select-btn]',
			vueContainer : '#vue-container',
			loadUrl : '/fms-action/locations-related-admin'
		}
		var addEvent = function(){
			$this.find(setting.selectBtn).off('click').on('click', function(e){
				e.preventDefault();
				var id = $(this).data('selectBtn');
				_self.fireEvent( 'selectAdminUser', this, [id] );
			});
		} 
		var createAdminUserList = function(){
			new Vue({
				el : '[data-component-admin-userlist] #vue-container',
				data : {
					activeClass : '',
					adminUsers : []
				},
				created(){
					var $this = this;
					Core.Utils.ajax(setting.loadUrl, 'GET', {locationId:locationId, role:'Athlete'}, function(data){
						var data = $.parseJSON( data.responseText );
						if( data.result == true ){
							var users =[];
							$.each( data.adminUsers, function( idx, data ){
								if( currentUserId != data.id ){
									users.push({
										id : data.id,
										userId : data.login,
										name : data.name,
										email : data.email,
									})
								}
							})
							$this.adminUsers = users;
							$this.activeClass = 'create';

							// 처음 로드 했을때만 ajax 통신해서 정보 가져옴 한번 세팅후에는 그댜로 사용
							isLoaded = true;
							_self.fireEvent( 'createComplete' );
						}
					});
					/*
					fetch(setting.loadUrl, {
						method: 'POST',
						body: 'locationId=""'
					})
					.then((response) => {
						console.log( response );
						if( response.ok ){
							console.log( response );
							return '[{"name" : "A"}, {"name" : "B"}, {"name" : "C"}]'
						}	
						throw new Error('Error');
					})
					.then((json) => {
						var data = JSON.parse(json);
						console.log( data );
						var users =[];
						$.each( data, function( idx, data ){
							users.push({
								name : data.name
							})
						})
						this.adminUsers = users;
						_self.fireEvent( 'createComplete' );
					})
					.catch((error) => {
						UIkit.modal.alert('담당자 정보를 불러오지 못하였습니다.<br/> 다시 시도해 주세요.').on('hide.uk.modal', function(){
							location.reload();
						});
					});
					*/
				},
				updated(){
					addEvent();
				}
			});
		}
		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				_self = this;
				args = arguments[0];
				$this = $(setting.selector);
				isDirectShow = ( args.isDirectShow ) ? args.isDirectShow : false;
				locationId = ( args.locationId ) ? args.locationId : '';
				currentUserId = ( args.adminUserId ) ? args.adminUserId : null;
				if( isDirectShow == true ){
					createAdminUserList();
				}
				return this;
			},
			create:function(){
				if( !isLoaded ){
					createAdminUserList();
				}else{
					_self.fireEvent( 'createComplete' );
				}
			},
			isLoaded:function(){
				return isLoaded;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_admin_userlist'] = {
		constructor:AdminUserList,
		attrName:'data-component-admin-userlist'
	}
})(Core);