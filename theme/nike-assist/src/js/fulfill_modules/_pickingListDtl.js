(function(Core){
	Core.register('module_picking_detail', function(sandbox){
		var $itemList, fpId, isConsolidateItem;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				//sandbox.validation.init( $this.find('#order-success-form') );

				modal = UIkit.modal('#common-modal', {center:true});

				fpId = $this.find('input[name="fpId"]').val();
				isConsolidateItem = $this.find('input[name="isConsolidateItem"]').val();
				$itemList = $this.find('.product_list > li');

				$this.find("a[id='quantityUpdate']").on("click", Method.updateQuantity );
				$this.find("a[id='pickingComplete']").on("click", Method.pickingComplete );
				$this.find("a[id='pickingRevert']").on("click", Method.pickingRevert );
				$this.find("[data-remove-cancelled-item-btn]").on("click", Method.removeCancelledItem )
				$this.find("[data-select-athlete-btn]").on("click", Method.selectAthlete );
				$this.find("[data-excel-btn]").on('click', Method.downExcel);

				/*
				$this.find('[data-picking-btn]').on("click", Method.setPicking);
				
				$this.find('[data-send-btn]').on("click", Method.sendMsg);
				$this.find("[data-fdk-btn]").on('click', function(e){
					console.log($(this).text());
					Method.testFdk($(this).text());
				});
				*/

				var qtyComponent = sandbox.getComponents('component_quantity', {context:$this}, function(){
					this.addEvent('change', function(val){
						console.log(val);
					});
				})
			},

			submitPicking:function( opt, params ){
				var action = opt.action;
				var sucMsg = opt.sucMsg;
				var func = opt.sucCallback || null;
				//var errorMsg = "";
				var param = {};
				/*
				switch(type){
					case 'removeCancelledItem':
						action = '/fms-action/remove-picking-item';
						sucMsg = '취소된 상품이 정상적으로 삭제 되었습니다.';
					break;
					case 'assignPickingManager':
						action = '/fms-action/assign-picking-manager';
						sucMsg = '담당자가 정상적으로 지정되었습니다.';
					break;
					case 'updateQuantity':
						action = '/fms-action/update-picked-quantity';
						sucMsg = 'Picking 수량 정상적으로 변경 되었습니다';
						//errorMsg = 'Picking 수량 변경을 정상적으로 완료하지 못했습니다. 다시 시도해주세요.';
					break;
					case 'pickingComplete':
						action = '/fms-action/complete-picking-list';
						sucMsg = 'Picking 완료 처리가 정상적으로 완료되었습니다.';
						//errorMsg = 'Picking 완료 처리를 정상적으로 완료하지 못했습니다. 다시 시도해주세요.';
					break;
					case 'pickingRevert':
						action = '/fms-action/init-picking-list';
						sucMsg = 'Picking 확인 취소 처리가 정상적으로 완료되었습니다.';
						//errorMsg = 'Picking 확인 취소 처리를 정상적으로 완료하지 못했습니다. 다시 시도해주세요.';
					break;
				}
				*/
				param = {
					fpId : fpId,
					id : fpId,
					isConsolidateItem : isConsolidateItem,
					pickedItems : Method.getPickupItemInfo()
				}

				param = $.extend( param, params );

				sandbox.utils.ajax(action, 'GET', param, function(data){
					var data = $.parseJSON( data.responseText );
					if(!_.isEmpty(data.errors)){
						var msg = "";
						if( !_.isEmpty(data.errors.message)){
							msg = data.errors.message;
						}else{
							msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
						}
						
						UIkit.modal.alert(msg).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						})
					}else{
						UIkit.modal.alert(sucMsg).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							if( func == null ){
								location.reload();
							}else{
								func.call();
							}
							/*
							if( type == 'updateQuantity' || type == 'assignPickingManager' ){
								location.reload();
							}else{
								location.href = sandbox.utils.contextPath + '/fms-picking-list';
							}
							*/
						});
					}
				});
			},
			selectAthlete:function(){
				var adminUserList = sandbox.getComponents('component_admin_userlist', {context:Method.$this});
				if( adminUserList ){
					var adminUserModal = UIkit.modal('#admin-user-modal', {center:true});
					if( adminUserList.isLoaded() ){
						adminUserModal.show();
					}else{
						// 이미 로드 되었다면 이벤트 중복 등록을 하지 않는다.
						// 담당자 리스트 생성
						adminUserList.create();
						// 생성 완료시 modal 창 오픈
						adminUserList.addEvent('createComplete', function(){
							adminUserModal.show();
						})
						// 담당자 선택시
						adminUserList.addEvent('selectAdminUser', function( adminUser ){
							adminUserModal.hide();

							// 담당지 정보가 정상이라면
							if( adminUser != null ){
								var assignMyOwn = false;
								if( adminUser == 'assignMyOwn'){
									adminUser = '';
									assignMyOwn = true;
								}
								Method.submitPicking({
									action : '/fms-action/assign-picking-manager',
									sucMsg : (!assignMyOwn && adminUser=="") ? '담당자 지정을 보류하였습니다.' : '담당자를 지정했습니다.'
								}, 
								{
									adminUserId:adminUser,
									assignMyOwn:assignMyOwn 
								});
							}else{
								UIkit.modal.alert('담당자 정보가 잘못 되었습니다. 다시 시도해 주세요.').on('hide.uk.modal', function(){
									location.reload();
								})
							}
						})
					}
				}else{
					UIkit.modal.alert('담당자 정보를 불러오지 못하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function(){
						location.reload();
					})
				}
			},
			removeCancelledItem:function(){
				UIkit.modal.confirm('취소된 상품을 List에서 제외하고<br />Picking을 진행 합니다. 진행 하시겠습니까?', function(){
					Method.submitPicking({
						action : '/fms-action/remove-picking-item',
						sucMsg : '취소된 상품이 정상적으로 삭제 되었습니다.',
						sucCallback : function(){
							location.href=$("#fulfill-mobile-menu #picking-list").attr('href');
						}
					});
				})
			},
			pickingRevert:function(){
				UIkit.modal.confirm("Picking List를 삭제하고<br/>'Picking 지시대기' 상태로 되돌립니다.", function(){
					Method.submitPicking({
						action : '/fms-action/init-picking-list',
						sucMsg : 'Picking 확인 취소 처리가 정상적으로 완료되었습니다.',
						sucCallback : function(){
							location.href=$("#fulfill-mobile-menu #picking-list").attr('href');
						}
					});
				})
			},
			updateQuantity:function(){
				UIkit.modal.confirm("Picking 수량을 '임시저장' 하시겠습니까?", function(){
					Method.submitPicking({
						action : '/fms-action/update-picked-quantity',
						sucMsg : '정상적으로 처리되었습니다.'
					});
				})
			},
			pickingComplete:function(){
				var msg = '';
				if( Method.getIsAllPicking() ){
					msg = "'Picking 완료' 처리하시겠습니까?";
				}else{
					msg = "Picking 수량이 지시수량 보다 부족한 상품이 있습니다.<br/>'Picking 완료' 처리하시겠습니까?";
				}
				UIkit.modal.confirm(msg, function(){
					Method.submitPicking({
						action : '/fms-action/complete-picking-list',
						sucMsg : '정상적으로 처리되었습니다.',
						sucCallback : function(){
							location.href=$("#fulfill-mobile-menu #picking-list").attr('href');
						}
					});
				});
			},
			getIsAllPicking:function(){
				var isAll = true;
				$.each( $itemList, function( idx, data ){
					var quantity = Number($(this).find('[name="quantity"]').val());
					var pickedQuantity = Number($(this).find('[name="pickedQuantity"]').val());
					if( quantity != pickedQuantity ){
						isAll = false;
						return false;
					}
				});
				return isAll;
			},
			getPickupItemInfo:function(){
				var items = {};
				$.each( $itemList, function( idx, data ){
					var key = $(this).find('[name="pickingItemXrefId"]').val();
					var value = Number($(this).find('[name="pickedQuantity"]').val());
					items[key] = value;
				})
				return items;
			},

			downExcel:function(){
				UIkit.modal.alert('개발 진행 중 입니다.');
				return;
				var param = $('input[name="processConfirmNumber"]').val();
				location.href = sandbox.utils.contextPath + '/fulfillment/pickingDetailListExcel?processConfirmNumber=' + param;
			},
			testFdk:function(tp){
				var trNo = $('input[name=trNo]').val();
				var amount = $('input[name=amount]').val();
				var authoNo = $('input[name=authoNo]').val();
				var authoDate = $('input[name=authoDate]').val();
				var action = sandbox.utils.contextPath + '/fdk/AjaxTestFdk';
				console.log(trNo);
				var param = {type:tp};
				if(tp == 'before'){
					param = {type:tp, trNo:trNo, amount:amount, authoNo:authoNo, authoDate:authoDate};
				}
				sandbox.utils.ajax(action, 'GET', param, function(data){
					console.log(data.responseText);
					var data = $.parseJSON( data.responseText );
					//console.log(data.success.message);
				});
			},
			sendMsg:function(){
				var orderNumber = $('input[name=ordNo]').val();
				var action = sandbox.utils.contextPath + '/fulfillment/sendMessageTest';
				console.log(orderNumber);
				param = {orderNumber:orderNumber};
				sandbox.utils.ajax(action, 'GET', param, function(data){
					console.log(data.responseText);
					var data = $.parseJSON( data.responseText );
					//console.log(data.success.message);
				});
			}
		}


		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-picking-detail]',
					attrName:'data-module-picking-detail',
					moduleName:'module_picking_detail',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);