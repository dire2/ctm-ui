(function(Core){
	Core.register('module_returned_list', function(sandbox){
		var Method = {
			orderId:null,
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				orderId = $('input[name="orderId"]').val();
				$('#order-list').on('click', 'li > a', Method.detailViewConfirm );
			},
			detailViewConfirm:function(e){
				e.preventDefault();
				var $form = $(this).find('#paramForm');
				var params = sandbox.utils.getQueryParams($form.serialize());
				var url = $(this).attr('href');
				var phoneNumber = params.phoneNumber;
				var orderNumber = params.returnNumber;
				var userName = decodeURIComponent(params.userName);
				if( _.isEmpty(phoneNumber) || _.isEmpty(orderNumber) || _.isEmpty(userName) ){
					UIkit.modal.alert('주문 정보가 올바르지 않습니다.<br/>확인 후 다시 시도해 주세요.').on('hide.uk.modal', function() {
						location.reload();
					});
					return;
				}

				UIkit.modal.confirm("'주문내역상세'를 조회하시려면 본인인증이 필요합니다.<br/>진행하시겠습니까?", function(){
					var otpModal = UIkit.modal('#popup-layer-order-custom');
					otpModal.show();
					var otp = sandbox.getModule('module_authenticate');
					otp.reset({
						isDirectAuth : true,
						customText : '<h6 style="margin:15px 0px;">이름 : '+ userName + '</h6><h6 style="margin:15px 0px;">연락처 : ' + phoneNumber + '</h6>',
						authData : {
							orderNumber : orderNumber,
							messageType : 'KAKAO'
						}
					});
					otp.success(function(){
						location.href = url;
					}).fail( function(){
						UIkit.modal.alert('본인 인증에 실패 하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function() {
							location.reload();
						});
					})
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-returned-list]',
					attrName:'data-module-returned-list',
					moduleName:'module_returned_list',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});

})(Core);
