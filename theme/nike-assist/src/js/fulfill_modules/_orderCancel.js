// 사용안함
(function(Core){
	Core.register('module_order_cancel', function(sandbox){
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $form = $("form[id='main-search-form']");
				var $searchInput = $("input[id=main-search]");
				var $searchLabel = $("label[for='search']");
				var $searchBtn = $("button[id=main-search-btn]");

				// WebPOS 메인 페이지에 검색어 입력 값이 없으면 검색 불가.
				$searchInput.keydown(function() {
					if (event.keyCode == 13) {
						event.preventDefault();
						if( $.trim($searchInput.val()) != '' ){
							var name = $("#main_search_type option:selected").val();
							$searchInput.attr('name', name );
							$form.submit();
						}else{
							UIkit.modal.alert('검색어를 입력하세요.').on('hide.uk.modal', function(){
								$searchLabel.trigger('click');
							});
						}
					}
				});

				// WebPOS 메인 페이지에서 검색어 입력이 안된상태에서는 검색버튼 비활성화.
				if ($searchInput.val() == "") {
					$searchBtn.prop("disabled", true);
				}

				// WebPOS 메인 페이지의 검색어 입력이되면 검색 버튼 활성화.
				$searchInput.on('change', function(){
					if ($searchInput.val() == "") {
						$searchBtn.prop("disabled", true);
					} else {
						$searchBtn.prop("disabled", false);
					}
				});

				// WebPOS 메인 페이지 검색 포커스 제어 (포커스 활성화).
				$searchLabel.click(function(event) {
					$("#mainSearch").attr('class','input-textfield width-max focus');
					$searchInput.focus();
				});

				$searchInput.focusin(function(event) {
					$searchInput.val('');
				});

				// WebPOS 메인 페이지 검색 포커스 제어 (포커스 비활성화).
				$searchInput.focusout(function(event) {
					if( $.trim($searchInput.val()) == ''){
						$("#mainSearch").attr('class','input-textfield width-max');
						$("body").focus();
					}
				});

				var query = sandbox.utils.getQueryParams(location.href);

				if( query.searchType != null ){
					if( query[query.searchType] ){
						$searchLabel.trigger('click');
						$searchInput.val(decodeURIComponent(query[query.searchType]).split("+").join(" "));
					}
				}

				$('[data-cancel-btn]').on('click', function(e){
					e.preventDefault();

					var queryParams = sandbox.utils.getQueryParams($(this).closest('form').serialize());

					UIkit.modal.confirm('주문 상세 내역은 본인 인증 후 확인 가능합니다.<br/>' + queryParams.phoneNumber + ' 로 본인인증 진행 하시겠습니까?', function(){
						var otpModal = UIkit.modal('#popup-layer-order-custom');
						otpModal.show();

						var modalInput = $(otpModal.dialog).find('input[name="identifier"]');
						modalInput.focus();
						modalInput.val( queryParams.phoneNumber );
						modalInput.closest('.input-textfield').addClass('disabled');

						var otp = sandbox.getModule('module_authenticate');
						otp.success(function(){
							alert('success');
							location.href = '/walk-through-as-customer?siteId='+ queryParams.siteId +'&customerId='+ queryParams.customerId +'&targetUrl=/account/order/cancel/' + queryParams.orderId ;
						}).fail(function(){
							UIkit.modal.alert('본인 인증에 실패 하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function() {
								location.reload();
							});
						});
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-cancel]',
					attrName:'data-module-order-cancel',
					moduleName:'module_order_cancel',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);