(function(Core){
	Core.register('module_fulfillmobilegnb', function(sandbox){
		var modal = UIkit.modal('#common-modal', {center:true});
		var param = {};

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0];
				var clickIS = false;
				var isSearch = false;

				if( $("body").attr("data-device")==="pc") {
					$("#mobile-menu").removeClass("uk-offcanvas");
					$(".btn-slide-close").remove();
				 }

				var $mobile = $('#mobile-menu');
				$mobile.find('.mobile-onedepth_list, .mobile-twodepth_list').on('click', '> a', function(e){ // webPos 수정 .mobile-twodepth_list 추가
					if(!$(this).hasClass('link')){
						e.preventDefault();
						$(this).siblings().show().stop().animate({'left':0}, 300);
					}
				});

				$mobile.find('.location .icon-arrow_left').on('click', function(e){ // webPos 수정 .icon-arrow_left 추가
					e.preventDefault();
					$(this).parent().parent().stop().animate({'left':-270}, 300, function(){
						$(this).css('left', 270).hide();
					});
				});

				$mobile.on('show.uk.offcanvas', function(event, area){
					try {
						Core.ga.pv('pageview', '/mobileMenu');
					} catch (error) { }
				});

				$mobile.on('hide.uk.offcanvas', function(event, area){
					if(isSearch){
						sandbox.getModule('module_search').searchTrigger();
					}
					isSearch = false;
				});

				$mobile.find('.mobile-lnb-search').on('click', function(e){
					e.preventDefault();
					isSearch = true;
					UIkit.offcanvas.hide();
				});

				$('#fulfill-mobile-menu').find(".btn-slide-close").click(function(){
					UIkit.offcanvas.hide();
				});

				$('#fulfill-mobile-menu').find(".btn-setting").click(function(){
					UIkit.offcanvas.hide();
				});

				//  @ 이영선
				//	@ 모든 화면에서 무조건 호출되어 기본 값 받아오는 스크립트 제거
				/*
				$(document).ready(function(){
					var url = sandbox.utils.contextPath + '/fulfillment/summary';
					sandbox.utils.ajax(url, 'GET', {}, function(data){
					var result = JSON.parse(data.responseText)
					$('.bopis_noti').text(result.PHYSICAL_PICKUP_SUBMITTED + result.PHYSICAL_REQUEST_PROCESSING + result.PHYSICAL_PICKUP_PACKED);	// BOPIS 출고카운트

						if($('.d_board_list').length>0){
							var PHYSICAL_SHIP_SUBMITTED = result.PHYSICAL_SHIP_SUBMITTED;					// 일반출고 결제완료
							var PHYSICAL_SHIP_PICKED = result.PHYSICAL_SHIP_PICKED;							// 일반 picking완료
							var PHYSICAL_SHIP_PACKED = result.PHYSICAL_SHIP_PACKED;							// 일반 상품준비완료
							var PHYSICAL_PICKUP_SUBMITTED = result.PHYSICAL_PICKUP_SUBMITTED;				// BOPIS 결제완료
							var PHYSICAL_REQUEST_PROCESSING = result.PHYSICAL_REQUEST_PROCESSING;			// BOPIS 주문확인
							var PHYSICAL_PICKUP_PACKED = result.PHYSICAL_PICKUP_PACKED;						// BOPIS 상품준비완료
							var RMA = result.RMA;															// 회수관리

							$('.orderList').find('.orderSummary').text(PHYSICAL_SHIP_SUBMITTED);
							$('.delivery').find('.deliverySummary').text(PHYSICAL_SHIP_PICKED);
							$('.productReady').find('.productSummary').text(PHYSICAL_SHIP_PACKED);
							$('.bopisOrder').find('.bopisOrderSummary').text(PHYSICAL_PICKUP_SUBMITTED);
							$('.bopisOrderConfirm').find('.bopisOrderConfirmSummary').text(PHYSICAL_REQUEST_PROCESSING);
							$('.bopisDelivery').find('.bopisDeliverySummary').text(PHYSICAL_PICKUP_PACKED);
							$('.recall').find('.recallSummary').text(RMA);
						}
					});
				});
				*/
				// var url = sandbox.utils.contextPath + '/fulfillment/summary/bopis';

				// sandbox.utils.ajax(url, 'GET', {}, function(data){
				// 	var result = JSON.parse(data.responseText)
				// 	$('.bopis_noti').text(result.cnt);
				// });

				// fulfillment 모바일 메뉴 ( 1depth -> 2depth)
				$('.fulfill-mobile-onedepth_list').on('click', '> a', function(e){
					e.preventDefault();
					$(this).siblings().show().stop().animate({'left':0}, 300);
				});

				// fulfillment 모바일 메뉴 ( 2depth -> 1depth)
				$('.fulfill-location').on('click', function(e){
					e.preventDefault();
					$(this).parent().stop().animate({'left':-270}, 300, function(){
						$(this).css('left', 270).hide();
					});
				});

				// WebPOS 모바일 메뉴 활성화시 onedepth 메뉴로 보이기 위한 하위 메뉴 스타일 처리.
				$(".header-menu_mobile").click(function(){
					$(".mobile-menu_twodepth").css("left", "270px");
					$(".mobile-menu_twodepth").css("display", "none");
					$(".mobile-menu_threedepth").css("left", "270px");
					$(".mobile-menu_threedepth").css("display", "none");

					$(".user-state").show();
					$(".mobile-menu_onedepth li").show();
				});

				$('.btn-pos_notice').on('click', function(){
					param = {};
					Method.callNotice();
				});

				$('.btn-pos_manual').on('click', function(){
					param = {};
					Method.callmanual();
				});

				$('.uk-modal').delegate(".list-link", 'click', function() {
					param['pagetype']  = "view";
					param['storageId'] = $(this).find('#list-id').val();

					Method.callNotice();
				});

				$('.uk-modal').delegate(".btn-prev", 'click', function() {
					param = {};
					Method.callNotice();
				});

				$('.uk-modal').delegate("#pre-list-link", 'click', function() {
					param = {};
					Method.callNotice();
				});

				$('.uk-modal').delegate(".uk-modal-close", 'click', function() {
					$('#common-modal').find('.contents').empty();
				});

				$('.uk-modal').delegate(".list-link-manual", 'click', function() {
					param['pagetype']  = "view";
					param['storageId'] = $(this).find('#list-id-manual').val();

					Method.callmanual();
				});

				$('.uk-modal').delegate(".btn-prev-manual", 'click', function() {
					param = {};
					Method.callmanual();
				});

				$('.uk-modal').delegate("#pre-list-link-manual", 'click', function() {
					param = {};
					Method.callmanual();
				});

				$('.uk-modal').delegate(".uk-modal-close", 'click', function() {
					$('#common-modal').find('.contents').empty();
				});


			},
			callNotice:function(){
				sandbox.setLoadingBarState(true);
				sandbox.utils.walkThrough('site', '/notice');
				/*
				var pcFilter = "win16|win32|win64|mac|macintel";

				if(navigator.platform) {
					if(pcFilter.indexOf(navigator.platform.toLowerCase()) < 0) {
						// mobile
						$('.btn-slide-close').trigger('click');
					}
				}

				var url = sandbox.utils.contextPath + '/fulfillment/cscenter/notice';

				sandbox.utils.promise({
					url:url,
					type:'GET',
					data:param
				}).then(function(data){
					var defer = $.Deferred();
					var appendTxt = $(data).find('.content-area').html();
					$('#common-modal').find('.contents').empty().append(appendTxt);
					sandbox.moduleEventInjection(appendTxt, defer);
					modal.show();
					return defer.promise();
				}).fail(function(errorMsg){
					UIkit.modal.alert(errorMsg);
				});
				*/
			},

			callmanual:function(){
				sandbox.setLoadingBarState(true);
				sandbox.utils.walkThrough('site', '/guide');
				/*
				var pcFilter = "win16|win32|win64|mac|macintel";

				if(navigator.platform) {
					if(pcFilter.indexOf(navigator.platform.toLowerCase()) < 0) {
						// mobile
						$('.btn-slide-close').trigger('click');
					}
				}

				var url = sandbox.utils.contextPath + '/fulfillment/cscenter/ctmManual';

				sandbox.utils.promise({
					url:url,
					type:'GET',
					data:param
				}).then(function(data){
					var defer = $.Deferred();
					var appendTxt = $(data).find('.content-area').html();
					$('#common-modal').find('.contents').empty().append(appendTxt);
					sandbox.moduleEventInjection(appendTxt, defer);
					modal.show();
					return defer.promise();
				}).fail(function(errorMsg){
					UIkit.modal.alert(errorMsg);
				});
				*/
			}

		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-fulfillmobilegnb]',
					attrName:'data-module-fulfillmobilegnb',
					moduleName:'module_fulfillmobilegnb',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
