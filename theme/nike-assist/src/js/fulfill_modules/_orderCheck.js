(function(Core){
	Core.register('module_ordercheck', function(sandbox){
		var totalCount=0;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;

				modal = UIkit.modal('#common-modal', {center:true});
				totalCount = $this.find('input[name="totalRecords"]').val();

				if (totalCount > 0) {
					$("footer").css('bottom', "0");
					$("footer").css('width', "100%");
					$('footer').css('position', "fixed");
				}

				// BOPIS 주문, 일반출고주문 리스트 체크 활성 / 비활성
				$(this).on('click', '.list_check', function() {
					var sameDayTemp = $(this).find("input[name=recordFlag]").val();
					var sameDayChkCnt = $("input:hidden[name=recordId]:checked").parent("a").find("input[value='sameDay']").length;
					var nonSameDayChkCnt = $("input:hidden[name=recordId]:checked").parent("a").find("input[value='nonSameDay']").length;
					if(sameDayTemp == 'sameDay' && nonSameDayChkCnt > 0){
						//alert("please only sameDay check.");
						return;
					}else if(sameDayTemp == 'nonSameDay' && sameDayChkCnt > 0){
						//alert("please only sameDay check.");
						return;
					}

					if ($(this).find("span").attr("class") == "webpos-ns-check") {
						$(this).removeClass("active");
						$(this).find("span").removeClass("webpos-ns-check");
						$(this).find("input[name='recordId']").attr('checked', false);
					} else {
						$(this).addClass("active");
						$(this).find("span").addClass("webpos-ns-check");
						$(this).find("input[name='recordId']").attr('checked', true);
					}
				});

				$this.find("a[id='orderCheck']").on("click", function(){ Method.orderCheck( false, 'order' ) });					
				$this.find("a[id='sameDayCheck']").on("click", function(){ Method.orderCheck( true, 'sameDayOrder' ) });
				$this.find("a[id='shipCheck']").on("click", function(){ Method.orderCheck( true , 'shipOrder') });
				
			},
			orderCheck:function(isPickupAll, isPtype){

				// BOPIS, 주문확인 페이지 '주문확인' 버튼
				var fgIds=[], conf, fgId, url, param, list, selectedList, isAutoPickupList=false, checkedLength, msg;

				//주문제한 변수 추가
				var checkedLengthLimit = 20;
					/*
					if(typeof _GLOBAL.ORDER_CONFIG !== undefined){
						if(typeof _GLOBAL.ORDER_CONFIG.ORDER_CHECK_LIMIT !== undefined){
							checkedLengthLimit = Number(_GLOBAL.ORDER_CONFIG.ORDER_CHECK_LIMIT);
						}
					}
					 */
				selectedList = $('.fcontents_wrap').find(".webpos-ns-check");
				list = $('.fcontents_wrap').find('#order-list>li')
				var sameDayCnt = $("input:hidden[name=recordFlag]").parent("a").find("input[value='sameDay']").length;
				var nonSameDayCnt = $("input:hidden[name=recordFlag]").parent("a").find("input[value='nonSameDay']").length;
				
				if( isPickupAll ){
					if(isPtype == 'sameDayOrder') {
						checkedLength = sameDayCnt;
					} else if(isPtype == 'shipOrder') {
						checkedLength = nonSameDayCnt;
					} else {
					   checkedLength = list.length;
					}
				} else {
					checkedLength = selectedList.length;
				}
				if( checkedLength > checkedLengthLimit ){
					UIkit.modal.alert('최대 ' + checkedLengthLimit + ' 건 까지 가능합니다. <br/> ' + checkedLengthLimit + ' 건 이하로 주문을 선택 후 시도해 주세요.');
					return;
				}
				if (checkedLength == 0) {
					UIkit.modal.alert('선택된 항목이 없습니다.');
					return;
				}
				var sameDayChkCnt = $("input:hidden[name=recordId]:checked").parent("a").find("input[value='sameDay']").length;
				var nonSameDayChkCnt = $("input:hidden[name=recordId]:checked").parent("a").find("input[value='nonSameDay']").length;
				if(isPtype == 'shipOrder' && isPickupAll || isPtype == 'order' && nonSameDayChkCnt > 0 && sameDayChkCnt == 0 ) {
					msg ="일반택배 주문 총 " + checkedLength + " 건을 '주문확인' 처리하시겠습니까?";
				} else if(isPtype == 'sameDayOrder' && isPickupAll || isPtype == 'order' && sameDayChkCnt > 0 && nonSameDayChkCnt == 0) {
					msg ="오늘도착 주문 총 " + checkedLength + " 건을 '주문확인' 처리하시겠습니까?";
				} else if(isPtype == 'order' && sameDayChkCnt > 0 || nonSameDayChkCnt > 0) {
					//msg ="'오늘도착' or '일반택배' 주문 총 " + checkedLength + " 건을 '주문확인' 처리하시겠습니까?";
					return;
				}
								
				UIkit.modal.confirm(msg,
					function(){
						// 전체 주문 확인시
						if( isPickupAll ){
							if(isPtype == 'sameDayOrder') {
								$.each( list, function( index, data ){
									if($(this).find('input[name="recordFlag"]').val() == 'sameDay') {
										fgIds[index] = 'fgId=' + $(this).find('input[name="recordId"]').val();
									} 
								});
							} else {
								$.each( list, function( index, data ){
									if($(this).find('input[name="recordFlag"]').val() == 'nonSameDay') {
										fgIds[index] = 'fgId=' + $(this).find('input[name="recordId"]').val();
									}
								});
							}
						}else{
							$.each( selectedList, function( index, data ){
								fgIds[index] = 'fgId=' + $(this).closest('.list_check').find('input[name="recordId"]').val();
							})
						}						

						fgId = fgIds.join('&');

						// 자동 picking list 처리 여부 confirm
						UIkit.modal.confirm('다음 진행하실 업무를 선택해주세요',
							function(){
								// 자동 picking 리스트 사용 선택시
								isAutoPickupList = true;
							},{ labels: {'Ok': 'Picking List', 'Cancel': 'Picking 지시대기'}, modal : false }
						).on('hide.uk.modal', function(){
							// 자동 생성일때는 담당자 지정
							if( isAutoPickupList ){
								var adminUserList = sandbox.getComponents('component_admin_userlist', {context:Method.$this});
								if( adminUserList ){

									var adminUserModal = UIkit.modal('#admin-user-modal', {center:true});

									if( adminUserList.isLoaded() ){
										adminUserModal.show();
									}else{
										// 이미 로드 되었다면 이벤트 중복 등록을 하지 않는다.
										// 담당자 리스트 생성
										adminUserList.create();
										// 생성 완료시 modal 창 오픈
										adminUserList.addEvent('createComplete', function(){
											adminUserModal.show();
										})
										// 담당자 선택시
										adminUserList.addEvent('selectAdminUser', function( adminUser ){
											adminUserModal.hide();
											// 담당지 정보가 정상이라면
											if( adminUser != null ){
												var assignMyOwn = false;
												if( adminUser == 'assignMyOwn'){
													adminUser = '';
													assignMyOwn = true ;
												}
												Method.submitOrderCheck( fgId, isAutoPickupList, isPickupAll, adminUser, assignMyOwn );
											}else{
												UIkit.modal.alert('담당자 정보가 잘못 되었습니다. 다시 시도해 주세요.').on('hide.uk.modal', function(){
													location.reload();
												})
											}
										})
									}
								}else{
									UIkit.modal.alert('담당자 정보를 불러오지 못하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function(){
										location.reload();
									})
								}
							}else{
								// 자동 생성이 아닐때
								Method.submitOrderCheck( fgId, isAutoPickupList, isPickupAll, "", false );
							}
						});
					},
					function(){

					},{ modal : false }
				);

			},

			submitOrderCheck:function( fgId, isAutoPickupList, isPickupAll, adminUser, assignMyOwn ){
				var action = '/fms-action/order-confirm';
				var param = fgId + '&isAutoPickupList='+isAutoPickupList+'&isAll='+isPickupAll+'&adminUserId='+adminUser+'&assignMyOwn='+assignMyOwn;

				sandbox.utils.ajax(action, 'GET', param, function(data){
					var data = $.parseJSON( data.responseText );

					var msg = "";
					if(!_.isEmpty(data.errors)){
						msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
					}else{
						msg = "정상적으로 처리되었습니다.";
					}

					UIkit.modal.alert(msg, { labels: { 'Ok': '확인'}, modal : false }).on("hide.uk.modal", function(){
						sandbox.setLoadingBarState(true);
						location.reload();
					});

				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-ordercheck]',
					attrName:'data-module-ordercheck',
					moduleName:'module_ordercheck',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);