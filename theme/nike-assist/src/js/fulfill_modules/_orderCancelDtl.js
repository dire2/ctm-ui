// 당매장 주문 관리 메뉴
(function(Core){
	Core.register('module_order_cancel_detail', function(sandbox){
		var $this, isSelectAddress, isAblePartial, isMid, deliverySearch;
		var Method = {
			moduleInit:function(){
				$this = $(this);
				var args = arguments[0] || {};
				$this.find('[data-cancel-submit-btn]').on('click', Method.orderCancel);
				$this.find('[data-receipt-btn]').on('click', Method.printReceipt);

				isAblePartial = $("#isAblePartial").val();
				isMid = $("#isMid").val();

				// 배송지 변경 적용 버튼
				$this.find('[data-changeshippinginfo-btn]').on("click", Method.setChangeShippingInfo);

				$this.find('[data-closechangeshippinginfo-btn]').on("click", Method.setCloseChangeShippingInfo);
				$this.find('[data-upperclosechangeshippinginfo-btn]').on("click", Method.setCloseChangeShippingInfo);

				//postcode-search daum 우편번호 API
				//var addressModal = UIkit.modal("#popup-daum-postcode", {modal: false});
				//var element_wrap = document.getElementById('daum-postcode-container');

				var $zipCodeInput = $(this).find('[name="address.postalCode"]');
				var $zipCodeDisplay = $(this).find('[data-postalCode]');

				deliverySearch = sandbox.getComponents('component_searchfield', {context:$this, selector:'.search-field', resultTemplate:"#address-find-list"}, function(){
					// 검색된 내용 선택시 zipcode 처리
					this.addEvent('resultSelect', function(data){
						var zipcode = $(data).data('zip-code5');
						var city = $(data).data('city');
						var doro = $(data).data('doro');

						var $input = this.getInputComponent().setValue( city + doro );

						$zipCodeInput.val( zipcode );
						$zipCodeDisplay.text( zipcode );
						$zipCodeDisplay.parent().removeClass("uk-hidden");
					});
				});


				/*
				$this.find('.btn_search, #addressline1').on('click', function(e){
					e.preventDefault();
					daum.postcode.load(function(){
						addressModal.show();
						new daum.Postcode({
							oncomplete: function(data) {
								var zipcode=  data.zonecode;
								var doro = data.address;

								$this.find('#addressline1').val(doro);
								$this.find('#addressline2').focus();

								$zipCodeInput.val( zipcode );
								$zipCodeDisplay.text( zipcode );
								$zipCodeDisplay.parent().removeClass("uk-hidden");
								isSelectAddress = true;

								//postcode 모달창을 닫아주고 addressline2로 포커스이동
								addressModal.hide();
								$this.find('#addressline2').focus();
							},
							// 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
							onresize : function(size) {
								element_wrap.style.height = $(window).height() - 46 + 'px'; //size.height+'px';
							},
							width : '100%'
						}).embed(element_wrap);
					});
				});
				*/
			},
			orderCancel:function(e){
				e.preventDefault();
				//var currentLocation = location.href;
				//var queryParams = sandbox.utils.getQueryParams(currentLocation);
				/*
				var siteIds = Core.Utils.strToJson($('[data-info-mapped-sites]').val());
				var siteId = '';
				if(_.isArray(siteIds) == false || siteIds.length == 0){
					Core.ui.modal.alert('관할 사이트 설정이 잘못 되었습니다.<br/>관리자에게 문의하세요');
					return;
				}else{
					siteId = siteIds[0];
				}
				*/
				var siteId = sandbox.sessionHistory.getHistory('siteId');
				var customerId = sandbox.sessionHistory.getHistory('customerId');
				var orderId = sandbox.sessionHistory.getHistory('orderId');
				var url = '/walk-through-as-customer?siteId='+ siteId +'&customerId='+ customerId +'&targetUrl=/account/order/cancel/' + orderId;
				// 부분 반품이 불가능 하거나 매입전이면 무조건 전체 취소만 가능
				if( isAblePartial != 'true' || isMid == 'false'){
					url += "&toApp=true"
				}
				url += '&callbackUrl=' + location.origin + location.pathname; //currentLocation.replace(/&/g, '%7C').replace(/=/g, '%3D');
				location.replace( url );
			},
			setCloseChangeShippingInfo:function(e){
				e.preventDefault();
				UIkit.modal.confirm('배송정보 변경을 취소 하시겠습니까?', function(){
					$('.uk-modal-close').click();
					sandbox.setLoadingBarState(true);
					location.reload();
				});
			},
			setChangeShippingInfo:function(e){
				e.preventDefault();
				/*
				var action = sandbox.utils.contextPath + '/fulfillment/updateAddress';
				var addressId = $("#addressId").val();
				var fullName = $("#fullName").val();
				var city = $("#city").val();
				var addressLine1 = $("#addressLine1").val();
				var addressLine2 = $("#addressLine2").val();
				var phoneNumber = $("#phoneNumber").val();
				var postalCode = $.trim($(".postalCodeValue").text());
				var param = {'addressId' : addressId, 'fullName' : fullName, 'city' : city, 'addressLine1' : addressLine1, 'addressLine2' : addressLine2, 'postalCode' : postalCode, 'phoneNumber' : phoneNumber};

				var msg="배송정보를 변경하시겠습니까?";

				if ($("#fullName").val() == "") {
					UIkit.modal.alert("받으시는분을 입력해주세요.");
					return;
				}
				if ($("#addressLine1").val() == "") {
					UIkit.modal.alert("주소를 입력해주세요.");
					return;
				}
				if ($("#addressLine2").val() == "") {
					UIkit.modal.alert("상세주소를 입력해주세요.");
					return;
				}
				if ($("#phoneNumber").val() == "") {
					UIkit.modal.alert("휴대폰 번호를 입력해주세요.");
					return;
				}

				*/

				var $form = $this.find('#paramForm');
				var queryParams = sandbox.utils.getQueryParams($form.serialize());

				sandbox.validation.init( $form );
				sandbox.validation.validate( $form );
				if(!sandbox.validation.isValid( $form )){
					return false;
				}

				if( !deliverySearch.getValidateChk() ){
					UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
					return false;
				}

				UIkit.modal.confirm("배송정보를 변경하시겠습니까?", function(){
					sandbox.utils.ajax('/order-action/modify-shipping-address', 'POST', $form.serialize(), function(data){
						var data=$.parseJSON(data.responseText);
						var msg = "";
						if(!_.isEmpty(data.errors)){
							msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
						}else{
							msg = "정상적으로 처리되었습니다.";
						}

						UIkit.modal.alert(msg).on("hide.uk.modal", function(){
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}, true)
				});
			},
			printReceipt:function(){
				//
				var serializeData = $('#payment-form').serialize()+'&'+$('#returnOrderForm').serialize();
				var formData = serializeData.replace(/\+/g, '%20');
				var queryParams = sandbox.utils.getQueryParams(formData);
				var itemLen = $('[data-product-item]').length;
				var arrProductname = [];
				var arrProductcnt = [];
				var arrProductprice = [];
				var arrRetailPrice = [];
				var arrSaleprice = [];
				var arrSaleRate = [];
				var arrSelltype = [];
				var mobileurl = '';
				// 하나라도 취소가 안된 상품이 있는 상태이면 - pay, 전체 취소가 된 상태이면 - paycancel
				var mode = Method.getPrintTypeByOrderStatus(); // 주문 영수증 - pay, 취소영수증 - paycancel
				var sellType = ''; // 주문 영수증 - 정상, 취소 영수증일시 - 취소
				var price = 0;
				var retailPrice = 0;
				var quantity = 0;
				var totalQuantity = 0;
				var checkType = true;
				var payamount = queryParams.payamount;
				var mathSymbol = '';
				var saleRate = 0;
				var totalRetailPrice = 0;

				for(var i = 0 ; i < itemLen; i++){
					sellType = (queryParams.stat instanceof Array) ? queryParams.stat[i] : queryParams.stat;
					quantity = (queryParams.quantity instanceof Array) ? queryParams.quantity[i] : queryParams.quantity;
					totalQuantity += parseInt(quantity);
					price =  (queryParams.price instanceof Array) ? queryParams.price[i] : queryParams.price;
					retailPrice = (queryParams.price instanceof Array) ? queryParams.retailPrice[i] : queryParams.retailPrice;
					checkType = true;
					// 부분취소된 경우 cancelled 이외의 상품만 출력
					if( mode == 'cancel-partial'){
						mathSymbol = '-';
						if( sellType == 'CANCELLED' ){
							checkType = false;
						}else{
							payamount += ( price * quantity )
						}
					}else if( mode == 'cancel'){
						mathSymbol = '-';
					}

					if( checkType ){
						totalRetailPrice += parseInt(retailPrice) * quantity;
						arrProductname.push((queryParams.productName instanceof Array) ?
										queryParams.productName[i]+'%20'+queryParams.model[i]+'%20'+queryParams.opt[i] : queryParams.productName+'%20'+queryParams.model+'%20'+queryParams.opt);
						arrProductcnt.push(mathSymbol+quantity);
						arrProductprice.push(encodeURIComponent(mathSymbol + sandbox.utils.price(retailPrice)));
						arrRetailPrice.push(encodeURIComponent(sandbox.utils.price(retailPrice * quantity)));
						arrSaleprice.push(encodeURIComponent(mathSymbol+sandbox.utils.price(price * quantity)));
						arrSelltype.push(encodeURI((sellType=='CANCELLED') ? '취소' : '정상')); // 주문 영수증 - 정상, 취소 영수증일시 - 취소
						//totalcnt += (queryParams.quantity instanceof Array) ? (queryParams.quantity[i] * 1) : queryParams.quantity;

						saleRate = Math.floor((100-((parseInt(price)/parseInt(retailPrice))*100)));
						arrSaleRate.push(encodeURIComponent(((saleRate > 0) ? saleRate+'%':'')));
					}
				}

				if( mode == 'cancel-partial' || mode == 'cancel'){
					mode = 'paycancel';
				}
				var surtaxInt = Math.round(Number(payamount) / 11);
				var surtax = encodeURIComponent(mathSymbol + sandbox.utils.price(surtaxInt));
				var taxamount = encodeURIComponent(mathSymbol+sandbox.utils.price(Number(payamount)-surtaxInt));
				var transFormPayamount = encodeURIComponent(mathSymbol+sandbox.utils.price(payamount));
				var transFormRetailPayamount = encodeURIComponent(mathSymbol+sandbox.utils.price(totalRetailPrice));//mathSymbol 추가
				var discount = encodeURIComponent(sandbox.utils.price(totalRetailPrice - parseInt(queryParams.payamount) + parseInt(queryParams.totalFulfillmentCharges)));//배송비 제외 추가
				if(queryParams.totalFulfillmentCharges == '0'){//당일배송 배송비
					var transFormFulfillmentCharges = encodeURIComponent(sandbox.utils.price(queryParams.totalFulfillmentCharges));
				}else{
					var transFormFulfillmentCharges = encodeURIComponent(mathSymbol+sandbox.utils.price(queryParams.totalFulfillmentCharges));
				}
				
				var queryString =  "";
				queryString += "&orderno=" + ((itemLen > 1) ? queryParams.orderno[0] : queryParams.orderno);
				queryString += "&ceoname=" + queryParams.ceoname;
				queryString += "&storeaddress=" + queryParams.storeaddress;
				queryString += "&storeno=" + queryParams.storeno;
				queryString += "&storetel=" + queryParams.storetel;
				queryString += "&storename=" + queryParams.storename;
				queryString += "&paydatetime=" + queryParams.paydate.match(/[0-9]{2}/g).join('/') +'%20'+ queryParams.paytime.match(/[0-9]{2}/g).join(':');
				queryString += "&storeposno=" + queryParams.storeposno;
				queryString += "&productname=" + Method.getReplaceCode(arrProductname.join('|'));
				queryString += "&unitcost=" + arrProductprice.join('|');
				queryString += "&amount=" + arrProductcnt.join('|');
				queryString += "&sale=" + arrSaleRate.join('|');
				queryString += "&sellingprice=" + arrSaleprice.join('|');
				queryString += "&selltype=" + arrSelltype.join('|');
				queryString += "&issurtax=issurtax";
				queryString += "&customerid=" + (queryParams.customerid || '');
				queryString += "&taxamount=" + taxamount;
				queryString += "&surtax=" + surtax;
				queryString += "&servicecharge=servicecharge";
				queryString += "&totalsalesamount=" + transFormPayamount;
				queryString += "&discount=" + discount;
				queryString += "&sum=" + transFormRetailPayamount;
				queryString += "&fairymoney=" + transFormPayamount;
				queryString += "&shortchange=shortchange";
				queryString += "&cardno=" + ((queryParams.cardno.length === 16) ? queryParams.cardno.match(/[0-9]{4}/g).join('-') : queryParams.cardno);
				queryString += "&cardcompany=" + queryParams.cardcompany;
				queryString += "&month=" + queryParams.month;
				queryString += "&payno=" + queryParams.payno;
				queryString += "&payamount=" + transFormPayamount;
				queryString += "&totalprodcount=" + totalQuantity;
				queryString += "&mobileurl=" + mobileurl;
				queryString += "&totalFulfillmentCharges=" + transFormFulfillmentCharges;//당일배송 배송비
				
				console.log(sandbox.utils.getQueryParams(queryString));

				window.location.href = "seamless://print=ok&mode=" + mode +queryString;
			},
			getReplaceCode:function(stg){
				return stg.replace(/%26/gi, '%20');
			},
			getPrintTypeByOrderStatus:function(){
				// 상품의 상태값에 따른 프린트 타입 설정
				var itemLen = $('[data-product-item]').length;
				var cancelledItemLen = $('[data-product-item] > input[name="stat"].cancelled').length;

				// 전체 상품이 취소 된 상태이면 
				if( itemLen == cancelledItemLen ){
					return 'cancel';
				}

				// 하나도 취소 되지 않은 상태
				if( cancelledItemLen == 0){
					return 'pay';
				}
				
				// 부분 취소 된 상태
				return 'cancel-partial';
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-cancel-detail]',
					attrName:'data-module-order-cancel-detail',
					moduleName:'module_order_cancel_detail',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
