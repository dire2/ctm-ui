$(document).ready(function() {

	// 기간설정 클릭시 calender_wrap 노출
	 if($(".f-radiobox.order_search").length > 0) {
		var setting = $(".f-radiobox.order_search li > label");
		setting.click(function(){
		  	$setting = $(this);
		  	if ( $setting.hasClass("on") ){
		  		$setting.removeClass("on");
		 	} else {
		 		setting.removeClass("on");
		 		$setting.addClass("on");
		 	}
		});
	}

	// input text 입력시 label 감추기
	var placeholderTarget = $('.calender_box input[type="text"]');

	  //포커스시
	  placeholderTarget.on('focus', function(){
	    $(this).siblings('label').fadeOut('fast');
	});

	  //포커스아웃시
	placeholderTarget.on('focusout', function(){
	    if($(this).val() == ''){
	      $(this).siblings('label').fadeIn('fast');
	    }
	});

	// calender_wrap input 닫기버튼
	$('.del_btn_wrap').click(function() {
		$(this).prev('').prev('label').css("display","none");
		$(this).prev('input').val('').trigger('change').focus();
	});

	/* 바코드 2018-01-30 삭제
	$('.tab_menu_wrap a').click(function(){
		var $this = $(this);
		var viewCont = $this.attr('href');
		var tabCont = $('.tab-cont');
		var tabList = $this.parent('li');

		tabList.addClass('on').siblings('li').removeClass('on');
		tabCont.hide();
		$(viewCont).show();
	});*/

	var Method = {
		numberWithCommas:function(x){
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '원';
		}
	}

	// seamless 주문상세 레이어 - 주문취소 신청 클릭시 체크
	$(".order_cancel .ful_list_type .cancelPrdList a").click(function() {
		if ($(this).attr("value") != "order.status.payment.complete") {
			UIkit.modal.alert("주문취소 불가능한 상품입니다.");
		} else {
			if ($(this).hasClass("active")) {
				$(this).removeClass("active");
				var itemAmount = $(this).children('ul').children('li').children('div').children('p').eq(1).attr('value');
				var beforeItemQuantity = $(this).parent('li').children('dl').children('input').eq(0).attr('value');
				var itemQuantity = $(this).parent('li').children('dl').children('input').eq(1).attr('value');
				if ($(this).attr("value") == "order.status.payment.complete") {
					$("#"+$(this).parent('li').children('dl').children('dd').children('div').children('select').attr('id')).val(beforeItemQuantity);
					$("#"+$(this).parent('li').children('dl').children('input').eq(1).attr('id')).val("");
					var cancelAmount = $("#cancelAmount").val();
					if (cancelAmount == null || cancelAmount == "") {
						cancelAmount = 0;
					}
					$("#cancelAmount").val(parseInt(cancelAmount)-parseInt(itemAmount)*parseInt(itemQuantity));
					$("#cancelAmount").text(Method.numberWithCommas(parseInt(cancelAmount)-parseInt(itemAmount)*parseInt(itemQuantity)));
				}
			} else {
				$(this).addClass("active");
				var itemAmount = $(this).children('ul').children('li').children('div').children('p').eq(1).attr('value');
				var itemQuantity = $(this).parent('li').children('dl').children('input').eq(0).attr('value');
				if ($(this).attr("value") == "order.status.payment.complete") {
					$("#"+$(this).parent('li').children('dl').children('input').eq(1).attr('id')).val(itemQuantity);
					var cancelAmount = $("#cancelAmount").val();
					if ($("#cancelAmount").val() == null || $("#cancelAmount").val() == "") {
						cancelAmount = 0;
					}
					$("#cancelAmount").val(parseInt(cancelAmount)+parseInt(itemAmount)*parseInt(itemQuantity));
					$("#cancelAmount").text(Method.numberWithCommas(parseInt(cancelAmount)+parseInt(itemAmount)*parseInt(itemQuantity)));
				}
			}
		}
	});

	// seamless 주문상세 레이어 - 반품신청 리스트 클릭시 체크
	$(".order_cancel .ful_list_type .returnPrdList a").click(function() {
		if ($(this).attr("value") != "fulfillment.status.delivered") {
			UIkit.modal.alert("반품신청 불가능한 상품입니다.");
		} else {
			if ($(this).hasClass("active")) {
				$(this).removeClass("active");
				var itemAmount = $(this).children('ul').children('li').children('div').children('p').eq(1).attr('value');
				var beforeItemQuantity = $(this).parent('li').children('dl').children('input').eq(0).attr('value');
				var itemQuantity = $(this).parent('li').children('dl').children('input').eq(1).attr('value');
				if ($(this).attr("value") == "fulfillment.status.delivered") {
					$("#"+$(this).parent('li').children('dl').children('dd').children('div').children('select').attr('id')).val(beforeItemQuantity);
					$("#"+$(this).parent('li').children('dl').children('input').eq(1).attr('id')).val("");
					var returnAmount = $("#returnAmount").val();
					if (returnAmount == null || returnAmount == "") {
						returnAmount = 0;
					}
					$("#returnAmount").val(parseInt(returnAmount)-parseInt(itemAmount)*parseInt(itemQuantity));
					$("#returnAmount").text(Method.numberWithCommas(parseInt(returnAmount)-parseInt(itemAmount)*parseInt(itemQuantity)));
				}
			} else {
				// $(".ful_list_type li a").removeClass("active");
				$(this).addClass("active");
				var itemAmount = $(this).children('ul').children('li').children('div').children('p').eq(1).attr('value');
				var itemQuantity = $(this).parent('li').children('dl').children('input').eq(2).attr('value');
				if ($(this).attr("value") == "fulfillment.status.delivered") {
					$("#"+$(this).parent('li').children('dl').children('dd').children('div').children('select').attr('id')).val(itemQuantity);
					$("#"+$(this).parent('li').children('dl').children('input').eq(1).attr('id')).val(itemQuantity);
					var returnAmount = $("#returnAmount").val();
					if ($("#returnAmount").val() == null || $("#returnAmount").val() == "") {
						returnAmount = 0;
					}
					$("#returnAmount").val(parseInt(returnAmount)+parseInt(itemAmount)*parseInt(itemQuantity));
					$("#returnAmount").text(Method.numberWithCommas(parseInt(returnAmount)+parseInt(itemAmount)*parseInt(itemQuantity)));
				}
			}
		}
	});

	if($("#search_text").val()){
		$(".search-input").addClass("focus");
		$(".search-input input[name='searchText']").focus();
	}

	// 상세 검색 입력값이 있으면 클래스 추가
	if($(".search-input input[name='searchText']").val() != ""){
		$(".search-input").addClass("focus");
	}

	// 검색 창 클릭 시 아이콘 사라짐
	$(".search-input label[for='search']").click(function(){
		$(".search-input").addClass("focus");
		$(".search-input input[name='searchText']").focus();
	});

	// 검색창 포커스 사라질 때 입력값이 없을 때만 클래스 추가
	$(".search-input input[name='searchText']").focusout(function(){
		if($(this).val() == ""){
			$(".search-input").removeClass("focus");
		}
	});

	// 필터 고정 스크롤을 위한 높이값 추가
	$(document).on("click", ".btn-filter-open", function(){
        var windowHeight = $(window).height();
        
        $(".section-filter.fulfill").css("height", windowHeight + "px");
        $(".section-filter.fulfill .scroll-wrap").css("height", windowHeight + "px");
    });

    // PC 버전일 때 GNB 상시 활성화
    if (window.matchMedia("(min-width: 960px)").matches){
    	// $(".mobile-menu_wrap").addClass("uk-active");
    	// $(".uk-offcanvas-bar").addClass("uk-offcanvas-bar-show").css({
    	// 	"clip" : "rect(0px, 280px, 100vh, 0px)"
    	// });
    }

    // admin 에서 site 로 이동시, site 에서 admin으로 이동시 redirect 처리
	$("[data-walk-through]").on('click', function(e){
		e.preventDefault();

		var url = $(this).attr("href");
		var target = $(this).data("walk-through");

		Core.Utils.walkThrough( target, url );

		/*
		var $form = $("#tokenForm");
		var url = $(this).attr("href");
		var target = $(this).data("walk-through");
		var method = (target == 'site') ? 'GET' : 'POST';
		var action = (target == 'site') ? '/admin-redirect-to/site' : Core.Utils.contextPath + '/walk-through-admin'
		
		if( !_.isEmpty( $form )){
			$form.attr({"action": action, "method" : method });
			$form.append("<input name='redirectUrl' type='hidden' value='" + url + "' />");
			$form.submit();
		}
	
		*/

		// 이미 로그아웃이 되버리기 때문에 walk-through는 못쓰고 그냥 url 체크해서 site 로그아웃 페이지로 넘어가야함 
		/*
		if( url == '/logout' ){
			$.ajax({
				url : '/adminLogout.htm',
				type : 'GET',
				success:function(){
					
				}
			})
		}
		*/
	})

	// 로그아웃 클릭시 admin 로그아웃 처리 후 페이지 이동
	$("#logoutBtn").on("click", function(e){
		e.preventDefault();
		var url = $(this).attr("href");
		


		/*
		var url = "";
		var currentUrl = Core.Utils.url.getCurrentUrl();
		if( currentUrl.indexOf('prod') > -1 ){
			url = "https://assist-breeze.nike.co.kr/kr/ko_kr/logout";
		}else if( currentUrl.indexOf('uat-vpn-nike') > -1 ){
			url = "https://uat-vpn-nike.brzc.kr/kr/ko_kr/logout";
		}else{
			url = "https://uat-assist-nike.brzc.kr/kr/ko_kr/logout";
		}
		*/
		// 어드민을 로그아웃 시키기 때문에 walkThrough 를 사용할수 없다. 
		Core.Utils.ajax( '/adminLogout.htm', 'GET', {}, function(){
			if( Core.Utils.url.getCurrentUrl().indexOf('localhost') > -1){
				location.reload();	
			}else{
				location.href=url;	
			}
		})

		/*
		$.ajax({
			url : '/adminLogout.htm',
			type : 'GET'
		}).done(function(){
			//Core.cookie.setCookie('BREEZE_ADMIN_SIMPLE_REMEMBER_ME_COOKIE', 'BREEZE_ADMIN_SIMPLE_REMEMBER_ME_COOKIE', {expires:'Thu, 01 Jan 1970 00:00:01 GMT'});
			if( Core.Utils.url.getCurrentUrl().indexOf('localhost') > -1){
				location.reload();	
			}else{
				location.href=url;	
			}
		})
		*/
	})

	/*
	$(window).bind("pageshow", function(event) {
		if (event.originalEvent.persisted) {
			document.location.reload();
		}
	});
	*/


	// 로그인 유지 기능 처리를 위한 스크립트 였는데 적용해도 정상적으로 처리되지 않은 상태라서 기능 자체를 제거했기 때문에 현재 사용안함
	/*
	$("#logout").on("click", function(e){
		e.preventDefault();
		var url = $(this).attr("href");

		$.ajax({
			url : url,
			type : 'GET'
		}).done(function(){
			location.href = Core.Utils.contextPath + '/';
		})

		//Core.cookie.setCookie('SPRING_SECURITY_REMEMBER_ME_COOKIE', 'SPRING_SECURITY_REMEMBER_ME_COOKIE', {expires:'Thu, 01 Jan 1970 00:00:01 GMT'});
		//location.href = url;
	})
	*/

	/*
	if( !_GLOBAL.CUSTOMER.ISSIGNIN ){
		setInterval(function(){
			Core.cookie.setCookie('SPRING_SECURITY_REMEMBER_ME_COOKIE', 'SPRING_SECURITY_REMEMBER_ME_COOKIE', {expires:'Thu, 01 Jan 1970 00:00:01 GMT'});	
		}, 500)
	}
	*/

	//if( Core.Utils.url.getCurrentUrl().indexOf('adm') > -1 || Core.Utils.url.getCurrentUrl().indexOf('admin') > -1){
		// admin 세션유지 
		var sessionInerval = setInterval( function(){
			$.ajax({
				url : Core.Utils.contextPath + '/health-status-check',
				type : 'GET'
			}).fail(function(){
				//clearInterval( sessionInerval );
			})
		}, 290000 )
	//}

	// uikit modal 버튼 기본 label 설정 
	if( UIkit != null ){
		UIkit.modal.labels = {Ok: "확인", Cancel: "취소"}
	}

 });	
