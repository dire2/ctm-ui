(function(Core){
	Core.register('module_fulfillment_main', function(sandbox){
		var startDay, today, now;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0];
				Method.$that = $this;
				today = args.today;
				now = args.now;

				// fms.dashboard.start.day 값이 있으면 무조건 날짜로 처리하고 0이면 한달로 처리한다.
				startDay = Method.$that.find('[data-start-day]').data('start-day') || 0;
				$this.find('[data-status-list] a').on('click', Method.redirectOnFilter )
			},
			redirectOnFilter:function(e){
				e.preventDefault();
				console.log( $(this).data('fg-type') );
				console.log( $(this).data('st-type') );

				var fgType = $(this).data('fg-type');
				var stType = $(this).data('st-type');
				var submitDateKey = 'order.submitDate';
				var shippingType = ''
				var url = '';
				var param = '';
				var submitTerm = '';

				var shippingListLimit = 50;
				if(typeof _GLOBAL.SHIPPING_CONFIG !== undefined){
					if(typeof _GLOBAL.SHIPPING_CONFIG.SHIPPING_LIST_LIMIT !== undefined){
						shippingListLimit = Number(_GLOBAL.SHIPPING_CONFIG.SHIPPING_LIST_LIMIT);
					}
				}

				switch( fgType ){
					// 사용안함
					case 'CANCELLED':
						switch( stType ){
							case 'ORDER_SUBMITTED':
							break;
							case 'PROCESSING':
							break;
							case 'REQUESTED':
							break;
							case 'FULFILLED':
							break;
						}
					break;
					

					case 'ROPIS':
						shippingType = 'type=PHYSICAL_PICKUP&pType=ROPIS&isJustReservation=true&'
						url = '/fms-ropis-list'
						switch( stType ){
							case 'ORDER_SUBMITTED':
								
							break;
							case 'PROCESSING':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_READY&custom.fulfillmentManageStatusFilterType=FULFILL_READY'
							break;
							case 'REQUESTED':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_REQUESTED&custom.fulfillmentManageStatusFilterType=FULFILL_REQUESTED'
							break;
							case 'FULFILLED':
								param = 'status=DELIVERED&fulfillmentTrackings.trackingStatus=DELIVERED&pStatus=DELIVERED'
							break;
						}
					break;
					case 'BOPIS':
						shippingType = 'type=PHYSICAL_PICKUP&pType=BOPIS&isJustReservation=false&'
						url = '/fms-bopis-list';
						switch( stType ){
							case 'ORDER_SUBMITTED':
								
							break;
							case 'PROCESSING':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_READY&custom.fulfillmentManageStatusFilterType=FULFILL_READY'
							break;
							case 'REQUESTED':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_REQUESTED&custom.fulfillmentManageStatusFilterType=FULFILL_REQUESTED'
							break;
							case 'FULFILLED':
								param = 'status=DELIVERED&fulfillmentTrackings.trackingStatus=DELIVERED&pStatus=DELIVERED'
							break;
						}
					break;
					case 'SHIPPING':
						shippingType = 'type=PHYSICAL_SHIP&'
						url = '/fms-shipping-list';
						switch( stType ){
							case 'ORDER_SUBMITTED':
								
							break;
							case 'PROCESSING':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_READY&custom.fulfillmentManageStatusFilterType=FULFILL_READY&maxIndex=' + shippingListLimit + '&pageSize=' + shippingListLimit
							break;
							case 'REQUESTED':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_REQUESTED&custom.fulfillmentManageStatusFilterType=FULFILL_REQUESTED'
							break;
							case 'FULFILLED':
								param = 'status=DELIVERED&fulfillmentTrackings.trackingStatus=DELIVERED&pStatus=DELIVERED'
							break;
						}
					break;

					case 'SAMEDAY':																
						shippingType = 'type=PHYSICAL_SHIP&isProviderFlag=true&'
						url = '/fms-sameday-list';
						switch( stType ){
							case 'ORDER_SUBMITTED':
							break;
							case 'PROCESSING':
								param = 'status=PROCESSING&pStatus=PROCESSING_FULFILL_READY&custom.fulfillmentManageStatusFilterType=FULFILL_READY'
							break;
							case 'REQUESTED':
								param = 'status=PROCESSING&fulfillmentTrackings.trackingStatus=REQUESTED&pStatus=PROCESSING_FULFILL_REQUESTED'
							break;
							case 'FULFILLED':
								param = 'status=DELIVERED&fulfillmentTrackings.trackingStatus=DELIVERED&pStatus=DELIVERED'
							break;
						}
					break;

					case 'RETURN':
						submitDateKey = 'fulfillmentGroupItem.orderItem.order.submitDate';
						url = '/fms-return-collect'
						switch( stType ){
							case 'RETURN_APPROVE':
								param = 'fulfillmentGroupItem.fulfillmentGroup.status=READY&rStatus=READY'
							break;
							case 'RETURN_FULFILL':
								param = 'fulfillmentGroupItem.fulfillmentGroup.status=FULFILLED&rStatus=FULFILLED'
							break;
						}
					break;
				}
				
				// 링크 없으면 그냥 무시
				if( url == ''){
					return;
				}
				Core.ui.loader.show();
				// 결제완료 클릭시 주문 내역으로 링크 변경
				if( stType=='ORDER_SUBMITTED' ){
					url = '/fms-order-confirm';
				}else{
					shippingType = "";
				}
				
				var startTime = ' 00:00:00';
				var endTime = ' 23:59:59';
				var submitDate, day, month, year, stdDate, etdDate;
				/*
				var date = new Date( now );
				date.setMonth(date.getMonth()-1);
				day = date.getDate();
				month = date.getMonth() + 1;
				year = date.getFullYear();
				*/
				
				if (startDay == 0) {
					submitTerm = '&term=month';
					submitDate = moment(now, 'YYYY.MM.DD').subtract(1, 'months').format('YYYY.MM.DD') + startTime + '|' + now + endTime;
				} else {
					//& stdDate=20190821 & etdDate=20190821 &

					stdDate = moment(now, 'YYYY.MM.DD').subtract(startDay, 'days').format('YYYYMMDD');
					etdDate = moment(now, 'YYYY.MM.DD').format('YYYYMMDD');
					submitTerm = '&term=setting' + '&stdDate=' + stdDate + '&etdDate=' + etdDate;

					if (startDay == 3) submitTerm = '&term=threeday';
					if (startDay == 7) submitTerm = '&term=week';

					submitDate = moment(now, 'YYYY.MM.DD').subtract(startDay, 'days').format('YYYY.MM.DD') + startTime + '|' + now + endTime;
				}
				
				if(fgType =='SAMEDAY') {
					shippingType += 'pType=SAMEDAY&';
				} else if(fgType == 'SHIPPING'){
					shippingType += 'pType=SHIPPING&';
				} 
				
				url = url + '?' + submitDateKey + '=' + submitDate + '&sortProperty=' + submitDateKey + submitTerm + '&sortDirection=DESCENDING&' + shippingType + param;
				console.log( url );
				location.href = url
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-fulfillment-main]',
					attrName:'data-module-fulfillment-main',
					moduleName:'module_fulfillment_main',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);