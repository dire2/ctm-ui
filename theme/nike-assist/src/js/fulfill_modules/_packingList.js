(function(Core){
	Core.register('module_packing_list', function(sandbox){
		var shippingType;
		var labelType = {
			shipping:['pack', 'packall'],
			bopis:['pick', 'pickall'],
			ropis:['rick', 'rickall'],
			samedayShipping:['spack', 'spackall']
		}
		var Method = {
			$itemList:null,
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				shippingType = $this.find('input[name="shippingType"]').val();
				$this.on('click', '[data-picking-create-btn]', Method.pickingCreate ); // pickup 버튼
				$this.on('click', '[data-packing-complete-btn]', Method.packingComplete ); // 상품준비완료
				$this.on('click', '[data-pickup-btn]', Method.pickup ); // 상품인도 버튼
				$this.on('click', '[data-fulfill-btn]', Method.fulfillSubmit ); // 출고완료
				$this.on('click', '[data-cancel-btn]', Method.cancelOrderSubmit ); // 주문취소
				$this.on('click', '[data-show-modify-quantity-btn]', Method.showHideModifyQuantity ); // 수량 변겅 UI toggle tab component 가 동작을 안해서 만들어 놓음
				$this.on('click', '[data-modify-quantity-btn]', Method.modifyQuantitySubmit ); // 수량 변경

				/*
				$this.find('[data-picking-create-btn]').on('click', Method.pickingCreate);
				$this.find('[data-packing-complete-btn]').on('click', Method.packingComplete);
				$this.find('[data-pickup-btn]').on('click', Method.pickup);
				$this.find('[data-fulfill-btn]').on('click', Method.fulfillSubmit);
				$this.find('[data-cancel-btn]').on('click', Method.cancelOrderSubmit);
				$this.find('[data-show-modify-quantity-btn]').on('click', Method.showHideModifyQuantity);
				$this.find('[data-modify-quantity-btn]').on('click', Method.modifyQuantitySubmit);
				*/

				var toggleComponent = sandbox.getComponents('component_tabs', {context:$this}, function(){
					this.addEvent('tabClick', function(index){
						var $tabContainer = $(this).closest('form').find('.toggle-container-tab');
						if($tabContainer.hasClass('uk-hidden')){
							$tabContainer.removeClass('uk-hidden');
						}else{
							$tabContainer.addClass('uk-hidden');
						}
					});
				});

				//cs 처리
				$this.on('click', '[data-packing-cs-btn]', Method.shortageSubmit);

				//개별인쇄
				$this.on('click', '[data-print-btn]', function(){
					console.log( '개별인쇄' );
					var type = labelType[shippingType][0];
					window.location.href = 'seamless://print=ok&mode=label&labeltype='+ type + '&' + Method.lableReceipt($(this).closest('form'), type);
				});

				//전체인쇄
				$this.on('click', '[data-print-total-btn]', function(){
					console.log( '전체인쇄' );
					var type = labelType[shippingType][1];
					var allQueryString = '';
					$this.find('form').each(function(i){
						allQueryString += '&' + Method.lableReceipt($(this).closest('form'), type);
					});

					var transAllQueryParams = sandbox.utils.getQueryParams(allQueryString);
					var keyLabelType = (type === 'packall' || type === 'spackall') ? 'packingnum' : 'pickingnum'
					var isArray = (transAllQueryParams[keyLabelType] instanceof Array) ? true : false;
					var arrQueryString = [];

					if(type === 'packall' || type === 'spackall'){
						arrQueryString[0] = 'packingnum=' + (isArray ? transAllQueryParams.packingnum.join('%7C') : transAllQueryParams.packingnum);
						arrQueryString[1] = 'cusname=' + (isArray ? transAllQueryParams.cusname.join('%7C') : transAllQueryParams.cusname);
						arrQueryString[2] = 'custel=' + (isArray ? transAllQueryParams.custel.join('%7C') : transAllQueryParams.custel);
						arrQueryString[3] = 'cusaddress=' + (isArray ? transAllQueryParams.cusaddress.join('%7C') : transAllQueryParams.cusaddress);
						arrQueryString[4] = 'prodinfo=' + (isArray ? transAllQueryParams.prodinfo.join('%7C%7C') : transAllQueryParams.prodinfo);
						arrQueryString[5] = 'fgId=' + (isArray ? transAllQueryParams.fgId.join('%7C') : transAllQueryParams.fgId);
					}else if(type === 'pickall' || type === 'rickall'){
						arrQueryString[0] = 'pickingnum=' + (isArray ? transAllQueryParams.pickingnum.join('%7C') : transAllQueryParams.pickingnum);
						arrQueryString[1] = 'pickingdate=' + (isArray ? transAllQueryParams.pickingdate.join('%7C') : transAllQueryParams.pickingdate);
						arrQueryString[2] = 'productname=' + (isArray ? transAllQueryParams.productname.join('%7C') : transAllQueryParams.productname);
						arrQueryString[3] = 'productstyle=' + (isArray ? transAllQueryParams.productstyle.join('%7C') : transAllQueryParams.productstyle);
						arrQueryString[4] = 'productsize=' + (isArray ? transAllQueryParams.productsize.join('%7C') : transAllQueryParams.productsize);
						arrQueryString[5] = 'productquantity=' + (isArray ? transAllQueryParams.productquantity.join('%7C') : transAllQueryParams.productquantity);
						arrQueryString[6] = 'cusname=' + (isArray ? transAllQueryParams.cusname.join('%7C') : transAllQueryParams.cusname);
						arrQueryString[7] = 'custel=' + (isArray ? transAllQueryParams.custel.join('%7C') : transAllQueryParams.custel);
					}

					window.location.href = 'seamless://print=ok&mode=label&labeltype=' + type + '&' + arrQueryString.join('&');
				});

				// 2019-03-28 packingList 선택 기능 추가
				// Picking 완료, 상품준비완료 filterType일때만 클릭활성화
				if(Method.getParameterByName('custom.fulfillmentManageStatusFilterType') === 'FULFILL_READY'|| Method.getParameterByName('custom.fulfillmentManageStatusFilterType') === 'FULFILL_REQUESTED'){
					$(this).on('click', '#packing-form>.check_area', function() {
						if ($(this).parents('form').find('.toggle-container').hasClass("uk-hidden")) {
			              if ($(this).parents('form').hasClass("sy-check-active")) {
			                $(this).parents('form').removeClass("sy-check-active");
			                $(this).find("input[name='pickingItemXrefId']").attr('checked', false);
			              } else {
			                $(this).parents('form').addClass("sy-check-active");
			                $(this).find("input[name='pickingItemXrefId']").attr('checked', true);
			              }
						}
					});

					$(this).on('click', '[data-show-modify-quantity-btn]', function() {
						$(this).parents('form').removeClass("sy-check-active");
					});
				}

				// 2019-03-28 packingCompleteTotal 추가
				$this.find("a[id='partialPackingComplete']").on("click", function(){ Method.packingCompleteTotal(false)});
				$this.find("a[id='allPackingComplete']").on("click", function(){ Method.packingCompleteTotal(true)});


				// 2019-03-28 fulfillSubmitTotal 추가
				$this.find("a[id='partialFulfillComplete']").on("click", function(){ Method.fulfillSubmitTotal(false)});
				$this.find("a[id='allFulfillComplete']").on("click", function(){ Method.fulfillSubmitTotal(true )});
			},
			modifyQuantitySubmit:function(e){
				e.preventDefault();
				var pickingItemXrefId = $(this).closest('.toggle-container').find('input[name="pickingItemXrefId"]').val();
				var quantity = $(this).closest('.toggle-container').find('input[name="pickedQuantity"]').val();
				var items = {};
				items[pickingItemXrefId] = quantity

				var _self = this;
				UIkit.modal.confirm('Picking 수량을 변경 하시겠습니까?', function(){
					Method.packingSubmit.call(_self, [e],{
						action : '/fms-action/modify-picked-quantity',
						sucMsg : '정상적으로 처리되었습니다.',
						method : 'GET'
					},{
						pickedItems : items
					});
				});
			},
			showHideModifyQuantity:function(e){
				e.preventDefault();
				var $tabContainer = $(this).closest('form').find('.toggle-container');
				if($tabContainer.hasClass('uk-hidden')){
					$tabContainer.removeClass('uk-hidden');
				}else{
					$tabContainer.addClass('uk-hidden');
				}
			},
			lableReceipt:function($form, labelType){
				var serializeData = $form.serialize();
				var formData = serializeData.replace(/\+/g, '%20');
				var queryParams = sandbox.utils.getQueryParams(formData);
				var itemLen = $form.find('.product_list > li').length;

				var arrProductInfo = [];
				for(var i=0; i<itemLen; i++){
					arrProductInfo.push((queryParams.productname instanceof Array) ?
					queryParams.productname[i]+'%2F'+queryParams.model[i]+'%2F'+queryParams.opt[i]+'%2F'+queryParams.quantity[i]
					: queryParams.productname+'%2F'+queryParams.model+'%2F'+queryParams.opt+'%2F'+queryParams.quantity);
				}

				var arrQueryString = [];
				var isArray = queryParams.productname instanceof Array;
				if(labelType === 'pack' || labelType === 'packall' || labelType === 'spackall' || labelType === 'spack'){
					arrQueryString[0] = 'packingnum=' + queryParams.orderNumber;
					arrQueryString[1] = 'cusname=' + queryParams.userName;
					arrQueryString[2] = 'custel=' + queryParams.phoneNumber;
					arrQueryString[3] = 'cusaddress=' + (queryParams.address1 + '%20' + queryParams.address2);
					arrQueryString[4] = 'prodinfo=' + Method.getReplaceCode(arrProductInfo.join('%7C'));
					arrQueryString[5] = 'fgId=' + queryParams.fgId;
				}else if(labelType === 'pick' || labelType === 'pickall' || labelType === 'rick' || labelType === 'rickall'){
					arrQueryString[0] = 'pickingnum=' + queryParams.orderNumber;
					arrQueryString[1] = 'pickingdate=' + queryParams.submitDate;
					arrQueryString[2] = 'productname=' + Method.getReplaceCode((isArray ? queryParams.productname.join('%7C') : queryParams.productname));
					arrQueryString[3] = 'productstyle=' + (isArray ? queryParams.model.join('%7C') : queryParams.model);
					arrQueryString[4] = 'productsize=' + (isArray ? queryParams.opt.join('%7C') : queryParams.opt);
					arrQueryString[5] = 'productquantity=' + (isArray ? queryParams.quantity.join('%7C') : queryParams.quantity);
					arrQueryString[6] = 'cusname=' + queryParams.userName;
					arrQueryString[7] = 'custel=' + queryParams.phoneNumber;
				}else{
					arrQueryString[0] = 'packingnum=not found label type labelType is : '+labelType ;
					arrQueryString[1] = 'cusname=not found label type';
					arrQueryString[2] = 'custel=not found label type';
					arrQueryString[3] = 'cusaddress=not found label type';
					arrQueryString[4] = 'prodinfo=not found label type';
				}

				return arrQueryString.join('&');
			},
			getReplaceCode: function (stg) {
				return stg.replace(/%26/gi, '%20');
			},
			packingSubmit:function( e, opt, params ){
				var $form = $(this).closest('#packing-form');
				var queryParams = sandbox.utils.getQueryParams($form.serialize());

				var action = opt.action;
				var sucMsg = opt.sucMsg;
				var func = opt.sucCallback || null;
				var method = opt.method || 'GET';
				//var errorMsg = "";
				var param = {};

				param = {
					fgId : queryParams.fgId,
					id : queryParams.fgId,
					csrfToken : queryParams.csrfToken
				}

				param = $.extend( param, params );

				sandbox.utils.ajax(action, method, param, function(data){
					var data = $.parseJSON( data.responseText );
					if(!_.isEmpty(data.errors)){
						var msg = "";
						if( !_.isEmpty(data.errors.message)){
							msg = data.errors.message;
						}else{
							msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
						}

						UIkit.modal.alert(msg).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						})
					}else{
						UIkit.modal.alert(sucMsg).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							if( func == null ){
								location.reload();
							}else{
								func.call();
							}
						});
					}
				});
			},
			cancelOrderSubmit:function(e){
				e.preventDefault();
				var _self = this;
				var $form = $(this).closest('#packing-form');
				var params = sandbox.utils.getQueryParams($form.serialize());
				var phoneNumber = params.phoneNumber;
				var orderNumber = params.orderNumber;
				var userName = decodeURIComponent(params.userName);
				var cancelReason = params.cancelReason;

				if( _.isEmpty(cancelReason)){
					UIkit.modal.alert('픽업 취소 사유를 선택해 주세요.');
					return false;
				}

				if( _.isEmpty(phoneNumber) || _.isEmpty(orderNumber) || _.isEmpty(userName) ){
					UIkit.modal.alert('주문 정보가 올바르지 않습니다.<br/>확인 후 다시 시도해 주세요.').on('hide.uk.modal', function() {
						location.reload();
					});
					return;
				}

				UIkit.modal.confirm("'픽업 취소'는 본인인증이 필요합니다.<br /> 진행하시겠습니까?", function(){
					var otpModal = UIkit.modal('#popup-layer-order-custom');
					otpModal.show();
					var otp = sandbox.getModule('module_authenticate');
					otp.reset({
						isDirectAuth : true,
						customText : '<h6 style="margin:15px 0px;">이름 : '+ userName + '</h6><h6 style="margin:15px 0px;">연락처 : ' + phoneNumber + '</h6>',
						authData : {
							orderNumber : orderNumber,
							messageType : 'KAKAO'
						}
					});
					otp.success(function(){
						Method.packingSubmit.call(_self, [e],{
							action : '/order-action/void-entire-order',
							sucMsg : '정상적으로 처리되었습니다.',
							method : 'POST',
						},{
							cancelledReasonType : cancelReason
						});

					}).fail( function(){
						location.reload();
					});
				});
			},
			fulfillSubmit:function(e){
				e.preventDefault();
				var _self = this;
				var fulfillmentModal = UIkit.modal('#popup-fulfillment-confirm', {modal:false, center:true, bgclose:false}).show();
				fulfillmentModal.element.find(".cntText").html("");

				fulfillmentModal.element.find(".js-modal-confirm, .js-modal-confirm-cancel").off('click').on('click', function( pe ){
					pe.preventDefault();
					if( $(this).is('.js-modal-confirm')){
						// 확인 클릭시
						Method.packingSubmit.call(_self, [e],{
							action : '/order-action/fulfill',
							sucMsg : '정상적으로 처리되었습니다.',
							method : 'POST'
						});
					}
					fulfillmentModal.hide();
				});
				/*
				UIkit.modal.confirm("'출고완료' 처리하시겠습니까? <br /> <span >* </span>", function(){
					Method.packingSubmit.call(_self, [e],{
						action : '/order-action/fulfill',
						sucMsg : '정상적으로 처리되었습니다.',
						method : 'POST'
					});
				});
				*/
			},
			shortageSubmit:function(e){
				e.preventDefault();
				var _self = this;
				var $form = $(this).closest('#packing-form');
				var params = sandbox.utils.getQueryParams($form.serialize());
				var shortageReason = params.selectShortageReaseon;

				if( _.isEmpty(shortageReason)){
					UIkit.modal.alert('결품 사유를 선택해 주세요.');
					return false;
				}
				UIkit.modal.confirm('CS에 결품 주문취소를 요청합니다. 진행하시겠습니까?', function(){
					Method.privateShortageSubmit.call(_self, [e],{
						action : '/order-action/shortage',
						sucMsg : '정상적으로 처리되었습니다.',
						method : 'POST'
					});
				})
				/*
				UIkit.modal.confirm('해당 주문을 CS처리 하시겠습니까?').on('hide.uk.modal', function() {
					var $form = $(this).closest('#packing-form');
					var params = sandbox.utils.getQueryParams($form.serialize());
					console.log(params);
					sandbox.utils.promise({
						url:'/order-action/shortage',
						data:{fgId:params.fgId, csrfToken:params.csrfToken},
						method:'GET'
					}).then(function(data){
						console.log(data);
						UIkit.modal.alert('CS접수가 정상적으로 완료되었습니다.').on('hide.uk.modal', function() {
							location.reload();
						});
					});
				})
				*/
			},
			privateShortageSubmit:function( e, opt, params ){
				var $form = $(this).closest('#packing-form');
				var queryParams = sandbox.utils.getQueryParams($form.serialize());

				var action = opt.action;
				var sucMsg = opt.sucMsg;
				var func = opt.sucCallback || null;
				var method = opt.method || 'POST';
				//var errorMsg = "";
				var param = {};
				var shortageReason = $($form).find("select[name=selectShortageReaseon]").val();
				if(shortageReason == '' || shortageReason == null) return false; //결품사유 null check

				param = {
					fgId : queryParams.fgId,
					id : queryParams.fgId,
					csrfToken : queryParams.csrfToken,
					shortageReason : shortageReason
				}

				param = $.extend( param, params );

				sandbox.utils.ajax(action, method, param, function(data){
					var data = $.parseJSON( data.responseText );
					if(!_.isEmpty(data.errors)){
						var msg = "";
						if( !_.isEmpty(data.errors.message)){
							msg = data.errors.message;
						}else{
							msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
						}

						UIkit.modal.alert(msg).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						})
					}else{
						UIkit.modal.alert(sucMsg).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							if( func == null ){
								location.reload();
							}else{
								func.call();
							}
						});
					}
				});
			},
			pickupSubmit:function(){
				var defer = $.Deferred();
				var $form = $(this).closest('#packing-form');
				var params = sandbox.utils.getQueryParams($form.serialize());

				sandbox.utils.ajax('/order-action/pickup-delivery', 'POST', {fgId:params.fgId, csrfToken:params.csrfToken}, function(data){
					var data = $.parseJSON( data.responseText );
					if(!_.isEmpty(data.errors)){
						UIkit.modal.alert(data.errors.message).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						})
					}else{
						UIkit.modal.alert('정상적으로 처리되었습니다.').on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}
				});

				/*
				sandbox.utils.promise({
					url: '/order-action/fulfill',
					data : {fgId:params.fgId, csrfToken:params.csrfToken},
					method:'POST'
				}).then(function(data){
					if(!_.isEmpty(data.errors)){
						defer.reject('상품 인도 처리 중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.(배송처리)');
					}else{
						defer.resolve();
					}
					return defer.promise();
				}).then(function(data){
					return sandbox.utils.promise({
						url:'/order-action/delivery',
						data:{fgId:params.fgId, csrfToken:params.csrfToken},
						method:'POST',
					});
				}).then(function(data){
					if(!_.isEmpty(data.errors)){
						defer.reject('상품 인도 처리 중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.(배송완료처리)');
					}else{
						UIkit.modal.alert('상품 인도 처리가 정상적으로 완료되었습니다.').on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}
				}).fail(function(msg){
					UIkit.modal.alert(msg).on('hide.uk.modal', function() {
						sandbox.setLoadingBarState(true);
						location.reload();
					});
				});
				*/
				/*
				sandbox.utils.ajax('/order-action/fulfill', 'GET', {fgId: fgId}, function(data){
					var data = $.parseJSON( data.responseText );
					if(!_.isEmpty(data.errors)){
						UIkit.modal.alert(data.errors.message).on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						})
					}else{
						UIkit.modal.alert('상품 인도 처리가 정상적으로 완료되었습니다.').on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}
				});
				*/
			},
			pickup:function(e){
				e.preventDefault();
				var _self = this;
				var $form = $(this).closest('#packing-form');
				var params = sandbox.utils.getQueryParams($form.serialize());
				var phoneNumber = params.phoneNumber;
				var orderNumber = params.orderNumber;
				var userName = decodeURIComponent(params.userName);

				//QR인증 선택 체크용 구분자 추가
				$('*[data-selected-pickup-form="true"]').removeAttr('data-selected-pickup-form');
				$($form).attr('data-selected-pickup-form','true');

				if( _.isEmpty(phoneNumber) || _.isEmpty(orderNumber) || _.isEmpty(userName) ){
					UIkit.modal.alert('주문 정보가 올바르지 않습니다.<br/>확인 후 다시 시도해 주세요.').on('hide.uk.modal', function() {
						location.reload();
					});
					return;
				}

				//qr코드 인증 처리용 ordernumber 처리 부. 이전에 설정된 active값들 삭제 후 재정의
				if($(document).find('[data-active-order]').length > 0){
					$(document).find('[data-active-order]').removeAttr('data-active-order');
				}
				$(_self).attr('data-active-order', orderNumber);

				$('#account-auth').on({
					// modal show상태에서 click 이벤트 정의
					'show.uk.modal': function(){

						//OTP인증 클릭 이벤트 정의
						$(this).find('[data-auth-otp]').off('click').on('click',function(event){
							event.preventDefault();

							var otpModal = UIkit.modal('#popup-layer-order-custom');
							otpModal.show();
							var otp = sandbox.getModule('module_authenticate');
							otp.reset({
								isDirectAuth : true,
								customText : '<h6 style="margin:15px 0px;">이름 : '+ userName + '</h6><h6 style="margin:15px 0px;">연락처 : ' + phoneNumber + '</h6>',
								authData : {
									orderNumber : orderNumber,
									messageType : 'KAKAO'
								}
							});
							otp.success(function(){
								if( shippingType == 'ropis'){
									UIkit.modal.confirm('매장예약 주문입니다. 매장 내 POS 로 결제해야 합니다.<br /> 진행하시겠습니까?', function(){
										Method.pickupSubmit.call(_self, [e]);
									}, function(){
										UIkit.modal.alert('매장 내 POS로 결제 후 다시 시도해 주세요').on('hide.uk.modal', function() {
											sandbox.setLoadingBarState(true);
											location.reload();
										});
									})
								}else{
									Method.pickupSubmit.call(_self, [e]);
								}
							}).fail( function(){
								UIkit.modal.alert('본인 인증에 실패 하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function() {
									location.reload();
								});
							});

							UIkit.modal("#account-auth").hide();
						});

						//QR인증 클릭 이벤트 정의
						$(this).find('[data-auth-qr]').on('click',function(event){
							UIkit.modal("#account-auth").hide();
						});
					}
				});

				//선택 된 상품이 1개 이상 일 때 실행
				if($('#order-list').find('.sy-check-active').length > 0){
					UIkit.modal("#account-auth").show();
				}else{
					alert('선택 된 상품이 없습니다.');
				}

				/*
				UIkit.modal.confirm("'픽업완료'는 본인인증이 필요합니다.<br /> 진행하시겠습니까?", function(){
					var otpModal = UIkit.modal('#popup-layer-order-custom');
					otpModal.show();
					var otp = sandbox.getModule('module_authenticate');
					otp.reset({
						isDirectAuth : true,
						customText : '<h6 style="margin:15px 0px;">이름 : '+ userName + '</h6><h6 style="margin:15px 0px;">연락처 : ' + phoneNumber + '</h6>',
						authData : {
							orderNumber : orderNumber,
							messageType : 'KAKAO'
						}
					});
					otp.success(function(){
						if( shippingType == 'ropis'){
							UIkit.modal.confirm('매장예약 주문입니다. 매장 내 POS 로 결제해야 합니다.<br /> 진행하시겠습니까?', function(){
								Method.pickupSubmit.call(_self, [e]);
							}, function(){
								UIkit.modal.alert('매장 내 POS로 결제 후 다시 시도해 주세요').on('hide.uk.modal', function() {
									sandbox.setLoadingBarState(true);
									location.reload();
								});
							})
						}else{
							Method.pickupSubmit.call(_self, [e]);
						}
					}).fail( function(){
						UIkit.modal.alert('본인 인증에 실패 하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function() {
							location.reload();
						});
					});
				});
				 */
			},
			packingComplete:function(e){
				e.preventDefault();
				var _self = this;
				UIkit.modal.confirm("'상품준비완료' 처리하시겠습니까?", function(){
					Method.packingSubmit.call(_self, [e],{
						action : '/order-action/fulfill-request',
						sucMsg : '정상적으로 처리되었습니다.',
						method : 'GET'
					});
				})
				/*
				UIkit.modal.confirm('상품준비완료 처리 하시겠습니까?', function(){
					sandbox.utils.ajax('/order-action/fulfill-request', 'GET', {fgId: fgId}, function(data){
						var data = $.parseJSON( data.responseText );
						if(!_.isEmpty(data.errors)){
							var msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
							UIkit.modal.alert(msg).on('hide.uk.modal', function() {
								sandbox.setLoadingBarState(true);
								location.reload();
							})
						}else{
							UIkit.modal.alert('상품준비완료가 정상적으로 완료되었습니다.').on('hide.uk.modal', function() {
								sandbox.setLoadingBarState(true);
								location.reload();
							});
						}
					});
				})
				*/
			},
			pickingCreate:function(e){
				e.preventDefault();
				var _self = this;
				var ftiId = $(this).data('picking-create-btn');
				UIkit.modal.confirm('해당 상품을 Picking 하시겠습니까?', function(){
					Method.packingSubmit.call(_self, [e],{
						action : '/fms-action/create-picking-list',
						sucMsg : 'Picking List에 정상적으로 추가 되었습니다.',
						method : 'GET',
					},{
						ftiId: ftiId,
						adminUser:''
					});
				})
				/*
				var ftiId = $(this).data('picking-create-btn');
				UIkit.modal.confirm('해당 상품을 Picking 하시겠습니까?', function(){
					sandbox.utils.ajax('/fms-action/create-picking-list', 'GET', {ftiId: ftiId}, function(data){
						var data = $.parseJSON( data.responseText );
						if(!_.isEmpty(data.errors)){
							var msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
							UIkit.modal.alert(data.errors.message).on('hide.uk.modal', function() {
								sandbox.setLoadingBarState(true);
								location.reload();
							})
						}else{
							UIkit.modal.alert('Picking Target List에 정상적으로 추가 되었습니다.').on('hide.uk.modal', function() {
								sandbox.setLoadingBarState(true);
								location.reload();
							});
						}
					});
				});
				*/
			},
			// 2019-03-28 fulfillSubmitTotal 추가
			fulfillSubmitTotal:function(isFulfillAll){
			   var fgIds=[], fgId, param, list, selectedList, checkedLength;
			   selectedList = $('.fcontents_wrap').find(".sy-check-active");
			   list = $('.fcontents_wrap').find('#order-list>li');

				//주문제한 변수 추가
				var checkedLengthLimit = 20;

                if(typeof _GLOBAL.ORDER_CONFIG !== undefined){
                    if(typeof _GLOBAL.ORDER_CONFIG.ORDER_CHECK_LIMIT !== undefined){
                        checkedLengthLimit = Number(_GLOBAL.ORDER_CONFIG.ORDER_CHECK_LIMIT);
                    }
                }

			   if( isFulfillAll ){
			      checkedLength = list.length;
			   }else{
			      checkedLength = selectedList.length;
			   }
			   if( checkedLength > checkedLengthLimit ){
			      UIkit.modal.alert('최대 ' + checkedLengthLimit + ' 건 까지 가능합니다. <br/> ' + checkedLengthLimit + ' 건 이하로 주문을 선택 후 시도해 주세요.');
			      return;
			   }
			   if (checkedLength == 0) {
			      UIkit.modal.alert('선택된 항목이 없습니다.');
			      return;
			   }
			   if (isFulfillAll) {
			      $.each(list, function( index, data) {
			         fgIds[index] = 'fgId=' + $(this).find('input[name="fgId"]').val();
			      });
			   } else {
			      $.each(selectedList, function( index, data) {
			         fgIds[index] = 'fgId=' + $(this).closest('form').find('input[name="fgId"]').val();
			      });
			   }
			   fgId = fgIds.join('&');

			   var action = '/order-action/fulfill';
			   param = fgId;

			   var fulfillmentModal = UIkit.modal('#popup-fulfillment-confirm', {modal:false, center:true, bgclose:false}).show();
			   fulfillmentModal.element.find(".cntText").html("총 " + checkedLength + "건 ");

			   fulfillmentModal.element.find(".js-modal-confirm, .js-modal-confirm-cancel").off('click').on('click', function( pe ){
			      pe.preventDefault();
			      if( $(this).is('.js-modal-confirm')){
			         // 확인 클릭시
			         sandbox.utils.ajax(action, 'POST', param, function(data) {
			            var data = $.parseJSON( data.responseText );
			            if(!_.isEmpty(data.errors)){
			               var msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
			               UIkit.modal.alert(msg).on('hide.uk.modal', function() {
			                  sandbox.setLoadingBarState(true);
			                  location.reload();
			               })
			            }else{
			               UIkit.modal.alert('정상적으로 처리되었습니다..').on('hide.uk.modal', function() {
			                  sandbox.setLoadingBarState(true);
			                  location.reload();
			               });
			            }
			         });
			      }
			      fulfillmentModal.hide();
			   });
			},
			// 2019-03-28 packingCompleteTotal 추가
			packingCompleteTotal:function( isPackingAll ) {
			   var fgIds=[], fgId, param, list, selectedList, checkedLength;
			   selectedList = $('.fcontents_wrap').find(".sy-check-active");
			   list = $('.fcontents_wrap').find('#order-list>li');

				//주문제한 변수 추가
				var checkedLengthLimit = 20;

				if(typeof _GLOBAL.ORDER_CONFIG !== undefined){
					if(typeof _GLOBAL.ORDER_CONFIG.ORDER_CHECK_LIMIT !== undefined){
						checkedLengthLimit = Number(_GLOBAL.ORDER_CONFIG.ORDER_CHECK_LIMIT);
					}
				}

			   if( isPackingAll ){
			      checkedLength = list.length;
			   }else{
			      checkedLength = selectedList.length;
			   }
			   if( checkedLength > checkedLengthLimit ){
					UIkit.modal.alert('최대 ' + checkedLengthLimit + ' 건 까지 가능합니다. <br/> ' + checkedLengthLimit + ' 건 이하로 주문을 선택 후 시도해 주세요.');
					return;
			   }
			   if (checkedLength == 0) {
			      UIkit.modal.alert('선택된 항목이 없습니다.');
			      return;
			   }

			   UIkit.modal.confirm("총 " + checkedLength + " 건을 '상품준비완료' 처리하시겠습니까?", function(){
			      // 전체 상품준비완료 확인시
			      if (isPackingAll) {
			         $.each(list, function( index, data) {
			            fgIds[index] = 'fgId=' + $(this).find('input[name="fgId"]').val();
			         });
			      } else {
			         $.each(selectedList, function( index, data) {
			            fgIds[index] = 'fgId=' + $(this).closest('form').find('input[name="fgId"]').val();
			         });
			      }
			      fgId = fgIds.join('&');

			      var action = '/order-action/fulfill-request';
			      var param = fgId;

			      sandbox.utils.ajax(action, 'GET', param, function(data) {
			         var data = $.parseJSON( data.responseText );
			         if(!_.isEmpty(data.errors)){
			            var msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
			            UIkit.modal.alert(msg).on('hide.uk.modal', function() {
			               sandbox.setLoadingBarState(true);
			               location.reload();
			            })
			         }else{
			            UIkit.modal.alert('상품준비완료가 정상적으로 완료되었습니다.').on('hide.uk.modal', function() {
			               sandbox.setLoadingBarState(true);
			               location.reload();
			            });
			         }
			      });

			   });
			},
			// 2019-03-29 getParameterByName 추가
			getParameterByName:function(name) {
			    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			        results = regex.exec(location.search);
			    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-packing-list]',
					attrName:'data-module-packing-list',
					moduleName:'module_packing_list',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});

})(Core);
