// 당매장 주문관리와 반품 신청에서 사용 됨
(function(Core){
	Core.register('module_return', function(sandbox){
		var Method = {
			$itemList:null,
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;

				var searchComponent = sandbox.getComponents('component_searchfield', {context:$this} );
				var filterModule = sandbox.getModule('module_fulfillmentfilter', {context:$this});

				var $form = $("form[id='main-search-form']");
				var $searchInput = $("input[id=main-search]");
				var $searchLabel = $("label[for='search']");
				var $searchBtn = $("button[id=main-search-btn]")

				// WebPOS 메인 페이지에 검색어 입력 값이 없으면 검색 불가.
				$searchInput.keydown(function() {
					if (event.keyCode == 13) {
						event.preventDefault();
						if( $.trim($searchInput.val()) != '' ){
							var name = $("#main_search_type option:selected").val();
							var value = $searchInput.val();
							var minLen = filterModule.getMinLenByFilterType( name );

							if( value.length < minLen){
								UIkit.modal.alert('최소 ' + minLen + '자 이상을 입력하세요.');
								return false;
							}

							$searchInput.attr('name', name );
							$form.submit();
						}else{
							UIkit.modal.alert('검색어를 입력하세요.').on('hide.uk.modal', function(){
								$searchLabel.trigger('click');
							});
						}
					}
				});

				// WebPOS 메인 페이지에서 검색어 입력이 안된상태에서는 검색버튼 비활성화.
				if ($searchInput.val() == "") {
					$searchBtn.prop("disabled", true);
				}

				// WebPOS 메인 페이지의 검색어 입력이되면 검색 버튼 활성화.
				$searchInput.on('change', function(){
					if ($searchInput.val() == "") {
						$searchBtn.prop("disabled", true);
					} else {
						$searchBtn.prop("disabled", false);
					}
				});

				// WebPOS 메인 페이지 검색 포커스 제어 (포커스 활성화).
				$searchLabel.click(function(event) {
					$("#mainSearch").attr('class','input-textfield width-max focus');
					$searchInput.focus();
				});

				$searchInput.focusin(function(event) {
					$searchInput.val('');
				});

				// WebPOS 메인 페이지 검색 포커스 제어 (포커스 비활성화).
				$searchInput.focusout(function(event) {
					if( $.trim($searchInput.val()) == ''){
						$("#mainSearch").attr('class','input-textfield width-max');
						$("body").focus();
					}
				});

				var query = sandbox.utils.getQueryParams(location.href);

				if( query.searchType != null ){
					if( query[query.searchType] ){
						$searchLabel.trigger('click');
						$searchInput.val(decodeURIComponent(query[query.searchType]).split("+").join(" "));
					}
				}

				$('#order-list').on('click', 'li > a', Method.detailViewConfirm );
			},
			detailViewConfirm:function(e){
				e.preventDefault();
				/*
				var param = $(this).find("#paramForm").serialize();
				var url = $(this).attr('href') +'?'+ param;
				var phoneNumber = $(this).find('input[name="phoneNumber"]').val();
				*/

				var $form = $(this).find('#paramForm');
				var params = sandbox.utils.getQueryParams($form.serialize());
				var urlParams = [
					'orderId='+params['orderId'],
					'customerId='+params['customerId'],
					'siteId='+params['siteId']
				];
				var url = $(this).attr('href') +'?'+ urlParams.join('&');
				var phoneNumber = params.phoneNumber;
				var orderNumber = params.orderNumber;
				var userName = decodeURIComponent(params.name);


				if( _.isEmpty(phoneNumber) || _.isEmpty(orderNumber) || _.isEmpty(userName) ){
					UIkit.modal.alert('주문 정보가 올바르지 않습니다.<br/>확인 후 다시 시도해 주세요.').on('hide.uk.modal', function() {
						location.reload();
					});
					return;
				}

				UIkit.modal.confirm("'주문내역상세'를 조회하시려면 본인인증이 필요합니다.<br/>진행하시겠습니까?", function(){
					var otpModal = UIkit.modal('#popup-layer-order-custom');
					otpModal.show();

					var otp = sandbox.getModule('module_authenticate');
					otp.reset({
						isDirectAuth : true,
						customText : '<h6 style="margin:15px 0px;">이름 : '+ userName + '</h6><h6 style="margin:15px 0px;">연락처 : ' + phoneNumber + '</h6>',
						authData : {
							orderNumber : orderNumber,
							messageType : 'KAKAO'
						}
					});
					otp.success(function(){
						location.href = url;
					}).fail( function(){
						UIkit.modal.alert('본인 인증에 실패 하였습니다. 다시 시도해 주세요.').on('hide.uk.modal', function() {
							location.reload();
						});
					})
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-return]',
					attrName:'data-module-return',
					moduleName:'module_return',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});

})(Core);
