(function(Core){
	Core.register('module_recalled', function(sandbox){
		var today, now;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0] || {};
				today = args.today;
				now = args.now;

				$this.on('click', '[data-show-recall-btn]', Method.showHideRecall ); // 수량 변겅 UI toggle tab component 가 동작을 안해서 만들어 놓음
				
				/*
				var toggleComponent = sandbox.getComponents('component_tabs', {context:$this}, function(){
					this.addEvent('tabClick', function(index){
						var $tabContainer = $(this).closest('form').find('.toggle-container-tab');
						if($tabContainer.hasClass('uk-hidden')){
							$tabContainer.removeClass('uk-hidden');
						}else{
							$tabContainer.addClass('uk-hidden');
						}
					});
				});
				*/

				var quantityComponent = sandbox.getComponents('component_quantity', {context:$this});

				$this.on('click', '[data-delivery-btn]', function(e){
					e.preventDefault();
					var $form = $(this).closest('form');
					var queryParams = sandbox.utils.getQueryParams($form.serialize());
					if(queryParams.fgId == null){
						UIkit.modal.alert('배송 정보가 올바르지 않습니다.<br/>관리자에게 문의 하세요.');
						return;
					}

					UIkit.modal.confirm('수거완료처리를 진행하시겠습니까?', function(){
						sandbox.utils.ajax('/order-action/delivery', 'POST', {fgId:queryParams.fgId, csrfToken:queryParams.csrfToken} , function(data){
							var data = $.parseJSON( data.responseText );
							var msg = "";
							if(!_.isEmpty(data.errors)){
								msg = "수거완료처리중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.";
							}else{
								msg = "수거완료처리가 정상적으로 완료되었습니다.";
							}

							UIkit.modal.alert(msg).on("hide.uk.modal", function(){
								sandbox.setLoadingBarState(true);
								location.reload();
							});
						});
					});

				})

				$this.on('click', '[data-recall-btn]', function(e){
					e.preventDefault();
					var $form = $(this).closest('form');
					var queryParams = sandbox.utils.getQueryParams($form.serialize());
					if(queryParams.quantity === '' || queryParams.quantity <= 0){
						//UIkit.modal.alert('회수 수량을 선택해주세요');
						UIkit.modal.alert('회수 수량이 올바르지 않습니다.<br/>관리자에게 문의 하세요.');
						return;
					}

					var confirmMsg = '';
					var defaultMsg = '상품 회수처리를 진행하시겠습니까?'

					//당일 배송일때 귀책사유에 따라 msg 추가 
					if(queryParams.isProviderFlag == 'GOODSFLOW'){
						
						var returnReason = $form.find('input[name=returnReason]').val();
						var originalFulfillmentFee = $form.find('input[name=originalFulfillmentFee]').val();
						var responsibility; 
						var addMsg = ''; 

						if(queryParams.responsibility == 'SELLER'){
							responsibility = '판매자 귀책';		
							addMsg = '<span style="color:red;">(오늘도착 배송비 '+ originalFulfillmentFee + '원 포함되어 환불됩니다.)</span>'
						}else{
							responsibility = '구매자 귀책';	
						}

						confirmMsg = responsibility + ' - ' + returnReason + '<br/>';
						if(queryParams.responsibility == 'SELLER')confirmMsg += addMsg + '<br/>';
						confirmMsg += defaultMsg;
						
					}else{
						confirmMsg = defaultMsg;
					}

					UIkit.modal.confirm(confirmMsg, function(){
						//queryParams.itemId
						sandbox.utils.ajax('/order-action/collect', 'POST', {collectedItemId:queryParams.itemId, collectedItemQuantity:queryParams.quantity, csrfToken:queryParams.csrfToken} , function(data){
							var data = $.parseJSON( data.responseText );
							var msg = "";
							if(!_.isEmpty(data.errors)){
								msg = "회수 처리 중 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.";
								// 환불에서 에러 발생시
								if( !_.isEmpty(data.isCsr) ){
									msg = "회수 처리 후 환불 과정에서 에러가 발생 하였습니다.<br/>관리자에게 문의 하세요.";
								}
								//msg = String($.map(data.errors, function(item){ return item.message })).split(',').join('<br/>');
							}else{
								console.log( data.refundComplete );
								// 환불까지 완료되었으면, refundComplete TRUE, 회수만 됐다면 False
								if( data.refundComplete == true ){
									msg = "전체 상품 회수 및 환불처리가<br/>정상적으로 완료되었습니다."
								}else{
									msg = "상품 회수처리가 정상적으로 완료되었습니다.";
								}
							}

							UIkit.modal.alert(msg).on("hide.uk.modal", function(){
								sandbox.setLoadingBarState(true);
								location.reload();
							});
						});
					});
					/*
					sandbox.utils.promise({
						url:'/order-action/collect',
						method:'POST',
						data:{collectedItemId:'1212', collectedItemQuantity:queryParams.quantity, csrfToken:queryParams.csrfToken}
					}).then(function(data){
						UIkit.modal.alert('상품이 정상적으로 회수가 되었습니다.').on('hide.uk.modal', function() {
							sandbox.setLoadingBarState(true);
							location.reload();
						});
					}).fail(function(msg){
						if(msg) UIkit.modal.alert(msg);
					});
					*/
				});
			},
			showHideRecall:function(e){
				e.preventDefault();
				var $tabContainer = $(this).closest('form').find('.toggle-container');
				$tabContainer.toggleClass('uk-hidden');
			},

			loadDetailWithFgId:function(fulfillmentGroupId) {
				var startTime = ' 00:00:00';
				var endTime = ' 23:59:59';
				//http://localhost:8081/fms-return-collect?sortDirection=DESCENDING&term=month&stdDate=&etdDate=&sortProperty=fulfillmentGroupItem.orderItem.order.submitDate&fulfillmentGroupItem.orderItem.order.submitDate=2018.07.31+00%3A00%3A00%7C2018.08.31+23%3A59%3A59&rStatus=ALL&searchType=fulfillmentGroupItem.fulfillmentGroup&searchText=
				
				if( fulfillmentGroupId == null || fulfillmentGroupId == 'undefined'){
					Core.ui.modal.alert('바코드 정보를 다시 입력해 주시기 바랍니다.');
					return false;
				}else{
					// D로 시작하는지 체크
					if( Core.utils.string.startsWith(fulfillmentGroupId, 'D') == false){
						Core.ui.modal.alert('배송 ID를 입력하세요.');
						return false;
					}else{
						fulfillmentGroupId = fulfillmentGroupId.split('D').join('');
						fulfillmentGroupId = parseInt(fulfillmentGroupId); // 앞에 붙어있는 무의미한 0 을 제거하기 위함
					}
				}

				var url = "/fms-return-collect?term=month&rStatus=ALL&searchType=fulfillmentGroupItem.fulfillmentGroup&sortDirection=DESCENDING&sortProperty=fulfillmentGroupItem.orderItem.order.submitDate"
				url = url + "&fulfillmentGroupItem.orderItem.order.submitDate=" + moment(now, 'YYYY.MM.DD').subtract(1, 'months').format('YYYY.MM.DD') + startTime +  '|' + now + endTime ;
				url = url +  "&searchText=" + fulfillmentGroupId;
				url = url +  "&fulfillmentGroupItem.fulfillmentGroup=" + fulfillmentGroupId;

				location.href = url;

			}

		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-recalled]',
					attrName:'data-module-recalled',
					moduleName:'module_recalled',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			loadDetailWithFgId:Method.loadDetailWithFgId
		}
	});
})(Core);