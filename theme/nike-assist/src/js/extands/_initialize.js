// 현재 테마에서 기본적으로 호출 되어야 하는 스크립트
(function(Core){
	$(document).ready(function(){
		// <![CDATA[
		window.addEventListener('pageshow', function (event) {
			if (event.persisted) {
				Core.Loading.hide();
			}
		});
		$(":checkbox").attr("autocomplete", "off");
		$(":radio").attr("autocomplete", "off");

		if( _GLOBAL.CUSTOMER.ISSIGNIN && _GLOBAL.CUSTOMER.ADMIN_LOGIN_ID != null ){
			if(sessionStorage.getItem('isAdminUserIdSubmit') == null){
				sessionStorage.setItem('isAdminUserIdSubmit', true);
				location.href  = 'seamless://userinfo?loginId='+encodeURIComponent(_GLOBAL.CUSTOMER.ADMIN_LOGIN_ID);
			}
		}else{
			sessionStorage.removeItem('isAdminUserIdSubmit');
		}
		//]]>
	})
})(Core);