// Core 기본 utils 에 함수 추가
(function(Core){
	Core.Utils = $.extend(Core.Utils, {
		walkThrough:function(target, url){
			var $form = $("#tokenForm");
			var method = (target == 'site') ? 'GET' : 'POST';
			var action = (target == 'site') ? '/admin-redirect-to/site' : Core.Utils.contextPath + '/walk-through-admin'

			if( !_.isEmpty( $form )){
				$form.attr({"action": action, "method" : method });
				$form.append("<input name='redirectUrl' type='hidden' value='" + url + "' />");
				$form.submit();
			}
		}
	})
})(Core);