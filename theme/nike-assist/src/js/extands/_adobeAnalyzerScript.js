(function(Core){
	// 전역으로 사용될 기본 변수명
	var md = null;
	var queryString = "";
	dl = {};

	function init(){
		md = _GLOBAL.MARKETING_DATA();
		// context


		//기본 페이지 dl 셋팅
		// 기본 정보 이외의 추가 정보를 처리해야 하는 타입들
		switch( md.pageType ){

			case "search" :
				$.extend( dl, getSearchData());
			break;

		}

		$.extend( dl, getPageData());

		//console.log( dl );
		window._dl = dl;

		$(document).ready( function(){
			$("body").on("click", "[data-click-name]", function(e){
				//e.preventDefault();
				//var target = $(this).attr("target") || '_self';
				//var href = $(this).attr("href");
				var name = $(this).data("click-name");
				var area = $(this).data("click-area");
				var enable = $(this).data("click-enable");
				var endPoint = Core.getComponents('component_endpoint');
				//console.log(target);
				//console.log(href);
				//console.log(endPoint);
				if( enable != false ){
					endPoint.call('clickEvent', {area : area, name : name});
				}
				//alert("잠시 멈춤");
			})
		})
	}


	function getPageData(){
		var data = {};
			data.site_app_name = "nikeassistapp";
			data.page_division = "Commerce";
			data.country 	   = "kr";
			data.language 	   = "ko-KR";
			data.page_name     = getPageName();

			return data;
	}


	//검색 페이지 태깅.
	function getSearchData(){

		//  .kr/ko_kr/search?q=    <<<< 이런식으로 어디선가 검색어 없이 랜딩 되어 들어올 경우 스크립트 오류 발생
		//  때문에 분기처리 추가해줬음.
		if($("input[id='chk_search']").length > 0)	{
				var data = {};
				data.page_type = "search";
				var isResultFound = (md.searchInfo.totalCount > 0);

				data.onsite_search_phrase = md.searchInfo.keyword,
				data.onsite_search_result_page_type = ( isResultFound ? "onsite search results" : "no result found"),

				data.page_event = {}

				if( isResultFound ){
					data.page_event.onsite_search = true;
				}else{
					data.page_event.null_search = true;
				}
				return data;
		};
	}

	
	function getPageName(){
		// checkout 에서 키프트 카드, 적립금 등을 사용해서 url 이 바뀌더라도 checkout으로 처리
		if( md.pathName.indexOf("/giftcard/credit") == 0 || md.pathName.indexOf("/giftcard/apply") == 0 || md.pathName.indexOf("/giftcard/removeCredit") == 0 ){
			md.pathName = "/checkout";
		}

		if( md.pathName == "/"){
			   md.pathName = "/homepage";
		}
		//첫번째 / 제거
		var url = md.pathName.replace("/", "");
		return url.split("/");
	}



	function callTrackEvent( data ){
		if( _.isFunction( window._trackEvent )){
			_trackEvent( $.extend( {},  dl, data ) );
		}

		//Adobe Data 확인용 Break Point
		debug( $.extend( {},  dl, data ) );
		//console.log("break point");
	}

	function trackEvent( data ){
		//Adobe Data 확인용 Break Point
		debug( data );
	}

	function addEvent(){
		var endPoint = Core.getComponents('component_endpoint');
		var data = {};

			//클릭네임 클릭에어리어...기본
			endPoint.addEvent('clickEvent', function( param ){
				debug( "clickEvent" );

				data = {};
				data.link_name = "Click Links";
				data.click_name = param.name;

				// 슬리이더에서 배너 등록해서 사용시 처리
				if( param.area == "slider"){
					data.click_area = String(data.click_name).split("_")[0];
				}else{
					data.click_area = param.area;
				}
				data.page_event = {
					link_click : true
				}
				callTrackEvent( data );
			});

			//사이트 내에서 이벤트 호출.
			endPoint.addEvent('adobe_script', function( param ){
				callTrackEvent( param );
			});
	}



	function debug( data, alert ){
		//console.log( data );
		if( alert == true ){
			alert( data );
		}
	}
	Core.aa = {
		// 함수를 구분짓는것이 큰 의미는 없지만 추후 형태의 변화가 있을것을 대비해서 구분
		init : function(){
			init();
			addEvent();
		}
	}




})(Core);
