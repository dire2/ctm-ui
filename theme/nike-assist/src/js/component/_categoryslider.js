(function(Core){
	var CategorySlider = function(){
		'use strict';

		var $this, modal, args, endPoint;
		var setting = {
			selector:'[data-component-categoryslider]'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];
                $this = $(setting.selector);
                //change category primary image 
				$this.find('ul').on('mouseenter', 'li', function(e){
					e.preventDefault();					
                    var curImg = $(this).find("img").attr('src');                  
					curImg = curImg.replace("thumbnail", "browser");
                    //$(this).closest(".action-hover").find(".item-imgwrap").children("img").attr('src',curImg);
                    //over
                    _self.fireEvent('sliderOver', this, [curImg]);
				 });
	 
                /*$this.find('ul').on('mouseleave', 'li', function(e){
                    e.preventDefault();
                    var curImg = $(this).find("img").attr('src');
                });*/
                
                var count = $this.find('ul').children().length;  
                if(count <= 3){
                    $this.find("#btn_prev_smallthumb").hide();
                    $this.find("#btn_next_smallthumb").hide();
                }else{
                    $this.find("#btn_next_smallthumb").show();
                    $this.find("#small_slider_curr_pos").val(0);
                    $this.find("#small_slider_curr_page").val(1);

                    var pageCnt = Math.trunc(count / 3);            
                    if(count % 3 != 0){
                        $this.find("#small_slider_lastimg_cnt").val(count-(pageCnt*3));
                        pageCnt++;
                    }                
                    $this.find("#small_slider_page_cnt").val(pageCnt);               
                }

                //set ul width
                $this.find('ul').each(function() {
                    $(this).width($(this).find('li').length*90);
                });


                $this.find("#btn_prev_smallthumb").click(function(){
                    var currPos = Number($this.find("#small_slider_curr_pos").val());
                    var currPage = Number($this.find("#small_slider_curr_page").val());
                    var pageCnt = Number($this.find("#small_slider_page_cnt").val());
                    var lastImgCnt = Number($this.find("#small_slider_lastimg_cnt").val());
                    var goSliderPos = currPos;
                    if(currPage == 1){
                        console.log("Warning, first page !!!");
                        return false;
                    }
                    if(pageCnt == 2){
                            goSliderPos=0;
                    }else{
                        if(pageCnt == currPage){
                            if(lastImgCnt == 0)
                                goSliderPos = currPos - 183;
                            else
                                goSliderPos = currPos - 61*lastImgCnt;
                        }else{
                            goSliderPos = currPos - 183;
                        }
                    }                
                    currPage--;
                    $this.find("#small_slider_curr_page").val(currPage);
                    $this.find("#small_slider_curr_pos").val(goSliderPos);                    
                    $this.find('ul').css("transform","translate( -"+goSliderPos+"px, 0px) translateZ(0px)");
                    $this.find('ul').css("transition-duration","0.5s");
                    if(currPage == 1){
                        $this.find("#btn_prev_smallthumb").hide();
                        $this.find("#btn_next_smallthumb").show();
                    }    
                    console.log("go this position ::: " + goSliderPos);
                })
             
                 $this.find("#btn_next_smallthumb").click(function(){
                    var currPos = Number($this.find("#small_slider_curr_pos").val());
                    var currPage = Number($this.find("#small_slider_curr_page").val());
                    var pageCnt = Number($this.find("#small_slider_page_cnt").val());
                    var lastImgCnt = Number($this.find("#small_slider_lastimg_cnt").val());
                    var goSliderPos = currPos;
                    if(currPage == pageCnt){
                        console.log("Warning, last page !!!");
                        return false;
                    }
                    if((pageCnt-currPage) > 1 || ((pageCnt-currPage) == 1 && lastImgCnt == 0)){
                        goSliderPos=currPos+183;
                    }
                    if((pageCnt-currPage) == 1 && lastImgCnt != 0){
                        goSliderPos = currPos + 61*lastImgCnt;
                    }
                    currPage++;
                    $this.find("#small_slider_curr_page").val(currPage);
                    $this.find("#small_slider_curr_pos").val(goSliderPos);                    
                    $this.find('ul').css("transform","translate( -"+goSliderPos+"px, 0px) translateZ(0px)");
                    $this.find('ul').css("transition-duration","0.5s");
                    if(currPage == pageCnt){
                        $this.find("#btn_prev_smallthumb").show();
                        $this.find("#btn_next_smallthumb").hide();
                    }
                    console.log("next button, go this position ::: " + goSliderPos);
                })
				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_categoryslider'] = {
		constructor:CategorySlider,
		attrName:'data-component-categoryslider'
	}
})(Core);
