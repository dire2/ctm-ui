(function(Core){
	var OrderInfoForm = function(){
		'use strict';
		
		var setting = {
			selector:'[data-component-orderinfoform]'
		}
		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var args = arguments[0];
				var $this = $(setting.selector);
				var textComponent = Core.getComponents('component_textfield', {context:$this});
				var checkboxComponent = Core.getComponents('component_checkbox', {context:$this}, function(){
					this.addEvent('change', function(val){
						if(val){
							textComponent.setValue('ctmdummy@nike.com');
						}else{
							textComponent.setValue('');
						}
					});
				});
				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_orderinfoform'] = {
		constructor:OrderInfoForm,
		reInit:true,
		attrName:'data-component-orderinfoform'
	}

})(Core);
