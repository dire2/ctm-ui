(function(Core){
    var qrCode = function(){
        'use strict';

        var $this, args;
        var setting = {
            selector:'[data-component-qrcode]'
        }

        var Closure = function(){}
        Closure.prototype = {
            setting:function(){
                var opt = Array.prototype.slice.call(arguments).pop();
                $.extend(setting, opt);
                return this;
            },
            init:function(){
                // var _self = this;
                args = arguments[0];
                $this = $(setting.selector);

                function requestQRCodeUserId( param ){
                    BLC.ajax({
                        url:Core.Utils.contextPath + '/checkout/customerAllocationUsingQRCode',
                        type:"GET",
                        data:{"qrcode":param}
                    }, function(data){
                        if(!data){
                            UIkit.modal.alert("본인인증에 실패하였습니다.");
                        }else{
                            location.reload();
                        }
                    });
                }

                function packingRequestQRCodeUserId( param ){
                    var $form = $('*[data-selected-pickup-form="true"]');
                    var formQuery = $form.serialize();
                    var formObject = new URLSearchParams(formQuery);

                    var orderNumber = formObject.get('orderNumber');
                    var shippingType = formObject.get('shippingType');
                    var fgId = formObject.get('fgId');
                    var csrfToken = formObject.get('csrfToken');

                    $($form).removeAttr('data-selected-pickup-form');

                    var url = Core.Utils.contextPath + '/qr-certification';

                    Core.Utils.ajax(url, 'GET', {"orderNumber":orderNumber,"qrcode":param}, function(data) {
                        if(data.responseText == "\"true\"") {
							var pickupSubmitParams = {fgId:fgId,csrfToken:csrfToken};
							if(shippingType == 'ropis'){
								UIkit.modal.confirm('매장예약 주문입니다. 매장 내 POS 로 결제해야 합니다.<br /> 진행하시겠습니까?', function(){
    								Core.Utils.ajax('/order-action/pickup-delivery', 'POST', pickupSubmitParams, function(data){
    									var data = $.parseJSON( data.responseText );
    									if(!_.isEmpty(data.errors)){
    										UIkit.modal.alert(data.errors.message).on('hide.uk.modal', function() {
    											location.reload();
    										})
    									}else{
    										UIkit.modal.alert('정상적으로 처리되었습니다.').on('hide.uk.modal', function() {
    											location.reload();
    										});
    									}
    								});
								}, function(){
									UIkit.modal.alert('매장 내 POS로 결제 후 다시 시도해 주세요').on('hide.uk.modal', function() {
										location.reload();
									});
								})
							}else{
								Core.Utils.ajax('/order-action/pickup-delivery', 'POST', pickupSubmitParams, function(data){
									var data = $.parseJSON( data.responseText );
									if(!_.isEmpty(data.errors)){
										UIkit.modal.alert(data.errors.message).on('hide.uk.modal', function() {
											location.reload();
										})
									}else{
										UIkit.modal.alert('정상적으로 처리되었습니다.').on('hide.uk.modal', function() {
											location.reload();
										});
									}
								});
							}
                        } else {
                            UIkit.modal.alert("본인인증에 실패하였습니다.");
                        }
					},true);
                }

                function addEvent(){
                    endPoint.addEvent('qrScanComplete', function( param ){
                         requestQRCodeUserId( param );
                    });
                    endPoint.addEvent('packingQrScanComplete', function( param ){
                        packingRequestQRCodeUserId( param );
                    });
                }

                // event binding
                var endPoint;
                var clickQRCode = setting.selector.addEventListener('click', function(){
                    endPoint = Core.getComponents('component_endpoint');
                    if( typeof endPoint.listeners == "undefined" ){
                        addEvent();
                    }else{
                        if( typeof endPoint.listeners.qrScanComplete == "undefined" ){
                            addEvent();
                        }
                    }
                });

                return this;
            },
            callback:function(param){
                var endPoint = Core.getComponents('component_endpoint');
                endPoint.call( 'qrScanComplete', param);

                /*
                APP 내 User ID javascript 콜 예시 >
                    Core.getComponents('component_qrcode').callback(param);
                */
            },
            callbackPacking:function(param){
                var endPoint = Core.getComponents('component_endpoint');
                endPoint.call( 'packingQrScanComplete', param);

                /*
                APP 내 User ID javascript 콜 예시 >
                    Core.getComponents('component_qrcode').callbackPacking(param);
                */
            }
        }

        Core.Observer.applyObserver(Closure);
        return new Closure();
    }

    Core.Components['component_qrcode'] = {
        constructor:qrCode,
        reInit:true,
        attrName:'data-component-qrcode'
    }
})(Core);