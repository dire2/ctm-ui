(function(Core){
	'use strict';
	Core.register('module_minicart', function(sandbox){
		var args;
		var endPoint;
		var Method = {
			$that:null,
			$closeBtn:null,

			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

				$this.on('click', '[data-remove-item]',  function(e){
					e.preventDefault();
					var model = $(this).closest(".order-list").find("input[name='model']").val();
					var name = $(this).closest(".order-list").find("[data-eng-name]").data("eng-name");
					Method.removeItem( $(this).attr('href'), model, name);
				});

				$this.on('click', '[data-checkout-btn]', function(e){
					e.preventDefault();
					var url = $(this).attr('href');
					var qty = 0;
					var info = $this.find('.cart-order_list .order-list');
					var itemList = [];

					$.each( info, function( index, productData ){
						var id = $(productData).find( '[data-id]').data('id');
						var model = $(productData).find( '[data-model]').data('model');
						var name = $(productData).find( '[data-name]').data('name');
						var retailPrice = $(productData).find( '[data-retail-price]').data('retail-price');
						var salePrice = $(productData).find( '[data-sale-price]').data('sale-price');
						var quantity = $(productData).find( '[data-quantity]').data('quantity'); 
						itemList.push({
							id : id,
							model : model,
							name : name,
							price : salePrice,
							retailPrice : retailPrice,
							quantity : quantity
						})
					})

					$.each(itemList, function(index, item){
						qty += parseInt(item.quantity);
					});
					
					if(qty > 2){
						UIkit.modal.confirm("장바구니 전체 " + qty + "개의 상품을 일반택배로 주문하시겠습니까?", function() {
							Method.submitCheckout(itemList, url);
						});
					}else{			   
						Core.Utils.ajax(Core.Utils.contextPath + '/cart/checkSamedayDelivery', 'POST', '', function(data){
							var response = data.responseText;
								if(response == 'true'){
									var samedayDeliveryFee = $('#samedayDeliveryFee').val().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									UIkit.modal.confirm("오늘 도착 상품 " + qty + "개를 주문하시겠습니까? <br/>배송비는 " + samedayDeliveryFee + "원이 적용됩니다.", function() {
										Method.submitCheckout(itemList, url);
									});
								}else{
									UIkit.modal.confirm("장바구니 전체 " + qty + "개의 상품을 일반택배로 주문하시겠습니까?", function() {
										Method.submitCheckout(itemList, url);
									});
								}
						}, true, true);
					}
				});

				$this.on('click', '[data-keep-shopping]', function(e){
					// e.preventDefault();
					Method.hide();
				});
			},
			submitCheckout:function(itemList, url){
				endPoint.call('checkoutSubmit',{ itemList : itemList });
				location.href = url;
			},
			show:function(){
				//UIkit.offcanvas arguments type : selector:string, option:object
				UIkit.offcanvas.show('#minicart', {target:'#minicart', mode:'slide'});
			},
			hide:function(){
				//uikit 사용으로 hide는 필요없는 상황
				UIkit.offcanvas.hide('#minicart');
			},
			update:function( callback ){
				var obj = {
					'mode':'template',
					'templatePath':'/cart/partials/miniCart',
					'resultVar':'cart',
					'cache':new Date().getTime()
				}

				sandbox.utils.ajax(sandbox.utils.contextPath + '/processor/execute/cart_state', 'GET', obj, function(data){
					Method.$that.empty().append(data.responseText);
					var miniCartItem = $(args.miniCartCnt);
					var $data = $(data.responseText);
					var itemSize = $data.filter('input[name=itemSize]').val();
					var cartId = $data.filter('input[name=cartId]').val();

					miniCartItem.text(itemSize);

					if( itemSize == 0 ){
						miniCartItem.addClass('empty');
					}else{
						miniCartItem.removeClass('empty');
					}

					if( callback ){
						callback( { cartId : cartId} );
					}
					Method.show();
				});
			},
			removeItem:function( url, model, name ){
				// error 체크와 ajax 로딩 처리 추가 되야 함
				UIkit.modal.confirm("상품을 삭제 할까요?", function(){
					sandbox.utils.ajax(url, 'GET', {}, function(data){
						var param = sandbox.utils.url.getQueryStringParams( url );
						param.model = model;
						param.name = name;
						endPoint.call( 'removeFromCart', param );
						Method.update();
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-minicart]',
					attrName:'data-module-minicart',
					moduleName:'module_minicart',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			show:Method.show,
			hide:Method.hide,
			update:Method.update
		}
	});
})(Core);
