(function(Core){
	Core.register('module_launchproduct', function(sandbox){
		var $that;
		// var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				$this = $(this);
				var args = arguments[0];

                //상품정보 스크롤 이동
                var _conH = $('.lc-prd-conts .product-info').height();
                var _conBox = $('.lc-prd-conts .lc-prd-images').height();
                var _winH = $(window).height();
                var _conOT = $('.lc-prd-conts').offset().top;
                var _winST = $(window).scrollTop();
				var _pt = (_winH - _conH)/2;
				
				$(window).load(function(){
					Chesk();
				});
				$(window).scroll(function(){
					Chesk();
				});
				$(window).resize(function(){
					Chesk();
				});
		
				function Chesk (){	
					$('.lc-prd-conts .prd-con-wrap').css({'paddingTop': _pt});
					posCon();
				}
				
				function posCon(){
					// _conH = $('.lc-prd-conts .prd-con-wrap').height();
					_conH = $('.lc-prd-conts .product-info').height();
					_conBox = $('.lc-prd-conts .lc-prd-images').height();
					_winH = $(window).height();
					_conOT = $('.lc-prd-conts').offset().top;
					_winST = $(window).scrollTop();
					_pt = (_winH - _conH)/2;
		
					// console.log(_conH, _conBox, _winH, _conOT, _winST, _pt);
					
					$('.lc-prd-conts .prd-con-wrap').css({'paddingTop': _pt});
					if(_winST > _conOT && _winST < _conOT + _conBox - _winH){
						$('.lc-prd-conts').find('.prd-con-wrap').addClass('fix').css({'top': 0});
					} else if(_winST > _conOT + _conBox - _winH){
						// $('.lc-prd-conts').find('.prd-con-wrap').removeClass('fix').css('top', _conBox - _conH - _winH/2);
						$('.lc-prd-conts').find('.prd-con-wrap').removeClass('fix').css('top', _conBox - _winH);
					} else {
						$('.lc-prd-conts').find('.prd-con-wrap').removeClass('fix').css('top', 0);
					}
				}

				//입고알림 문자받기 show or hide
				Method.displayRestockAlarm(args);
			},
			displayRestockAlarm:function(args){
				if(args && undefined != args.skudata){
					for(var index = 0; args.skudata.length > index; index++){
						if(0==args.skudata[index].quantity){
							//enable 입고알림문자받기
							$('#set-restock-alarm').show();
							return;
						}
					}
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-launchproduct]',
					attrName:'data-module-launchproduct',
					moduleName:'module_launchproduct',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
