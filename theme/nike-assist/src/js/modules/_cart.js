(function(Core){
	Core.register('module_poscart', function(sandbox){
		var $this, endPoint;
		var Method = {
			moduleInit:function(){

				// modal layer UIkit 사용
				$this = $(this);
				var modal = UIkit.modal('#common-modal');
				endPoint = Core.getComponents('component_endpoint');

				var addonComponents = sandbox.getComponents('component_addon_product_option', {context:$this, optionTemplate:'#order-addon-sku-option'}, function(i){
					var _self = this;

					this.addEvent('submit', function(data){
						var $this = $(this);

						UIkit.modal.confirm('장바구니에 상품을 담으시겠습니까?', function(){
							var itemRequest = {};
							var addToCartItems = _self.getChildAddToCartItems();
							var keyName='';

							for(var i=0; i<addToCartItems.length; i++){
								keyName = 'childOrderItems[' + i + ']';
								for(var key in addToCartItems[i]){
									itemRequest['childAddToCartItems['+i+'].'+key] = addToCartItems[i][key];
								}
							}

							//애드온 orderId 알아야함
							itemRequest['addOnOrderId'] = _self.getAddOnOrderId();
							itemRequest['isAddOnOrderProduct'] = true;
							itemRequest['csrfToken'] = $this.closest('form').find('input[name=csrfToken]').val();

							BLC.ajax({
								url:$this.closest('form').attr('action'),
								type:"POST",
								dataType:"json",
								data:itemRequest
							}, function(data){
								if(data.error){
									UIkit.modal.alert(data.error);
								}else{
									location.href = sandbox.utils.url.getCurrentUrl();
								}
							});
						});
					});
				});

				//옵션 변경
				$(this).on('click', '.optchange-btn', function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var $parent = $(this).closest('.product-opt_cart');
					var id = $parent.find('input[name=productId]').attr('value');
					var quantity = $parent.find('input[name=quantity]').attr('value');
					var url = $parent.find('input[name=producturl]').attr('value');
					var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
					var size = $parent.find('input[name$=SIZE]').attr('value');
					var obj = {'qty':quantity, 'orderitemid':orderItemId, 'quickview':true, 'size':size}
 
					$parent.find('[data-opt]').each(function(i){
						var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
						for(var key in opt){
							obj[key] = opt[key];
						}
					});

					sandbox.utils.ajax(url, 'GET', obj, function(data){
						var domObject = $(data.responseText).find('#quickview-wrap');
						$(target).find('.contents').empty().append(domObject[0].outerHTML)
						$(target).addClass('quickview');
						sandbox.moduleEventInjection(domObject[0].outerHTML);
						modal.show();
					});

				});

				//카트에 추가
				$(this).on('click', '.addcart-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem.call(this, {type:'addcart'});
				});


				//수량 변경. - 기존 ctm소스 삭제대상
				$(this).on('change', '.sel-box', function(e){
					e.preventDefault();

					var $parent = $(this).closest('.product-opt_cart');

					var id = $parent.find('input[name=productId]').attr('value');
					var quantity = $parent.find('select[id=cart-qty]').val()
					var url = sandbox.utils.contextPath + "/cart/modify";
					var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
					var productId = $parent.find('input[name=productId]').attr('value');
					var sessionId = $parent.find('input[name=csrfToken]').attr('value');
					var obj = {'quantity':quantity, 'removeCartItemId':orderItemId, 'csrfToken':sessionId, 'productId':productId}

					$parent.find('[data-opt]').each(function(i){
						var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
						for(var key in opt){
							obj['itemAttributes['+ $(this).attr('data-attribute-name') +']'] = opt[key];
						}
					});

					BLC.ajax({
						url:url,
						type:"POST",
						dataType:"json",
						data:obj
					}, function(data, extraData){
						console.log(data);
						if(data.errorMessage != null){
							//alert(data.error);
							//data.error 는 '장바구니 옵션변경 시 에러가 발생했습니다.' 로 노출 된다.
							//alert(data.error);
							UIkit.modal.alert(data.error).on('hide.uk.modal', function() {
								window.location.assign(sandbox.utils.contextPath + '/cart');
							});
						} else {
							UIkit.modal.alert("수량이 변경되었습니다.").on('hide.uk.modal', function() {
								window.location.assign(sandbox.utils.contextPath + '/cart');
							});
						}
					});
				});

				//당일배송 구매하기 선택 시
				$(this).on('click', '.btn_tooltipShipping', function(e){
					$(this).parents('dl').next('.tooltipShipping').toggle();
					return false;
				});
				$(this).on('click', '.cart-btn-order', function(e){
				   e.preventDefault();				   
				   var qty = 0;
				   var qtyArray = $('.product-opt_cart').find('input[name="quantity"]');
				   $.each(qtyArray, function(index, item){
					   qty += parseInt(item.value);
				   });
				   
				   if(qty > 2){
						UIkit.modal.confirm("장바구니 전체 " + qty + "개의 상품을 일반택배로 주문하시겠습니까?", function() {
							location.href = $('.cart-btn-order').attr('href');
						});
				   }else{			   
						Core.Utils.ajax(Core.Utils.contextPath + '/cart/checkSamedayDelivery', 'POST', '', function(data){
							  var response = data.responseText;
								if(response == 'true'){
									var samedayDeliveryFee = $('#samedayDeliveryFee').val().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									UIkit.modal.confirm("오늘 도착 상품 " + qty + "개를 주문하시겠습니까? <br/>배송비는 " + samedayDeliveryFee + "원이 적용됩니다.", function() {
										location.href = $('.cart-btn-order').attr('href');
									});
								}else{
									UIkit.modal.confirm("장바구니 전체 " + qty + "개의 상품을 일반택배로 주문하시겠습니까?", function() {
										location.href = $('.cart-btn-order').attr('href');
									});
								}
						  }, true, true);
				   }	   
				});

				//장바구니에서 주문하기 checkquantity 사용안함
				/*
				$(this).on('click', '.cart-btn-order', function(e){
					e.preventDefault();
					var checkQuantityUrl = sandbox.utils.contextPath + "/cart/checkquantity"
					// if(addonComponents){
					// 	e.preventDefault();

					// 	if(sandbox.utils.getValidateChk(addonComponents)){
					// 		location.href = $(this).attr('href');
					// 	}
					// }

					var checkQuantityStr = "";
					var checkOuantityList = [];
					$(".product-opt_cart").each(function(idx){
						var skuId = $(this).find("input[name=skuId]").val();
						var orderItemQuantity = $(this).find("input[name=quantity]").val();
						var itemName = $(this).find("a.tit").data("name");
						checkQuantityStr += "&skuId="+skuId;
						checkOuantityList.push({skuId : skuId, orderItemQuantity : orderItemQuantity, itemName:itemName});
					});

					//console.log("checkOuantityList :" +checkOuantityList);


					sandbox.utils.ajax(checkQuantityUrl, 'GET', checkQuantityStr, function(data){
						if( data.status == 200){
							var quantityInfo = JSON.parse(data.responseText)
							//console.log("quantityInfo : "+data.responseText);
							var isQuantityShortage = false; //수량부족 아이템 존재 여부
							var shortageItem = [];
							var hasSoldOutItem = false; //품절 아이템 존재 여부
							$(checkOuantityList).each(function(idx){
								var skuId = checkOuantityList[idx].skuId;
								var orderItemQuantity = checkOuantityList[idx].orderItemQuantity;

								var dbMaxQuantity = quantityInfo[skuId];
								if(dbMaxQuantity == 0){
									hasSoldOutItem = true;
								}else if(orderItemQuantity > dbMaxQuantity){
									isQuantityShortage = true;
									checkOuantityList[idx].dbMaxQuantity = dbMaxQuantity;
									shortageItem.push(checkOuantityList[idx]);
								}

							});

							if(hasSoldOutItem){
								//alert("[품절] 상품이 존재합니다. 삭제 후, 진행해 주세요.");
								UIkit.modal.alert("[품절] 상품이 존재합니다. 삭제 후, 진행해 주세요.")
									.on('hide.uk.modal', function() {
									window.location.assign(sandbox.utils.contextPath + '/cart');
								});
								return false;
							}

							if(isQuantityShortage===true){
								var msg = "<p align='center'>[상품 부족]</p>";
								$(shortageItem).each(function(sIdx){
									msg += "<p align='center'>" + shortageItem[sIdx].itemName + "</p>";
									msg += "<p align='center'> 주문 가능 수량 : " + shortageItem[sIdx].dbMaxQuantity + "개</p>";
								});
								UIkit.modal.alert(msg)
									.on('hide.uk.modal', function() {
									window.location.assign(sandbox.utils.contextPath + '/cart');
								});
								return false;
							}
							location.href = sandbox.utils.contextPath + "/checkout?edit-shipping=true";
						}
					});
				});
				*/

				//카트 전체 삭제
				$(this).on('click', '.btn-delete-all', function(e){
					// var cartListSize = $('.product-opt_cart').size();

					// UIkit.modal.confirm('삭제하시겠습니까?', function(){
					// 	for (var i = 0; i < cartListSize; i++) {
					// 		var $cartList = $('.product-opt_cart').eq(i);
					// 		var orderItemId = $cartList.find('input[name=orderItemId]').attr('value');
					// 		var productId = $cartList.find('input[name=productId]').attr('value');

					// 		var url = '/cart/remove';
					// 		var obj = {'orderItemId':orderItemId, 'productId':productId}

					// 		Core.Loading.show();

					// 		BLC.ajax({
					// 			async : false,
					// 			url:url,
					// 			type:"GET",
					// 			data:obj
					// 		}, function(){
					// 			_.delay(function(){
					// 			}, 1000);
					// 		});
					// 	};

					// 	_.delay(function(){
					// 		window.location.href = '/cart';
					// 	}, 1000);
					// }, function(){},
					// {
					// 	labels: {'Ok': '확인', 'Cancel': '취소'}
					// });

					e.preventDefault();
					var url = $(this).attr('href');
					console.log(url);

					UIkit.modal.confirm('장바구니에 담긴 상품을 모두 삭제하시겠습니까?', function(){

						Core.Loading.show();

						BLC.ajax({
							url: url,
							type: "GET"
						}, function(data) {
							if (data.error ) {
								UIkit.modal.alert(data.error);
							} else {
								window.location.reload();

								_.delay(function(){
								}, 1000);
							}
						});

						_.delay(function(){
							window.location.href = sandbox.utils.contextPath + '/cart';
						}, 1000);
					}, function(){},
					{
						labels: {'Ok': '확인', 'Cancel': '취소'}
					});
				});
			},
			addItem:function(opt){

				var $parent = $(this).closest('.product-opt_cart');
				var id = $parent.find('input[name=productId]').attr('value');
				var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
				var quantity = $parent.find('input[name=quantity]').attr('value');
				var sessionId = $(this).siblings().filter('input[name=csrfToken]').val();
				var obj = {'productId':id, 'orderItemId':orderItemId ,'quantity':quantity, 'csrfToken':sessionId}
				var url = $(this).closest('form').attr('action');
				var method = $(this).closest('form').attr('method');

				$parent.find('[data-opt]').each(function(i){
					var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
					for(var key in opt){
						obj['itemAttributes['+ $(this).attr('data-attribute-name') +']'] = opt[key];
					}
				});

				sandbox.utils.ajax(url, method, obj, function(data){
					var jsonData = sandbox.rtnJson(data.responseText, true) || {};
					var url = sandbox.utils.url.removeParamFromURL( sandbox.utils.url.getCurrentUrl(), $(this).attr('name') );

					if(jsonData.hasOwnProperty('error')){
						$.cookie('pageMsg', jsonData.error);
					}
					window.location.assign(url);
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-poscart]',
					attrName:'data-module-poscart',
					moduleName:'module_poscart',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
/* ASSIST_modules_file_cart */
