(function(Core){
	Core.register('module_order_delivery', function(sandbox){
		var $this=null,
			args=null;

		var Method = {
			$popAddressModal :null,
			$beforeAddress:null,
			$newAddress:null,
			isNewAddress:false,
			isSelectAddress:false,
			isEditedAddress:false,
			moduleInit:function(){
				$this = $(this);
				args = arguments[0] || {};

				var isExpiredSession = false;
				if(Object.keys(args).length>0 && args.maxInactiveInterval>0) {
					var expireTime = new Date().getTime()+args.maxInactiveInterval*1000;
					var checkSessionTimeOut = function() {
						if(new Date().getTime()>=expireTime) {
							UIkit.modal.alert("로그인 만료로 로그인 페지이로 이동됩니다.").on('hide.uk.modal', function() {
								Core.Loading.show();
								window.location.reload();
							});
							return true;
						} else {
							return false;
						}
					}
					var checkSession = setInterval(function() {
						if(checkSessionTimeOut()==true) {
							clearInterval(checkSession);
						}
					}, 60000);//1 minute
				}

				Method.$popAddressModal = UIkit.modal("#popup-customer-address", {modal: false});
				/*Method.$beforeAddress = $this.find('[data-before-address]');
				Method.$newAddress = $this.find('[data-new-address]');*/

				// 배송지 타입이 없는건 비회원이라는 뜻
				Method.isNewAddress = ( $this.find('[data-address-type]').length == 0 ) ? true : false;

				var $personalMsg = $(this).find('[name="personalMessageText"]');
				var $personalSelect = $(this).find('select[name="selectPersonalMessage"]');
				// select가 안되어있고 msg가 있으면 직접입력 처리
				if( $personalMsg.val() != "" && $personalSelect.val() =="" ){
					$personalSelect.val('dt_1');
					$personalMsg.closest(".input-textfield").removeClass('uk-hidden');
				}

				var $personalMsgSelect = sandbox.getComponents('component_select', {context:$this}, function(){
					this.addEvent("change", function(){
						var value = $(this).val();
						if(value == ''){
							$personalMsg.val('');
							$personalMsg.closest(".input-textfield").addClass('uk-hidden');
						}else if(value == 'dt_1'){
							// 직접입력일 경우
							$personalMsg.val('');
							$personalMsg.closest(".input-textfield").removeClass('uk-hidden');
						}else{
							//$personalMsg.val( $(this).find("option:selected").val() + "||" + $(this).find("option:selected").text() );
							$personalMsg.val( $(this).find("option:selected").text());
							$personalMsg.closest(".input-textfield").addClass('uk-hidden');
						}
					});
				});

				Method.isSelectAddress = ( $(this).find('[name="isSearchAddress"]').val() == 'true' );
				var $zipCodeInput = $(this).find('[name="address.postalCode"]');
				var $zipCodeDisplay = $(this).find('[data-postalCode]');

				//validate check
				sandbox.validation.init( $this.find('#shipping_info form'));

				$('input[name="address.addressLine1"]').on('change', function (e){
					Method.isEditedAddress = true; //우편번호 주소 입력 부가 언제든지 수정가능 한 상태이므로 수정된다면 한번 더 체크
				});

				// 배송지 정보 submit 시
				$this.find('[data-order-shipping-submit-btn]').on('click', function(e){
					e.preventDefault();
					
					//-- 당일배송 결제 전 체크로직
	               var samedayCheck = false;
	               var samedayDeliveryChecked = $('input[name=samedayDeliveryChecked]').val();
	               if(samedayDeliveryChecked == 'true'){
	                  var timecheck = false;
	                  var addressCheck = false;
	                  timecheck = samedayTimeCheck();
	                  if(timecheck){ 
	                     addressCheck = samedayAddressCheck();
	                  }
	                  if(addressCheck){
	                     samedayCheck = samedayInventoryCheck();
	                  }                     
	                  //당일배송 결제하기 선택 시 시간 체크
	                  function samedayTimeCheck(){
	                     var samedayArray = $('.product-opt_cart').find('input[name=samedayDelivery]');                     
	                     $.each(samedayArray, function (index, item) {
	                           var samedayItemId = item.attributes['samedayitem'].value;                  
	                           var fulfillLocationArray = $('input[name='+samedayItemId+']');
	                           
	                           $.each(fulfillLocationArray, function (index, item) {//당일배송 fg가 다른 경우 추가될때를 대비하여 each로 변경
	                              var fulfillLocation = item.attributes['locationId'].value;                        
	                              if(fulfillLocation != 'undefined'){   
	                                 var paramObj = {
	                                    'locationId':fulfillLocation
	                                 }
	                                 BLC.ajax({
	                                       url:Core.Utils.contextPath + '/checkout/checkSamedayDeliveryByLocation',
	                                       type:"GET",
	                                       async:false,
	                                       data:paramObj
	                                    }, function(data){
	                                       if(!data){
	                                          timecheck = false;
	                                          UIkit.modal.alert("오늘 도착 서비스 시간이 아닙니다. <br/>일반택배를 이용하시거나 다시 상품을 확인해주세요.");
	                                          return false;
	                                       }else{
	                                          timecheck = true;
	                                       }
	                                    });
	                                    if(!timecheck){                                 
	                                       return false;
	                                    }
	                              }
	                           });                                                
	                           if(!timecheck){
	                              return false;
	                           }                   
	                     });
	                     return timecheck;
	                  }
	                  //당일배송 결제하기 선택 시 지역 체크
	                  function samedayAddressCheck(){
						  var toAddress = $('[name="address.postalCode"]').val();
	                      if(toAddress != 'undefined' && toAddress.length > 0){   
	                        var paramObj = {
								'toAddress':toAddress
	                        }
	                        BLC.ajax({
	                            url:Core.Utils.contextPath + '/checkout/checkSamedayDeliveryAddressByGoodsflow',
	                            type:"POST",
	                            async:false,
	                            data:paramObj
	                         }, function(data){
								if(!data.result){
									UIkit.modal.alert(data.resultMsg);
	                               return false;
	                            }else{
	                            addressCheck = true;
	                          }
	                         });
	                      }else{
	                         UIkit.modal.alert("주소를 입력해주세요.");
	                         return false;
	                      }
	                   return addressCheck;
	                  }
	                  //당일배송 결제하기 선택 시 재고 체크
	                  function samedayInventoryCheck(){
	                     var orderId = $('#orderId').val();
	                     if(orderId != 'undefined'){
	                       var paramObj = {
	                         'orderId':orderId
	                       }
	                       BLC.ajax({
	                           url:Core.Utils.contextPath + '/checkout/checkSamedayDeliverySkuInventoryByOrder',
	                           type:"GET",
	                           async:false,
	                           data:paramObj
	                        }, function(data){
	                           if(!data){
	                              UIkit.modal.alert("현재 오늘 도착 가능한 재고가 없습니다. <br/>다시 상품을 확인해주세요.");
	                              return false;
	                           }else{
	                              samedayCheck = true;
	                           }
	                        });
	                     }
	                     return samedayCheck;
	                  }                     
	               }else{ //당일배송 아니면 true
	                  samedayCheck = true;
	               }               
					//-- 당일배송 결제 전 체크로직 

					if(samedayCheck){
						var $form = $(this).closest('form');
						Method.inputValidate($form).then(function($form){
							if (!Method.isSelectAddress || Method.isEditedAddress) {
								UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
								return;
							}
							var queryParams = sandbox.utils.getQueryParams($form.serialize());
							$form.submit();
						}).fail(function(msg){
							console.log(msg);
						});
					}
				});

				
				// 배송지 선택 버튼
				$this.closest('[data-order-tab]').find('[data-customer-address-btn]').on('click', function(e){
					e.preventDefault();
					Method.showCustomerAddressList();
				});


				//postcode-search daum 우편번호 API
				/*var addressModal = UIkit.modal("#popup-daum-postcode", {modal: false});
				var element_wrap = document.getElementById('daum-postcode-container');
				$this.find('.btn_search').on('click', function(e){
					e.preventDefault();
					daum.postcode.load(function(){
						addressModal.show();
						new daum.Postcode({
							oncomplete: function(data) {
								var zipcode = data.zonecode;
								var doro = data.address;

								$this.find('#addressline1').val(doro);
								$this.find('#addressline2').focus();

								$zipCodeInput.val( zipcode );
								$zipCodeDisplay.text( zipcode );
								$zipCodeDisplay.parent().removeClass("uk-hidden");
								Method.isSelectAddress = true;

								//postcode 모달창을 닫아주고 addressline2로 포커스이동
								addressModal.hide();
								$this.find('#addressline2').focus();
							},
							// 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
							onresize : function(size) {
								element_wrap.style.height = $(window).height() - 46 + 'px'; //size.height+'px';
							},
							width : '100%',
							hideMapBtn : true,
							hideEngBtn : true
						}).embed(element_wrap);
					});
				});*/


				Method.isSelectAddress = ( $(this).find('[name="isSearchAddress"]').val() == 'true' );
				var $zipCodeInput = $(this).find('[name="address.postalCode"]');
				var $zipCodeDisplay = $(this).find('[data-postalCode]');
				var deliverySearch = sandbox.getComponents('component_searchfield', {context:$this, selector:'.search-field', resultTemplate:'#address-find-list'}, function(){
					// 검색된 내용 선택시 zipcode 처리
					this.addEvent('resultSelect', function(data){
						var zipcode = $(data).data('zip-code5');
						var city = $(data).data('city');
						var doro = $(data).data('doro');

						this.getInputComponent().setValue(city + ' ' + doro);

						$zipCodeInput.val( zipcode );
						$zipCodeDisplay.text( zipcode );
						$zipCodeDisplay.parent().removeClass("uk-hidden");
						Method.isSelectAddress = true;
						Method.isEditedAddress = false;
					});
				});


				//기존에 입력된(orderInfoFrom) 정보 가져와 세팅
				$this.find('[data-phone]').val($('.phone').text()).focusout();

				// 배송지 입력 타입 버튼 선택시
				/*$this.find('[data-address-type]').on('show.uk.switcher', function(){
					Method.isSelectAddress = deliverySearch.getValidateChk();
					Method.updateAddressInput();
				});*/

			},
			addCustomerAddressEvent: function () {
				// 배송지 선택 모듈 select 이벤트 호출( 배송지 선택했을때 호출됨 )
				var customerAddressComponent = sandbox.getComponents('component_customer_address', { context: $this }, function () {
					this.addEvent('select', function (address) {
						Method.updateCustomerAddress(address);
						if (Method.$popAddressModal.isActive()) {
							Method.$popAddressModal.hide();
						}
					});
				});
			},
			showCustomerAddressList: function () {
				var customerId = $this.find('#customerId').val();
				if (customerId == null) {
					UIkit.modal.alert("고객정보가 올바르지 않아 주소를 불러올 수 없습니다.");
					return;
				}
				var obj = {
					'mode': 'template',
					'templatePath': '/modules/customerAddress',
					'needCustomerAddress': 'Y',
					'customerId': customerId
				}
				sandbox.utils.ajax(sandbox.utils.contextPath + '/processor/execute/customer_info', 'GET', obj, function (data) {
					var appendHtml = $(data.responseText);
					Method.$popAddressModal.element.find('.contents').empty().append(appendHtml[0].outerHTML);
					sandbox.moduleEventInjection(appendHtml[0].outerHTML);
					Method.addCustomerAddressEvent();
					Method.$popAddressModal.show();
				});
			},
			inputValidate:function($form){
				var deferred = $.Deferred();
				var arrIsValid = [];
				var isTrue = true;

				sandbox.validation.validate($form);
				if(!sandbox.validation.isValid($form)){
					isTrue = false;
				}

				if(isTrue) deferred.resolve($form);
				else deferred.reject('input.form.valid.fail');

				return deferred.promise();
			},

			updateCustomerAddress:function( data ){
				console.log(data);
				var $target = Method.$beforeAddress;
				if( $this.find('[data-user-name]').length > 0 ){
					$this.find('[data-user-name]').html($.trim(data.fullName));
				}

				if( $this.find('[data-phone]').length > 0 ){
					$this.find('[data-phone]').html($.trim(data.phoneNumber));
				}

				if( $this.find('[data-postalCode]').length > 0 ){
					$this.find('[data-postalCode]').html($.trim(data.postalCode));
				}

				/*
				if( $target.find('[data-address]').length > 0 ){
					$target.find('[data-address]').html($.trim(data.addressLine1 + ' ' + data.addressLine2));
				}
				*/

				// 변경된 값 input 에 적용
				$this.find('input[name="address.fullName"]').val($.trim(data.fullName));
				$this.find('input[name="address.phonePrimary.phoneNumber"]').val($.trim(data.phoneNumber));
				$this.find('input[name="address.addressLine1"]').val($.trim(data.addressLine1));
				$this.find('input[name="address.addressLine2"]').val($.trim(data.addressLine2));
				$this.find('input[name="address.postalCode"]').val($.trim(data.postalCode));
			},

			updateAddressInput:function(){
				/*if( Method.$beforeAddress.hasClass('uk-active')){
					Method.isNewAddress = false;
					Method.$beforeAddress.find('input').attr('disabled', false );
					Method.$newAddress.find('input').attr('disabled', true );
				}else{
					Method.isNewAddress = true;
					Method.$beforeAddress.find('input').attr('disabled', true );
					Method.$newAddress.find('input').attr('disabled', false );
				}*/
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-delivery]',
					attrName:'data-module-order-delivery',
					moduleName:'module_order_delivery',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
