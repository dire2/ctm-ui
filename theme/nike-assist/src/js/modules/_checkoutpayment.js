(function(Core){
	Core.register('module_order_payment', function(sandbox){
		var endPoint;
		var Method = {
			moduleInit:function(){
				endPoint = Core.getComponents('component_endpoint');
				var $this = $(this);
				Method.$that = $this;
				Method.tempPayFlg = false;
				// vat 변경 적용을 위한 날짜
				$this.find('[data-checkout-btn]').on("click", function(e){
					e.preventDefault();
					if(!$(this).hasClass('disabled')){
						Method.tempPayFlg = $(this).data("temp-flg");
						Method.checkout();
						$(this).addClass('disabled');
					}
				});

				$('#step-back-link').click(function(){
					//window.location.assign( sandbox.utils.contextPath + '/checkout?edit-shipping=true&save-adit-shipping=true&stateType=FORM');
					history.back();
				});

			},
			checkout:function(e){
				var isCheckoutAgree = Method.$that.find('[name="isCheckoutAgree"]').is(':checked');
				var _self = $(this);

				// 결제 방법에 따른 처리
				var $activeItem = Method.$that.find('[name="payData"].active');

				if( !isCheckoutAgree ){
					UIkit.modal.alert("상품, 가격, 할인, 배송정보에 동의해주세요");
					return;
				}

				sandbox.utils.promise({
					url: sandbox.utils.contextPath + '/checkout/request',
					type:'GET'
				}).then(function(data){
					sandbox.setLoadingBarState(true);
					if(!data.isError){
				    	endPoint.call("orderSubmit");
						//fdk 모듈
						if( $activeItem.data('provider') == 'FDK' ){
							Method.checkoutFdk( $activeItem, data);
						}else{
							UIkit.modal.alert('provider FDK 설정이 필요합니다.');
						}
					}else{
						Core.Loading.hide();
						UIkit.modal.alert(data._global).on('hide.uk.modal', function(){
							location.href = sandbox.utils.contextPath + '/cart';
						});
				    }
				}).fail(function(msg){
					sandbox.setLoadingBarState(false);
					UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
				});
			},
			checkoutFdk:function( $activeItem, totalAmount ){
				var cardCashSe = $activeItem.data("card-cash-se");
				var delngSe = 1; // 1: 승인 , 0 : 취소
				var splpc = totalAmount['total_amount'].amount || Method.$that.find('input[name="transactionTotal"]').val(); //공급가
				var vat = Math.round(Number(splpc) / 11); //부과세
				var taxxpt = 0; //면세액
				var instlmtMonth = Method.$that.find('[name="cardQuota"] > option:selected').val(); //할부개월수
				var aditInfo = 'orderId=' + $('input[name=cartId]').val();
				var callbackScript = "Core.getModule('module_order_payment').callBackFdk";
				var arrQueryString = [
					"callbackScript="+callbackScript,
					"cardCashSe="+cardCashSe,
					"delngSe="+delngSe,
					"splpc="+splpc,
					"vat="+vat,
					"taxxpt="+taxxpt,
					"instlmtMonth="+instlmtMonth,
					"aditInfo="+encodeURIComponent(aditInfo),
					"uscMuf=CTM0"+$("input[name=cartId]").val()
				];

				if(!Method.tempPayFlg){
					sandbox.utils.ajax(sandbox.utils.contextPath + '/js/finpaylog', 'GET', "status=checkoutfdk&"+arrQueryString.join('&'), function(){}, true, false);
					window.location.href = "seamless://pay=ok&mode=req&"+arrQueryString.join('&');
				}else{
					//임의생성
					var d = new Date();
					var year = d.getFullYear().toString().substr(2,4);
					var month = (d.getMonth() + 1)< 10 ? "0"+(d.getMonth() + 1) : (d.getMonth() + 1);
					var day = (d.getDate())< 10 ? "0"+(d.getDate()) : (d.getDate());
					var hh = d.getHours().toString();
					var mm = d.getMinutes().toString();
					var ss = d.getSeconds().toString();
					var confmDe = year + "" + month + "" + day;
					var confmTime = hh + "" + mm + "" + ss; 

					var confmNo = confmDe + "" + confmTime;
					var REFERENCE_NO = "REF_"+confmNo;

					var resp = "";
						resp += "&cardCashSe=CARD";
						resp += "&delngSe=1";
						resp += "&setleSuccesAt=O";
						resp += "&setleMssage=%EB%AC%B4%EC%84%9C%EB%AA%85%EA%B1%B0%EB%9E%98";
						resp += "&confmNo="+confmNo;
						resp += "&confmDe="+confmDe;
						resp += "&confmTime="+confmTime;
						resp += "&cardNo=1234************";
						resp += "&instlmtMonth="+instlmtMonth;
						resp += "&cardNm=국민카드";
						resp += "&referenceNo=A1231231231111";
						resp = "cardCashSe=CARD&delngSe=1&setleSuccesAt=O&setleMssage=%EB%AC%B4%EC%84%9C%EB%AA%85%EA%B1%B0%EB%9E%98&confmNo=04010012&confmDe=180118&confmTime=112315&cardNo=949019**********&instlmtMonth=0&issuCmpnyNm=%ED%98%84%EB%8C%80&puchasCmpnyNm=%ED%98%84%EB%8C%80&splpc="+splpc+"&vat="+vat+"&taxxpt=0&issuCmpnyCode=04&puchasCmpnyCode=04&cardNm=%ED%98%84%EB%8C%80%EA%B0%9C%EC%9D%B8%EC%9D%BC%EB%B0%98%EC%B9%B4%EB%93%9C&aditInfo=&bizrno=3100000000&trmnlNo=20150600&mrhst=%EB%86%8D%ED%98%91%ED%85%8C%EC%8A%A4%ED%8A%B85&mrhstAdres=%EC%84%9C%EC%9A%B8%EA%B4%80%EC%95%85%EA%B5%AC%EB%82%A8%EB%B6%80%EC%88%9C%ED%99%98%EB%A1%9C1926%28%EB%B4%89%EC%B2%9C%EB%8F%99%2C%EA%B2%BD&mrhstRprsntv=test1&mrhstTelno=03111111111&REFERENCE_NO=011804000012";
						//resp = "cardCashSe=CARD&delngSe=1&setleSuccesAt=X&setleMssage="+ encodeURIComponent("오류") +"&mrhstRprsntv=test1&mrhstTelno=03111111111&REFERENCE_NO=011804000012";
						Core.getModule('module_order_payment').callBackFdk(resp);
				}
			},
			fdkCancel:function(respObj){
				var arrQueryParams = [
					"callbackScript=Core.getModule('module_order_payment').callBackCancelFdk",
					"cardCashSe=CARD",
					"delngSe=0",
					"splpc=" + respObj.splpc,
					"vat=" + Math.round((Number(respObj.splpc) * 1) / 11),
					"taxxpt=0",
					"instlmtMonth=" + respObj.instlmtMonth,
					"aditInfo=order_no%3D" + respObj.aditInfo,
					"srcConfmNo=" + respObj.confmNo,
					"srcConfmDe=" + respObj.confmDe
				];
				window.location.href = "seamless://pay=cancel&mode=req&"+arrQueryParams.join('&');
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-payment]',
					attrName:'data-module-order-payment',
					moduleName:'module_order_payment',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			callBackFdk:function(resp){
				//finpay log
				sandbox.utils.ajax(sandbox.utils.contextPath + '/js/finpaylog', 'GET', 'status=callBackFdk&' + resp, function(){}, true, false);

				var respObj = sandbox.utils.getQueryParams(decodeURIComponent(resp));
				if(respObj.setleSuccesAt == "X"){
					sandbox.utils.ajax(sandbox.utils.contextPath + '/js/finpaylog', 'GET', {'status':'fail', 'setleSuccesAt':respObj.setleSuccesAt}, function(){}, true, false);

					UIkit.modal.alert(respObj.setleMssage).on('hide.uk.modal', function(){
						var cartId = Method.$that.find("input[name='cartId']").val();
						location.href = sandbox.utils.contextPath + '/checkout/request/'+ cartId;
						sandbox.setLoadingBarState(false);
					});
				}else{
					sandbox.utils.ajax(sandbox.utils.contextPath + '/js/finpaylog', 'GET', {'status':'success', 'setleSuccesAt':respObj.setleSuccesAt}, function(){}, true, false);

					//앱결제 후 주문완료단계에서 에러가 난경우 다시 카드취소를 시킨다.
					//location.href = url + params;
					var orderId = Method.$that.find('input[name="cartId"]').val();
					var queryParams = sandbox.utils.getQueryParams(resp);
					queryParams['orderId'] = orderId;

					sandbox.utils.promise({
						url:sandbox.utils.contextPath + Method.$that.find("input[name=payData]").data("m-redirect-url"),
						method:'GET',
						data:queryParams,
						custom:true
					}).then(function(data){
						return sandbox.utils.promise({
							url:sandbox.utils.contextPath + '/checkout/orderSuccess',
							method:'GET',
							data:{orderId:orderId}
						});
					}).then(function(data){
						if(data.result){
							location.href = sandbox.utils.contextPath + '/fdk/confirmation/' + data.orderNumber;
						}else{
							UIkit.modal.alert('생성된 주문이 없습니다.').on('hide.uk.modal', function(){
								Method.fdkCancel(respObj);
							});
						}
					}).fail(function(msg){
						// finpay 결제취소
						UIkit.modal.alert('주문완료시 오류가 발생하였습니다. 결제를 취소합니다.').on('hide.uk.modal', function(){
							Method.fdkCancel(respObj);
						});
					});
				}
			},
			callBackCancelFdk:function(resp){
				/* finpay 결제취소 콜백 */
				var respObj = sandbox.utils.getQueryParams(decodeURIComponent(resp));
				if(respObj.setleSuccesAt === 'X'){
					//앱카드취소 실패
					if(respObj.setleMssage === '원거래없음'){
						UIkit.modal.alert('결제 시 사용한 카드가 아니거나 원거래 내역이 존재하지 않습니다.');
					}else{
						UIkit.modal.alert(respObj.setleMssage);
					}
				}else if(respObj.setleSuccesAt === 'O'){
					//앱카드취소 성공
					respObj['fdkCardCancel'] = 'Y';
					UIkit.modal.alert(respObj.setleMssage).on('hide.uk.modal', function(){
						sandbox.setLoadingBarState(true);
						location.href = '/';
					});
				}
			}
		}
	});
})(Core);
