(function(Core){
	Core.register('module_order_success', function(sandbox){
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				sandbox.validation.init( $this.find('#order-success-form') );

				if( $this.find('input[name="email"]').val() == ''){
					UIkit.alert("test");
				}

				$this.find('[data-mail-btn]').on("click", Method.sendMail );
				$this.find('[data-receipt-btn]').on("click", Method.sendReceipt );
			},
			sendMail:function(){
				var $form = $(this).closest('form');
				sandbox.validation.validate( $form );
				console.log(sandbox.validation.val);
				console.log(sandbox.validation.isValid( $form ));
				if(!sandbox.validation.isValid( $form )){
					UIkit.modal.alert("이메일을 형식에 맞게 입력해 주세요.");
					return;
				}

				/*
				"orderNumber": $('input[name=orderno]').val(),
				"email":$('input[name=email]').val(),
				"storename":$('input[name=storename]').val(),
				"storeaddress":$('input[name=storeaddress]').val(),
				"storetel":$('input[name=storetel]').val()
				*/
				var action = sandbox.utils.contextPath + '/sendMessage/execute';
				var param = {
					'type':'EMAIL',
					'targeter':$('input[name=email]').val(),
					'messageKey':'webposPaymentComplete',
					'content':Method.$that.find('#email-tmpl').html()
				};

				var msg="이메일을 발송하시겠습니까?";
				UIkit.modal.confirm(msg, function(){
					sandbox.utils.ajax(action, 'POST', param, function(data){
						var json=$.parseJSON(data.responseText);
						console.info(json.result);
						// 성공
						if (json.result) {
							UIkit.modal.alert("이메일이 발송되었습니다.");
						} else {
							UIkit.modal.alert(json.errorMsg);
						}
					}, true)
				},function(){},{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			},
			sendReceipt:function(){
				var $form = $(this).closest('form');
				var formData = $form.serialize().replace(/\+/g, '%20');
				var queryParams = sandbox.utils.getQueryParams(formData);
				sandbox.validation.validate( $form );

				var $items = $('#order-summary').find('.order-list');
				var arrProductname = [];
				var arrProductcnt = [];
				var arrRetailPrice = [];
				var arrProductprice = [];
				var arrSaleprice = [];
				var arrSaleRate = [];
				var arrSelltype = [];
				var totalcnt = 0;
				var mobileurl = '';

				var price = 0;
				var retailPrice = 0;
				var saleRate = 0;
				var totalRetailPrice = 0;
				var quantity = 1;
				var totalQuantity = 0;

				for(var i = 0 ; i < $items.length ; i++){
					quantity = (queryParams.quantity instanceof Array) ? queryParams.quantity[i] : queryParams.quantity;
					totalQuantity += parseInt(quantity);
					arrProductname.push((queryParams.productName instanceof Array) ?
									queryParams.productName[i]+'%20'+queryParams.model[i]+'%20'+queryParams.opt[i] : queryParams.productName+'%20'+queryParams.model+'%20'+queryParams.opt);
					arrProductcnt.push(quantity);					
					arrProductprice.push((queryParams.price instanceof Array) ? encodeURIComponent(sandbox.utils.price(queryParams.retailPrice[i])) : encodeURIComponent(sandbox.utils.price(queryParams.retailPrice)));

					retailPrice = (queryParams.price instanceof Array) ? queryParams.retailPrice[i] : queryParams.retailPrice;
					price = (queryParams.price instanceof Array) ? queryParams.price[i] * queryParams.quantity[i] : queryParams.price * queryParams.quantity;

					totalRetailPrice += parseInt(retailPrice) * quantity;
					arrRetailPrice.push(encodeURIComponent(sandbox.utils.price(retailPrice)));
					arrSaleprice.push(encodeURIComponent(sandbox.utils.price(price)));

					arrSelltype.push(encodeURI('정상'));
					totalcnt += (queryParams.quantity instanceof Array) ? (queryParams.quantity[i] * 1) : queryParams.quantity;

					saleRate = Math.floor((100-((parseInt(price)/parseInt(retailPrice))*100)));
					arrSaleRate.push(encodeURIComponent(((saleRate > 0) ? saleRate+'%':'')));
				}

				var surtaxInt = Math.round(Number(queryParams.payamount) / 11);
				var surtax = encodeURIComponent(sandbox.utils.price(surtaxInt));
				var taxamount = encodeURIComponent(sandbox.utils.price(Number(queryParams.payamount)-surtaxInt));
				var transFormPayamount = encodeURIComponent(sandbox.utils.price(queryParams.payamount));
				var transFormRetailPayamount = encodeURIComponent(sandbox.utils.price(totalRetailPrice));
				var discount = encodeURIComponent(sandbox.utils.price(totalRetailPrice - parseInt(queryParams.payamount) + parseInt(queryParams.totalFulfillmentCharges)));//배송비 제외 추가
				var transFormFulfillmentCharges = encodeURIComponent(sandbox.utils.price(queryParams.totalFulfillmentCharges));//당일배송 배송비
				var cardno = '';
				//console.log(cardno.length + " : " + cardno.substring(0,4));
				if(queryParams.cardno.length === 16){
					cardno = queryParams.cardno.substring(0,4) + "-" + queryParams.cardno.substring(4,8) + "-" + queryParams.cardno.substring(8,12) + "-" + queryParams.cardno.substring(12,16);
				}
				//console.log(cardno);
				//console.log(taxamount);
				//console.log(surtax);
				//storeaddress = storeaddress.replace("&#40;", ")");
				//console.log(storeaddress);

				//날짜형식수정 paydate paytime
				var dt = queryParams.paydate.substring(0,2) + "/" + queryParams.paydate.substring(2,4) + "/" + queryParams.paydate.substring(4,6);
				var tm = queryParams.paytime.substring(0,2) + ":" + queryParams.paytime.substring(2,4) + ":" + queryParams.paytime.substring(4,6);

				/*
				var orderno = $('input[name=orderno]').val();
				var payno = $('input[name=payno]').val();
				var paydate = $('input[name=paydate]').val();
				var paytime = $('input[name=paytime]').val();
				var cardno = $('input[name=cardno]').val();
				var month = $('input[name=month]').val();
				var cardcompany = $('input[name=cardcompany]').val();
				var cardNm = $('input[name=cardNm]').val();
				var storeposno = $('input[name=storeposno]').val();
				var storeno = $('input[name=storeno]').val();
				var storename = $('input[name=storename]').val();
				var ceoname = $('input[name=ceoname]').val();
				var storeaddress = $('input[name=storeaddress]').val();
				var storetel = $('input[name=storetel]').val();
				var payamount = $('input[name=payamount]').val();
				var customerid = $('input[name=customerid]').val();
				*/


				var queryString =  "";
				queryString += "&orderno=" + queryParams.orderno;
				queryString += "&ceoname=" + queryParams.ceoname;
				queryString += "&storeaddress=" + queryParams.storeaddress;
				queryString += "&storeno=" + queryParams.storeno;
				queryString += "&storetel=" + queryParams.storetel;
				queryString += "&storename=" + queryParams.storename;
				queryString += "&paydatetime=" + (dt + '%20' + tm);
				queryString += "&storeposno=" + queryParams.storeposno;
				//item list
				queryString += "&productname=" + Method.getReplaceCode(arrProductname.join('|'));
				queryString += "&unitcost=" + arrProductprice.join('|');
				queryString += "&amount=" + arrProductcnt.join('|');
				queryString += "&sale=" + arrSaleRate.join('|');
				queryString += "&sellingprice=" + arrSaleprice.join('|');
				queryString += "&selltype=" + arrSelltype.join('|');

				queryString += "&issurtax=issurtax";
				queryString += "&customerid=" + queryParams.customerid;
				queryString += "&taxamount=" + taxamount;
				queryString += "&surtax=" + surtax;
				queryString += "&servicecharge=";
				queryString += "&totalsalesamount=" + transFormPayamount;
				queryString += "&discount=" + discount;
				queryString += "&sum=" + transFormRetailPayamount;
				queryString += "&fairymoney=" + transFormPayamount;
				queryString += "&shortchange=shortchange";
				queryString += "&cardno=" + queryParams.cardno;
				queryString += "&cardcompany=" + queryParams.cardcompany;
				queryString += "&month=" + queryParams.month;
				queryString += "&payno=" + queryParams.payno;
				queryString += "&payamount=" + transFormPayamount;
				queryString += "&totalprodcount=" + totalQuantity;
				queryString += "&mobileurl=" + mobileurl;
				queryString += "&totalFulfillmentCharges=" + transFormFulfillmentCharges;//당일배송 배송비

				//console.log(queryString);
				window.location.href = "seamless://print=ok&mode=pay"+queryString;
			},
			getReplaceCode: function (stg) {
				return stg.replace(/%26/gi, '%20');
			}
		}


		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-success]',
					attrName:'data-module-order-success',
					moduleName:'module_order_success',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
