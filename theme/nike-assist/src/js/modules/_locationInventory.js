(function(Core){
	Core.register('module_locationinventory', function (sandbox) {

		var $this,
			latitude,
			longitude,
			url,
			orderBy,
			skuData,
			checkedRadio,
			appendOption,
			appendTxt,
			currentOption,
			serviceMenData={},

			pickupLocation={
				currentLocation:null,
				storeList:[],
				storeAll: false,
				locationAll: false,
			},
			pickupConfirm={isTaskConfirm:false, customer:null, storeInfo:null, itemRequest:null},

			pickupQuantity={
				isTaskQuantity:false,
				productPrice:0,
				newValue:0,
				min:0,
				max:0,
				size:0,
				quantity:{
					maxQuantity:1,
					msg:'개 까지 구매가능 합니다.',
					quantityStateMsg:'상품의 수량이 없습니다.'
				}
			},

			totalPickupLocation=[], //Json 호출
			location_inventory={},

			args={};


		// 사용자 위치 정보 조회 (재고 보유 매장의 위치 정보를 얻어와 내주변 정렬하기 위한 기능)
		function findGeoLocation (isOnLoad) {
			var positionOpt = {
				enableHighAccuracy : true,
				maximumAge : 5000
			};

			if (navigator.geolocation) {// 위치정보 사용
				navigator.geolocation.getCurrentPosition(function (position) {
						if(serviceMenData['latitude'] != position.coords.latitude || serviceMenData['longitude'] != position.coords.longitude) {
							serviceMenData['latitude']  = position.coords.latitude;  // API RESERVE
							serviceMenData['longitude'] = position.coords.longitude; // API RESERVE
						}
						pickupLocation.currentLocation = serviceMenData;

						if(isOnLoad===false) {
							loadStoreIfRadioChange();
						}
					}, // 내 위치 정보 있을 경우
					null, // 위치정보 사용 관련 에러
					positionOpt
				);
			} else {
				UIkit.notify("내 위치 정보를 찾을 수 없습니다..", {timeout:3000, pos:'top-center',status:'warning'});
				//showPositionError({"error":"ERROR - Geolocation is not supported by this agent"});
			}
		}
		findGeoLocation(true);


		// 내 주변순 위도 : 경도
		function calculateDistance (lat1, lon1, lat2, lon2) {
			var theta = lon1 - lon2;
			var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

			dist = Math.acos(dist);
			dist = rad2deg(dist);

			dist = dist * 60 * 1.1515;
			dist = dist * 1.609344; // 킬로미터 단위적용

			function deg2rad (deg) {
				return (deg * Math.PI / 180.0);
			}

			function rad2deg (rad) {
				return (rad * 180 / Math.PI);
			}
			return dist;
		}


		//vue 컴포넌트 초기화
		var vueContainer = new Vue({
			el: '#idLocationSearch',
			data: {
				'location': pickupLocation,
				'confirm': pickupConfirm,
				'quantity': 0,

				'skuIdNe': '',
				'stateList': [],
				'storeTypeSide': [],
				'storeType': [],
				'stype': [],
				'localSelect': [],

				'dataList': [],
				'inventory': [],

				'currentSort': 'quantity',
				'currentSortDir': 'asc',

				'tag': false,
				'flag': false,
			},

			created: function () {
				var vm = this;
				sandbox.utils.promise({
					url:sandbox.utils.contextPath +'/processor/execute/store',
					method:'GET',
					data:{
						'mode':'template',
						'templatePath':'/page/partials/storeList',
						'isShowMapLocation':false
					},
				}).then(function (data) {
					var $defer = $.Deferred();
					var data = sandbox.utils.strToJson(data.replace(/&quot;/g, '"'));
					if(data !== ''){
						vm.dataList = data;
						$defer.resolve(data);
					}else{
						$defer.reject('location info is empty');
					}
					return $defer.promise();
				}).then(function(data){

				}).fail(function (msg) {
					//UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
				});
			},

			computed: {
				storeList: function (s) {
					var vm = this;
					var flag = this.flag;

					vm.stateList = this.storeType.concat(this.localSelect); // 매장유형 / 지역선택 병합

					return this.dataList.filter(function (row, index) { // 주소 데이터

						var _id = row.id;
						var _customTaxonomyValue = row.customTaxonomyValue;
						var _state = row.state;
						var _stortype =  _.map(row.storeType, _.trim);
						var _index = index;

						row.locationTarget = calculateDistance(serviceMenData.latitude, serviceMenData.longitude, row.latitude, row.longitude);

						return vm.inventory.some(function (size) {
							var _size = size.fulfillmentLocationId;

							if (_id == _size) {
								row.quantity = size.quantityAvailable;
							}

							return vm.stateList.some(function (tag) {
								if (!flag) {
									return _id == _size;
								} else {
									var _nametg = '';
									var _name = false;
									tag.nameS ? _nametg = tag.nameS.trim() : 'null';

									// var storeLocal = tag.nameL == _state || _.includes(_stortype, _nametg)
									var storeLocal = tag.nameL === _state || tag.nameS === _customTaxonomyValue;

									return _id == _size && storeLocal && tag.active === true;
								}
							});
						});
					}).sort(function(a,b){
						// var modifier = 1;
						if (vm.currentSortDir === 'desc') {
							if (a[vm.currentSort] < b[vm.currentSort]) return 1;
							if (a[vm.currentSort] > b[vm.currentSort]) return -1;
						} else { // asc
							if (a[vm.currentSort] < b[vm.currentSort]) return -1;
							if (a[vm.currentSort] > b[vm.currentSort]) return 1;
						}
						return 0;
					});
				},

				storeType: function () {
					return this.storeType
				},

				localSelect: function () {
					return this.localSelect
				}
			},

			methods: {
				pickupLocation: function (skuId) {
					var vm = this;
					this.skuIdNe = skuId;

					var obj = {
						'skuId': skuId,
						'json': 'true',
						'fType': 'PHYSICAL_PICKUP'
					}
					// ajax:function(url, method, data, callback, isCustom, isLoadingBar, delay, dataType, cache){
					sandbox.utils.ajax(sandbox.utils.contextPath + '/processor/execute/pickable_inventory', 'GET', obj, function(data){
						vm.inventory = JSON.parse(data.responseText);
						
						// 매장유형 / 지역 초기화
						vm.dataList.filter(function (row, index) {
							vm.storeType[index] = { 'active': false, 'view': false, 'id': row.id, 'nameS': row.customTaxonomyValue};
							vm.localSelect[index] = { 'active': false, 'view': false, 'id': row.id, 'nameL': row.state};
						});

						// 매장유형
						vm.storeType.filter(function (row) {
							var _id = row.id;
							vm.inventory.some(function (size) {
								var _size = size.fulfillmentLocationId;
								if (_id == _size) row.view = true;
							});
							if (row.view !== true) row.nameS = '';
						});
						vm.storeType = _.unionBy(vm.storeType, vm.storeType, 'nameS');

						// 지역
						vm.localSelect.filter(function (row) {
							var _id = row.id;
							vm.inventory.some(function (size) {
								var _size = size.fulfillmentLocationId;
								if (_id == _size) row.view = true;
							});
							if (row.view !== true) row.nameL = '';
						});
						vm.localSelect = _.unionBy(vm.localSelect, vm.localSelect, 'nameL');


						// 사이즈 변경 시 초기화
						vm.sort('quantity');
						vm.storeToggle('false');
						vm.locationToggle('false');
					}, true, false, 1200);
				},

				sort: function (s) {
					switch (s) {
						case 'locationTarget' : this.currentSortDir = 'asc'; break;
						case 'name' : this.currentSortDir = 'asc'; break;
						case 'quantity' : this.currentSortDir = 'desc'; break;
					}
					this.currentSort = s;
				},

				storeToggle: function (bools) {
					this.flag = JSON.parse(bools);
					var vm = this;
					if (this.flag) {
						pickupLocation.storeAll = false;
					} else {
						vm.storeType.filter(function (row) {
							row.active = false;
						});
						pickupLocation.storeAll = true;
					}
				},

				locationToggle: function (bools) {
					this.flag = JSON.parse(bools);
					var vm = this;
					if (this.flag) {
						pickupLocation.locationAll = false;
					} else {
						vm.localSelect.filter(function (row) {
							row.active = false;
						});
						pickupLocation.locationAll = true;
					}
				}
			}
		});


		//template
		Vue.component('pickup-location', {
		  props:['currentLocation', 'storeList', 'storeAll', 'locationAll', 'dp', 'st', 'ls', 'sort', 'stg', 'ltg'],
		  template:'<div>\
		  <section class="section-location-filter wrap-detail-filter">\
			  <!--/* 정렬방식 */-->\
			  <div class="scroll-wrap">\
				  <h2 class="f-title">정렬방식</h2>\
				  <a class="btn-close webpos-ns-pop-close"></a>\
				  <ul class="p-radiobox">\
					  <li>\
						  <input type="radio" id="surrounding-asc" name="orderBy" value="D" />\
						  <label for="surrounding-asc" v-on:click="sort(\'locationTarget\')">내 주변순</label>\
					  </li>\
					  <li>\
						  <input type="radio" id="store-asc" name="orderBy" value="N" />\
						  <label for="store-asc" v-on:click="sort(\'name\')">매장명순</label>\
					  </li>\
					  <li>\
						  <input type="radio" id="sku-desc" name="orderBy" value="Q" />\
						  <label for="sku-desc" v-on:click="sort(\'quantity\')">재고순</label>\
					  </li>\
				  </ul>\
				  <!--/* 매장유형 */-->\
				  <h2 class="f-title">매장유형</h2>\
				  <ul class="p-checkbox">\
					  <li>\
						  <input type="checkbox" id="storeAll" name="storelocal" v-model="storeAll"  />\
						  <label for="storeAll" v-on:click="stg(false)">전체</label>\
					  </li>\
					  <li v-for="(tag, index) in st" v-if="tag.view === true">\
						  <input type="checkbox" name="storelocal" v-model="tag.active" v-bind:id="\'storelocal\' + index" />\
						  <label v-bind:for="\'storelocal\' + index" v-on:click="stg(true)"><span class="label">{{ tag.nameS }}</span></label>\
					  </li>\
				  </ul>\
				  <!--/* 지역선택 */-->\
				  <h2 class="f-title">지역선택</h2>\
				  <ul class="p-checkbox">\
					  <li>\
						  <input type="checkbox" id="locationAll" name="location" v-model="locationAll"  />\
						  <label for="locationAll" v-on:click="ltg(false)">전체</label>\
					  </li>\
					  <li v-for="(tag, index) in ls" v-if="tag.view === true">\
						  <input type="checkbox" name="location" v-model="tag.active" v-bind:id="\'location\' + index" />\
						  <label v-bind:for="\'location\' + index" v-on:click="ltg(true)"><span class="label">{{ tag.nameL }}</span></label>\
					  </li>\
				  </ul>\
			  </div>\
			  <div class="mobile-menu_link">\
				  <a class="reset-button large button-line" href="javascirpt:;">초기화</a>\
				  <a class="result-button large result-button-line" href="javascirpt:;">결과보기</a>\
			  </div>\
		  </section>\
		  <div class="current-location-area">\
			  <div class="dim" data-brz-dim=""></div>\
			  <div class="filter-category-wrap">\
				  <button class="btn-detail-search btn-filter-open"><i class="webpos-ns-detail"></i>상세검색</button>\
				  <span class="num">상품 보유 매장 {{dp.length}}개</span>\
			  </div>\
			  <table class="t-view t-store">\
				  <colgroup>\
					  <col width="70%" />\
					  <col width="30%" />\
				  </colgroup>\
				  <thead>\
					  <tr>\
						  <th scope="col">매장명</th>\
						  <th scope="col">수량</th>\
					  </tr>\
				  </thead>\
				  <tbody>\
					  <template v-if="dp.length > 0">\
						  <tr v-for="store in dp" class="storeList" v-bind:data-locationid="store.id" v-bind:data-state="store.state">\
							  <td>\
								  <p>{{store.name}}</p>\
								  <p class="txt-address">\
									  <span>({{store.zip}}) {{store.address1}} {{store.address2}}</span>\
									  <span><a v-bind:href="\'tel:\'+store.phone">{{store.phone}}</a></span>\
								  </p>\
							  </td>\
							  <td>\
								  <span class="quantity" v-if="store.quantity <= 10">{{store.quantity}}</span>\
								  <span class="quantity" v-if="store.quantity > 10">10+</span>\
							  </td>\
						  </tr>\
					  </template>\
					  <template v-else>\
						  <tr>\
							  <td class="uk-text-center" colspan="2">\
								  매장정보가 없습니다.\
							  </td>\
						  </tr>\
					  </template>\
				  </tbody>\
			  </table>\
		  </div>\
		</div>'
		});


		var modal = UIkit.modal('#common-modal');

		var Method = {
			moduleInit:function () {
				var $_this = $(this);
				args = arguments[0];

				// 상품보유매장 레이어 팝업 호출
				$(".product-option-container").on('click', '#locationinventory', function(e){

					var checkSkuId;

					// 모든 사이즈의 체크된 내용을 일단 삭제한다. (체크된 값이 중복으로 발생되지 않기위함)
					$('.uk-modal').find('.product-option-container').find('.input-radio').find("input[type=radio]").removeAttr('checked');

					_.delay(function(){
						// 삭제후 실제 체크된 값의 checked 값을 부여.
						$('.uk-modal').find('.product-option-container').find('.input-radio.checked').find("input[type=radio]").attr('checked', true);
					}, 100);

					// 보유매장 호출시 마다 상세검색의 옵션값을 제거한다.
					$('.section-location-filter').find("input[type='radio']").attr("checked", false);

					// 보유매장 호출시 마다 상세검색의 옵션값을 제거한다.
					$('.section-location-filter').find("input[type='checkbox']").attr("checked", false);

					if ($("#product-option_radio")[0] != null) {
						// 상품상세 페이지에서 선택된 옵션을 상품보유매장에 적용.
						appendOption  = $("#product-option_radio")[0].outerHTML;
					}
					$('#location-option').empty().append(appendOption);

					// 선택된 옵션값을 상품보유매장 화면에 표시한다.
					currentOption = $(".pinfo-item-box").find("input[type=radio]:checked").siblings(".selected").eq(0).text();
					if (currentOption == '') {
						// 선택된 사이즈가 없을 경우 문구.
						$('.pop-detail-title').find('.select-size').empty().text('사이즈를 선택해주세요.');
						$('#idLocationSearch').hide();
					} else {
						// 사이즈가 선택되었으면 그값을 입력.
						$('.pop-detail-title').find('.select-size').empty().append(currentOption);
						$_this.find('.pop-detail-title').click();
						$_this.find('#idLocationSearch').show();
					}

					$('.uk-modal').css('z-index', "999"); // 보유매장 modal창이 뒤로 보이지 않기 위해 추가.

					// 상품상세의 sku 정보를 저장.
					skuData = sandbox.utils.strToJson($('.info-wrap_product_n').attr('data-sku-data'));

					// 체크된 옵션의 value 저장.
					checkedRadio = $("#product-option_radio").find("input[type=radio]:checked").attr('value');

					// 상세검색의 정렬방법을 저장.
					orderBy = $('.section-location-filter').find("input[type='radio']:checked").val();

					// 체크된 값과 sku 데이터를 비교하여 체크된 옵션의 sku_id를 저장.
					for (var i=0; i<skuData.length; i++) {
						if (checkedRadio == skuData[i].selectedOptions[0]) {
							checkSkuId = skuData[i].skuId;
						}
					}

					// 파라미터 세팅.
					serviceMenData['skuId']   = checkSkuId;
					serviceMenData['orderBy'] = orderBy;

					if (checkSkuId != null) {
						// skuIdCallList(checkSkuId)
						vueContainer.pickupLocation(checkSkuId);
					}
				});

				// modal창에서 사이즈 선택시 반응.
				$('.uk-modal').on('click', '.input-radio', function(){
					var checkSkuId;
					$this = $(this);

					$_this.find('.pop-detail-title').click();
					$_this.find('#idLocationSearch').show();

					// 상품의 재고가 없으면 선택 불가.
					if ($this.find("input[type=radio]").attr('disabled') != 'disabled') {

						// if($('.pop-detail-title').hasClass('uk-active')){
						//     $('.pop-detail-title').click();
						// }
						// 상품상세의 sku 정보를 저장. async 
						// skuData = sandbox.utils.strToJson($('.info-wrap_product_n').attr('data-sku-data'));
						var skuData = sandbox.getComponents('component_product_option', {context:$(document)}).getDefaultSkuData();
						var redioValue = $this.find("input[type=radio]").siblings("label").text();
						$('.select-size').empty().append(redioValue); // 사이즈 표시 변경.

						$('.uk-modal').find('.input-radio').find('.selected').attr('class', ""); // 선택내용 제거.

						$('.uk-modal').find('.input-radio').find("input[type='radio']").attr('checked', false); // 옵션 체크 제거.
						$this.find("input[type='radio']").attr('checked', true); // 선택된 옵션으로 체크 여부 추가.

						// 체크된 옵션의 value 저장.
						checkedRadio = $this.find("input[type=radio]").attr('value');

						// 상세검색의 정렬방법을 저장.
						orderBy = $('.section-location-filter').find("input[type='radio']:checked").val();

						// 체크된 값과 sku 데이터를 비교하여 체크된 옵션의 sku_id를 저장.
						for (var i=0; i<skuData.length; i++) {
							if (checkedRadio == skuData[i].selectedOptions[0]) {
								checkSkuId = skuData[i].skuId;
							}
						}

						// 파라미터 세팅.
						serviceMenData['skuId']   = checkSkuId;
						serviceMenData['orderBy'] = orderBy;

						$this.find('label').addClass('selected'); // 현재 값으로 선택되었다고 추가.

						if (checkSkuId != null) {
							// loadLocationInventory();
							vueContainer.pickupLocation(checkSkuId);
						}
					}
				});

				// 상세검색 초기화
				$('.uk-modal').delegate(".reset-button", 'click', function () {
					vueContainer.sort('quantity');
					vueContainer.storeToggle('false');
					vueContainer.locationToggle('false');
				});

				// 상세검색 결과보기.
				$('.uk-modal').delegate(".result-button", 'click', function() {
					$(".uk-modal.uk-open").css({"overflow-y" : "auto"});
					$(".uk-modal.uk-open .uk-modal-dialog").css({
						"height" : "auto",
						"overflow" : "auto",
						"position" : "relative",
						"left" : "auto",
						"top" : "auto"
					});

					$(".locationInventories .dim").removeClass('active');
					$(".section-location-filter").removeClass('active').css({"left":"-330px", "opacity":0});
				});
			}
		}


		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-locationinventory]',
					attrName:'data-module-locationinventory',
					moduleName:'module_locationinventory',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
