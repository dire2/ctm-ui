(function(Core){
	Core.register('module_checkout', function(sandbox){
		var Method = {
			moduleInit:function(){
				//당일배송 재고부족 시 장바구니 이동
				var cartRequiresLock = $('input[name=cartRequiresLock]').val();
				var samedayDeliveryNotAvailable = $('input[name=samedayDeliveryNotAvailable]').val();
				var samedayMessage = $('input[name=samedayDeliveryNotAvailableMessage]').val();
				if(cartRequiresLock == 'true' && samedayDeliveryNotAvailable == 'true'){
					UIkit.modal.alert(samedayMessage).on('hide.uk.modal', function() {
						Core.Loading.show();
						window.location.assign(sandbox.utils.contextPath + '/cart');
					});
				}
				var $this = $(this);
				$this.find('[data-order-tab] .header').on("mousedown", Method.updateOrderTab);

				$this.find('.repair').click(function(e){
					e.preventDefault();
					var url = $(this).attr('href');

					UIkit.modal.confirm('수정하실경우 입력하신 정보가 초기화 됩니다.', function(){
						location.href = url;
					}, function(){},{
						labels: {'Ok': '확인', 'Cancel': '취소'}
					});
				});
			},

			updateOrderTab:function(e){
				e.preventDefault();
				var $icon = $(this).find('[class^="icon-toggle-"]');
				var $view = $(this).closest('.order-tab').find('.view');
				var $preview = $(this).find('.preview');

				if( $view.length > 0 ){
					$preview.toggleClass('uk-hidden');
					$icon.toggleClass('uk-hidden');
					$view.toggleClass('uk-hidden');
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-checkout]',
					attrName:'data-module-checkout',
					moduleName:'module_checkout',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
