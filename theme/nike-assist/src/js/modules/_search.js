(function(Core){
	Core.register('module_search', function(sandbox){
		var $this, args, clickIS, endPoint, latestKeywordList, arrLatestKeywordList = [], isSaveLatest;
		var modal = UIkit.modal('#popup-layer-search', {center:true});

		//초기값 설정, 쿠키에 저장 
		isSaveLatest = $.cookie('isSaveLatest') 
		if(isSaveLatest == undefined){
			$.cookie('isSaveLatest', 'true');
			isSaveLatest = $.cookie('isSaveLatest');
		}
		var setSaveLatestText = function(){
			if(isSaveLatest == 'true'){
				$('#toggle-save-latest').text("검색어저장 끄기");
			} else {
				$('#toggle-save-latest').text("검색어저장 켜기");
			}
		}
		setSaveLatestText();

		var setSearchKeyword = function(keyword){
			var pattern = new RegExp(keyword, 'g');
			arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList.replace(pattern, ''));
			arrLatestKeywordList.unshift(keyword);

			if(arrLatestKeywordList.length >= args.keywordMaxLen){
				arrLatestKeywordList = arrLatestKeywordList.slice(0, args.keywordMaxLen);
			}

			if(isSaveLatest == 'true'){
				$.cookie('latestSearchKeyword', arrLatestKeywordList.join(','));
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				clickIS = false;

				latestKeywordList = $.cookie('latestSearchKeyword') || '';
				arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList || '');
				endPoint = Core.getComponents('component_endpoint');

				sandbox.getComponents('component_searchfield', {context:$this, resultTemplate:'#search-list', resultWrap:'.etc-search-wrap'}, function(){
					this.addEvent('resultSelect', function(data){
                        var text = $(data).text();

                        //nike는 인기검색어 앞에 순번이 있어 아이템 선택시 순번 제거 필요. 
                        if(text.lastIndexOf('10', 0) === 0){
                            text = text.substring(4);
                        } else if(text.match(/^\d/)){
                            text = text.substring(3);
                        }

						var endPointData = {
							key : text,
							text : text
                        }
                        
						endPoint.call( 'searchSuggestionClick', endPointData );
						this.getInputComponent().setValue(text);
						setSearchKeyword(text);
						location.href = sandbox.utils.contextPath + '/search?q='+ text;
					});

					this.addEvent('beforeSubmit', function(data){
						setSearchKeyword(data);
					});

					// this.addEvent('removeList', function(keyword){
					// 	// 삭제버튼 이벤트를 받는 부분
					// 	// keyword 가 all 일때 쿠키의 최근 검색어를 모두 삭제
					// 	if(keyword === 'all'){
					// 		$.cookie('latestSearchKeyword', '');
					// 	}else{
					// 		latestKeywordList = latestKeywordList.replace(keyword, '');
					// 		$.cookie('latestSearchKeyword', latestKeywordList);
					// 	}
					// });

					/* 최근검색어 */
					if(args.isLatestKeyword === 'true'){
						this.setResultPrepend('#keyword-container', '#latest-search-keyword', {
							label:'최근 검색어',
							keyword:arrLatestKeywordList
						});
					}
				});

				// search btn (search field show&hide)
				$('.gnb-search-btn').click(function(e){
					e.preventDefault();

					if(clickIS){
						clickIS = false;
						$this.removeClass('active');
					}else{
						clickIS = true;
						$this.addClass('active');
					}
				});

				$('#barcodeLink').click(function(){
					$('.uk-modal-close').click();
					window.location.href="seamless://barcode=product";
				});
				
				//최근 검색어 삭제 버튼 
				$('#delete-all-latest').click(function(e){
					// $.removeCookie('latestSearchKeyword', { path: '/' });
					$.cookie('latestSearchKeyword', '');
					//최근검색어 리스트 삭제
					$('#latest-keyword > li').remove();
					$('#latest-keyword').html('<li class=\"list less\">최근 검색어가 없습니다.</ul>');
                   
					//검색입력창 최근 검색어 삭제 
					// $this.find($('#search')).val("");
				});

				//검색어 저장 토글 
				$('#toggle-save-latest').click(function(e){	
					// $.cookie('isSaveLatest', !isSaveLatest);
					if(isSaveLatest == 'true'){
						isSaveLatest = 'false';
					} else {
						isSaveLatest = 'true';
					}
					$.cookie('isSaveLatest', isSaveLatest);
					setSaveLatestText();

					if (isSaveLatest == 'false') {
						$.cookie('latestSearchKeyword', '');
						$('#latest-keyword').empty;
						$('#latest-keyword').html('<li class=\"list less\">검색어 저장 기능이 꺼져있습니다.</ul>');
						// $('#latest-keyword > li').text('검색어 저장 기능이 꺼져있습니다.');
					} else {
						if ($('#latest-keyword').find('.list').find("span").length == 0) {
							$('#latest-keyword').empty;
							$('#latest-keyword').html('<li class=\"list less\">최근 검색어가 없습니다.</ul>');
						}
					}
				});

				$('.btn-search-delete').click(function(ketword){
					var keywordList = latestKeywordList.split(',');
					var keywordLength = $('#latest-keyword').find("li").length;
					var thisIndex = $(this).closest("li").index();
					var afterKeywordList = [];
					var cookieResult;

					for(var i = 0; i < keywordLength; i++) {
						afterKeywordList[i] = keywordList[i];
					}

					afterKeywordList.splice(thisIndex, 1);
					cookieResult = afterKeywordList.join(',');

					$.cookie('latestSearchKeyword', '');
					$.cookie('latestSearchKeyword', cookieResult);
					$(this).closest('li').remove();
					
					if ($('#latest-keyword').find('.list').find("span").length == 0) {
						$('#latest-keyword').html('<li class=\"list less\">최근 검색어가 없습니다.</ul>');
					}
				});

				// $("#search").bind("keyup", function(){
				// 	re = /[~!@\#$%^&*\()\-=+_']/gi; 
				// 	var temp=$("#search").val();

				// 	if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
				// 		$("#search").val(temp.replace(re,"")); 
				// 	} 
				// });

			},
			openInit:function() {
				if(isSaveLatest == 'false'){
					$('#latest-keyword').empty;
					$('#latest-keyword').html('<li class=\"list less\">검색어 저장 기능이 꺼져있습니다.</ul>');
				}
			},
			message:function(){
				UIkit.modal.confirm('<center>인식에 실패했습니다.<br/>재시도 하시겠습니까?</center>', function(){
					window.location.href = 'seamless://barcode=product';
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			},
			productUrlCheck:function(productUrl){
				sandbox.utils.promise({
					url:sandbox.utils.contextPath + productUrl,
					type:'GET'
				}).then(function(data){
					window.location.href = sandbox.utils.contextPath + productUrl;
				}).fail(function(msg){
					UIkit.modal.alert('<center>유효하지 않은 페이지입니다.<br/>바코드 번호를 확인해주세요.</center>');
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-search]',
					attrName:'data-module-search',
					moduleName:'module_search',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			searchTrigger:function(){
				$('.gnb-search-btn').trigger('click');
			},
			barcodeResultSearch:function(resp){
				window.location.href = sandbox.utils.contextPath + '/search?q=' + resp + '&isUpc=true';
			},
			seachPopupActive:function(){
				modal.show();
				$('.input-textfield').removeClass('focus');
				Method.openInit();
			}
		}
	});
})(Core);
