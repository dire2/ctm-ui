(function(Core){
	Core.register('module_product', function(sandbox){
		var currentFirstOptValue = '';
		var currentQuantity = 1;
		var itemAttributes = '';
		var miniOptionIS = false;
		var objProductOption = {};
		var minOffsetTop = 30;
		var maxOffsetTop = 0;
		var args = null;
		var $this;
		var imgCurrentIndex;
		var categoryId = '';
		var productId = '';
		var skuId = '';
		var isQuickView = false;
		var isQuantity = true;
		var productOption;
		var quantity;
		var endPoint;
		var privateId;
		var currentSkuData;
		var pickupModal;
		var itemRequest;

		var $optionWrap;
		var $galleryWrap;
		var $productDetailWrap;
		var optionWrapOffsetTop;
		var previousScrollTop = 0;
		var $descriptionWrap;
		var longDescription;

		var quantityCheck = function(inventoryType, maxQty){
			var obj = {isQuantity:false, maxQty:0}
			if(inventoryType !== 'UNAVAILABLE'){
				if(inventoryType === 'CHECK_QUANTITY'){
					obj.isQuantity = (maxQty > 0) ? true : false;
					obj.maxQty = maxQty;
				}else if(inventoryType === 'ALWAYS_AVAILABLE'){
					obj.isQuantity = true;
					obj.maxQty = null;
				}
			}else{
				obj.isQuantity = false;
				obj.maxQty = 0;
			}

			return obj;
		}

		var defaultSkuSetup = function(productOptComponents){
			var skuData, quantityState;
			if(!productOptComponents) return;
			if(quantity){
				if(Array.isArray(productOptComponents)){
					$.each(productOptComponents, function(i){
						skuData = this.getDefaultSkuData()[0];
						quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
						quantity[i].setMaxQuantity(quantityState.maxQty);
						isQuantity = quantityState.isQuantity;
					});
				}else{
					skuData = productOptComponents.getDefaultSkuData()[0];
					quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
					quantity.setMaxQuantity(quantityState.maxQty);
					isQuantity = quantityState.isQuantity;
				}
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				categoryId = sandbox.utils.getQueryParams(location.href).categoryid;
				productId = args.productId;
				privateId = args.privateId;
				endPoint = sandbox.getComponents('component_endpoint');

				//임시 오픈용 nike 입고 알림 팝업 띄우기
				var isReservation = sandbox.utils.getQueryParams(location.href).hasOwnProperty('restock');
				if(isReservation){
					setTimeout(function(){
						$('.stocked-wrap').find('a[href=#restock-notification]').trigger('click');
					}, 300);
				}

				var $dim = $('[data-miniOptionDim]');
				var guideModal = UIkit.modal('#guide', {modal:false});
				var commonModal = UIkit.modal('#common-modal', {modal:false});
				var quantityModal = UIkit.modal('#order-count-select', {keyboard:false, bgclose:false, modal:false});
				var miniCartModule = sandbox.getModule('module_minicart');
				var gallery = sandbox.getComponents('component_gallery', {context:$this});
				var $infoHeightWrap = $('[data-info-height]');
				var isOverQuantity = false;

				$productDetailWrap = $(".product-detail_wrap");
				$galleryWrap = $this.find('.product-gallery-wrap');
				$optionWrap = $this.find('.product-option-container');
				optionWrapOffsetTop = ($optionWrap.length > 0) ? $optionWrap.offset().top : 0;

				var addonProductGroup = {};
				var addonComponents = sandbox.getComponents('component_addon_product_option', {context:$(document)}, function(i){
					var INDEX = 0;
					var key = this.getAddonId();

					if(!addonProductGroup.hasOwnProperty(key)){
						addonProductGroup[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}
					addonProductGroup[key].push(this);

					this.addEvent('addToAddOnItem', function(){
						var privateId = arguments[0];
						for(var i=0; i<addonProductGroup[key].length; i++){
							if(i != INDEX){
								addonProductGroup[key][i].setTrigger(privateId);
							}
						}
					});
				});

				quantity = sandbox.getComponents('component_quantity', {context:$(document)}, function(i){
					var INDEX = i;
					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
					});

					this.addEvent('overQuantity', function(){
						isOverQuantity = true;
					});
				});


				var currentOptValueId = '';
				productOption = sandbox.getComponents('component_product_option', {context:$(document)}, function(i){ //product Option select components
					var CURRENT_INDEX = i;
					var INDEX = 0;
					var _self = this;
					var key = this.getProductId();


					if(!objProductOption.hasOwnProperty(key)){
						objProductOption[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}

					objProductOption[key].push(this);

					this.addEvent('changeFirstOpt', function(firstOptName, optionName, productId, value, valueId, id){
						if(currentOptValueId != valueId){
							currentOptValueId = valueId;

							for(var i=0; i<objProductOption[productId].length; i++){
								if(i != INDEX){
									skuId = '';
									objProductOption[productId][i].setTrigger(optionName, value, valueId);
								}

								if(optionName !== 'COLOR'){
									objProductOption[productId][i].getValidateChk();
								}
							}

							if(optionName === 'COLOR'){
								gallery.setThumb(value);
							}
						}

					});

					this.addEvent('skuComplete', function(skuOpt){
						currentSkuData = skuOpt
						if(quantity){
							var quantityState = quantityCheck(skuOpt.inventoryType, skuOpt.maxQty);
							isQuantity = quantityState.isQuantity;
							skuId = skuOpt.id;

							if(args.isDefaultSku !== 'true'){
								if(Array.isArray(quantity)){
									//quantity[CURRENT_INDEX].setQuantity(1); //cart modify인 경우 수량 초기화 안함
									quantity[CURRENT_INDEX].setMaxQuantity(quantityState.maxQty);
								}else{
									//quantity.setQuantity(1); //cart modify인 경우 수량 초기화 안함
									quantity.setMaxQuantity(quantityState.maxQty);
								}
							}
						}
					});
				});

				/* 매장예약 */
				$('.reservation-btn').click(function(e){
					e.preventDefault();
					sandbox.getModule('module_header').reDirect().setLogin(function(){
						sandbox.utils.ajax(location.href, 'GET', {reservationview:true}, function(data){
							var domObject = $(data.responseText).find('#reservation-wrap');
							$('#common-modal').find('.contents').empty().append(domObject[0].outerHTML);
							sandbox.moduleEventInjection(domObject[0].outerHTML);
							commonModal.show();
						});
					});
				});

				/* isDefaultSku - true  ( option이 없는 경우 )  */
				if(args.isDefaultSku === 'true') defaultSkuSetup(productOption);



				/* cart Update */
				$('[data-add-item]').each(function(i){
					var INDEX = i;

					$(this).find('.btn-link').click(function(e){
						e.preventDefault();
						var _self = $(this);
						var addToCartPromise = Method.moduleValidate(INDEX);

						addToCartPromise.then(function(qty){
							var $form = _self.closest('form');
							itemRequest = BLC.serializeObject($form);
							itemRequest['productId'] = productId;
							itemRequest['quantity'] = qty;

							/* 당일배송 추가 */
							var addSameday = $('[data-add-sameday]').data('add-sameday');
							if(addSameday == true){
								itemRequest['userDefinedFields[samedayDeliveryChecked]'] = true;
							} else{
								itemRequest['userDefinedFields[samedayDeliveryChecked]'] = false;
							}

							/* 애드온 상품 추가 */
							var $deferred = $.Deferred();
							var addonProductIndex = 0;
							if(addonComponents){
								for(var key in addonProductGroup){
									if(addonProductGroup[key][0].getValidateChk()){
										var childItems = addonProductGroup[key][0].getChildAddToCartItems();
									    for(var i=0; i<childItems.length; i++){
									        for(var key in childItems[i]){
												itemRequest['childAddToCartItems['+addonProductIndex+'].'+key] = childItems[i][key];
									        }
									    }
										addonProductIndex++;
									}else{
										$deferred.reject();
									}
								}
							}else{
								$deferred.resolve(itemRequest);
							}
							return $deferred.promise();
						}).then(function(itemRequest){
							var $deferred = $.Deferred();
							quantityModal.show();
							$('#order-count-select .quantity_confirm').off().click(function(e){
								if(!isOverQuantity){
									// 당일배송 변경
									var addSameday = $('[data-add-sameday]').data('add-sameday');
									if(addSameday == true){
									   itemRequest['userDefinedFields[samedayDeliveryChecked]'] = true;
									   UIkit.notify('오늘도착 상품 옵션이 선택되었습니다.', {timeout:1500,pos:'top-center',status:'success'});
									}else{
									   itemRequest['userDefinedFields[samedayDeliveryChecked]'] = false;
									   UIkit.notify('일반택배 상품 옵션이 선택되었습니다.', {timeout:1500,pos:'top-center',status:'success'});
									}
									if(Array.isArray(quantity)){
										itemRequest['quantity'] = quantity[0].getQuantity();
									}else{
										itemRequest['quantity'] = quantity.getQuantity();
									}
									quantityModal.hide();
									$deferred.resolve(itemRequest);
								}
								isOverQuantity = false;
							});

							$('#order-count-select .quantity_cancel').off().click(function(e){
								quantityModal.hide();
								$deferred.reject();
							});
							return $deferred.promise();
						}).then(function(itemRequest){
							var $deferred = $.Deferred();

							if(!sandbox.getModule('module_header').getIsSignIn()){
								UIkit.modal('#order-signin-check').show();
								$('#order-signin-check .uk-modal-confirm').off().click(function(e){
									e.preventDefault();
									sandbox.getModule('module_header').setLogin(function(data){
										if(data.isSignIn){
											$deferred.resolve(itemRequest);
										}else{
											$deferred.reject();
										}
									});
								});

								$('#order-signin-check .uk-modal-close').off('click.close').on({
									'click.close':function(e){
										$deferred.reject();
									}
								});
							}else{
								$deferred.resolve(itemRequest);
							}

							return $deferred.promise();
						}).then(function(itemRequest){
							var $form = _self.closest('form');
							var actionType = _self.attr('action-type');
							var url = _self.attr('href');

							switch(actionType){
								case 'externalLink' :
									//외부링크
									window.location.href = url;
									break;
								default :
									BLC.ajax({
										url:url,
										type:"POST",
										dataType:"json",
										data:itemRequest
									}, function(data, extraData){
										if(commonModal.active) commonModal.hide();
										if(data.error){
											UIkit.modal.alert(data.error);
										}else{
											var cartData = $.extend( data, {productId : productId, quantity : itemRequest.quantity, skuId : skuId });
											if(actionType === 'add'){
												miniCartModule.update( function( callbackData ){
													if( callbackData != null ){
														cartData.cartId = callbackData.cartId
													}
													endPoint.call('addToCart', cartData );
												});
											}else if(actionType === 'modify'){
												var url = sandbox.utils.url.removeParamFromURL( sandbox.utils.url.getCurrentUrl(), $(this).attr('name') );
												sandbox.setLoadingBarState(true);
												endPoint.call( 'cartAddQuantity', cartData );
												_.delay(function(){
													window.location.assign( url );
												}, 500);
											}else if(actionType === 'redirect'){
												sandbox.setLoadingBarState(true);
												endPoint.call( 'buyNow', cartData );
												window.location.assign( sandbox.utils.contextPath + '/checkout' );
											}
										}
									});
									break;
							}
						}).fail(function(msg){
							if(commonModal.active) commonModal.hide();
							if(msg !== '' && msg !== undefined){
								UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
							}
						});
					});
				});

				//scrollController
				var scrollArea = sandbox.scrollController(window, document, function(percent){
					var maxOffsetTop = this.getScrollTop($('footer').offset().top);
					var maxHeight = this.setScrollPer(maxOffsetTop);

					if(percent < minOffsetTop && miniOptionIS){
						miniOptionIS = false;
						$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
						$('.mini-option-wrap').find('.info-wrap_product_n').removeClass('active');
						$dim.removeClass('active');
					}else if(percent >= minOffsetTop && percent <= maxOffsetTop && !miniOptionIS){
						miniOptionIS = true;
						$('.mini-option-wrap').stop().animate({bottom:0}, 200);
					}else if(percent > maxOffsetTop && miniOptionIS){
						miniOptionIS = false;
						$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
						$('.mini-option-wrap').find('.info-wrap_product_n').removeClass('active');
						$dim.removeClass('active');
					}
				}, 'miniOption');

				//PDP 상품 설명 영역 스크롤 : 갤러리 영역내 위치 고정
				var summaryScrollController = sandbox.scrollController(window, document, function(per, scrollTop){
					if(sandbox.reSize.getState() === 'pc'){
						if($galleryWrap.height() > $optionWrap.height() && $optionWrap.height() + optionWrapOffsetTop > $(window).height()){

							var galleryHeight = $galleryWrap.height();
							var detailHeight = $productDetailWrap.height();

							//스크롤이 옵션영역 하단에 걸리는 순간
							if( scrollTop > optionWrapOffsetTop + $optionWrap.height() - $(window).height() && scrollTop < optionWrapOffsetTop + galleryHeight - $(window).height() ){
								!$optionWrap.hasClass("fixed") && $optionWrap.removeClass('fixed absolute bottom top').removeAttr("style").addClass('fixed bottom');
							}
							//스크롤이 하단으로 내려갔을 때
							else if( scrollTop >= optionWrapOffsetTop + galleryHeight - $(window).height() ){
								!$optionWrap.hasClass("absolute") && $optionWrap.removeClass('fixed absolute bottom top').removeAttr("style").addClass('absolute').css("bottom", detailHeight - galleryHeight + "px");
							}
							//스크롤이 상단으로 올라갔을 때
							else if( scrollTop <= optionWrapOffsetTop + $optionWrap.height() - $(window).height() ){
								!$optionWrap.hasClass("top") && $optionWrap.removeClass('fixed absolute bottom top').removeAttr("style").addClass('absolute top');
							}
							else {
							    $optionWrap.removeClass('fixed absolute bottom top').removeAttr("style");
							}




						} else {
							//아코디언 정보를 펼친 경우 갤러리 길이 보다 상품옵션이 더 길어 질수 있다
							$optionWrap.removeClass('fixed absolute bottom top').removeAttr("style");
						}
					}
					//스크롤 업/다운 구분을 위해 이전 스크롤 위치 기억
					previousScrollTop = scrollTop;

				}, 'product');

				$('.minioptbtn').click(function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product_n').addClass('active');
					$dim.addClass('active');
				});

				$('.mini-option-wrap').on('click', '.close-btn', function(e){
					// console.log('mini-option-wrap');
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product_n').removeClass('active');
					$dim.removeClass('active');
				});


				//guide option modal
				$this.find('.option-guide').on('click', function(e){
					e.preventDefault();
					guideModal.show();
				});


				$('.uk-quickview-close').click(function(e){
					guideModal.hide();
					isQuickView = true;
				});

				guideModal.off('.uk.modal.product').on({
					'hide.uk.modal.product':function(){
						if(isQuickView){
							setTimeout(function(){
								$('html').addClass('uk-modal-page');
								$('body').css('paddingRight',15);
							});
						}
					}
				});

				//PDP summary에서 상품 설명의 이미지 제거한 내용을 영역에 붙임.
				if($this.find('[data-long-description]').attr('data-long-description')){
					var html = $.parseHTML($this.find('[data-long-description]').attr('data-long-description'));
					var modifyHtml;

					// $(html).find('div.imgArea').remove().find('script').remove();
					// $(html).find('h3').remove().find('script').remove();
					// for (var i=0; i<$(html).find('ul').length; i++) {
					// 	if (i != 0) {
					// 		$(html).find('ul').eq(i).remove().find('script').remove();
					// 	}
					// }
					// for (var i=0; i<$(html).find('ul').length; i++) {
					// 	if (i != 0) {
					// 		$(html).find('ul').eq(i).remove().find('script').remove();
					// 	}
					// }

					if ($(html).find('h2').eq(0).text() != '') {
						modifyHtml = "<h1>" + $(html).find('h2').eq(0).html() + "</h1>";
						if ($(html).find('ul').eq(0).text() != '') {
							modifyHtml += "<ul>" + $(html).find('ul').eq(0).html() + "</ul>";
						}
					}

					$this.find('#pdp-description-summary').empty().append(modifyHtml);
				}

        		// 상품 정보 영역의 높이 줄임처리 (상품정보, 유의사항)
				$infoHeightWrap.each(function(e){
					// e.preventDefault();
					var argmts = sandbox.rtnJson($(this).attr('data-info-height'), true) || {};
					var pdpInfoSubjectHeight=78;
					var readMoreHeight=65;
					var infoType = argmts.infoType;
					var outerHeight = parseInt(argmts.outerHeight);
					var shortenHeight =  outerHeight-readMoreHeight;
					var contentsHeight = shortenHeight - pdpInfoSubjectHeight;
					var $descriptionWrap;
					if(infoType === 'attention-guide'){
						$descriptionWrap = $(this);
					} else if(infoType === 'product-detail'){
						$descriptionWrap = $(this).closest('.pop-detail-content');
					}

					if(argmts && ($descriptionWrap.outerHeight() > outerHeight || $descriptionWrap.outerHeight() === 0)){
						if(infoType === 'attention-guide'){
							$descriptionWrap.outerHeight(shortenHeight);
							$descriptionWrap.find('.guide-area').height(contentsHeight).css({'overflow':'hidden'});
							$descriptionWrap.find('#read-more').show();
						} else if(infoType === 'product-detail'){
							if($descriptionWrap.find('.conArea').length > 0){
								$descriptionWrap.find('.conArea').height(shortenHeight).width('100%').css({'overflow':'hidden'});
							}

							else if($descriptionWrap.find('.sectionR').length  > 0){
								//$descriptionWrap.find('.sectionR').height(shortenHeight).css({'overflow':'hidden'});
								//상품 설명 두번째 항목까지만 노출하고 이후 항목은 비노출처리 한다.
								//상품 설명 두번째 항목도 2줄까지만 보이도록 multi-line ellipsis 처리 한다.
								$descriptionWrap.find('.sectionR > ul:gt(2)').each(function(){
									$(this).hide();
									$(this).prev("h3") && $(this).prev("h3").hide();
								});
							}

						}
					}
				});

				$('.btn-modal-link').on('click', function(){
					var validateChk = (args.isDefaultSku === 'true') ? true : false;
					var qty = 0;

					if(args.isDefaultSku === 'false'){
						if(Array.isArray(productOption)){
							$.each(productOption, function(i){
								validateChk = this.getValidateChk('옵션을 선택해 주세요.');
							});
						}else{
							validateChk = productOption.getValidateChk('옵션을 선택해 주세요.');
						}
					}

					if ($('.opt-list').find("input[type='radio']").is(":checked")) {
						$('.order-count-select').trigger('click');
					}
				});

				//1 on 1 이미지 외 상품 설명 제거. 어드민 상품 속성에 porduct1on1이 true인 경우에만 PDP 화면 아래쪽에 표시됨.
				if($this.find('[data-1on1-description]').length > 0){
					var html = $.parseHTML($this.find('[data-long-description]').attr('data-long-description'));
					$(html).find('div.proInfo').remove().find('script').remove();
					$this.find('[data-1on1-description]').empty().append(html);
				}

				$(".detail-review").css('margin-top', "999px");

				// 리뷰 비활성시 Lazy 기능 비활성화 될 수 있게 스타일 변경.
				$(".review-active").find('.pop-detail-title').on('click', function() {
					if (!$('.review-active').find('.pop-detail-content').hasClass("uk-active")) {
						$('.detail-review').css('margin-top', "0px");
					} else {
						$('.detail-review').css('margin-top', "999px");
					}
				});

				// scrollup 위치 어긋나는 문제가 있어 추가.
				$('.pop-detail-title').click(function() {
					$('.scrollup').css('display', "none");
				});
				
				// 당일배송 휴무일 체크
				var holidayToday = false;
				var holidayList = $('#samedaybtn').attr('holidayList');
				if(holidayList != 'undefined' && holidayList != null){
					var curDay = $('#samedaybtn').attr('curDay');
					$('#samedaybtn').attr('holidayList').split(',').map(function(p){
						 var holiday = p.split('=')[0]
						 holiday = holiday.replace(/[^0-9]/g,'');
						 if(curDay == holiday){
							holidayToday = true;
							return false;
						 }
					});					
				}
				// 당일배송 매장 체크
				/*
				var storeCheck = true; //default show
				var storeLocation = $('#samedaybtn').attr('location');
				if(storeLocation != 'undefined' && storeLocation != null){
				   BLC.ajax({
					  url:Core.Utils.contextPath + '/checkout/checkSamedayDeliveryByLocation',
					  type:"GET",
					  async:false,
					  data:{'locationId' : storeLocation}
				   }, function(data){
					  if(!data){
						 storeCheck = false; //당일배송 매장 아닌경우 false
						 return false;
					  }
				   });                  
				}
				if(holidayToday == false && storeCheck == true){
				   $('#samedaybtn').show();
				}
				*/
			},
			moduleValidate:function(index){
				var INDEX = index;
				var deferred = $.Deferred();
				var validateChk = (args.isDefaultSku === 'true') ? true : false;
				var qty = 0;

				if(args.isDefaultSku === 'false'){
					validateChk = sandbox.utils.getValidateChk(productOption, '옵션을 선택해 주세요.');
				}

				if(Array.isArray(quantity)){
					qty = quantity[INDEX].getQuantity();
				}else{
					qty = quantity.getQuantity();
				}

				if(validateChk && isQuantity && qty != 0){
					deferred.resolve(qty);
				}else if(!isQuantity || qty == 0){
					deferred.reject(args.errMsg);
				}else{
					deferred.reject();
				}

				return deferred.promise();
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product]',
					attrName:'data-module-product',
					moduleName:'module_product',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$this = null;
				args = [];
				productOption = null;
				quantity = null;
			},
			getItemRequest:function(){
				return itemRequest;
			},
			getSkuData:function(){
				return currentSkuData;
			}
		}
	});
})(Core);
