(function(Core){
	Core.register('module_mobilegnb', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0];
				var clickIS = false;
				var isSearch = false;				

				// if( $("body").attr("data-device")==="pc") {
				// 	$("#mobile-menu").removeClass("uk-offcanvas");
				//  }
				
				 
				var $mobile = $('#mobile-menu');
				$mobile.find('.mobile-onedepth_list, .mobile-twodepth_list').on('click', '> a', function(e){ // webPos 수정 .mobile-twodepth_list 추가
					if(!$(this).hasClass('link')){
						e.preventDefault();
						$(this).siblings().show().stop().animate({'left':0}, 300);
					}
				});

				$mobile.find('.location .icon-arrow_left').on('click', function(e){ // webPos 수정 .icon-arrow_left 추가
					e.preventDefault();
					$(this).parent().parent().stop().animate({'left':-270}, 300, function(){
						$(this).css('left', 270).hide();
					});
				});

				$mobile.on('show.uk.offcanvas', function(event, area){
					try {
						Core.ga.pv('pageview', '/mobileMenu');
					} catch (error) { }
				});

				$mobile.on('hide.uk.offcanvas', function(event, area){
					if(isSearch){
						sandbox.getModule('module_search').searchTrigger();
					}
					isSearch = false;
				});

				$mobile.find('.mobile-lnb-search').on('click', function(e){
					e.preventDefault();
					isSearch = true;
					UIkit.offcanvas.hide();
				});

				$mobile.find(".btn-slide-close").click(function(){
					UIkit.offcanvas.hide();
				});

				// WebPOS 모바일 메뉴 활성화시 onedepth 메뉴로 보이기 위한 하위 메뉴 스타일 처리.
				$(".header-menu_mobile").click(function(){
					$(".mobile-menu_twodepth").css("left", "270px");
					$(".mobile-menu_twodepth").css("display", "none");	
					$(".mobile-menu_threedepth").css("left", "270px");
					$(".mobile-menu_threedepth").css("display", "none");

					$(".user-state").show();
					$(".mobile-menu_onedepth li").show();
				});
				
				// WebPOS Fulfillment 이동 스크립트.
				$(".btn-fulfillment").click(function(e){
				    e.preventDefault();
				    /*
				    var url = $(this).find('a').attr('href');
					//location.href = sandbox.utils.contextPath + "/fulfillment";
					//location.href = "http://uat-adm-assist-nike.brzc.kr/fms-dashboard";
					location.href = url;
					*/
				});

				//2019-05-27 native app pushToken 요청 
				if($(this).find("input[name='assistLocation']")){
				    var assistLocation = $(this).find("input[name='assistLocation']").val();
				    
    				if(sessionStorage.getItem('requestPushToken') == "false" && assistLocation !== undefined){
    					try{
    						window.location.href = "webtoapp://requestPushToken?assistLocation="+assistLocation
    					}catch(error){
    						
    					}
    					sessionStorage.setItem('requestPushToken', true);
    				}
				}
			
				$(window).load(function(){
    				//2019-05-30 push 알림을 클릭하고 들어온경우 (파라미터로 판단), 어드민페이지로 이동하기위해 walkThrough 이동 
                    function getParameterByName(name) {
                	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                	        results = regex.exec(location.search);
                	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                	}
                
                	var pushRedirectUrl = getParameterByName('pushredirect');
                
                	//app push 알림 클릭시 pushRedirectUrl에 온 파라미터 맞춰서 redirect 
                	 if(pushRedirectUrl == "fms-order-confirm"){
                	    if(!Core.getModule('module_header').getIsSignIn()){
                	        history.replaceState({}, null, location.pathname);
                			$('.btn-link02').click();
                        }else{
				            var now = moment().format('YYYY.MM.DD');
				            var startTime = ' 00:00:00';
				            var endTime = ' 23:59:59';
				            var submitDate = moment(now, 'YYYY.MM.DD').subtract(3, 'day').format('YYYY.MM.DD');
				            
                    		var url = "/fms-order-confirm?sortDirection=DESCENDING&term=threeday&sortProperty=order.submitDate&pType=ALL&searchType=order.orderNumber"
                    		url = url + "&order.submitDate=" + submitDate  + startTime +  '|' + now + endTime;
                    		var target = "admin";
                    
                    		Core.Utils.walkThrough( target, encodeURIComponent(url) );
                		}
                	}
            	});

			}
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-mobilegnb]',
					attrName:'data-module-mobilegnb',
					moduleName:'module_mobilegnb',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);