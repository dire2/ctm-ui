(function(Core){
	Core.register('module_posmain', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $form = $('#main-search-form'); // form
				var $searchWrap = $('#mainSearch'); // 검색 버튼 외부 span
				var $input = $('#main-search'); // 검색 input
				var $label = $('label[for="search"]'); // placeholder 노출용
				var $del = $('#mainDelete'); // 검색어 삭제 버튼

				$input.keydown(function(e) {
					if( $(this).val() != "" && e.keyCode == 13 ){
						$form.submit();
					}
				})

				$form.on('click', function(){
					$input.focus();
				});

				$label.on('click', function(){
					$input.focus();
				});

				$input.focus(function(e) {
					$searchWrap.addClass('focus');
				});

				$input.focusout(function(e) {
					if( $(this).val() == "" ){
						$searchWrap.removeClass('focus');
					}
					$("body").focus();
				});

				$del.on('click', function(e){
					e.preventDefault();
					if( $input.val() != "" ){
						$input.val('');
					}
					$input.focus();
				})

				if ($input.val() != "") {
					$input.focus();
				};
				/*
				// WebPOS 메인 페이지에 검색어 입력 값이 없으면 검색 불가.
				$("input[id=main-search]").keydown(function() {
					if ($("input[id=main-search]").val() == "" && event.keyCode == 13) {
						event.preventDefault();
					}
				});

				// WebPOS 메인 페이지에서 검색어 입력이 안된상태에서는 검색버튼 비활성화.
				if ($("input[id=main-search]").val() == "") {
					$("button[id=main-search-btn]").prop("disabled", true);
				}
				
				// WebPOS 메인 페이지의 검색어 입력이되면 검색 버튼 활성화.
				$('#main-search').on('change', function(){
					if ($("input[id=main-search]").val() == "") {
						$("button[id=main-search-btn]").prop("disabled", true);
					} else {
						$("button[id=main-search-btn]").prop("disabled", false);
					}
				});

				$('#main-search-form').on('click', function(){
					$("#mainSearch").attr('class','input-textfield width-max focus');
					$("input[name='q']").focus();
				});
				
				// WebPOS 메인 페이지 검색 포커스 제어 (포커스 활성화).
				$("label[for='search']").click(function(event) {
					$("#mainSearch").attr('class','input-textfield width-max focus');
					$("input[name='q']").focus();
				});
				
				$('#mainDelete').on('click', function(event){
					event.preventDefault();
					$("input[name='q']").val('').focus();
				})
				
				// WebPOS 메인 페이지 검색 포커스 제어 (포커스 비활성화).
				$("input[name='q']").focusout(function(event) {
					if( $(this).val() == "" ){
						$("#mainSearch").attr('class','input-textfield width-max');    
					}
					//$("input[name='q']").val(''); //검색어 내용 삭제.
					$("body").focus();
				});
				*/

				$("#barcodeCall").click(function(){
					location.href = "seamless://barcode=product";
				});

			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-posmain]',
					attrName:'data-module-posmain',
					moduleName:'module_posmain',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);