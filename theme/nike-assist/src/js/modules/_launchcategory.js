(function(Core){
	Core.register('module_launchcategory', function(sandbox){
		var $that, category;
		// var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $cate = $(".launch-category");
				var $cate_header = $(".launch-lnb");

                if(arguments[0] && undefined != arguments[0].category){
					category = arguments[0].category;
				}

				//upcoming일때는 헤더 우측 뷰 변경 아이콘 숨기기
				if(category === 'upcoming'){
					$this.find('.toggle-box').hide();
				}
				// console.log('category:', category);
				

				var Listform = {
				    grid : function(setCookie){
				        $(".launch-list-item", $cate).removeClass("gallery").addClass("grid");
						$(".toggle-box span", $cate_header).removeClass("ns-grid").addClass("ns-feed");
						if(setCookie){$.cookie("launch_view_mode", "grid" , {path : "/"});}
				        
				    },
				    gallery : function(setCookie){
				        $(".launch-list-item", $cate).removeClass("grid").addClass("gallery");
				        $(".toggle-box span", $cate_header).removeClass("ns-feed").addClass("ns-grid");
				        if(setCookie){$.cookie("launch_view_mode", "gallery" , {path : "/"});}
					}
				};
				
				//날짜 순으로 상품 정렬
				var productListOrderedByDate = $cate.find('.launch-list-item').slice().sort(function(el1, el2){
					// var date1 = new Date($(el1).data('active-date')).getTime();
					// var date2 = new Date($(el2).data('active-date')).getTime();
					var date1 = Method.DateParse($(el1).data('active-date'));
					var date2 = Method.DateParse($(el2).data('active-date'));
				
					//descending
					//return -(date1 > date2) || +(date1 < date2) || (isNaN(date1)) - (isNaN(date2));
					//ascending
					return +(date1 > date2) || -(date1 < date2) || (isNaN(date1)) - (isNaN(date2));
				});

                //up-coming의 경우 날짜 순으로 정렬필요
				if(category ==="upcoming"){				
					$cate.find('.uk-grid').empty().append(productListOrderedByDate);
					
					(function(list){
						 var xYear = "", xMonth = "", xDay = "";
						 list.each(function(){
							 var 
								 $this = $(this),
								 date = $this.attr("data-active-date").split(" ")[0],
								 arrDate = date.split("-"),
								 year = arrDate[0],
								 month = arrDate[1],
								 day = arrDate[2];
								 
								 //월이나 일이 바뀌면
								 if ( xMonth !== month || xDay !== day ){
									 $this.before("<em class='upcoming-tit-date'>" + month + "월 " + day + "일</em>")
								 }
								 
								 //update state
								 xYear = year;
								 xMonth = month;
								 xDay = day;
 
						});
					
					})($cate.find('.launch-list-item'));
				 }

				if( $.cookie("launch_view_mode") && category !== "upcoming" ){
					( $.cookie("launch_view_mode") === "gallery" ) ? Listform.gallery(true) : Listform.grid(true);
				} 
				//쿠키가 없는 경우(우측 뷰 변경 아이콘을 누르지 않은 경우)
				//feed는 기본이 gallery, in-stock는 기본이 grid, updoming은 gallery만 있음.
				else if(!$.cookie("launch_view_mode")){
                    if(category ==="in-stock"){
						Listform.grid(false);
					} else {
						Listform.gallery(false)
					}
				}
				else {
					( category === "in-stock" ) && Listform.grid(true);
				}				

				$cate.css("opacity", "1");

                //마우스 클릭시 탭 하단 선택 표시
				$('.launch-menu').on('click', 'li', function(){
					$('.launch-menu li.on').removeClass('on');
					$(this).addClass('on');
				});
                //우측 뷰 변경 아이콘 
				$this.find('.toggle-box a').on('click', function(e){
				    
				    if( category === "feed" || category === "in-stock" ){
				        if( $(".launch-list-item", $cate).eq(0).hasClass("gallery") ){
				            Listform.grid(true);
				        }
				        else {
				            Listform.gallery(true);
				        }
				    }
				    else {
				    }
				    
					e.preventDefault();
					
				});
				//카테고리에서 상픔 클릭시 상단 탭 선택표시 off 및 우측 아이콘 숨김
				$('.launch-category .uk-grid div a').on('click', function(){
					$('.launch-menu li.on').removeClass('on');
				});
			},
			DateParse:function(dateStr){
				var a=dateStr.split(" ");
				var d=a[0].split("-");
				var t=a[1].split(":");
				return new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-launchcategory]',
					attrName:'data-module-launchcategory',
					moduleName:'module_launchcategory',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
