Handlebars.registerHelper('isHiddenInfo', function(key, option) {
	if((key !== 'storeType') && (key !== '지번')){
		return option.fn(this);
	}
});

Handlebars.registerHelper('isStoreType', function(key, option) {
	if((key === 'storeType')){
		return option.fn(this);
	}
});


//valueOf() => indexOf('ntc')  수정
//storeType 이 nrc,ntc,...direct 스트링 형태로 올경우 아이콘 노출이 안되는 현상 발생.
//Handlebars.registerHelper('isReservation', function(key, option) {
//	if(this && this.valueOf() === 'reservation'){
//		return option.fn(this);
//	}
//});

//ASSIST 추가.
Handlebars.registerHelper('isAssist', function(key, option) {
   //if(option.data.root.isFulfill==true && option.data.root.fulfillmentType.indexOf('PHYSICAL_PICKUP') != '-1'){
    if(option.data.root.additionalAttributes.storeType != undefined){
       if(option.data.root.additionalAttributes.storeType.indexOf('assist') != '-1'){
          return option.fn(this);
       }
    }
});

//nfs
Handlebars.registerHelper('isNFS', function(key, option) {
    if(option.data.root.additionalAttributes.storeType != undefined){
       if(option.data.root.additionalAttributes.storeType.indexOf('nfs') != '-1'){
          return option.fn(this);
       }
    }
});

Handlebars.registerHelper('isNRC', function(key, option) {
	if(this && this.indexOf('nrc') != '-1'){
		return option.fn(this);
	}
});

Handlebars.registerHelper('isNTC', function(key, option) {
	if(this && this.indexOf('ntc') != '-1'){
		return option.fn(this);
	}
});

Handlebars.registerHelper('isReservation', function(key, option) {
	if(this && this.indexOf('reservation') != '-1'){
		return option.fn(this);
	}
});

Handlebars.registerHelper('isDirect', function(key, option) {
	if(this && this.indexOf('direct') != '-1'){
		return option.fn(this);
	}
});

Handlebars.registerHelper('isBusinessHours', function(key, option) {
	if((this['name'] === '영업시간') ){
		return option.fn(this['value']);
	}
});

//handlebars helper
Handlebars.registerHelper('addClass', function(index, option){
   if(index == 0){
	  return 'active';
   }else{
	  return ''
   }
});

// Handlebars.registerHelper('showLog', function(key, option){
// console.log(option);
//  });
