
(function(Core){
	// 전역으로 사용될 기본 변수명
	var md = null;
	var queryString = "";
	var country = 'kr';
	var currencyType = 'KRW';

	function init(){
        // console.log('GTM');
		md = _GLOBAL.MARKETING_DATA();
		// console.log('md: ', md);
		isMobile = _GLOBAL.DEVICE.IS_MOBILE();
		queryString = Core.Utils.url.getQueryStringParams( Core.Utils.url.getCurrentUrl());
		setClintDataLayer(getClintDataLayer());
	}

	function getClintDataLayer(){
		var dl = {};
		// 기본값 설정
		dl.order = {
			currencyCode: currencyType,
			orderId: '',
			promoCodeList: {},
			shipping: 0,
			subtotal: 0,
			tax: 0,
			total: 0,
		}
		dl.filter = '';
		dl.productList = [];
		switch( md.pageType ){
			case "category" :
				$.extend( dl, getCategoryData());
			break;
			case "product" :
				$.extend( dl, getProductData());
			break; 
			case "cart" :
				$.extend( dl, getCartData());
			break;
			case "checkout" :
				$.extend( dl, getCheckoutData());
			break;
			case "confirmation" :
				$.extend( dl, getOrderConfirmationData());
			break;
			case "register":
				$.extend( dl, getRegisterStartData());
			break;
			case "registerSuccess":
				$.extend( dl, getRegisterSuccessData());
			break;
			case "content":
			break;
		}
		return $.extend( dl, getPageData());
	}

	function setClintDataLayer(dl){
		window.marketingClientDataLayer = window.marketingClientDataLayer || [];
		window.marketingClientDataLayer.push(dl);
		// console.log(window.marketingClientDataLayer);
	}

	function getPageData(){
		var data = {};
		var pageName = '';

		data.application = {
            country: "kr",
            device: isMobile == true ? 'Mobile' :	"Desktop",
			language: "ko-KR"
        }
		data.consumer = {
			currencyType: currencyType
		},
		data.loggedIn = _GLOBAL.CUSTOMER.ISSIGNIN ? 'Y' : 'N';
		
		switch(md.pageType){
			case 'home' :
				pageName = 'home';
				data.event = 'onLandingPageViewed';
			break;
			case 'category' :
				pageName = 'gridwall';
			break;
			case 'product' :
				pageName = 'pdp';
			break;
			case 'cart' :
				pageName = 'cart';
			break;
			case 'checkout' :
				pageName = 'checkout';
				if( md.marketingData.checkoutInfo != null ){
					pageName = md.marketingData.checkoutInfo.step;
				}
			break;
			case 'confirmation' :
				pageName = 'order confirmation';
			break;
			case 'register' :
				pageName = 'registration';
			break;
			case 'registerSuccess' :
				pageName = 'member joined';
			break;

			case 'content' :
				if( Core.utils.string.startsWith(md.pathName, '/l') ){
					pageName = 'landing page';
				}
			break;
		}
		data.application.view = pageName;
		return data;
	}

	// 카테고리 정보
	function getCategoryData(){
		var data = {};
		data.event = 'onProductListViewed';
		data.filter = '';
		if( md.categoryInfo != null ){
			data.ecommerce = {
				currencyCode: currencyType,
				impressions: makeProducts(md.itemList)
			}
		}
		data.productList = makeProducts(md.itemList, 'detail');
		return data;
	}

	// 상품정보
	function getProductData(){
		var data = {};
		data.event = 'onProductViewed';
		if( md.productInfo != null ){
			data.ecommerce = {
				detail: {
					products: makeProducts([md.productInfo])
				}
			}
			data.productList = makeProducts([md.productInfo], 'detail');
		}
		return data;
	}

	// 카트 정보
	function getCartData(){
		var data = {};
		data.event = 'onCartViewed';
		data.productList = makeProducts(md.itemList, 'detail');
		return data;
	}

	// 주문서 정보
	function getCheckoutData(){
		var data = {};
		data.event = 'onCheckoutViewed';
		var checkoutStep = '';
		if( md.marketingData.checkoutInfo != null ){
			checkoutStep = md.marketingData.checkoutInfo.step;
		}
		if( checkoutStep == 'payment'){
			data.productList = makeProducts(md.itemList, 'detail');
		}
		return data;
	}

	// 주문완료 정보
	function getOrderConfirmationData(){
		var data = {};
		data.event = 'onOrderCompleted';
		var promoList = (function(){
			var list = [];
			try{
				$.each( md.promoList, function( index, promoData ){
					list.push(promoData.name);
				});
			}catch(e){ console.log(e)}
			return list;
		}())
		data.ecommerce = {
			currencyCode: currencyType,
			purchase: {
				actionField: {
					id: md.orderId,
					coupon: promoList.join('|'),
					revenue: md.orderTotalAmount,
					shipping: md.orderShippingTotalAmount,
					tax: 0
				},
				products: makeProducts(md.itemList)
			}
		}
		data.order = {
			currencyCode: currencyType,
			orderId: md.orderNumber,
			promoCodeList: promoList,
			shipping: md.orderShippingTotalAmount,
			subtotal: md.orderSubTotalAmount,
			tax: 0,
			total: md.orderTotalAmount
		},
		data.productList = makeProducts(md.itemList, 'detail');
		return data;
	}

	// 가입 시작 정보
	function getRegisterStartData(){
		var data = {};
		data.event = 'onRegistrationViewed';
		return data;
	}

	// 회원가입 완료 페이지
	function getRegisterSuccessData(){
		var data = {};
		data.event = 'onMemberJoined';
		return data;
	}

	function makeProducts( itemList, type ){
		var products = [];
		try{
			var isDetail = ( type == 'detail');
			$.each( itemList, function( index, productData ){
				var url = productData.url;
				var name = String(productData.name);
				var breadCrumbs = productData.breadcrumbs;
				if(url == null){
					return;
				}
				var product = {
					brand: function(){
						var brand = 'Nike';
						// Nike, Nike Pro, Jordan, ACG, NikeLab, Nike Sportswear, NBA
						// acg는 스포츠 웨어에 속하지만 먼저 체크하여 넘김
						if(url.indexOf('acg-') > -1 || url.indexOf('-acg') > -1){
							brand = 'ACG';
						}else if(url.indexOf('-np-') > -1){
							brand = 'Nike Pro';
						}else if(url.indexOf('/nike-sportswear/') > -1){
							brand = 'Nike Sportswear';
						}else if(url.indexOf('/ap/basketball/') > -1 || url.indexOf('/eq/basketball/') > -1){
							brand = 'NBA';
						}
						
						if( name.indexOf('조던') > -1){
							brand = 'Jordan';
						}else if(name.indexOf('NikeLab') > -1){ // nikelab은 유니크한 값이 없어서 정확하게 체크 할 수 없다.
							brand = 'NikeLab';
						}
						return brand;
					}(),
					category: function(){
						// 신발 FOOTWEAR, 
						// 의류, 모자 APPAREL, 
						// 애플워치, 가방, 양말 EQUIPMENT
						var category = '';
						if(url.indexOf('/fw/') > -1){
							category = 'FOOTWEAR';
						}else if(url.indexOf('/ap/') > -1){
							category = 'APPAREL';
						}else if(url.indexOf('/eq/') > -1 || url.indexOf('/aw/') > -1){
							category = 'EQUIPMENT';
						}
						return category;
					}(),
					id : productData.id,
					name: String(productData.name),
					quantity: productData.quantity == null ? 1 : productData.quantity,
				}
				if( !isDetail ){
					product.price = productData.price;
					product.variant = productData.model;
				}else{
					product.cloudProductId = '';
					product.color = productData.model;
					product.country = country;
					product.currentPrice = productData.price || productData.retailPrice;
					product.currentPriceTotal = product.currentPrice * product.quantity; // 확인 필요
					product.fullPrice = productData.retailPrice;
					product.genders = (function(){
						var genders = [];
						if(url.indexOf('/men/') > -1){
							genders = ['MEN'];
						}else if(url.indexOf('/women/') > -1){
							genders = ['WOMEN'];
						}else if(url.indexOf('/adult-unisex/') > -1){
							genders = ['MEN,WOMEN'];
						}else if(url.indexOf('/kids/') > -1 || url.indexOf('/baby/') > -1 || url.indexOf('/junior/') > -1){
							if(breadCrumbs.indexOf('남아') > -1){
								genders = ['BOYS'];
							}else if(breadCrumbs.indexOf('여아') > -1){
								genders = ['GIRLS'];
							}else{
								genders = ['BOYS,GIRLS'];
							}
						}
						return genders;
					}());
					product.inventoryLevel = '';
					product.isNikeByYou = false;
					product.isOnSale = ( product.currentPrice < product.fullPrice );
					product.size = (function(){
						var size = '';
						if(productData.option != null && productData.option.length > 0){
							try{
								for(  var key in productData.option[0] ){
									size = productData.option[0][key];
								}
							}catch(e){console.log(e);}
						}
						return size;
					}())
					product.sku = productData.skuId || '';
					product.subCategory = []; // 확인 필요
					// "Tennis"
					// "Skateboarding"
					// "Running"
					// "Training      /bra
					// Soccer/Football
					// Swimming
					// "American Football"
				}
				products.push( product );
			})
		}catch (error) { 
			console.log(error);
		}
		return products;
	}

	function addEvent(){
		var endPoint = Core.getComponents('component_endpoint');
		var data = {};
		var selectedSize = '';
		endPoint.addEvent('pdpColorClick', function( param ){
			// console.log('color', param);
			data = getClintDataLayer();
			try{
				data.event = 'onColorChange';
				data.productList[0].color = param.model;
				setClintDataLayer(data);
			}catch(e){console.log(e);}
		});
		endPoint.addEvent('pdpOptionClick', function( param ){
			// console.log('size', param);
			data = getClintDataLayer();
			try{
				data.event = 'onSizeSelect';
				data.productList.push({
					size : param.value,
					sku : (function(){
						var optionId = param['data-product-options'][0].selectedValue;
						var skuId = '';
						/*
						$.each(param['data-product-options'][0].allowedValues, function(index, option){
							if( option.value == param.value){
								optionId = option.id;
								return false;
							}
						})
						*/
						$.each(param.skuData, function(index, sku){
							if( sku.SIZE == optionId ){
								skuId = sku.skuId;
								return false;
							}
						})
						return skuId;
					}())
				})
				selectedSize = param.value;
				setClintDataLayer(data);
			}catch(e){console.log(e);}
		});
		endPoint.addEvent('addToCart', function( param ){
			// console.log('addToCart', param);
			data = getClintDataLayer();
			try{
				data.event = 'onProductAdded';
				data.ecommerce = {
					currencyCode: currencyType,
					add: {
						products : data.ecommerce.detail.products.slice()
					}
				}
				data.ecommerce.add.products[0].quantity = param.quantity;
				data.ecommerce.add.products[0].price = param.price.amount;

				data.productList[0].size = selectedSize;
				data.productList[0].sku = param.skuId;
				data.productList[0].quantity = param.quantity;

				setClintDataLayer(data);
			}catch(e){console.log(e);}
		})
		endPoint.addEvent('changeSelect', function( param ){
			// 추후 
			if( param.name == 'sort'){
				// console.log('changeSelect', param);
				data = getClintDataLayer();
				try{
					data.event = 'onProductListFiltered';
					data.filter = (function(){
						switch(param.value) {
							case 'price asc':
								return 'priceAsc';
							break;
							case 'price desc':
								return 'priceDesc';
							break;
							case 'default':
								return 'newest';
							break;
							default :
								return '';
							break
						}
					})();
					setClintDataLayer(data);
				}catch(e){console.log(e);}
			}
		})
	}

	function debug( data, alert ){
		//console.log( data );
		if( alert == true ){
			alert( data );
		}
	}
	Core.gtm = {
		// 함수를 구분짓는것이 큰 의미는 없지만 추후 형태의 변화가 있을것을 대비해서 구분
		init : function(){
			init();
			addEvent();
		}
	}

})(Core);