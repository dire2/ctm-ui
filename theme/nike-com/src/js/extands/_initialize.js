// 현재 테마에서 기본적으로 호출 되어야 하는 스크립트
(function(Core){
	$(document).ready(function(){
		// <![CDATA[

		var md = _GLOBAL.MARKETING_DATA();
		var autoOpenModalId = Core.Utils.getQueryParams(Core.Utils.url.getCurrentUrl()).am;
		var isAutoModalOpen = autoOpenModalId != null;
		if (_.isEqual(md.pageType, 'home') && isAutoModalOpen) {
			switch (autoOpenModalId) {
				case 'fp':
					if (!_GLOBAL.CUSTOMER.ISSIGNIN){
						$('[data-btn-forgot-password]').trigger('click');
					}else{
						UIkit.modal.alert('이미 로그인 되어 있습니다.');
					}
					break;

				default:
					break;
			}
		}
		
		window.addEventListener('pageshow', function (event) {
			if (event.persisted) {
				Core.Loading.hide();
			}
		});
		$(":checkbox").attr("autocomplete", "off");
		$(":radio").attr("autocomplete", "off");

		var channelFunnels = $('input[name="channelFunnels"]').val(); // admin에 등록된 채널정보
		var rUrl = document.referrer;
		var cUrl = Core.Utils.url.getCurrentUrl();
		var rUri = Core.Utils.url.getUri(rUrl); 
		var cUri = Core.Utils.url.getUri(cUrl);
		var params = Core.Utils.getQueryParams(cUrl);
		var cQueryParams = [];

		$.each(params, function (data) {
			cQueryParams.push(data);
		})

		/*
		console.log(cQueryParams);
		console.log(rUri);
		console.log(cUri);
		*/
		
		// 등록된 유입채널 정보가 있을 때
		if (!_.isEmpty(channelFunnels)){
			channelFunnels = channelFunnels.split(',');
			if (_.isEmpty(rUrl)) {
				rUri.host = '';
			}
			
			/*
			console.log(channelFunnels);
			*/
			// param에 유입채널 정보가 있고 referrer 가 현재 사이트가 아니면
			if (!_.isEqual(rUri.host, cUri.host) && _.intersection(channelFunnels, cQueryParams).length > 0) {
				sessionStorage.setItem('AFFILIATE_INFLOW_URL', cUri.url);
				sessionStorage.setItem('AFFILIATE_INFLOW_PARAM', cUri.query.slice(1, 250));
			}
		}
		
		if (!_.isEqual(rUri.host, cUri.host)){
			if (_.intersection(['broadcastId','orderCallBackId'], cQueryParams).length == 2) {
				sessionStorage.setItem('LIVE_COMMERCE_INFLOW_BROADCAST_ID', params.broadcastId);
				sessionStorage.setItem('LIVE_COMMERCE_INFLOW_ORDERCALLBACK_ID', params.orderCallBackId);
			}else{
				sessionStorage.removeItem('LIVE_COMMERCE_INFLOW_BROADCAST_ID');
				sessionStorage.removeItem('LIVE_COMMERCE_INFLOW_ORDERCALLBACK_ID');	
			}
		}
		

		
		/*
		$('[data-global-click-area]').on('click', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			setTimeout(function(){
				window.location.href = url;
			}, 300)
		})
		*/
		
		/*
		$.removeCookie('MAIN_LINK');
		$('[data-click-logo]').on('click', function(e){
			e.preventDefault();
			$.cookie('MAIN_LINK', true);
			location.href = $(this).attr('href');
		})
		*/

		/*
		@pck 20210802 닷컴 HOME 화면 파라미터 제거처리 이슈로 cp변수 수집 불가 현상 발생, /extands/adobeAnalyzerScript init()으로 이동
		if (typeof (history.replaceState) == 'function'){
			if (Core.Utils.url.getUri(Core.Utils.url.getCurrentUrl()).path == '/kr/ko_kr/'){
				history.replaceState({}, '', '/kr/ko_kr/');
			}
		}
		 */
		//]]>
	})
})(Core);