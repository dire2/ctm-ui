(function (Core) {
   Core.register('module_new_gnb', function (sandbox) {	  
	  	   
      var $this, endPoint;
      var scrollTemp = 0;

      var Method = {
         moduleInit: function () {
            $this = $(this);
            endPoint = Core.getComponents('component_endpoint');
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('[data-preheader-area]').outerHeight();

            $(window).scroll(function (event) {
               didScroll = true;
            });
            
            setInterval(function () {
               if (didScroll) {
                  Method.hasScrolled();

                  didScroll = false;
               }
            },  1);
            
            
            Method.customServiceActiveValidate(); //웹 또는 모바일에서  Custom Product화면(PDP) GNB메뉴 노출되게 방어로직 추가
            
            $(window).resize(win_resize);            
            function win_resize(){
            	var win_w = $(window).outerWidth();
            	var body_w = $('html, body').outerWidth();

            	$('[data-module-new-gnb]').css('width', win_w);
            	$('.wrapper').css('width', win_w);
            	$('.footer-contents').css('width', win_w);
            	Method.customServiceActiveValidate(); //Custom Service에서 세로>가로>세로 시 GNB메뉴 활성화 숨김
            }

            //Website 상단메뉴 활성화
            $('[data-menulink-area]').on('mouseenter', Method.gnbMenuOn);
            
            //Website 상단메뉴 비활성화
            $('[data-nkgnb-area]').on('mouseleave', Method.gnbMenuOff);
            
            //검색창 열기 및 검색
            $('[data-new-searcharea]').on('click', Method.prepareSearch);
            
            //검색창 닫기
            $('[data-new-searchclose]').on('click', Method.searchClose);

            //검색창 입력 시 발생되는 event
            $('[data-newsearch-input]').on('keyup', Method.searchKeyUp);

            //검색창 Submit event
            $('form#search-form').on('submit', Method.searchSubmit);

            //추천 검색어 최초 click event init
            $('[data-popular-search]').on('click', Method.popularSearchClick);

            //Mobile menu 활성화 event
            $('[data-btn-mobilemenu]').click(Method.mobileMenuActive);
            
            //MobileMenu 1depth -> 2depth  
            $('.next_depth').click(Method.mobileMenuFS);

            //MobileMenu 2depth -> 3depth 
            $('.nxt_dp').click(Method.mobileMenuST);
            
            //MobileMenu 2depth -> 1depth
            $('.depth2 .back_m_menu').click(Method.mobileMenuFirstBack);
            
            //MobileMenu 3depth -> 2depth
            $('.depth3 .back_m_menu').click(Method.mobileMenuSecondBack);
            
            //상단 로그인 후 자신의 이름 mouseenter 시 메뉴 활성화
            $('[data-user-dropmenu]').on('mouseenter',Method.signOnUserMenuOn);
            
            //상단 로그인 후 자신의 이름 mouseleave 시 메뉴 비활성화
            $('[data-user-dropmenu], [data-user-area]').on('mouseleave',Method.signOnUserMenuOff);
            
            //Mobile 다시 검색하기 기능
            $('.search_less_txt_link a').on('click',Method.prepareSearch);
            
            //검색어 삭제 버튼
            $('[data-searchclear-btn]').on('click',Method.searchClear);
            
            //mobile -> pc 로 이동시 mobile 관련된 메뉴 및 효과 비활성화
            $(window).resize(Method.mobileEffectOff);
            
            //검색어 삭제 버튼 hide 
            $('[data-searchclear-btn]').on('click',Method.searchHide);
            
            //dim close
            $('[data-dimd-area]').on('click',Method.dimClose);
            
            $('[data-btn-member-sub-menu]').on('click',Method.mobileMemberMenuOn);

            //현재 url과 매칭되는 depth로 go
            Method.activeCurrentSubmenu();

            //최근 검색어 존재 시 출력
            Method.getRecentlyKeywords();

            //자동 검색어 처리 부
            Method.initAutoCompleteKeyword();
         },
         activeCurrentSubmenu:function(){
            var   depthClickEvent = document.getElementsByClassName('depth_link');
                  if(depthClickEvent.length == 0) return false;

                  for(var i = 0 ; i < depthClickEvent.length ; i++) {
                     depthClickEvent[i].addEventListener('click', function () {
                        sessionStorage.setItem('currentClickedLink', this.dataset.clickName);
                     });
                  }

            //session으로 이전 페이지 gnb 클릭여부 체크
            var currentMenuItem = null;
            if(sessionStorage.getItem('currentClickedLink') !== null) {
               var   currentClickedLink = sessionStorage.getItem('currentClickedLink');
                     currentMenuItem = document.querySelector("[data-container-mobile-menu] [data-click-name='"+ currentClickedLink +"']");
                     sessionStorage.removeItem('currentClickedLink');
            }

            if(currentMenuItem !== null){
               //depth 2 처리부
               if( currentMenuItem.classList.contains('depth2_link') ){
                  $(currentMenuItem).closest('.mob_gnb > ul.next_pnl > li').addClass('on');
                  $(currentMenuItem).closest('.mobile_menu_inner').addClass('rotate');
                  $(currentMenuItem).addClass('current_link');
               }
               //depth 3 처리부
               if( currentMenuItem.classList.contains('depth3_link') ){
                  $(currentMenuItem).closest('ul.next_pnl > li').addClass('on');
                  $(currentMenuItem).closest('.mob_gnb > ul.next_pnl > li').addClass('on');
                  $(currentMenuItem).closest('.mobile_menu_inner').addClass('rotate');
                  $(currentMenuItem).closest('.mobile_menu_panel.depth2').addClass('rotate');
                  $(currentMenuItem).addClass('current_link');
               }
            }
         },
         getRecentlyKeywords:function(){
            /*
               Variable validations
             */
            var recentlyKeywordListArea =
                ( document.querySelector("[data-recently-searched-keyword-area]") !== null ) ?
                    document.querySelector("[data-recently-searched-keyword-area]") : false;
            var recentlyKeywordListElement =
                ( document.querySelector("[data-recently-searched-keyword-list]") !== null ) ?
                    document.querySelector("[data-recently-searched-keyword-list]") : false;

            if(recentlyKeywordListArea == false) return false;
            if(recentlyKeywordListElement == false) return false;
            if( Method.getSession('recentlySearchKeywords') == false ) {
               recentlyKeywordListArea.style.display = "none";
               return false;
            }else{
               //update list
               Method.updateRecentlyKeywordList(false);

               var deleteAllButton = recentlyKeywordListArea.querySelector("[data-delete-recently-searched-keywords]");
               if(deleteAllButton !== null){
                  deleteAllButton.addEventListener('click', function (event){
                     recentlyKeywordListElement.remove();
                     recentlyKeywordListArea.style.display = "none";
                     Method.deleteSession('recentlySearchKeywords');
                  });
               }

            }
         },
         updateRecentlyKeywordList:function ( isResetList ){

            var doResetList = false;
                doResetList = (isResetList == true) ? true : false;

            var   recentlySearchKeywords = [];
                  recentlySearchKeywords = Method.getSession("recentlySearchKeywords").split('||');
            if( recentlySearchKeywords.length == 0 ) return false;

            var   recentlyKeywordListElement =
                  ( document.querySelector("[data-recently-searched-keyword-list]") !== null ) ?
                    document.querySelector("[data-recently-searched-keyword-list]") : false;
            if( recentlyKeywordListElement == false ) return false;


            if( doResetList ) {
               while (recentlyKeywordListElement.firstChild){
                  recentlyKeywordListElement.removeChild(recentlyKeywordListElement.firstChild);
               }
            }

            for(var i = (recentlySearchKeywords.length - 1); i >= 0 ; i-- ){
                  if(recentlySearchKeywords[i] !== undefined){
                     var   listElement = document.createElement('li');
                     listElement.dataset.keywordIndex = i.toString();

                     var recentlyLink = document.createElement('a');
                     recentlyLink.href = "/kr/ko_kr/search?q=" + recentlySearchKeywords[i];
                     recentlyLink.text = recentlySearchKeywords[i];

                     recentlyLink.addEventListener('click', function (event){
                        var   data = {};
                        data.onsite_search_phrase = this.text;
                        data.onsite_search_type = "recently search";
                        data.onsite_search_result_page_type = "onsite search results";
                        data.page_event = {onsite_search : true};

                        sessionStorage.setItem( 'adobeAnalyzerJSON', JSON.stringify(data) );
                     });

                     listElement.appendChild(recentlyLink);

                     var   deleteButton = document.createElement('button');
                     deleteButton.classList.add('delete-keyword');
                     deleteButton.dataset.deleteRecentlyKeywordIndex = i.toString();
                     deleteButton.innerHTML = '<span class="ns-x-small"></span>';

                     deleteButton.addEventListener('click', function (event){
                        var _self = this;
                        var index = _self.dataset.deleteRecentlyKeywordIndex;

                        var keyword = "";
                        if( document.querySelector('[data-keyword-index="' + index + '"] a').text !== null )
                           keyword = document.querySelector('[data-keyword-index="' + index + '"] a').text;

                        document.querySelector('[data-keyword-index="' + index + '"]').remove();
                        Method.deleteRecentlyKeyword(index);

                        endPoint.call('deleteRecentlySearchKeyword', keyword);
                     });

                     listElement.appendChild(deleteButton);

                     recentlyKeywordListElement.appendChild(listElement);
                  }
               }

         },
         deleteRecentlyKeyword:function (index){
            if(index == null || index == "") return false;

            var recentlyKeywordListArea =
                ( document.querySelector("[data-recently-searched-keyword-area]") !== null ) ?
                    document.querySelector("[data-recently-searched-keyword-area]") : false;
            if(recentlyKeywordListArea == false) return false;

            var recentlySearchKeywords = [];
                recentlySearchKeywords = Method.getSession("recentlySearchKeywords").split('||');

            if(recentlySearchKeywords.length > 0){
               recentlySearchKeywords.splice(index, 1);

               if(recentlySearchKeywords.length == 0){
                  Method.deleteSession('recentlySearchKeywords');
                  recentlyKeywordListArea.style.display = "none";
               }else{
                  Method.setSession('recentlySearchKeywords', recentlySearchKeywords.join('||'));

                  //update list
                  Method.updateRecentlyKeywordList(true);
               }
            }else{
               Method.deleteSession('recentlySearchKeywords');
               recentlyKeywordListArea.style.display = "none";
            }
         },
         mobileMemberMenuOn:function(e){
            e.preventDefault();
            $(this).closest('[data-mobilemenuinner-area]').addClass('rotate');
            var targetMenu = $('[data-container-mobile-menu]').find('>li').length - 1;
            $('[data-container-mobile-menu]').find('>li').removeClass('on').eq(targetMenu).addClass('on');
         },
         customServiceActiveValidate:function(){
        	 if($('#custom-modal').hasClass('uk-open')){
                 $('[data-header-area]').hide();  //Custom Service 모달창이 활성되 되어있으면 GNB메뉴 숨김
              }else
            	 $('[data-header-area]').show();  //Custom Service가 모달창이 비활성화 되어있으면 GNB메뉴 보여줌
            },
         searchKeyUp:function(event){
            var currentVal = $(this).val();
            if(currentVal == ''){
               $(this).parents('[data-searchiner-area]').siblings('[data-searchlist-area]').removeClass('active');
               Method.gnbSearchInit();
               $('[data-searchclear-btn]').hide();
            } else{ 
               $(this).parents('[data-searchiner-area]').siblings('[data-searchlist-area]').addClass('active');  
               $('[data-searchclear-btn]').show();                 
            }
         },
         searchSubmit:function(event){
            //@pck 2021-04-06 execution before submit to search form
            //the Enter key event

            if(typeof event !== 'undefined' && event.type === "submit"){
               var searchText = (typeof $(this).find('[data-newsearch-input]')[0] !== 'undefined') ? $(this).find('[data-newsearch-input]')[0].value : null;
               if(searchText !== null){

                  Method.addSearchedKeyword(searchText); //최근 검색어 저장

                  var   data = {};
                        data.onsite_search_phrase = searchText;
                        data.onsite_search_type = "natural search";
                        data.onsite_search_result_page_type = "onsite search results";
                        data.page_event = {onsite_search : true};

                  sessionStorage.setItem( 'adobeAnalyzerJSON', JSON.stringify(data) );
                  endPoint.call('searchSubmitted', {searchText: searchText});
               }
            }
         },
         popularSearchClick:function(){
            //@pck 2021-04-06 execution before submit to search form
            //the popular keyword click event

            var searchText = $(this).text();
            if(searchText !== ''){

               Method.addSearchedKeyword(searchText); //최근 검색어 저장

               var   data = {};
               data.onsite_search_phrase = searchText;
               data.onsite_search_type = "recommended search";
               data.onsite_search_result_page_type = "onsite search results";
               data.page_event = {onsite_search : true};

               sessionStorage.setItem( 'adobeAnalyzerJSON', JSON.stringify(data) );
               endPoint.call('searchPopularSuggestionClick', {searchText: searchText});
            }
         },
         searchClose:function(){
            $(this).closest('[data-gnbsearchwrap-area]').removeClass('open');
            var clickEnableTimer = setTimeout(function() {
               $('[data-click-name="search icon"]').data('click-enable', true);
            }, 100);
            $('[data-preheader-area]').removeClass('hdn');
            $('.header h1').removeClass('top');
            $('[data-dimd-area]').removeClass('on');
            $('html, body').removeClass('not_soroll');
            $('[data-searchlist-area]').removeClass('active');
            $("[data-newsearch-input]").val('');
            $('.pre_clear_btn').css('display', 'none');
            Method.gnbSearchInit();
         },
         getParameterByName:function(name){
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
             var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                     results = regex.exec(location.search);
             return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
         },
         PopularSearchSave:function(){
            var isPopularSearch = $('[data-search-area]').find('li').is('[data-popular-search]');
            if(isPopularSearch){
               sessionStorage.setItem('PopularSearch', $('[data-popular-search]').parents().html());
            }            
         },
         getPopularSearch:function(){
            return sessionStorage.getItem("PopularSearch");
         },
         gnbSearchInit:function(){
            var gnbBaseHtml = "";
            $('[data-searchlist-area]').find('p').css("display","block");
            $('[data-search-area]').empty();
            $('#ui-id-1').empty()
            $('[data-search-area]').html(Method.getPopularSearch());
            //추천 검색어 click event
            $('[data-popular-search]').on('click', Method.popularSearchClick);
         },
         gnbIsSearchList:function(){
            var result = true;
            var gnbSearchResult = $('#ui-id-1').html();
            if($.trim($('#ui-id-1').html())==''){
               result = false;   
            }
            return result;
         },
         initAutoCompleteKeyword:function(){
            if($("#search") == null) return false;

            // sessionStorage 데이터 사용
            var autoSearchKeywordList = JSON.parse(sessionStorage.getItem('autoSearchKeyword'));
            if( autoSearchKeywordList == null) return false;

            $("#search").autocomplete({
               source: function (request, response) {
                  var re = $.ui.autocomplete.escapeRegex(request.term);
                  var matcher = new RegExp(re + "/*", "i");
                  var a = $.grep(autoSearchKeywordList, function (item, index) {
                     return matcher.test(item);
                  });
                  a = a.splice(0, 10);
                  response(a);
               },
               open: function( event, ui ) {
                  $(".search_list").removeClass("active");
                  $("[data-searchlist-area]").hide();

                  $("[data-autocomplete-keywords]").show();
                  $("[data-autocomplete-keywords]").addClass("active"); //자동완성 검색어 활성화
               },
               close: function( event, ui ) {
                  $(".search_list").removeClass("active");
                  $("[data-autocomplete-keywords]").hide();

                  $("[data-searchlist-area]").show();
                  $("[data-searchlist-area]").addClass("active"); //추천 검색어 활성화
               },
               select: function( event, ui ) {

                  Method.addSearchedKeyword(ui.item.label);

                  if(ui.item.label !== ''){
                     var	data = {};
                     data.onsite_search_phrase = ui.item.label;
                     data.onsite_search_type = "autocomplete search";
                     data.onsite_search_result_page_type = "onsite search results";
                     data.page_event = {onsite_search : true};

                     sessionStorage.setItem( 'adobeAnalyzerJSON', JSON.stringify(data) );
                     endPoint.call('searchTypeaheadClick', {searchText : ui.item.label});
                  }

               },
               classes : {
                  'ui-autocomplete' : 'hide'
               },
               appendTo: "[data-autocomplete-keywords]",
               delay : 100,
               minLength:2
            })
                .autocomplete("instance")
                ._renderItem = function( ul, item ) {
                     var className = "ui-menu-item";
                     //PC 6개 노출, MO 10개 노출
                     if( $(ul).find("li").length > 5 ){
                        className = "ui-menu-item mobile-only"
                     }
                     return $( '<li class="'+ className +'"></li>' ).data("target", item.label)
                         .append( '<a href="/kr/ko_kr/search?q=' + item.value + '&pl=t" tabindex="-1">' + item.label + '</a>')
                         .appendTo( ul );
            };
         },
         hasScrolled:function(){
            var scroll = $(window).scrollTop(); 						//스크롤 최상단
            var pre_height =  $('[data-preheader-area]').outerHeight(); //고객센터 위치
            var all_h = $('.kr_nike').outerHeight(); 					// GNB 전체영역
            var sec_top = $('.section-header').length ? $('.section-header').offset().top : 0;
                 	    
            if(scroll >= sec_top){
               $('.section-header').addClass('fixed');
            }

            if (scroll > scrollTemp && scroll > 36 ) {
               $('.header').removeClass('reset');
               $('.header').addClass('nav_up');
               $('.header').removeClass('fixed');
               $('.section-header').removeClass('sticky');
            } else {
               if (scroll <= pre_height) {
                  $('.header').removeClass('fixed');
                  $('.header').removeClass('nav_up');
                  $('.section-header').removeClass('fixed');
                  $('.section-header').removeClass('sticky');
                  
               } else  if(scroll < scrollTemp ){
            	  $('.header').removeClass('reset');
            	  $('.header').removeClass('nav_up');
                  $('.header').addClass('fixed');
                  $('.section-header').addClass('sticky'); 
               }
            } 
            if(scroll <= 0){
    			$('.header').addClass('reset');
    			$('.header').removeClass('nav_up');
    			$('.header').removeClass('fixed');
    		}
            scrollTemp = scroll 
         },
         prepareSearch:function(){
            var gnbSeatText = $("[data-newsearch-input]").val();
            var searchCheck = false;
            var searchValidate = false
            var searchClickTarget = false
            var searchName = Method.getParameterByName('q');
            var searchNameValidate = false
            
            //검색어 Validate
            if (((typeof gnbSeatText != "undefined") && (typeof gnbSeatText.valueOf() == "string")) && (gnbSeatText.length > 0) && gnbSeatText.trim() != '') {
               searchValidate = true;
            }
            
            //검색 클릭에 대한 target을 확인하여 svg,button의 target 대상만 submit 가능 
            if(event.target.nodeName == 'SPAN' || event.target.nodeName == 'BUTTON'){
               searchClickTarget = true;
            }
            
            //입력된 검색어에 대한 PLP화면에서 다시 검색창을 띄울때 검색되지 않게 설정 
            if( searchValidate && $('[data-gnbsearchwrap-area]').hasClass('open') == false && searchClickTarget){
               searchCheck = true;
            }
            
            /**
             * 검색창 닫기
             * 1. 검색어 입력창에 검색어 없는 경우
             * 2. 이미 검색한 결과가 존재하는 경우 
             * @returns
             */
            if((searchName == gnbSeatText) && searchName.trim() != '' && gnbSeatText.trim() != ''){
               searchNameValidate = true;
            }
            if(((!searchValidate && $('[data-gnbsearchwrap-area]').hasClass('open') == true) ||
                  ( searchValidate && $('[data-gnbsearchwrap-area]').hasClass('open') == true) && searchNameValidate) && searchClickTarget){
               // 입력된 검색어에 대한 PLP화면에서 검색창 화면에 동일한 검색어가 존재하는 경우 검색 클릭 시 닫기
               // 단,지웠다가 동일한 검색어를 다시 입력한 경우 검색이 가능하다.
               if(!Method.gnbIsSearchList()){
                  $(this).closest('[data-gnbsearchwrap-area]').removeClass('open');
                  var clickEnableTimer = setTimeout(function() {
                     $('[data-click-name="search icon"]').data('click-enable', true);
                  }, 100);
                  $('[data-preheader-area]').removeClass('hdn');
                  $('.header h1').removeClass('top');
                  $('[data-dimd-area]').removeClass('on');
                  $('html, body').removeClass('not_soroll');
                  $('[data-searchlist-area]').removeClass('active');
                  return;
               }                  
            }
            
            $('[data-gnbsearchwrap-area]').addClass('open');
            var clickEnableTimer = setTimeout(function() {
               $('[data-click-name="search icon"]').data('click-enable', false);
            }, 100);
            $('[data-preheader-area]').addClass('hdn');
            $('.header h1').addClass('top');
            $('html, body').addClass('not_soroll');
            $("[data-newsearch-input]").focus();   
            if($(window).width() > 960){
            	$('[data-dimd-area]').addClass('on');	
            } 

            $('[data-dimd-area]').click(function(){
               $('[data-gnbsearchwrap-area]').removeClass('open');
               $('[data-dimd-area]').removeClass('on');
               $('[data-preheader-area]').removeClass('hdn');
               $("[data-newsearch-input]").val('');
               $('.pre_clear_btn').css('display', 'none');
               Method.gnbSearchInit();
            });
            
            //검색창이 활성화 된 상태에서만 검색 가능
            if(searchValidate && $('[data-gnbsearchwrap-area]').hasClass('open') == true && !searchCheck && searchClickTarget){
               
               $('#search-form').submit();
            }
            
            //Default 검색어 저장            	
           	Method.PopularSearchSave();

            
         },
         mobileMenuActive:function(){
            $('[data-mobilemenu-area]').addClass('open');
            $('[data-dimd-area]').addClass('on');
            $('.header').addClass('hdn');
            $('html, body').addClass('not_soroll');
            $('html, body').css('overflow-y', 'hidden');
            $('[data-header-area]').removeClass('nav_up');

            $('[data-dimd-area]').click(function(){
               $('[data-mobilemenu-area]').removeClass('open');
               $('.mobile_menu_panel').removeClass('view');
               $('[data-dimd-area]').removeClass('on');
               $('.header').removeClass('hdn');
               $('html, body').removeClass('not_soroll');
               $('html, body').removeAttr('style');

               // @pck 2021-09-13 Mobile GNB Journey change - keep gnb depth level
               //$('[data-mobilemenuinner-area]').removeClass('rotate');
               //$('[data-mobilemenuinner-area]').find('.mobile_menu_panel').removeClass('rotate');
            });          
            $('.mobile_menu').animate({ scrollTop : '0' }, 0);
         },
         mobileMenuFS:function(){
            $(this).closest('[data-mobilemenuinner-area]').addClass('rotate');  
            $(this).parent('li').addClass('on').siblings().removeClass('on');
         },
         mobileMenuST:function(){
            $(this).parent().parent().parent().parent('.depth2').addClass('rotate');
         },
         mobileMenuFirstBack:function(){
            $(this).closest('li').removeClass('open');      
            $(this).parent().parent().parent().parent().parent().removeClass('rotate');
         },
         mobileMenuSecondBack:function(){
            $(this).closest('li').removeClass('open');                  
            $(this).parents('.depth2').removeClass('rotate');
         },
         signOnUserMenuOn:function(){
            $(this).addClass('open');
         },
         signOnUserMenuOff:function(){
            $(this).removeClass('open');
         },
         mobileEffectOff:function(){
            if($(window).width() > 960) {
            // $('[data-dimd-area]').removeClass('on');
               $('[data-mobilemenu-area]').removeClass('open'); 
            // $('html, body').removeClass('not_soroll'); 
               $('.header').removeClass('hdn');
            }                     
            
         },
         gnbMenuOn:function(){
            $(this).parent().addClass('active').siblings().removeClass('active');
            $('[data-dimd-area]').addClass('on');
         },
         gnbMenuOff:function(){
            $(this).find('ul li').removeClass('active');
            $('[data-dimd-area]').removeClass('on');           
         },
         searchClear:function(){
        	 $("[data-newsearch-input]").val('').focus();
        	 $("[data-searchclear-btn]").hide();
        	 Method.gnbSearchInit();
         },
         dimClose:function(){
        	 $('[data-dimd-area]').removeClass('on');  
         },
         addSearchedKeyword:function( keyword ){
            var recentlySearchKeywords = [];

            if( Method.getSession('recentlySearchKeywords') !== false ) {
               recentlySearchKeywords = Method.getSession('recentlySearchKeywords').split('||');
            }

            recentlySearchKeywords.push(keyword);
            //최근 3개까지 저장
            if( recentlySearchKeywords.length > 3 ){
               recentlySearchKeywords.splice(0, 1);
            }
            Method.setSession('recentlySearchKeywords', recentlySearchKeywords.join('||'));
         },
         setSession:function(name, value) {
            sessionStorage.setItem(name, value);
         },
         getSession:function(name) {
            if( sessionStorage.getItem(name) !== null ){
               return sessionStorage.getItem(name);
            }else{
               return false;
            }
         },
         deleteSession:function (name) {
            sessionStorage.removeItem(name);
         }
      }

      return {
         init: function () {
            sandbox.uiInit({
               selector: '[data-module-new-gnb]',
               attrName: 'data-module-new-gnb',
               moduleName: 'module_new_gnb',
               handler: { context: this, method: Method.moduleInit }
            });
         }
      }
   });
})(Core);