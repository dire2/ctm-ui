(function(Core){
	Core.register('module_pagination', function(sandbox){
		var $this, args, currentPage, totalPageNum, totalPageCount, lineSize, pageSize, isHistoryBack = false, endPoint, scrollController;

		/*
			data-module-pagination="{
				type:scroll,
				totalCount:896,
				currentPage:1,
				pageSize:40,
				target:.item-list-wrap,
				api:/kr/ko_kr/w/men/fw,
				scrollWrapper:window,
				scrollContainer:document,
				lineSize:4}"
		*/
		var setSessionPaging = function(){
			sessionStorage.setItem('categoryPagingType', args.type);
			sessionStorage.setItem('categoryCurrentPage', currentPage + 1);
		}

		// 상품리스트 최소 높이 지정
		var setContentsHeight = function(){
			var $contentsBody = $('.contents-body');
			var filterHeight = $('.contents-side').height();
			$contentsBody.css('min-height', filterHeight);
		}

		// 사이드메뉴 sticky (pc일 경우만)
		$(window).on('resize', function() {
			var wH = $(window).width();
			if (wH > 1023) {
				setContentsHeight();
			}
		});

		var Method = {
			moduleInit:function(){
				var sessionCurrentPage = sessionStorage.getItem('categoryCurrentPage');
				var sessionLineSize = sessionStorage.getItem('categoryLineSize');
				endPoint = Core.getComponents('component_endpoint');

				$this = $(this);
				args = arguments[0];
				currentPage = (sessionCurrentPage) ? sessionCurrentPage : args.currentPage;
				pageSize = Number(args.pageSize);
				totalPageNum = Math.ceil(args.totalCount / pageSize);
				lineSize = (sessionLineSize !== null) ? sessionLineSize : args.lineSize;

				switch(args.type){
					case 'more' :
						Method.typeMore();
						break;
					case 'scroll' :
						Method.typeScroll();
						break;
				}
			},
			getPaging:function(){
				return (args.totalCount > pageSize * currentPage && totalPageNum > currentPage) ? currentPage++ : null;
			},
			typeMore:function(){
				if(currentPage >= totalPageNum){
					// $this.find('button, a').remove();
					$this.find('button, a').hide();
					return;
				}

				var isCategory = document.querySelector("[data-module-category]");
				if(isCategory !== null){

					$this.find('#load-more').on('click', function(e) {
						e.preventDefault();

						if(Method.getPaging()){
							sandbox.utils.ajax(args.api, 'GET', {'page':currentPage, 'pageSize':pageSize, 'lineSize':lineSize}, function(data){
								$(args.target).append($(data.responseText).find(args.target)[0].innerHTML);
								sandbox.moduleEventInjection($(data.responseText).find(args.target)[0].innerHTML);
								Method.updateCategoryMarketingScript(data);
								
								// 상품 어펜드 후 필터 스티키 다시 계산
								//20190820일 스니커즈 스크롤 페이징시 2페이 이후 오류 발생이 되어 실행 안되게 수정.
								if(Core.getModule('module_category')){
									sandbox.getModule('module_category').newHeaderSticky();
								}
								Method.setEndPoint( data );
								if(currentPage >= totalPageNum){
									$this.find('button, a').hide();
								}

								setSessionPaging();
							});
						}
					});

				}else{
					$this.find('button, a').off('click').on('click', function(e){
						e.preventDefault();

						var _self = this;
						if(Method.getPaging()){
							var sort = ($('a#review-sort-tab.review-filter.active').text() === '도움순' ? 'helpfulCount' :'auditable.dateCreated');
							var obj = {
								'mode': args.mode,
								'templatePath':args.templatePath,
								'resultVar': args.resultVar,
								'productId': args.productId,
								'pageSize':pageSize,
								'page':currentPage,
								'lineSize':lineSize,
								'_sort':sort,
								'_type_sort':'desc',
								'cache':new Date().getTime()
							}

							//add case for check photo only filter checked
							var reviewFilterPhotoOnly = document.querySelector('[data-checkbox-review-filter-photo-only]');
							if(reviewFilterPhotoOnly !== null) {
								if(reviewFilterPhotoOnly.checked == 'undefined') return false;

								// true면 포토만 보기 활성화
								if(reviewFilterPhotoOnly.checked){
									obj.imageFilter = 'true';
								}
							}

							sandbox.utils.ajax(args.api, 'GET', obj, function(data){
								if (args.api == Core.Utils.contextPath + '/processor/execute/review'){
									$(data.responseText).find('li').each(function(index){
										$('ul#review-list').append(this);
										sandbox.moduleEventInjection(this.innerHTML);
									});
								} else {
									sandbox.moduleEventInjection($(data.responseText).find(args.target)[0].innerHTML);
								}

								Method.setEndPoint( data );
								if(currentPage >= totalPageNum){
									$(_self).off('click');
									$(_self).hide();
									// $(_self).remove();
								}
								setSessionPaging();
							});

							// sandbox.utils.ajax(args.api, 'GET', {'page':currentPage, 'pageSize':pageSize, 'lineSize':lineSize}, function(data){
							// 	$(args.target).append($(data.responseText).find(args.target)[0].innerHTML);
							// 	sandbox.moduleEventInjection($(data.responseText).find(args.target)[0].innerHTML);
							// 	Method.setEndPoint( data );
							// 	if(currentPage >= totalPageNum){
							// 		$(_self).off('click');
							// 		$(_self).remove();
							// 	}
							// 	setSessionPaging();
							// });
						}
					});

				}
			},
			typeScroll:function(){
				if(currentPage >= totalPageNum) return;

				var _self = this;
				var isFirst = true;
				var isLoaded = true;
				var prevScrollTop = 0;
				var contentsHeightPer = 0;

				scrollController = sandbox.scrollController(window, document, function(percent){
					contentsHeightPer = this.getScrollTop($(args.target).offset().top + $(args.target).height());

					if(percent > contentsHeightPer && isLoaded && !isHistoryBack && this.getScrollPer() < percent && !isFirst){
						isLoaded = false;
						if(Method.getPaging()){
							sandbox.utils.ajax(args.api, 'GET', {'page':currentPage, 'pageSize':pageSize, 'lineSize':lineSize}, function(data){
								$(args.target).append($(data.responseText).find(args.target)[0].innerHTML);
								sandbox.moduleEventInjection($(data.responseText).find(args.target)[0].innerHTML);
								Method.updateCategoryMarketingScript(data);

								//@pck 2020-10-26 SNKRS GRID WALL - 스크롤 페이징 후 레이지 콘트롤 미호출로 재 호출
								if(data.responseText.indexOf('launch-category') !== -1){
									$('.launch-category .img-component').Lazy({
										visibleOnly: true,
										scrollDirection: 'vertical',
										afterLoad: function() {
											$('.launch-category .launch-list-item').addClass('complete');
										},
									});
								}

								// 상품 어펜드 후 필터 스티키 다시 계산
								//20190820일 스니커즈 스크롤 페이징시 2페이 이후 오류 발생이 되어 실행 안되게 수정.
								if(Core.getModule('module_category')){
									sandbox.getModule('module_category').newHeaderSticky();
								}
								Method.setEndPoint( data );
								if(currentPage >= totalPageNum){
									scrollController.destroy();
								}else{
									isLoaded = true;
									setSessionPaging();
								}
							});
						}
					}

					//새로고침, 히스토리백을 했을경우 돔오브젝트가 생성되지 못한 상황에서 스크롤의 위치가 최 하단으로 이동 하기 때문에
					//처음 로드 시점에서는 무조건 scroll 이벤트를 막는다.
					isFirst = false;
					setContentsHeight();

				}, 'pagination');
			},
			updateCategoryMarketingScript:function(data){
				// 로드된 상품 정보 마케팅 데이터에 추가 
				if( $(data.responseText).find('#categoryMarketingScript').length > 0 ){
					var marketingText = $(data.responseText).find('#categoryMarketingScript')[0].outerHTML;
					$('#categoryMarketingScript').find('#products').append($(marketingText).find('#products div'));	
				}

				if(Core.utils.is.isFunction(window.categoryMarketingDataInit)){
					categoryMarketingDataInit();
				}

			},
			setEndPoint:function( data ){
				var $products = $(data.responseText).find('.categoryMarketingScript #products div');
				var itemList = [];
				$products.each(function(index, data){
					itemList.push({
						id : $(data).data("id")
					});
				})
				// 로드한 정보를 기존 정보에 추가해야 할지? 우선은 이벤트쪽으로만 정보 전달
				endPoint.call('loadMoreProducts', {page : currentPage, pageSize: pageSize, lineSize: lineSize, itemList: itemList  })
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-pagination]',
					attrName:'data-module-pagination',
					moduleName:'module_pagination',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			setLineSize:function(size){
				lineSize = size;
				sessionStorage.setItem('categoryLineSize', lineSize);
			},
			getPagingType:function(){
				return args.type;
			},
			getTotalCount:function () {
				return args.totalCount; // BK
			},
			destroy:function(){
				if(args.type === 'scroll' && scrollController) scrollController.destroy();
				sessionStorage.setItem('categoryCurrentPage', 1);
			}
		}
	});
})(Core);
