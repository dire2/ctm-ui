(function(Core){
	var allSkuDataItem;
	Core.register('module_launchcategory', function(sandbox){
		var $that, category;
		// var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];


		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $cate = $(".launch-category");
				var $item = $(".launch-list-item.upcomingItem");
				var $cate_header = $(".launch-lnb");

				//@pck 2020-10-23 Lazy 개선
				document.addEventListener('readystatechange', function(event){
					if(document.readyState == 'complete'){
						$('.launch-category .img-component').Lazy({
							visibleOnly: true,
							scrollDirection: 'vertical',
							afterLoad: function() {
								$('.launch-category .launch-list-item').addClass('complete');
							},
						});
					}
				});

        		if(arguments[0] && undefined != arguments[0].category){
					category = arguments[0].category;
				}

				//upcoming일때는 헤더 우측 뷰 변경 아이콘 숨기기
				if(category === 'upcoming'){
					$this.find('.toggle-box').hide();
					// TheDraw일때 Adobe 태깅 추가
					$cate.find('[data-thedraw]').parents('[data-component-launchitem]>a').attr('data-click-area', 'snkrs').attr('data-click-name', 'upcoming: apply draw');
				}
				// console.log('category:', category);

				// TheDraw일때 Adobe 태깅 추가
				if(category === 'feed'){
					$cate.find('[data-thedraw]').parents('[data-component-launchitem]>a').attr('data-click-area', 'snkrs').attr('data-click-name', 'feed: apply draw');
				}


				var Listform = {
				    grid : function(setCookie){
								$(".item-list-wrap", $cate).removeClass("gallery").addClass("grid");
				        //$(".launch-list-item", $cate).removeClass("gallery").addClass("grid");
						$(".toggle-box span", $cate_header).removeClass("ns-grid").addClass("ns-feed");
						if(setCookie){$.cookie("launch_view_mode", "grid" , {path : "/"});}

				    },
				    gallery : function(setCookie){
								$(".item-list-wrap", $cate).removeClass("grid").addClass("gallery");
				        //$(".launch-list-item", $cate).removeClass("grid").addClass("gallery");
				        $(".toggle-box span", $cate_header).removeClass("ns-feed").addClass("ns-grid");
				        if(setCookie){$.cookie("launch_view_mode", "gallery" , {path : "/"});}
					}
				};

				//날짜 순으로 상품 정렬
				var productListOrderedByDate = $cate.find('.launch-list-item').slice().sort(function(el1, el2){
					var date1 = Method.DateParse($(el1).data('active-date'));
					var date2 = Method.DateParse($(el2).data('active-date'));
					return +(date1 > date2) || -(date1 < date2) || (isNaN(date1)) - (isNaN(date2));
				});

                //up-coming의 경우 날짜 순으로 정렬필요
				if(category ==="upcoming"){
					$cate.find('.uk-grid').empty();

                    // 노출할 상품이 하나도 없다면 상품없음 텍스트 노출
					if( productListOrderedByDate.length == 0 ){
                        $cate.find('.empty').removeClass('uk-hidden');
                        return;
                    }
                    $cate.find('.uk-grid').append(productListOrderedByDate);

					//@pck 2020-11-24
					// 날짜 순 정렬 시 component_categoryitem 인스턴스들이 살아있는 상태라 정상적인 moduleInjection 호출이 불가
					// 올바른 방법은 아니지만 임시로 강제 reinit
					Core.getComponents("component_categoryitem", {context:$cate}, function(){
						this.init();
					});

					// Launch 리스트 NOTIFY ME 버튼 노출
					$item.find('.item-notify-me').on('click', function (e) {
					// $('.uk-grid').on('click', '.item-notify-me', function (e) {
						var url = $(this).attr('url');

						Core.Utils.ajax(url, 'GET', {}, function (data) {
							$("#restock-notification").remove();

							var notifyPop = $(data.responseText).find('#restock-notification');
							$('body').append(notifyPop)

							Core.moduleEventInjection(data.responseText);

							/*
							var obj = {
								'productId': $item.find('[name="productId"]').val()
							}

							Core.Utils.ajax(Core.Utils.contextPath + '/productSkuInventory', 'GET', obj, function(data){
								var responseData = data.responseText;
								allSkuDataItem = Core.Utils.strToJson(responseData).skuPricing;
								// console.log(allSkuDataItem)
								// console.log(Method)
								// _self.fireEvent('skuLoadComplete', _self, [allSkuData, 'COMINGSOON']);
							}, false, true);
							*/

							var modal = UIkit.modal("#restock-notification");
							if (modal.isActive()) {
								modal.hide();
							} else {
								modal.show();
							}
						});

					});

					if (!Core.Utils.mobileChk) {
						$item.find('.btn-box-notify')
							.mouseenter(function() {
								$item.find('.info-sect').addClass('opacity');
							})
							.mouseleave(function() {
								$item.find('.info-sect').removeClass('opacity');
							});
					}


					(function(list){
						 var xYear = "", xMonth = "", xDay = "";
						 list.each(function(){
							 var $this = $(this),
								 date = $this.attr("data-active-date").split(" ")[0],
								 arrDate = date.split("-"),
								 year = arrDate[0],
								 month = arrDate[1],
								 day = arrDate[2];

								 //월이나 일이 바뀌면
								 if ( xMonth !== month || xDay !== day ){
									$this.before("<em class='upcoming-tit-date'>" + month + "월 " + day + "일</em>")
								 }

								 //update state
								 xYear = year;
								 xMonth = month;
								 xDay = day;

						});

					})($cate.find('.launch-list-item'));
				}

				if( $.cookie("launch_view_mode") && category !== "upcoming" ){
					( $.cookie("launch_view_mode") === "gallery" ) ? Listform.gallery(true) : Listform.grid(true);
				}
				//쿠키가 없는 경우(우측 뷰 변경 아이콘을 누르지 않은 경우)
				//feed는 기본이 gallery, in-stock는 기본이 grid, updoming은 gallery만 있음.
				else if(!$.cookie("launch_view_mode")){
                    if(category ==="in-stock"){
						Listform.grid(false);
					} else {
						Listform.gallery(false)
					}
				}
				else {
					( category === "in-stock" ) && Listform.grid(true);
				}

				$cate.css("opacity", "1");

                //마우스 클릭시 탭 하단 선택 표시
				$('.launch-menu').on('click', 'li', function(){
					$('.launch-menu li.on').removeClass('on');
					$(this).addClass('on');
				});
                //우측 뷰 변경 아이콘
				$this.find('.toggle-box a').on('click', function(e){

				    if( category === "feed" || category === "in-stock" ){
				        if( $(".item-list-wrap", $cate).eq(0).hasClass("gallery") ){
				            Listform.grid(true);
				        }
				        else {
				            Listform.gallery(true);
				        }
				        // @pck 2020-11-05 토글 시 레이지 호출 부 추가
						$('.launch-category .img-component').Lazy({
							visibleOnly: true,
							scrollDirection: 'vertical',
						});
				    }

				    //@pck 2020-11-05 ADOBE TAG - 썸네일 버튼 토글 상태 수집
					if(document.querySelector('[data-tag-pw-list]') !== null){
						var gridListEl = document.querySelector('[data-tag-pw-list]');
						var param = {};
						param.click_name = gridListEl.classList.contains("gallery") ? "fullview" : "thumbnail" ;

						switch (category) {
							case "feed" :
								param.click_area = "snkrs feed";
								break;
							case "in-stock" :
								param.click_area = "snkrs instock";
								break;
						}

						endPoint.call('snkrsThumbnailToggleClick', param);
					}

					e.preventDefault();

				});
				//카테고리에서 상픔 클릭시 상단 탭 선택표시 off 및 우측 아이콘 숨김
				$('.launch-category .uk-grid div a').on('click', function(){
					$('.launch-menu li.on').removeClass('on');
				});
			},
			DateParse:function(dateStr){
				var a=dateStr.split(" ");
				var d=a[0].split("-");
				var t=a[1].split(":");
				return new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-launchcategory]',
					attrName:'data-module-launchcategory',
					moduleName:'module_launchcategory',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			getTotalSkuData:function () {
				return allSkuDataItem; // BK
			},
			getTotalSkuNotify:function () {
				return 'COMINGSOON'; // BK
			}
		}
	});
})(Core);
