(function(Core){
	'use strict';
	Core.register('module_review_write', function(sandbox){
		var $deferred = null;
		var imgCount = 0;
		var removeId = null;
		var maxCount = 6;
		var currentStarCount = 0;
		var starCountIS = false;
		var fileLoad = null;
		var arrDescription = ['별로에요.', '그저 그래요.', '나쁘지 않아요.', '마음에 들어요.', '좋아요! 추천합니다.'];
		var imgTemplate = '<span class="preview-up-img"><a href="javascript:;" class="file-remove_btn"></a><img src="/kr/ko_kr/{{imgUrl}}?thumbnail" alt="{{imgAlt}}" /></span>';
		var inputHiddenTemplate = '<input type="hidden" name="fileList[{{id}}].fullUrl" class="file-{{id}}" value={{fullUrl}} /><input type="hidden" name="fileList[{{id}}].fileName" class="file-{{id}}" value={{fileName}} />';
		var $this, endPoint, name, model,page_type, $submitBtn, textAreaComponent, $thumbNailWrap, optionComponent, optionCount=0, checkedOptionCount=0;

		var Method = {
			moduleInit:function(){
				endPoint = Core.getComponents('component_endpoint');
				$this = $(this);
				$thumbNailWrap = $this.find('.thumbnail-wrap');
				name 		= $($this).find('input[name="name"]').val();
				model 		= $($this).find('input[name="model"]').val();
				page_type	= $($this).find('input[name="reviewId"]').val();   //수정인지(reviewId 있으면), 신규작성인지
				$submitBtn  = $($this).find('[data-btn-review-write-submit]');
				
				optionComponent = sandbox.getComponents('component_radio', {context:$this}, function(i){
					var _self = this;
					this.addEvent('change', function(target, val){
						Method.updateOptionState(_self);
						Method.updateSubmitBtnState();
					})
				});

				if( optionComponent != null ){
					if(!Core.Utils.is.isArray(optionComponent)){
						optionComponent = [optionComponent];
					}
					optionCount = optionComponent.length;
				}

				textAreaComponent = sandbox.getComponents('component_textarea', {context:$this}, function(i){
					this.addEvent('keyup', function(val){
						Method.updateSubmitBtnState();
					});
				});

				// fileLoad = sandbox.getComponents('component_file', {context:$this}, {setting:maxLength=6}, function(){
				fileLoad = sandbox.getComponents('component_file', {context:$this, maxLength:maxCount}, function(){
					var _self = this;
					this.addEvent('error', function(msg){
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
					});

					this.addEvent('upload', function(fileName, fileUrl){
						//console.log(fileName, fileUrl);
						$thumbNailWrap.append(sandbox.utils.replaceTemplate(imgTemplate, function(pattern){
							switch(pattern){
								case 'imgUrl' :
									return fileUrl;
									break;
								case 'imgAlt' :
									return fileName;
									break;
							}
						}));

						imgCount++;
						_self.setCurrentIndex(imgCount);
					});
				});

				imgCount = $thumbNailWrap.children().size();
				fileLoad.setCurrentIndex(imgCount);

				currentStarCount = $this.find('.rating-star a').filter('.active').length - 1;

				$this.find('.rating-star a').click(function(e) {
					e.preventDefault();
					var index = $(this).index() + 1;
					$(this).parent().children('a').removeClass('active');
					$(this).addClass('active').prevAll('a').addClass('active');

					$this.find('input[name=rating]').val(index*20);
					$this.find('input[name=starCount]').val(index);
					$this.find('.rating-description').text(arrDescription[index-1]);

					starCountIS = true;
					Method.updateSubmitBtnState();
				});

				if(currentStarCount >= 0){
					$this.find('.rating-star a').eq(currentStarCount).trigger('click');
				}

				$this.find('.uplode-preview-list').on('click', '.file-remove_btn', function(e){
					e.preventDefault();
					var index = $(this).attr('href');
					$(this).parent().remove();

					imgCount--;
					fileLoad.setCurrentIndex(imgCount);
				});

				$submitBtn.on('click', Method.submitWrite);

				// @pck 2021-06-15
				// 리뷰 작성자 개인정보(키,몸무게) 입력 부 추가
				Method.reviewWriterInfoController();
			},
			updateOptionState: function(optionTarget){
				var target = optionTarget.getThis();
				var $titleElement = $(target).closest('[data-review-option-wrap]').find('[data-review-option-title]');
				if( optionTarget.getValidateChk()){
					$titleElement.removeClass('error');
				}else{
					$titleElement.addClass('error');
				}
			},
			getAllCheckOption: function(){
				var isAllCheck = true;

				if( optionCount != 0 ){
					checkedOptionCount = 0;

					$.each( optionComponent, function(){
						if( this.getValidateChk()){
							checkedOptionCount++;
						}
					})
					if( optionCount != checkedOptionCount ){
						isAllCheck = false;
					}
				}
				return isAllCheck;
			},
			updateSubmitBtnState: function(){
				// 삭제 하지는 않았지만 사용하지 않음
				// 실시간으로 체크 하는 로직에서 작성 버튼 클릭시 처리 하는 형태로 변경
				return false;
				
				var isEnable = true;
				var reviewTitleText = _.trim($("input#title").val());

				if(!starCountIS){
					isEnable = false;
				}

				if(!starCountIS){
					isEnable = false;
				}

				if (reviewTitleText == ''){
					isEnable = false;
				};

				if( !Method.getAllCheckOption()){
					isEnable = false;
				}

				if(!textAreaComponent.getValidateChk()){
					isEnable = false;
				}

				if(isEnable){
					$submitBtn.removeClass('disabled');
				}else{
					$submitBtn.addClass('disabled');
				}
			},
			submitWrite: function(e){
				e.preventDefault();
				var $form = $this.find('#review-write-form');
				var reviewContentText = _.trim($("#comment").val());
				var reviewTitleText = _.trim($("input#title").val());
				var reviewstarCount = $("input[name=starCount]").val();

				var reviewEventCustomer = null;
					reviewEventCustomer = $("input[name=reviewEventCustomer]").val();
				
				if (reviewstarCount == '0'){
					Core.ui.modal.alert('별점을 선택해 주세요.');
					return;
				};
				
				if( !Method.getAllCheckOption()){
					var isReviewEventCustomerOptionValid = false;

					$.each( optionComponent, function(){
						if( this.getThis()[0].querySelector('input[name=reviewEventCustomer]') !== null ){
							isReviewEventCustomerOptionValid = this.getValidateChk();
						}
					});
					if(
						reviewEventCustomer !== null &&
						isReviewEventCustomerOptionValid == false &&
						optionCount - 1 == checkedOptionCount
					){
						Core.ui.modal.alert('개인정보 수집 및 이용 동의여부를 선택해 주세요.');
					}else{
						Core.ui.modal.alert('평가 항목을 모두 선택해 주세요.');
						$.each( optionComponent, function() {
							Method.updateOptionState(this);
						});
					}
					return;
				}

				if (reviewTitleText == ''){
					Core.ui.modal.alert('제목을 입력해 주세요.');
					return;
				};
				
				if (!textAreaComponent.getValidateChk()){
					Core.ui.modal.alert('상품리뷰를 5자 이상 작성해주세요.');
					return;
				};
				
				if (Core.utils.has.hasEmojis(reviewTitleText) || Core.utils.has.hasEmojis(reviewContentText)) {
					Core.ui.modal.alert('상품리뷰에 이모지를 사용할 수 없습니다.');
					return;
				};

				if(!starCountIS || !textAreaComponent.getValidateChk()){
					return;
				};

				$thumbNailWrap.children().each(function(i){
					var $this = $(this);
					$form.append(sandbox.utils.replaceTemplate(inputHiddenTemplate, function(pattern){
						switch(pattern){
							case 'id' :
								return i;
								break;
							case 'fileName' :
								return $this.find('img').attr('alt');
								break;
							case 'fullUrl' :

								return $this.find('img').attr('src');
								break;

								//수정시 마다 앞에 풀url( https://stg-cf-nike.brzc.kr) 이 붙는 현상으로. 쌈네일 이미지 깨지는 현상 발생
								//임시방편으로 수정
								// var r_fullUrl = $this.find('img').attr('src');
								// var st1 = 'https://static-breeze.nike.co.kr';
								// var st2 = 'https://stg-cf-nike.brzc.kr';
								// var s_fullUrl  = r_fullUrl.replace(st1,');
								// 	 s_fullUrl  = s_fullUrl.replace(st2,');
								//	 s_fullUrl  = s_fullUrl.replace('/kr/ko_kr',');
								//	 s_fullUrl  = s_fullUrl.replace('//','/');

								// return s_fullUrl

						}
					}));
				});

				var itemRequest = BLC.serializeObject($form);
				sandbox.utils.ajax($form.attr('action'), $form.attr('method'), itemRequest, function(res){
					var data = sandbox.rtnJson(res.responseText);

					if(data.hasOwnProperty('errorMessage') || !data){
						if($deferred) $deferred.reject(data.errorMessage);
						else UIkit.notify(data.errorMessage, {timeout:3000,pos:'top-center',status:'danger'});
					}else{
						/*리뷰 작성 시 상품 화면으로 되돌아 가면서 reflesh 되지 않아 우선 막음*/
						/*if($deferred) $deferred.resolve(data); else */
						var review_msg = '';
						endPoint.call("writeReview",{ name : name, model : model, starCount: itemRequest.starCount });
						Core.ga.action('review','write');
						if(page_type == ""){  //  위에 reviewId  수정, 신규 구분값
							// 기대평은 다른 메시지를 띄우지 않는다.
							if(itemRequest.reviewType=='PRODUCT'){
								review_msg ="리뷰가 작성되었습니다.";
							}else{
								review_msg ="50마일 지급이 완료되었습니다.";
							}
						}else{
							review_msg ="수정되었습니다.";
						}

						// 리뷰 작성 직 후 리뷰리스트 상에는 최근 작성글을 바로 가져올 수 없음
						UIkit.modal.alert(review_msg).on('hide.uk.modal', function() {
							//PDP 외 리뷰 작성 완료 시 redirectUrl로 refresh처리
							location.href = data.redirectUrl;
						});

					}
				}, true);
			},
			reviewWriterInfoController:function(){
				var reviewWriterInfoValue = document.querySelector('#reviewWriterInfo'),
					reviewWriterInfoValueObject = {},
					selectReviewWriterInfo = null;

				if(reviewWriterInfoValue == null) {
					return false;
				}

				function setValues(stringObject){
					if (stringObject === null || typeof(stringObject) == 'undefined'){
						return false;
					} else {
						reviewWriterInfoValue.value = '';
						var inputObject = JSON.parse(stringObject);

						for (var key in inputObject) {
							if (inputObject.hasOwnProperty(key)) {
								reviewWriterInfoValueObject[key] = inputObject[key];
							}
						}

						var result = JSON.stringify(reviewWriterInfoValueObject);
						result = result.substr(2, result.length - 3); //중괄호, 쌍따옴표 제거
						result = result.replace(/","/g, '"||"');
						result = result.replace(/"/g, ''); //쌍따옴표 제거

						// result ex) reviewWriterHeight:120~129||reviewWriterWeight:130~139
						reviewWriterInfoValue.value = result;
					}

					return true;
				}

				if(reviewWriterInfoValue !== null){

					var selectReviewWriterInfo = Core.getComponents('component_select');

					if( selectReviewWriterInfo.length > 0){

						var i = 0;

						while ( i < selectReviewWriterInfo.length ) {

							selectReviewWriterInfo[i].addEvent('change', function(event){
								if(this.value !== null){
									var stringObject = this.value.replace(/'/g, '"');
									setValues('{' + stringObject + '}'); //ui-kit selectbox 렌더 중 json data는 오류나는 현상이 발생함
								}
							});

							i++;
						}
					}
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-review-write]',
					attrName:'data-module-review-write',
					moduleName:'module_review_write',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$deferred = null;
				//console.log('destroy review-write module');
			},
			setDeferred:function(defer){
				$deferred = defer;
			},
			moduleConnect:function(){
				fileLoad.setToappUploadImage({
					fileName:arguments[0],
					fullUrl:arguments[1],
					result:(arguments[2] === '1') ? true : false
				});
			}
		}
	});
})(Core);