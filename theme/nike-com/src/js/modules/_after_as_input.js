(function (Core) {
	Core.register('module_repairable_result', function (sandbox) {
		var Method = {

			$newAddress: null,
			isNewAddress: false,
			isSelectAddress1: false,
			isSelectAddress2: false,

			moduleInit: function () {
				var $this = $(this);
				var $form = $this.find('#sfrm');
				sandbox.validation.init($form);

				// 배송지 정보 submit
				$this.find('[data-repairRequest-chk-btn]').on('click', function (e) {

					e.preventDefault();
					sandbox.validation.validate($form);

					if (sandbox.validation.isValid($form)) {

						// as유형 글자수
						// if($this.find("#repairReason").val().length < 10){
						//	 UIkit.modal.alert("고객님의 상품 AS 를 접수하기 위해 최소 10글자 이상 입력해주세요.");
						//	 return;
						// }

						if (sandbox.utils.has.hasEmojis($this.find('#repairReason').val())) {
							sandbox.ui.modal.alert('AS 접수 작성시 이모지를 사용 할 수 없습니다.');
							return false;
						}

						//유의사항 show....
						$this.find('#jq_tab').eq(0).removeClass('uk-hidden');

						//수거지 새로 입력일 경우
						if ($this.find("[aria-expanded]").eq(1).attr('aria-expanded') == 'true') {
							$("#s_receiveAddressPostalCode").val($("span[data-postalcode]").eq(0).text());
							$("#s_receiveAddressLine1").val($("#s_addr1").val());
							$("#s_receiveAddressLine2").val($("#s_addr2").val());
							$("#s_receiveAddressFullName").val($("#s_name").val());
							$("#s_receiveAddressPhone").val($("#s_phone").val());
						};

						//받으시는 분 새로 입력일 경우
						if ($this.find("[aria-expanded]").eq(3).attr('aria-expanded') == 'true') {
							$("#r_deliveryAddressPostalCode").val($("span[data-postalcode]").eq(1).text());
							$("#r_deliveryAddressLine1").val($("#r_addr1").val());
							$("#r_deliveryAddressLine2").val($("#r_addr2").val());
							$("#r_deliveryAddressFullName").val($("#r_name").val());
							$("#r_deliveryAddressPhone").val($("#r_phone").val());
						};

						//수선 수리범위 배열처리.
						var seq = $('input#st_repairReasonAttr:checked').map(function () {
							return this.value;
						}).get().join(',');

						$this.find('#repairReasonAttr').val(seq);  //수선 범위...

						//첨부 이미지 배열
						var tempArray = [];
						//		$this.find('[data-component-file]').find('.preview-up-img').find('img').each(function(){
						//		   tempArray.push($(this).attr("src"));
						//		});
						//    $this.find('#r_personalMessage').val(tempArray.join( '|' ));

						var ii = 1;
						$this.find('[data-component-file]').find('.as-lode-img').find('img').each(function () {
							//tempArray.push($(this).attr("src"));
							$this.find('#imageFullUrl' + ii).val($(this).attr("src"));
							ii = ii + 1;
						});

						//수거지 배송 메모저장..
						if ($this.find('#selectPersonalMessage option:selected').eq(0).text() == '직접입력') {
							$this.find('#r_personalMessage').val($this.find('input#personalMessageText').eq(0).val());
						} else if ($this.find('#selectPersonalMessage option:selected').eq(0).val() != '') {
							var r_msg = $this.find('#selectPersonalMessage option:selected').eq(0).text();
							$this.find('#r_personalMessage').val(r_msg);
						}

						//받는분 배송 메모저장..
						if ($this.find('#selectPersonalMessage option:selected').eq(1).text() == '직접입력') {
							$this.find('#d_personalMessage').val($this.find('input#personalMessageText').eq(1).val());
						} else if ($this.find('#selectPersonalMessage option:selected').eq(1).val() != '') {
							var d_msg = $this.find('#selectPersonalMessage option:selected').eq(1).text();
							$this.find('#d_personalMessage').val(d_msg);
						}

						// if($("span[data-postalcode]").eq(0).text()==""){
						//	   UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
						//	 	 return;
						// };

						//수거지 주소
						sandbox.validation.validate($form);
						if (sandbox.validation.isValid($form)) {
							if ($this.find("[aria-expanded]").eq(1).attr('aria-expanded') == 'true') {
								if (!Method.isSelectAddress1) {
									UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
									return;
								}
							}
						}
						//받는분 주소 체크
						if (sandbox.validation.isValid($form)) {
							if ($this.find("[aria-expanded]").eq(3).attr('aria-expanded') == 'true') {
								if (!Method.isSelectAddress2) {
									UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
									return;
								}
							}
						}


						//작성완료시 모달
						if ($(this).attr('data-repairRequest-chk-btn') == 'step2') {

							//수거지 정보 validation
							if ($this.find("[aria-expanded]").eq(1).attr('aria-expanded') == 'false') {
								if ($("#s_receiveAddressPostalCode").val() == null || $("#s_receiveAddressPostalCode").val() == '') {
									UIkit.modal.alert('수거지 우편 번호가 누락 되었습니다.주소를 새로 입력해 주세요.');
									return;
								}

								if ($("#s_receiveAddressLine1").val() == null || $("#s_receiveAddressLine1").val() == '') {
									UIkit.modal.alert('수거지 주소가 누락 되었습니다. 주소를 새로 입력해 주세요.');
									return;
								}

								if ($("#s_receiveAddressFullName").val() == null || $("#s_receiveAddressFullName").val() == '') {
									UIkit.modal.alert('수거지 고객명이 누락 되었습니다. 고객명을 새로 입력해 주세요.');
									return;
								}

								if ($("#s_receiveAddressPhone").val() == null || $("#s_receiveAddressPhone").val() == '') {
									UIkit.modal.alert('수거지 전화번호가 누락 되었습니다.전화번호를 새로 입력해 주세요.');
									return;
								}

								var checkRegular = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})/;
								if (!checkRegular.test($("#s_receiveAddressPhone").val())) {
									UIkit.modal.alert('올바른 수거지 휴대폰 번호를 새로 입력해주세요.');
									return;
								};
							};


							//배송지 정보 validation
							if ($this.find("[aria-expanded]").eq(3).attr('aria-expanded') == 'false') {
								if ($("#r_deliveryAddressPostalCode").val() == null || $("#r_deliveryAddressPostalCode").val() == '') {
									UIkit.modal.alert('배송지 우편 번호가 누락 되었습니다.주소를 새로 입력해 주세요.');
									return;
								}

								if ($("#r_deliveryAddressLine1").val() == null || $("#r_deliveryAddressLine1").val() == '') {
									UIkit.modal.alert('배송지 주소가 누락 되었습니다. 주소를 새로 입력해 주세요.');
									return;
								}

								if ($("#r_deliveryAddressFullName").val() == null || $("#r_deliveryAddressFullName").val() == '') {
									UIkit.modal.alert('배송지 고객명이 누락 되었습니다. 고객명을 새로 입력해 주세요.');
									return;
								}

								if ($("#r_deliveryAddressPhone").val() == null || $("#r_deliveryAddressPhone").val() == '') {
									UIkit.modal.alert('배송지 전화번호가 누락 되었습니다.전화번호를 새로 입력해 주세요.');
									return;
								}

								var checkRegular = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})/;
								if (!checkRegular.test($("#r_deliveryAddressPhone").val())) {
									UIkit.modal.alert('올바른 배송지 휴대폰 번호를 새로 입력해주세요.');
									return;
								};
							};
							
							UIkit.modal('#popup-as-application').show();
						}
					};
				});


				var deliverySearch = sandbox.getComponents('component_searchfield', { context: $this, selector: '.search-field', resultTemplate: '#address-find-list' }, function () {
					// 검색된 내용 선택시 zipcode 처리
					this.addEvent('resultSelect', function (data) {
						if ($("[aria-expanded]").eq(1).attr('aria-expanded') == 'true') {
							Method.isSelectAddress1 = true;
						}
						if ($("[aria-expanded]").eq(3).attr('aria-expanded') == 'true') {
							Method.isSelectAddress2 = true;
						}
					});
				});


				//배송지 목록 선택
				$this.find('[data-customer-address-select-btn]').on('click', function (e) {

					var i = $("button[data-customer-address-select-btn]").index(this);     //배송지 목록
					var t_x = $("#addt_tab_index").val();    //배송지 목록  선택한 index

					var city = $("input[name='city']").eq(i).val();
					var fullName = $("input[name='fullName']").eq(i).val();
					var postalCode = $("input[name='postalCode']").eq(i).val();
					var addressLine1 = $("input[name='addressLine1']").eq(i).val();
					var addressLine2 = $("input[name='addressLine2']").eq(i).val();
					var phoneNumber = $("input[name='phoneNumber']").eq(i).val();

					//tetx 변경
					$(".txt-name").eq(t_x).text(fullName);
					$("span#txt-zip").eq(t_x).text(postalCode);
					$(".text-box").eq(t_x).text(addressLine1 + '  ' + addressLine2);
					$("span#txt-phone").eq(t_x).text(phoneNumber);

					var addr_model_pop = UIkit.modal('#popup-customer-address', { modal: false });
					addr_model_pop.hide();

					//배송지 목록, 선택시 hidden insert..
					if (t_x == 0) {    //수거지....
						$("input#s_receiveAddressCity").val(city);
						$("input#s_receiveAddressFullName").val(fullName);
						$("input#s_receiveAddressPostalCode").val(postalCode);
						$("input#s_receiveAddressLine1").val(addressLine1);
						$("input#s_receiveAddressLine2").val(addressLine2);
						$("input#s_receiveAddressPhone").val(phoneNumber);
					} else {
						$("input#r_deliveryAddressCity").val(city);
						$("input#r_deliveryAddressFullName").val(fullName);
						$("input#r_deliveryAddressPostalCode").val(postalCode);
						$("input#r_deliveryAddressLine1").val(addressLine1);
						$("input#r_deliveryAddressLine2").val(addressLine2);
						$("input#r_deliveryAddressPhone").val(phoneNumber);
					}
					return false;
				});

				inAjaxing = false;  //중복신청 방지...
				//   $("#sfrm").submit();
				$('[data-repairRequest-sendit-btn]').on('click', function (e) {
					var per_url = "repairRequest";
					var obj = $("#sfrm").serialize();
					var addr_model_pop = UIkit.modal('#popup-as-application', { modal: false });

					if (inAjaxing) {  //중복 신청 방지...
						UIkit.modal.alert('처리중 입니다.').on('hide.uk.modal', function () {
						});
						return;
					}

					inAjaxing = true;

					// obj = {'orderId' : 1818};
					Core.Utils.ajax(per_url, 'POST', obj, function (data) {
						var jsonData = Core.Utils.strToJson(data.responseText, true) || {};
						if (jsonData.result == true) {
							UIkit.modal.alert('신청되었습니다.').on('hide.uk.modal', function () {
								sandbox.setLoadingBarState(true);
								location.href = 'repaired';
							});

							//UIkit.notify("입고 알림 신청이 삭제 되었습니다." , {timeout:3000,pos:'top-center',status:'success'});
						} else {
							//	UIkit.notify(args.removeMsg, {timeout:3000,pos:'top-center',status:'warning'});
							addr_model_pop.hide();
							UIkit.modal.alert(jsonData.errorMsg).on('hide.uk.modal', function () {
								sandbox.setLoadingBarState(true);
								location.href = 'repairable?dateType=1';
							});
						}
					});
				});

				//이미지 첨부S  -------------------

				$this.find('[data-upload-btn]').on('click', function (e) {

					if ($('[data-component-file] .as-lode-img').length == 6) {
						UIkit.modal.alert('파일은 6개 까지만 가능 합니다.');
						return false;
					};

					$this.find('#input-file').trigger('click');
					return false;
				});


				$this.find('#input-file').change(function (e) {
					setImgPreview.call(this);
				});


				var setImgPreview = function (target) {

					var errorMsg = "이미지 전송을 실패하였습니다.";

					if ($(this).val() === '') return false;
					$this.find("#fileupload-form").ajaxSubmit({
						success: function (data) {
							if (data.result == false) {  //실패시....
								UIkit.notify(errorMsg, { timeout: 3000, pos: 'top-center', status: 'danger' });
								return false;
							};

							var imgTemplate = '<span class="as-lode-img"><a href="javascript:;" class="file-remove_btn"></a><img src="/kr/ko_kr{{imgUrl}}" alt="{{imgAlt}}" style="width:56px;height:56px;"/></span>';
							var fileUrl = data.fullUrl;
							var fileName = data.fileName;

							$(".thumbnail-wrap").append(sandbox.utils.replaceTemplate(imgTemplate, function (pattern) {
								switch (pattern) {
									case 'imgUrl':
										return fileUrl;
										break;
									case 'imgAlt':
										return fileName;
										break;
								}
							}));
							//$(".thumbnail-wrap").append('<img src="/kr/ko_kr'+ data.fullUrl +'">');
						},
						error: function (data) {
							try{
								errorMsg = data.responseJSON.errorMessage;
							}catch(e){ console.log(e);}
							
							UIkit.notify(errorMsg, { timeout: 3000, pos: 'top-center', status: 'danger' });
							return false;
						}
					});
				};
				//이미지 첨부 E ---------------------------

				//첨부 이미지 삭제...
				$this.find('.thumbnail-wrap').on('click', '.file-remove_btn', function (e) {
					e.preventDefault();
					$(this).parent().remove();
				});

				//배송지 목록 클릭한 팝업 index
				$this.find('[data-customer-address-btn]').on('click', function (e) {
					$("#addt_tab_index").val($("[data-customer-address-btn]").index(this));
				});

				//배송지 목록 클릭한 팝업 index
				$this.find('input#isCheckoutAgree').on('change', function (e) {
					if ($this.find('input#isCheckoutAgree:checked').length == 3) {
						$this.find('div#jq_tab').eq(1).removeClass('uk-hidden');
						$this.find('div#jq_tab').eq(2).removeClass('uk-hidden');
					}
				});
			}
		}

		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-repairable-result]',
					attrName: 'data-module-repairable-result',
					moduleName: 'module_repairable_result',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);


//  AS 신청서 상세 조회   (/account/repaired)
(function (Core) {
	Core.register('module_repaired_list', function (sandbox) {

		var Method = {

			moduleInit: function () {
				var $this = $(this);
				var $_this = $("#popup-as-detail");

				//판정 결과버튼 없을시, 라인 삭제..
				$this.find('.order-list').each(function (i) {
					if ($(this).find("[data-click-area]").length == 0) {
						$(this).find(".item-btn").remove();
					}
				});

				//상세 조회
				$this.find("[data-repaired-number]").on('click', function (e) {

					UIkit.modal('#popup-as-detail').show();

					//var ix             = $("a[data-repaired-number]").index(this);
					var per_url = "repaired/detail";
					var obj = "repairNumber=" + $(this).attr('data-repaired-number');

					//모달창 초기화....
					$("a[aria-expanded]").eq(0).trigger('click');
					$("a[aria-expanded]").eq(2).trigger('click');
					$("li#jq_h_tab").addClass('uk-hidden');
					$("[data-repairaddr-chk-btn]").text('주소변경');

					Core.Utils.ajax(per_url, 'get', obj, function (data) {
						var jsonData = Core.Utils.strToJson(data.responseText, true) || {};

						if (jsonData.result == true) {

							str_price = String(jsonData.ro.repairOrderItem.price.amount);
							str_price = str_price.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

							// item_option =  $this.find("span[data-item-option]").eq(ix).attr('data-item-option');  //상품옵션
							//	console.log(item_option);

							//카테고리
							var categoryUrl = jsonData.ro.repairOrderItem.productUrl;

							if ((categoryUrl.indexOf("/men/fw") != -1) || (categoryUrl.indexOf("/women/fw") != -1)) {
								categoryUrl = "신발";
								$_this.find("div#div_repairReasonAttr").eq(0).show();
								$_this.find("div#div_repairReasonAttr").eq(1).hide();

							} else if ((categoryUrl.indexOf("/men/ap") != -1) || (categoryUrl.indexOf("/women/ap") != -1)) {
								categoryUrl = "의류";
								$_this.find("div#div_repairReasonAttr").eq(0).hide();
								$_this.find("div#div_repairReasonAttr").eq(1).show();
							} else {
								categoryUrl = "기타";
								$("div#div_repairReasonAttr").hide();
							};

							//유형
							var repairReasonType = jsonData.ro.repairReasonType;
							//console.log(repairReasonType);

							if (repairReasonType == "A") {
								repairReasonType = "유무상 수선 의뢰";
							} else {
								repairReasonType = "봉제/원단/부자재/사이즈 불량";
							};

							//as수선범위 선택.. 체크박스..
							$("span[data-component-checkbox]").removeClass('checked');  //체크초기화
							var str_repairReasonAttr = jsonData.ro.repairOrderItem.repairReasonAttr;

							$_this.find("span[data-component-checkbox]").each(function () {
								var vl = $(this).attr('data-component-checkbox');
								if (str_repairReasonAttr.indexOf(vl) != -1) {
									$(this).addClass('checked');
								}
							});

							//첨부이미지...
							$_this.find("div.thumbnail-wrap").html('');

							for (var i = 1; i < 7; i++) {

								if (eval('jsonData.ro.repairOrderItem.repairImg' + i) != null) {
									$_this.find("div.thumbnail-wrap").append("<span class='preview-up-img'><img src='" + eval('jsonData.ro.repairOrderItem.repairImg' + i) + "' style='width:56px;height:56px;'>&nbsp; </span> ");
								};
							};

							//동의
							for (var i = 1; i < 4; i++) {
								if (eval('jsonData.ro.repairOrderItem.repairAgree' + i)) {
									$_this.find("span#repairAgree").eq(i - 1).addClass('check');
								};
							};


							var resulr_status = jsonData.ro.status.type;  //진행사항...


							//주소변경 버튼.....
							if (resulr_status != "REQUEST") {
								$("a#btn_addr_send").hide();
							} else {
								$("a#btn_addr_send").show();
							}

							$_this.find("#categoryUrl").text(categoryUrl);   //카테고리
							$_this.find("#repairReasonType").text(repairReasonType);   //as유형
							//$_this.find("#jq_repairReason").text(result_repairReason(jsonData.ro.repairReason));  //설명
							$_this.find("#jq_repairReason").text(result_repairReason(jsonData.ro.repairReason));

							$_this.find("#jq_receiveAddressFullName").text(jsonData.ro.receiveAddress.fullName);   //우편번호
							$_this.find(".jq_receiveAddressPostalCode").text(jsonData.ro.receiveAddress.postalCode);   //주소1
							$_this.find("#jq_receiveAddr").text(jsonData.ro.receiveAddress.addressLine1 + '  ' + jsonData.ro.receiveAddress.addressLine2);   //주소2
							$_this.find("#jq_receiveAddressPhone").text(jsonData.ro.receiveAddress.phonePrimary.phoneNumber);   //핸드폰
							$_this.find("input#repairNumber").eq(0).val(jsonData.ro.repairNumber);   //repairNumber

							//	 if(jsonData.ro.r_personalMessage==null || jsonData.ro.r_personalMessage==''){  //수거메모
							//		 $("#jq_recevie").hide();
							//	} else{
							//
							//	}
							$_this.find("#jq_recevie_memotext").text(result_repairReason(jsonData.ro.r_personalMessage));   //수거메모
							$_this.find("#jq_deliveryAddressFullName").text(jsonData.ro.deliveryAddress.fullName);   //우편번호
							$_this.find(".jq_deliveryAddressPostalCode").text(jsonData.ro.deliveryAddress.postalCode);   //주소1
							$_this.find("#jq_deliveryAddr").text(jsonData.ro.deliveryAddress.addressLine1 + '  ' + jsonData.ro.deliveryAddress.addressLine2);   //주소2
							$_this.find("#jq_deliveryAddressPhone").text(jsonData.ro.deliveryAddress.phonePrimary.phoneNumber);   //핸드폰
							$_this.find("input#repairNumber").eq(1).val(jsonData.ro.repairNumber);   //repairNumber
							$_this.find("#jq_delivery_memo").text(result_repairReason(jsonData.ro.d_personalMessage));  //받는 메모

							//	 if(jsonData.ro.d_personalMessage==null || jsonData.ro.d_personalMessage==''){   //배송메모
							//		 $("#jq_delivery").hide();
							//	} else{
							//		 $_this.find("#jq_delivery_memo").text(jsonData.ro.d_personalMessage);
							//	}
						} else {
							UIkit.modal.alert(jsonData.result);
						}
					});
				});


				//취소..data-cancel-btn
				$this.find("[data-cancel-btn]").on('click', function (e) {

					var per_url = $(this).attr('data-cancel-btn');

					UIkit.modal.confirm('A/S 신청을 취소하시겠습니까? 취소하게 되면 등록내용이 삭제됩니다', function () {
						window.document.location.href = per_url;
						return;
					});
				});



				// 글자수 마킹...
				var result_text = function (ss, str) {

					if (str != null && str != '') {
						len = str.length;

						str1 = str.substring(0, ss);
						str2 = "";
						for (var i = ss; i < len; i++) {
							str2 = str2 + '*';
						};
						return str1 + str2;
					};
				};

				//상세설명   .repairReason.replace(/&#40;/gi,'(').replace(/&#41;/gi,')')
				var result_repairReason = function (html) {
					if (html != null) {
						var txt = document.createElement('textarea');
						txt.innerHTML = html.replace(/&#40;/gi, '(').replace(/&#41;/gi, ')').replace(/&#39;/gi, "''");
						return txt.value;
					}
				};


				// 금액 콤마...
				var result_price = function (str) {
					if (str != null) {
						return (str != '0') ? str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : str;
					}
				};


				//판정결과 pop...
				$this.find("[data-repaired-opinion]").on('click', function (e) {

					//api 개발로 인해, 특정시간 alert 안내창 노출
					var sdate = new Date("2019/06/19 02:00:00");
					var edate = new Date("2019/06/19 03:00:00");

					str_msg = "서비스 이용에 불편을 드려 죄송합니다.</br>안정화된 서비스를 제공하기 위해 시스템 점검 중입니다.</br>" +
						"점검 시간 동안 일시적으로 A/S 판정 결과 확인은 불가하며,</br>6월 19일 03시 이후부터 정상적으로 서비스 이용하실 수 있습니다."
					//	"※점검 일시 : 2019.05.20 22:00 ~ 2019.05.21 10:00 (12시간)"

					if (Date.now() >= sdate && Date.now() <= edate) {
						UIkit.modal.alert(str_msg).on('hide.uk.modal', function () {
						});
						return;
					}

					UIkit.modal('#repaired-opinion_detail').show();  //판정결과 모달 open...

					var $this = $("#repaired-opinion_detail");
					var per_url = "repaired/confirm";
					var obj = "repairNumber=" + $(this).attr('data-repaired-opinion');

					sandbox.validation.init($this);
					var member_data = sandbox.getModule('module_header').getCustomerInfo();
					//-----------------------------
					// var status          = $(this).attr('data-repaired-status');  //진행사항....
					//-----------------------------

					Core.Utils.ajax(per_url, 'get', obj, function (data) {
						var jsonData = Core.Utils.strToJson(data.responseText, true) || {};

						if (jsonData.result == true) {

							var pp = jsonData.paymentInfoGroups.length;  //결제 방법cnt..
							var payment_list = "";
							var payment_lists = "";
							var sactive = " active";

							for (var ii = 0; ii < pp; ii++) {

								str0 = jsonData.paymentInfoGroups[ii];
								str1 = jsonData.paymentInfoGroups[ii].additionalInfo;

								payment_list = "<div class='payment-method-item " + sactive + "' data-m-redirect-url='" + str1.m_repair_redirect_url + "' data-paymentType='" + str0.paymentType + "' data-method='" + str1.pay_method + "' data-identity-code='" + str1.imp_identity_code + "' data-provider='" + str0.paymentProvider + "' data-notice-url='" + str1.repair_notice_url + "' data-type='" + str0.paymentType + "' data-pg='" + str1.pg + "'>" +
									"<h6 class='payment-method-item-title'>" + str0.displayText + "</h6>" +
									"</div>";
								payment_lists = payment_lists + '' + payment_list;
								sactive = "";
							};

							$this.find("[data-payment-method]").html(payment_lists);

							//환불입력창...str_refundAmount

							$this.find("#result_repairNumber").text(jsonData.dto.repairNumber);
							$("input#str_repairNumber").val(jsonData.dto.repairNumber);              //환불 pop...hidden..

							$this.find("#result_submitDate").text(jsonData.dto.submitDate.substring(0, 10));   //신청일
							$this.find("#result_itemName").text(jsonData.dto.itemName);  //상퓸명
							$this.find("#result_p_model").text(jsonData.dto.model);   //모델명

							if (jsonData.dto.asFlag == "0") {
								$this.find("#result_asPriceFlag").text("수선");
							} else if (jsonData.dto.asFlag == "1") {
								$this.find("#result_asPriceFlag").text("회송");
							} else if (jsonData.dto.asFlag == "2") {
								$this.find("#result_asPriceFlag").text("반품");
							} else if (jsonData.dto.asFlag == "3") {
								$this.find("#result_asPriceFlag").text("보류");
							}

							$this.find("#result_reDecisionDate").text(jsonData.dto.reDecisionDate.substring(0, 10));  //판정날자  result_repairAgree1
							$this.find("td#result_confirmResult").html(result_repairReason(jsonData.dto.confirmResult)); //판정내용
							$this.find("td#result_confirmKind").html(result_repairReason(jsonData.dto.confirmKind)); //판정결과  ,

							//환불입력창...
							$("#refund-step1").find("[text_refundAmount]").text(result_price(jsonData.dto.refundAmount) + '원');
							$("#refund-step1").find("#str_refundAmount").val(jsonData.dto.refundAmount);  //hidden

							var repaired_status = jsonData.dto.status;     //as 신행 현황....

							//회송일 경우에만 판정내용, 결과 보여준다.  if(jsonData.dto.asFlag == "0"){
							if (jsonData.dto.asFlag == "1") {
								$this.find("#process_confirmResult").show();
								$this.find("#process_confirmKind").show();
							} else {
								$this.find("#process_confirmResult").hide();
								$this.find("#process_confirmKind").hide();
							}

							//결제 문구  , 버튼.....
							if (repaired_status == "PENDING") {
								$this.find("#btn_submit").hide();    //확인 Btn
								$this.find("#btn_cancel").show();    //취소 btn

							} else if (repaired_status == "PAYMENT_PROCESS") {

							} else {
								str_price_process = "";
								$this.find("#btn_submit").show();
								$this.find("#btn_cancel").hide();
							};

							//수선비..
							if (jsonData.dto.asAdjPrice != null && jsonData.dto.asAdjPrice > 0) {
								str_price_process = " (결제대기)";
							} else {
								str_price_process = "";
							}

							//배송비..
							if (jsonData.dto.deliveryAdjFee != null && jsonData.dto.deliveryAdjFee > 0) {
								str_price_process1 = " (결제대기)";
							} else {
								str_price_process1 = "";
							}

							// status...
							if (repaired_status == "PAYMENT_PROCESS" || repaired_status == "PAYMENT_COMPLETE" || repaired_status == "FULFILL_OUT" || repaired_status == "DELIVERED" || repaired_status == "COMPLETE") {
								if (jsonData.dto.asAdjPrice != null && jsonData.dto.asAdjPrice > 0) {
									str_price_process = " (결제완료)";
								} else {
									str_price_process = "";
								}

								//배송비..
								if (jsonData.dto.deliveryAdjFee != null && jsonData.dto.deliveryAdjFee > 0) {
									str_price_process1 = " (결제완료)";
								} else {
									str_price_process1 = "";
								}
							}

							//수선,배송비 status 출력...
							if (str_price_process != '') {
								$this.find("span#str_price_process").eq(0).text(str_price_process);
							} else {
								$this.find("span#str_price_process").eq(0).text('');
							}

							if (str_price_process1 != '') {
								$this.find("span#str_price_process").eq(1).text(str_price_process1);
							} else {
								$this.find("span#str_price_process").eq(1).text('');
							}

							$this.find("#result_asAdjPrice").text(result_price(jsonData.dto.asAdjPrice) + '원'); //수선비
							//$this.find("#result_repaired_status").text(str_repaired_status);  //진행사항.

							if (jsonData.dto.deliveryFeeFlag == "Y") {        //배송비
								result_deliveryFeeFlag = "고객 부담";
								deliveryAdjFee = result_price(jsonData.dto.deliveryAdjFee) + '원';
							} else {
								result_deliveryFeeFlag = "나이키 부담";
								deliveryAdjFee = result_price(jsonData.dto.deliveryAdjFee) + '원';
								//$this.find("#result_delivery_price").hide();
							};

							if (jsonData.dto.deliveryAdjFee != null && jsonData.dto.deliveryAdjFee > 0) {
								$this.find("#result_deliveryFeeFlag").text(result_deliveryFeeFlag);
							} else {
								$this.find("#result_deliveryFeeFlag").text('');
							}

							$this.find("#result_delivery_price").text(deliveryAdjFee);
							$this.find("#result_payment_btn").attr('data-repaired-number', jsonData.dto.repairNumber); //결제버튼
							$this.find("#result_payment_btn").attr('data-repairid', jsonData.dto.repairId); //repairId
							// $this.find("#result_payment_btn").attr('data-reparired-amount',jsonData.dto.asAdjPrice);   //결제금액

							//환불계좌 입력 버튼
							if ((repaired_status == "REFUND_READY") || (repaired_status == "REFUND_PROCESS" && jsonData.dto.accountNum == null)) {
								$this.find("#payment-return_btn").removeClass('uk-hidden');
								$this.find("#refund_text").show();    //환불 안내 메세지.
								$this.find("#btn_submit").hide();     //확인버튼 감추기
							} else {
								$this.find("#payment-return_btn").addClass('uk-hidden');
								$this.find("#refund_text").hide();
							};

							//결제 버튼, 결제 방법........
							if (repaired_status == "PENDING") {
								$this.find("[data-order-tab]").removeClass('uk-hidden');   //결제방법..
								$this.find("#result_payment_btn").removeClass('uk-hidden');
							} else {
								$this.find("[data-order-tab]").addClass('uk-hidden');
								$this.find("#result_payment_btn").addClass('uk-hidden');
							};

							//환불정보창  DIV_REFUND_PROCESS
							if ((repaired_status == "REFUND_PROCESS" && jsonData.dto.accountNum != null) || (repaired_status == "REFUND_COMPLETE")) {
								(jsonData.dto.accountName != null) ? $this.find("#result_accountName").text(result_repairReason(jsonData.dto.accountName)) : ''; //환불정보 ,은행
								$this.find("#result_ownerName").text(result_text(1, jsonData.dto.ownerName)); //이름
								$this.find("#result_accountNum").text(result_text(5, jsonData.dto.accountNum)); //계좌
								$this.find("#result_refundAmount").text(result_price(jsonData.dto.refundAmount) + '원'); //금액
								$this.find("#DIV_REFUND_PROCESS").show();
							} else {
								$this.find("#DIV_REFUND_PROCESS").hide();
							}

						} else {
							UIkit.modal.alert(jsonData.result);
						};
					});

				});

				//as취소
				$this.find("a[data-repaired-cancel]").on('click', function () {

					var per_url = "repaired/cancel";
					var obj = "repairNumber=" + $(this).attr('data-repaired-cancel');

					Core.Utils.ajax(per_url, 'get', obj, function (data) {
						var jsonData = Core.Utils.strToJson(data.responseText, true) || {};

						if (jsonData.result == true) {
							UIkit.modal.alert(jsonData.result);
							location.href = 'repaired';
						} else {
							UIkit.modal.alert(jsonData.result);
						}
					});
				});

			}
		}
		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-repaired-list]',
					attrName: 'data-module-repaired-list',
					moduleName: 'module_repaired_list',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);



(function (Core) {     //  신청조회 모달창   (/account/repaired_detail_pop)
	Core.register('module_repairable_pop', function (sandbox) {
		var Method = {

			moduleInit: function () {
				var $this = $(this);

				//주소변경....
				$this.find('[data-repairaddr-chk-btn]').on('click', function (e) {

					var ix = $("[data-repairaddr-chk-btn]").index(this);   //저장 버튼 index
					var $form1 = $this.find($(this).attr('data-repairaddr-chk-btn'));
					var str_text = $(this).text();
					var jq_obj = $(this).attr('jq_obj');  // 1,2

					if (str_text == "주소변경") {
						$this.find("li#jq_h_tab").eq(ix).removeClass('uk-hidden');
						$(this).text('변경확인');
					} else {

						if ($this.find("[aria-expanded]").eq(jq_obj).attr('aria-expanded') == 'true') {   //이전주소면....
							$this.find("li#jq_h_tab").eq(ix).addClass('uk-hidden');
							$(this).text('주소변경');

						} else {  // 새로입력...

							sandbox.validation.init($form1);
							e.preventDefault();
							sandbox.validation.validate($form1);

							if (sandbox.validation.isValid($form1)) {

								//배송 메세지 hidden....
								if (ix == 0) {	    //수거지 배송 메모저장..
									if ($this.find('#selectPersonalMessage option:selected').eq(0).text() == '직접입력') {
										$this.find('#r_personalMessage').val($this.find('input#personalMessageText').eq(0).val());
									} else if ($this.find('#selectPersonalMessage option:selected').eq(0).val() != '') {
										var r_msg = $this.find('#selectPersonalMessage option:selected').eq(0).text();
										$this.find('#r_personalMessage').val(r_msg);
									}

									if ($this.find("#receiveAddressPostalCode").val() == "") {
										UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
										return;
									}

								} else {	//받는분 배송 메모저장..
									if ($this.find('#selectPersonalMessage option:selected').eq(1).text() == '직접입력') {
										$this.find('#d_personalMessage').val($this.find('input#personalMessageText').eq(1).val());
									} else if ($this.find('#selectPersonalMessage option:selected').eq(1).val() != '') {
										var d_msg = $this.find('#selectPersonalMessage option:selected').eq(1).text();
										$this.find('#d_personalMessage').val(d_msg);
									}

									if ($this.find("#deliveryAddressPostalCode").val() == "") {
										UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
										return;
									}
								}

								var per_url = "repaired/updateAddress";
								var obj = $form1.serialize();

								//	 console.log(obj);

								Core.Utils.ajax(per_url, 'POST', obj, function (data) {
									var jsonData = Core.Utils.strToJson(data.responseText, true) || {};

									if (jsonData.result == true) {
										UIkit.modal.alert('저장 되었습니다.').on('hide.uk.modal', function () {
											sandbox.setLoadingBarState(true);
											location.reload();
										});

										// location.href = 'repaired';
									} else {
										UIkit.modal.alert(jsonData.result);
									}
								});
							};
						};
					};
				});

				//수정
				var deliverySearch = sandbox.getComponents('component_searchfield', { context: $this, selector: '.search-field', resultTemplate: '#address-find-list' }, function () {

					this.addEvent('resultSelect', function (data) {
						if ($("[aria-expanded]").eq(1).attr('aria-expanded') == 'true') {
							var zipcode = $(data).data('zip-code5');
							$("#receiveAddressPostalCode").val(zipcode);
						}

						if ($("[aria-expanded]").eq(3).attr('aria-expanded') == 'true') {
							var zipcode = $(data).data('zip-code5');
							$("#deliveryAddressPostalCode").val(zipcode);
						}
					});
				});

			}
		}
		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-repairable-pop]',
					attrName: 'data-module-repairable-pop',
					moduleName: 'module_repairable_pop',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);



//환불입력창 click.....validation chk

(function (Core) {
	Core.register('module_refund_form', function (sandbox) {
		var Method = {

			moduleInit: function () {
				var $this = $(this);
				var $that = $("#refund-step2");   // 환불요청 확인창...
				var $form = $this.find('#refund_from');
				sandbox.validation.init($form);

				$this.find('[refund_send_btn]').on('click', function (e) {       //환불창 OPEN...

					e.preventDefault();
					sandbox.validation.validate($form);

					if (sandbox.validation.isValid($form)) {

						var ownerName = $this.find("#ownerName").val();   //예금주
						var accountName = $this.find("#accountCode option:selected").text();  //은팽명
						var accountCode = $this.find("#accountCode option:selected").val(); //은팽코드
						var accountNum = $this.find("#accountNum").val();  //계좌
						var refundAmount = $this.find("#str_refundAmount").val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");   //환불금액

						//$this.find("#str_repairNumber").val(repairNumber);
						$this.find("#str_accountName").val(accountName);
						$that.find("#txt_refundAmount").text(refundAmount + '원');
						$that.find("#txt_ownerName").text(ownerName);
						$that.find("#txt_accountName").text(accountName);
						$that.find("#txt_accountNum").text(accountNum);

						UIkit.modal('#refund-step2').show();
					};
				});

				Method.$popAddressModal = UIkit.modal("#refund-etc", { modal: false });

				$this.find('[data-modal-btn]').on('click', function (e) {       //개인정보 자세히 보기...
					e.preventDefault();
					Method.$popAddressModal.show();
				});

				//환불계좌 숫자만 입력
				$this.find('#accountNum').on('keyup', function (e) {
					$(this).val($(this).val().replace(/[^0-9]/g, ""));
				});


				//환불창 submit
				$that.find('[refund_send_btn]').on('click', function (e) {

					param = $this.find('#refund_from').serialize();
					console.log(param);

					Core.Utils.ajax('repaired/refundInfo', 'post', param, function (data) {
						var jsonData = Core.Utils.strToJson(data.responseText, true) || {};

						if (jsonData.result == true) {
							UIkit.modal.alert('환불 요청이 완료되었습니다.').on('hide.uk.modal', function () {
								endPoint.call('clickEvent', { 'area': 'mypage', 'name': 'as status: bank account for refund: confirm alert (auto)' });
								sandbox.setLoadingBarState(true);
								location.reload();
							});
						} else {
							UIkit.modal.alert(jsonData.result);
						}
					});
				});


			}
		}

		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-refund-form]',
					attrName: 'data-module-refund-form',
					moduleName: 'module_refund_form',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);


// 수선비 결제  진행....
(function (Core) {
	Core.register('module_repared_payment', function (sandbox) {
		var endPoint;
		var Method = {
			$that: null,
			$submitBtn: null,
			$usafeContent: null,
			$agreeForm: null,
			moduleInit: function () {
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				endPoint = Core.getComponents('component_endpoint');

				var $this = $(this);
				Method.$that = $this;
				Method.$submitBtn = $this.find('[data-checkout-btn]');
				Method.$submitBtn.on("click", Method.checkout);
				$this.find('[data-payment-method]').find('.payment-method-item-title').on('click', Method.changePaymentMethod);
				$this.find('[data-cod-btn]').on("click", Method.codCheckout);

				$(document).on('click', '.payment-method-item-title', Method.changePaymentMethod_1);  //payment 종류 선택.

				//	 sandbox.validation.init( $this );
				//회원정보 셋팅
				var member_data = sandbox.getModule('module_header').getCustomerInfo();
				sfirstName = member_data.firstName;
				sphoneNumber = member_data.phoneNumber;
				semailAddress = member_data.emailAddress;
			},

			checkout: function (e) {
				e.preventDefault();
				$(this).attr("enabled")
				//var isCheckoutAgree = Method.$that.find('[name="isCheckoutAgree"]').is(':checked');
				var _self = $(this);

				// 결제 방법에 따른 처리
				var $activeItem = Method.$that.find("[data-payment-method]").find(".payment-method-item.active");

				//카카오 페이 chk...
				var payMethodParam = '';

				if (Method.$that.find("[data-payment-method]").find(".payment-method-item.active").data("type") == 'KAKAO_POINT') {
					payMethodParam = '?pay_method=point';
				}
				
				// PAYCO 2019-08-12
				if (Method.$that.find("[data-payment-method]").find(".payment-method-item.active").data("type") == 'PAYCO') {
					payMethodParam = '?pay_method=payco';
				}

				// NAVER_PAY 2020-09-08
				if (Method.$that.find("[data-payment-method]").find(".payment-method-item.active").data("type") == 'NAVER_PAY') {
					payMethodParam = '?pay_method=naver_pay';
				}

				var paymentType = $activeItem.data('paymenttype');   //pay_method
				var provider = $activeItem.data('provider');   //provider
				var repair_id = Method.$that.find("#result_payment_btn").attr('data-repairid');   //repairId
				var repairNumber = Method.$that.find("#result_payment_btn").attr('data-repaired-number');   //repairNumber

				sandbox.utils.promise({
					url: sandbox.utils.contextPath + '/account/repairCheckout?repairNumber=' + repairNumber + '&repair_id=' + repair_id + '&paymentType=' + paymentType + '&provider=' + provider,
					//url:sandbox.utils.contextPath + '/checkout/request'+payMethodParam,
					type: 'GET'
				}).then(function (data) {  //....
					sandbox.setLoadingBarState(true);
					if (data.result == true) {
						var paymentType = ($activeItem.length > 0) ? $activeItem.data("type") : null;
						reparired_amount = data.totalAmount.amount;  //결제금액
						// 결제 완료 상태 일때
						if (_self.data('checkout-btn') == 'complete') {
							_self.closest('form').submit();
							return;
						}
						//iamport 모듈..
						if ($activeItem.data('provider') == 'IAMPORT') {
							Method.checkoutIamport($activeItem, data.total_amount);
						}
					} else {
						sandbox.setLoadingBarState(false);


					}
				}).fail(function (msg) {
					sandbox.setLoadingBarState(false);
					UIkit.notify(msg, { timeout: 3000, pos: 'top-center', status: 'danger' });
					//	Method.updateSubmitBtn( true );
				});
			},

			// payment 정보 선택시
			changePaymentMethod_1: function (e) {
				e.preventDefault();
				var $item = $(this).closest('.payment-method-item');
				if (!$item.hasClass('active')) {
					$item.siblings().removeClass('active');
					$item.addClass('active');
				}
			},

			checkoutIamport: function ($activeItem, totalAmount) {
				// 결제 전일때
				var $orderinfo = $("#orderinfo-review");
				var $shippingInfo = $("#shipping-review");
				var $priceInfo = $("#order-summary");

				var IMP = window.IMP;


				var paymentMethod = $activeItem.data("method"); // 결제 수단
				var mRedirectUrl = $activeItem.data("m-redirect-url"); // 모바일 url
				var noticeUrl = $activeItem.data("notice-url"); // 노티피케이션 url
				var version = $activeItem.data("version") || 'old'; // 에스크로 분기 처리를 위한 값  new or old
				var escrow = $activeItem.data("escrow"); // 에스크로 사용 여부
				var identityCode = $activeItem.data("identity-code");       //IMP-CODE
				var appScheme = $activeItem.data("app-scheme"); // 모바일 앱 스키마 정보
				var pg = $activeItem.data("pg");

				var useReplaceReturnUrl = false;
				var cUrl = Core.Utils.url.getCurrentUrl();

				// 접근한 URL이 mshop 일 때
				if (cUrl.indexOf('www.nike') > -1) {
					useReplaceReturnUrl = true;
				} else {
					// 접근한 URL이 mshop 이 아닌데 deviceOs 가 ios 일때
					if (String(Core.Utils.url.getQueryStringParams(cUrl).deviceOs).toLowerCase() == 'ios') {
						useReplaceReturnUrl = true;
					}
				}

				if (useReplaceReturnUrl) {
					if (mRedirectUrl != null) {
						mRedirectUrl = mRedirectUrl.replace('m.nike', 'www.nike');
					}
				}

				if (Core.Utils.contextPath == '/kr/launch') {
					if (mRedirectUrl != null) {
						mRedirectUrl = mRedirectUrl.replace('/kr/ko_kr', '/kr/launch');
					}
				}

				if (paymentMethod == '' || identityCode == '' || pg == '') {
					UIkit.modal.alert('결제 수단 정보로 인한 문제가 발생하였습니다.<br/>고객센터(' + _GLOBAL.SITE.TEL + ')로 문의 주시면 신속히 처리 도와드리겠습니다.');
					return;
				}

				var $orderList = $priceInfo.find('[data-order]');
				var name = "나이키";
				if ($orderList.length > 1) {
					name += ' 외 ' + ($orderList.length - 1);
				}

				var buyerName = $.trim($orderinfo.find('[data-name]').data('name')) || $shippingInfo.find('[data-name]').data('name');
				//var reparired_amount   = Method.$that.find("#result_payment_btn").attr('data-reparired-amount');  //수선비
				var repairId = Method.$that.find("#result_payment_btn").attr('data-repairid');       //repairId
				var repaired_number = Method.$that.find("#result_payment_btn").attr('data-repaired-number');   //repair-number

				IMP.init(identityCode);

				param = {
					//pay_method : _GLOBAL.PAYMENT_TYPE_BY_IAMPORT[ paymentMethod ], // 추후 provider 에 따라 변수변경 *서버에서 내려오고 있음
					pg: pg,
					pay_method: paymentMethod, // 추후 provider 에 따라 변수변경
					merchant_uid: repairId + '_' + new Date().getTime(),
					name: "A/S비용 결제",
					amount: reparired_amount,     //결제금액
					buyer_email: semailAddress,
					//buyer_name:$orderinfo.find('[data-email]').data('email'),
					buyer_name: sfirstName,
					buyer_tel: sphoneNumber,
					buyer_addr: "서울특별시",
					buyer_postcode: "03470",
					m_redirect_url: mRedirectUrl,
					app_scheme: "undefined",
					notice_url: noticeUrl,
					repairId: repairId,
					bypass: { acceptmethod: "SKIN(#111)" }
				};

				var depositPeriod = $activeItem.find('[name="depositPeriod"]').val() || 2;

				if (paymentMethod == 'vbank') {
					param.vbank_due = moment().add(depositPeriod, 'day').format('YYYYMMDD2359');
					param.custom_data = $activeItem.find('form').serialize().replace(/=/gi, ':').replace(/&/gi, '|');
				}

				if (escrow == true) {
					param.escrow = true;
				}

				IMP.request_pay(param, function (rsp) {
					//결제 완료시
					if (rsp.success) {
						var msg = '결제가 완료되었습니다.<br>';
						msg += '고유ID : ' + rsp.imp_uid + '<br>';
						msg += '상점 거래ID : ' + rsp.merchant_uid + '<br>';
						msg += '결제 금액 : ' + rsp.paid_amount + '<br>';
						msg += 'custom_data : ' + rsp.custom_data + '<br>';

						if (rsp.pay_method === 'card') {
							msg += '카드 승인번호 : ' + rsp.apply_num + '<br>';
						} else if (rsp.pay_method === 'vbank') {
							msg += '가상계좌 번호 : ' + rsp.vbank_num + '<br>';
							msg += '가상계좌 은행 : ' + rsp.vbank_name + '<br>';
							msg += '가상계좌 예금주 : ' + rsp.vbank_holder + '<br>';
							msg += '가상계좌 입금기한 : ' + rsp.vbank_date + '<br>';
						}
						//alert( msg );
						sandbox.setLoadingBarState(true);

						if (rsp.pg_provider == 'kakaopay') {
							rsp.pay_method = 'point';
						}

						_.delay(function () {
							location.href = sandbox.utils.contextPath + '/repairCheckout/iamport-checkout/repair/hosted/return?merchant_uid=' + rsp.merchant_uid + '&imp_uid=' + rsp.imp_uid + '&pay_method=' + rsp.pay_method + '&custom_data=' + rsp.custom_data;
						}, 3000);

					} else {

						//실패 메시지에 따라 그냥 넘길것인지 어떤 액션을 취할것인지 확인
						//var msg = '결제에 실패하였습니다.' + '<br>';
						//msg += '에러내용 : ' + rsp.error_msg + '<br>';
						//UIkit.modal.alert(rsp.error_msg);

						sandbox.setLoadingBarState(false);

						if (rsp.error_msg == '결제를 취소하셨습니다') {
							endPoint.call('orderCancel');
						}

						UIkit.modal.alert(rsp.error_msg).on('hide.uk.modal', function () {
							sandbox.setLoadingBarState(true);
							//		var cartId = Method.$that.find("input[name='cartId']").val();
							location.href = sandbox.utils.contextPath + '/account/repaired';
						});
						//alert( msg );
					}
				});


			}
		}

		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-repared-payment]',
					attrName: 'data-module-repared-payment',
					moduleName: 'module_repared_payment',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);





// repairable  as신청 검색 필터
(function (Core) {
	Core.register('module_date_filter1', function (sandbox) {
		var Method = {
			$that: null,
			moduleInit: function () {
				var $this = $(this);
				Method.$that = $this;
				Method.$start = Method.$that.find('#start-date');
				Method.$end = Method.$that.find('#end-date');

				$this.find('[data-date-list] a').on('click', function () {
					var value = $(this).data('date').split(',');

					if (value[0] == "M" && value[1] == "-1") {   //1개월은 바로 이동.
						window.location.href = "repairable?dateType=1";
						return;
					};

					if (value != '') {
						Method.searchSubmit(moment().add(value[1], value[0]).format('YYYYMMDD'), moment().format('YYYYMMDD'), $(this).index());
					} else {
						window.location.href = "repairable";
					}
				});

				$this.find('[data-search-btn]').on('click', function () {
					if (Method.getValidateDateInput()) {

						var today = new Date();   //오늘 날짜 가져오기
						var dd = today.getDate();
						var mm = today.getMonth() + 1; //January is 0!
						var yyyy = today.getFullYear();
						if (dd < 10) {
							dd = '0' + dd
						}
						if (mm < 10) {
							mm = '0' + mm
						}
						str_today = yyyy + '-' + mm + '-' + dd;

						var start = Method.$start.val().toString();
						var end = Method.$end.val().toString();
						var date_chk = Method.dateDiff(moment(start, 'YYYY.MM.DD').format('YYYY-MM-DD'), str_today);  //2년 체크..
						//alert( start );
						//alert( moment(start, 'YYYYMMDD') );
						//alert( moment(start, 'YYYY.MM.DD').format('YYYYMMDD'));

						if (!date_chk) {
							UIkit.modal.alert('조회기간은 최대 2년 이내로 자료가 조회 가능합니다.');
							return;
						}

						Method.searchSubmit(moment(start, 'YYYY.MM.DD').format('YYYYMMDD'), moment(end, 'YYYY.MM.DD').format('YYYYMMDD'), 'detail');

					} else {
						UIkit.modal.alert('기간을 선택해 주세요!');
					}
				});


				// 초기화
				$this.find('[data-reset-btn]').on('click', Method.reset);

				// uikit datepicker module 적용
				$this.find('input[class="date"]').each(function () {
					if (!moment($(this).val(), 'YYYY.MM.DD').isValid()) {
						$(this).val('');
					}
					if ($.trim($(this).val()) != '') {
						$(this).val(moment($(this).val(), 'YYYYMMDD').format('YYYY.MM.DD'));
					}
					var datepicker = UIkit.datepicker($(this), {
						maxDate: true,
						format: 'YYYY.MM.DD'
					});

					datepicker.on('hide.uk.datepicker', function () {
						$(this).trigger('focusout');
						Method.updateDateInput();
					})
				})
			},

			dateDiff: function (_date1, _date2) {
				var diffDate_1 = _date1 instanceof Date ? _date1 : new Date(_date1);
				var diffDate_2 = _date2 instanceof Date ? _date2 : new Date(_date2);

				diffDate_1 = new Date(diffDate_1.getFullYear(), diffDate_1.getMonth() + 1, diffDate_1.getDate());
				diffDate_2 = new Date(diffDate_2.getFullYear(), diffDate_2.getMonth() + 1, diffDate_2.getDate());

				var d = true;
				var diff = Math.abs(diffDate_2.getTime() - diffDate_1.getTime());
				diff = Math.ceil(diff / (1000 * 3600 * 24));

				if (diff > 730) {
					d = false;
				}
				return d;
			},
			// 앞보다 뒤쪽 날짜가 더 뒤면 두값을 서로 변경
			updateDateInput: function () {
				var start = String(Method.$start.val());
				var end = String(Method.$end.val());

				if ($.trim(start) == '' || $.trim(end) == '') {
					return;
				}

				// 같다면
				//var isSame = moment(Method.$start.val()).isSame(Method.$end.val());
				// 작다면
				//var isBefore = moment(Method.$start.val()).isBefore(Method.$end.val());
				// 크다면

				var isAfter = moment(start, 'YYYY.MM.DD').isAfter(moment(end, 'YYYY.MM.DD'));

				if (isAfter) {
					var temp = Method.$end.val();
					Method.$end.val(Method.$start.val());
					Method.$start.val(temp);
				}
			},
			getValidateDateInput: function () {
				var start = String(Method.$start.val());
				var end = String(Method.$end.val());

				if (moment(start, 'YYYY.MM.DD').isValid() && moment(end, 'YYYY.MM.DD').isValid()) {
					return true;
				}
				return false;
			},
			searchSubmit: function (start, end, type) {
				var url = sandbox.utils.url.getCurrentUrl();
				url = sandbox.utils.url.removeParamFromURL(url, 'dateType');

				// 전체 검색
				if (_.isUndefined(start)) {
					url = sandbox.utils.url.removeParamFromURL(url, 'stdDate');
					url = sandbox.utils.url.removeParamFromURL(url, 'endDate');
				} else {
					var opt = {
						stdDt: start,
						endDt: end,
						dateType: type
					}

					url = sandbox.utils.url.appendParamsToUrl(url, opt)
				}

				window.location.href = url;

			},
			reset: function () {
				Method.$start.val('').trigger('focusout');
				Method.$end.val('').trigger('focusout');
			}
		}
		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-date-filter1]',
					attrName: 'data-module-date-filter1',
					moduleName: 'module_date_filter1',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);

// as신청조회 1개월로 날자 셋팅,링크
(function (Core) {
	Core.register('module_repair_menu', function (sandbox) {
		var Method = {

			moduleInit: function () {
				var $this = $(this);

				$this.find('#reapir_sch_btn').on('click', function (e) {

					var n = "1";    //1개월.
					var m = "0";
					var date = new Date();
					var start = new Date(Date.parse(date) - n);
					var today = new Date(Date.parse(date) - m * 1000 * 60 * 60 * 24);

					if (n < 10) {
						start.setMonth(start.getMonth() - n);
					}
					var yyyy = start.getFullYear();
					var mm = start.getMonth() + 1;
					var dd = start.getDate();

					var t_yyyy = today.getFullYear();
					var t_mm = today.getMonth() + 1;
					var t_dd = today.getDate();

					//  샘플... stdDt=20180918&endDt=20181018&dateType=1
					stdDt = yyyy + '' + addzero(mm) + '' + addzero(dd);
					endDt = t_yyyy + '' + addzero(t_mm) + '' + addzero(t_dd);
					window.document.location.href = "/kr/ko_kr/account/repairable?stdDt=" + stdDt + "&endDt=" + endDt + "&dateType=1";
					return;
				});

				function addzero(n) {
					return n < 10 ? "0" + n : n;
				}

			}
		}
		return {
			init: function () {
				sandbox.uiInit({
					selector: '[data-module-repair-menu]',
					attrName: 'data-module-repair-menu',
					moduleName: 'module_repair_menu',
					handler: { context: this, method: Method.moduleInit }
				});
			}
		}
	});
})(Core);
