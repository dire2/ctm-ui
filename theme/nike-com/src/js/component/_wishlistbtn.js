(function(Core){
	var WishListBtn = function(){
		'use strict';

		var $this, args, endPoint;
		var setting = {
			selector:'[data-component-wishlistbtn]',
			activeClass:'active'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];
				$this = $(setting.selector);
				endPoint = Core.getComponents('component_endpoint');
				var isAlready = args.isAlready == 'true';
				var md = _GLOBAL.MARKETING_DATA();
				/* wishlist */
				$this.click(function(e){
					e.preventDefault();

					var _self = $(this);
					var index = $(this).closest('[data-product-item]').data('index');

					var query = {
						index: index,
						productId:args.productId
					}

					Core.getModule('module_header').reDirect().setModalHide(true).setLogin(function(data){
						// 여기서 이미 추가된 상품에 대해서 처리해야함
						if(md.pageType == 'cart'){
							if( isAlready == true ){
								UIkit.notify('이미 추가된 상품입니다.', {timeout:3000,pos:'top-center',status:'success'});
								endPoint.call('alreayAddedWishlist', query);
								return;
							}
						}

						Core.Utils.ajax(args.api, 'GET', query, function(data){
							var jsonData = Core.Utils.strToJson(data.responseText, true) || {};
							if(jsonData.hasOwnProperty('error')){
								UIkit.notify(jsonData.error, {timeout:3000,pos:'top-center',status:'warning'});
							}else{
								if(jsonData.isWishListChk){
									_self.find('i').addClass('icon-wishlist_full');
									_self.find('i').removeClass('icon-wishlist');
									UIkit.notify(args.addMsg, {timeout:3000,pos:'top-center',status:'success'});
									isAlready = true;
									endPoint.call('addToWishlist', query);
								}else{
									_self.find('i').addClass('icon-wishlist');
									_self.find('i').removeClass('icon-wishlist_full');
									UIkit.notify(args.removeMsg, {timeout:3000,pos:'top-center',status:'warning'});
									endPoint.call('removeToWishlist', query);
								}
								/*
								if( _.isFunction( marketingAddWishList )){
									marketingAddWishList();
								}
								*/
							}
						});
					});
				});

				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_wishlistbtn'] = {
		constructor:WishListBtn,
		reInit:true,
		attrName:'data-component-wishlistbtn'
	}
})(Core);
