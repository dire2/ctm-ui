(function(Core){
	var Like = function(){
		'use strict';

		var $this, $helpfulBtn, $noHelpfulBtn
		var setting = {
			selector:'[data-component-like]',
			helpfulBtn: '[data-btn-helpful]',
			noHelpfulBtn: '[data-btn-no-helpful]'
		}

		function submitLike(type, url){
			Core.Utils.ajax(url, 'GET', {}, function(data){
				var args = Core.Utils.strToJson(data.responseText, true);

				if(args.result){
					updateLike(args.myHelpful||false, args.myNotHelpful||false, args.helpfulCnt||0, args.notHelpfulCnt||0);
					// _self.fireEvent('likeFeedBack', target, [args]);
				}else{
					UIkit.notify(args.errorMessage, {timeout:3000,pos:'top-center',status:'warning'});
				}
			});
		}

		function updateLike(myHelpful, myNotHelpful, helpfulCnt, notHelpfulCnt){
			$helpfulBtn.removeClass('selected');
			$noHelpfulBtn.removeClass('selected');

			if( myHelpful == true ){
				$helpfulBtn.addClass('selected');
			}else if( myNotHelpful == true ){
				$noHelpfulBtn.addClass('selected');
			}

			$helpfulBtn.find('[data-info-like-number]').text(helpfulCnt);
			$noHelpfulBtn.find('[data-info-like-number]').text(notHelpfulCnt);
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				$this = $(setting.selector);
				$helpfulBtn = $this.find(setting.helpfulBtn);
				$noHelpfulBtn = $this.find(setting.noHelpfulBtn);

				// review js에서 ajax콜 이 후 이벤트 정의가 되지 않는 구조적 문제를 해결하고자 콤포넌트 단위로 likeFeedBack을 정의하는 것으로 변경
				this.addEvent('likeFeedBack', function (data) {
					console.log('data: ', data)
					
					/*
					var feedBackTotal = 0;
					if (data.hasOwnProperty('HELPFUL')) {
						feedBackTotal = parseInt(data.HELPFUL);
					}
					if (data.hasOwnProperty('NOTHELPFUL')) {
						feedBackTotal = parseInt(data.NOTHELPFUL);
					}

					var currentFeedBackTotal = parseInt($(this).find('.num').text()); //현재 카운트

					if( currentFeedBackTotal >= feedBackTotal ){ // 2020-06-03 @pck 서버에서 간혹 추천 개수가 현재와 동일하게 오는 경우를 대비해서 서버 값이 미증가 시 강제로 증가
						currentFeedBackTotal++;
					}else{
						currentFeedBackTotal = feedBackTotal;
					}
					$(this).find('.num').text( String(currentFeedBackTotal) );
					*/
				});
				$helpfulBtn.on('click', function(e){
					e.preventDefault();
					submitLike('helfful', $(this).attr('href'));
				})

				$noHelpfulBtn.on('click', function(e){
					e.preventDefault();
					submitLike('noHelfful', $(this).attr('href'));
				})
				
				/*
				$btn.each(function(i){
					var $this = $(this);
					var url = $this.attr('href');

					$this.off('click').on('click',function(e){
						e.preventDefault();

						var target = this;
						Core.Utils.ajax(url, 'GET', {}, function(data){
							var args = Core.Utils.strToJson(data.responseText, true);

							if(args.result){
								_self.fireEvent('likeFeedBack', target, [args]);
							}else{
								UIkit.notify(args.errorMessage, {timeout:3000,pos:'top-center',status:'warning'});
							}
						});
					});
				});
				
				*/
				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_like'] = {
		constructor:Like,
		reInit:true,
		attrName:'data-component-like'
	}
})(Core);
