/*
    @pck 2020-12-08
    SNKRS MIRRORING
    GLOBAL UI SCRIPT

    **SNKRS 관련 인터렉션만 추가
    **GNB
*/
(function () {
    var snkrs;

    //Selectors
    var snkrsVariables,
        snkrsListItems, filterMenu,
        snkrsPDPGallery, snkrsPDPGallerySwiper, snkrsPDPGalleryFullscreenImage,
        btnMobileGnb, btnToggleListMode,
        mobileGnbElement, mainLayoutOverlay, contentWrapper;

    function querySelector(querySelectorName){
        if(querySelectorName == null || querySelectorName == "" || typeof querySelectorName == "undefined") return false;
        var tmpElement = document.querySelectorAll(querySelectorName);
        if(tmpElement.length > 0){
            if(tmpElement.length == 1){
                return tmpElement[0];
            }else {
                return tmpElement; //배열로 리턴
            }
        }
        return false;
    }

    function setLocalStorage(key, value){
        if( typeof localStorage == 'undefined' || localStorage == null) return false;
        localStorage.setItem(key, value);
    }
    function getLocalStorage(key){
        if( typeof localStorage == 'undefined' || localStorage == null) return false;
        return localStorage.getItem(key);
    }

    function changeListType(type, listItemsArray){
        var listType = 'grid'; //기본 선택 값 grid
        if(type !== null || type !== ''){
            listType = type;
        }

        // grid class 변경
        if(listItemsArray.length > 0){
            switch (listType){
                case 'feed' :
                    //Feed type 처리 부
                    for( i = 0; i < listItemsArray.length; i++ ){
                        listItemsArray[i].classList.remove('pb2-sm', 'va-sm-t', 'ncss-col-sm-6', 'ncss-col-md-3', 'ncss-col-xl-2', 'prl1-sm', 'grid-type');
                        listItemsArray[i].classList.add('pb2-sm', 'va-sm-t', 'ncss-col-sm-12', 'ncss-col-md-6', 'ncss-col-lg-4', 'pb4-md', 'prl0-sm', 'prl2-md', 'ncss-col-sm-6', 'ncss-col-lg-3', 'pb4-md', 'prl2-md','pl0-md', 'pr1-md');
                    }
                    break;
                case 'grid' :
                    //Grid type 처리부
                    for( i = 0; i < listItemsArray.length; i++ ){
                        listItemsArray[i].classList.remove('pb2-sm', 'va-sm-t', 'ncss-col-sm-12', 'ncss-col-md-6', 'ncss-col-lg-4', 'pb4-md', 'prl0-sm', 'prl2-md', 'ncss-col-sm-6', 'ncss-col-lg-3', 'pb4-md', 'prl2-md','pl0-md', 'pr1-md');
                        listItemsArray[i].classList.add('pb2-sm', 'va-sm-t', 'ncss-col-sm-6', 'ncss-col-md-3', 'ncss-col-xl-2', 'prl1-sm', 'grid-type');
                    }
                    break;
            }
        }
    }

    try {
        snkrs = {
            init : function (){
                snkrsVariables = querySelector("form[name=snkrsVariables]") ? querySelector("form[name=snkrsVariables]") : null;
                filterMenu = querySelector(".filters-menu .nav-items a.custom-link") ? querySelector(".filters-menu .nav-items a.custom-link") : null;

                //PDP 부
                snkrsPDPgallery = querySelector('[data-component-gallery]') ? querySelector('[data-component-gallery]') : null;
                snkrsPDPGalleryFullscreenImage = querySelector('[data-ui-gallery-fullscreen-image]') ? querySelector('[data-ui-gallery-fullscreen-image]') : null;

                //모바일 gnb 버튼 부
                btnMobileGnb = querySelector("[data-snkrs-ui-mobile-nav-button]") ? querySelector("[data-snkrs-ui-mobile-nav-button]") : null; //mobile GNB Button
                mainLayoutOverlay = querySelector(".main-layout .content-overlay") ? querySelector(".main-layout .content-overlay") : null; //mobile main overlay

                mobileGnbElement = querySelector("[data-module-mobilegnb]") ? querySelector("[data-module-mobilegnb]") : null; //mobile GNB Element
                contentWrapper = querySelector(".main-layout .content-wrapper") ? querySelector(".main-layout .content-wrapper") : null; //content wrapper

                btnToggleListMode = querySelector("[data-snkrs-ui-toggle-listmode]") ? querySelector("[data-snkrs-ui-toggle-listmode]") : null; //mobile GNB Button

                var listType = getLocalStorage('listType');
                //저장된 값이 있을 때 grid 타입 설정
                if(listType !== null){
                    switch (listType){
                        case 'feed' :
                            snkrs.GNB.toggleGridMode('feed');
                            break;
                        case 'grid' :
                            snkrs.GNB.toggleGridMode('grid');
                            break;
                    }
                }else{
                    setLocalStorage('listType', 'grid');
                }

                //모바일 GNB Toggle
                if(btnMobileGnb !== null){
                    btnMobileGnb.addEventListener("click", function(){
                        snkrs.GNB.toggleMobileGnb();
                    });
                }
                if(mainLayoutOverlay !== null){
                    mainLayoutOverlay.addEventListener("click", function(){
                        snkrs.GNB.toggleMobileGnb();
                    });
                }

                //토글 버튼
                if(btnToggleListMode !== null){
                    btnToggleListMode.addEventListener("click", function(){
                        var listType = 'grid';
                        if(getLocalStorage('listType') !== ''){
                            listType = getLocalStorage('listType') == 'feed' ? 'grid' : 'feed';
                        }
                        snkrs.GNB.toggleGridMode(listType);
                    });
                }

                //PDP sticky init
                this.PDP.scrollSticky();

                //PDP Gallery Swiper init
                if(snkrsPDPgallery !== null && snkrsPDPgallery.dataset.componentGallery.indexOf("snkrs") !== -1){ //snkrs type gallery에서만 작동
                    this.PDP.initGallerySwiper();
                }

                return snkrs;
            },
            VALUE : {
              getSwiper : function (){
                  return snkrsPDPGallerySwiper;
              },
              setSwiper : function (swiper){
                  snkrsPDPGallerySwiper = swiper;
              }
            },
            UTIL : {
                isMobile : function (){
                    var isMobileMatche = false;

                    if(window.matchMedia !== null){
                        isMobileMatche = window.matchMedia('only screen and (max-width: 1024px)').matches;
                    }

                    return isMobileMatche;
                }
            },
            GNB : {
                toggleMobileGnb : function (){
                    if(btnMobileGnb == null) return false;
                    if(mainLayoutOverlay == null) return false;
                    if(mobileGnbElement == null) return false;
                    if(contentWrapper == null) return false;

                    if( mobileGnbElement.classList.contains("show") ){
                        mobileGnbElement.classList.remove("show");
                        mainLayoutOverlay.classList.add("hide");
                        contentWrapper.classList.remove("hide");
                    } else { //모바일 GNB Show
                        mobileGnbElement.classList.add("show");
                        mainLayoutOverlay.classList.remove("hide");
                        mainLayoutOverlay.classList.remove("hide");
                        contentWrapper.classList.add("hide");
                    }
                },
                toggleGridMode : function (type){ // listMode : 'feed' or 'grid'
                    if(btnToggleListMode == null) return false;
                    snkrsListItems = querySelector("[data-component-launchitem]") ? querySelector("[data-component-launchitem]") : null; //SNKRS category list items;

                    var listType = 'grid'; //기본 선택 값 grid
                    if(type !== null || type !== ''){
                        listType = type;
                    }

                    //List Icon
                    var feedIcon = '<svg width="18px" height="18px" fill="#757575" class="feed-icon" viewBox="0 0 24 24"><path d="M0 18.64h24V24H0v-5.36zM0 0h24v16H0V0z"></path></svg>';
                    //Grid Icon
                    var gridIcon = '<svg width="18px" height="18px" fill="#757575" class="grid-icon" viewBox="0 0 24 24"><path d="M0 13.36h10.64V24H0V13.36zM13.36 0H24v10.64H13.36V0zm0 13.36H24V24H13.36V13.36zM0 0h10.64v10.64H0V0z"></path></svg>';

                    if(listType == 'feed'){ //feed 모드
                        btnToggleListMode.innerHTML = gridIcon;
                        if(btnToggleListMode.dataset.snkrsUiToggleListmode !== null) {
                            btnToggleListMode.dataset.snkrsUiToggleListmode = 'true';
                        }
                        btnToggleListMode.ariaLabel = '목록으로 제품보기';
                        setLocalStorage('listType', 'feed');
                        changeListType('feed', snkrsListItems);
                    }else{ //grid 모드
                        btnToggleListMode.innerHTML = feedIcon;
                        if(btnToggleListMode.dataset.snkrsUiToggleListmode !== null){
                            btnToggleListMode.dataset.snkrsUiToggleListmode = 'false';
                        }
                        btnToggleListMode.ariaLabel = '그리드로 제품보기';
                        setLocalStorage('listType', 'grid');
                        changeListType('grid', snkrsListItems);
                    }

                    //Lazy control
                    $('.launch-category .img-component').Lazy({
                        visibleOnly: true,
                        scrollDirection: 'vertical',
                        afterLoad: function() {
                            $('.launch-category .launch-list-item').addClass('complete');
                        },
                    });
                }
            },
            PDP : {
                scrollSticky : function (){
                    /*
                    SNKRS DRAW 시 PDP 내용이 짧으면 우측 부 DRAW신청 버튼 및 사이즈 선택부 가용 여백이 확보가 되지 않아서 클릭이 안되는 문제 해결
                    기존 오프셋을 이용한 세로 중앙정렬 방식에서 STICKY 방식으로 변경
                     */
                    var stickyEl = $('.card-product-component .fixie'),
                        stickyEloffset = stickyEl.offset(),
                        stickyTargetEl = $('.lc-prd-conts .prd-img-wrap'),
                        scrollDirectionOffset = $(window).scrollTop();

                        if(document.querySelector('[data-module-launchproduct]') == null) return false; // PDP 아닐 때 return

                    $(window).scroll(function(){
                        window.requestAnimationFrame(function () {
                            rightSideSticky($(window).scrollTop());
                        });
                    });

                    function rightSideSticky (offsetY){
                        var isMobile = window.matchMedia("(max-width: 1024px)")

                        if(!isMobile.matches){ //모바일이 아닐 때만 실행
                            if( (stickyEl == null) && (stickyEloffset == null) ) return;

                            (offsetY > stickyTargetEl.offset().top) ? stickyEl.addClass('sticky') : stickyEl.removeClass('sticky');

                            var scrollDirection = (offsetY >= scrollDirectionOffset) ? 'down' : 'up';
                            scrollDirectionOffset = offsetY;

                            var docViewBottom = offsetY + $(window).height(),
                                stickyTargetElBottomoffset = stickyTargetEl.offset().top + stickyTargetEl.outerHeight(),
                                bottomGap = ( $(window).height() - stickyEl.outerHeight() ) / 2;

                            if(scrollDirection == 'down'){
                                if( (stickyTargetElBottomoffset + bottomGap) <= docViewBottom ) {
                                    if(!stickyEl.hasClass('end'))stickyEl.addClass('end');
                                }
                            }else{
                                if( (stickyTargetElBottomoffset + bottomGap) > docViewBottom ) {
                                    if(stickyEl.hasClass('end')) stickyEl.removeClass('end');
                                }
                            }
                        }else{
                            if(stickyEl.hasClass('sticky')) stickyEl.removeClass('sticky');
                            if(stickyEl.hasClass('end')) stickyEl.removeClass('end');
                        }
                    }

                    rightSideSticky($(window).scrollTop());	//첫 load 시 스크롤 상태 확인
                },
                initGallerySwiper : function (){
                    // optimize resize
                    var optimizedResize = (function() {
                        var callbacks = [], running = false;

                        function resize() {
                            if (!running) {
                                running = true;

                                if (window.requestAnimationFrame) {
                                    window.requestAnimationFrame(runCallbacks);
                                } else {
                                    setTimeout(runCallbacks, 66);
                                }
                            }
                        }

                        function runCallbacks() {
                            if (window.NodeList && !NodeList.prototype.forEach) {
                                NodeList.prototype.forEach = Array.prototype.forEach;
                            }
                            callbacks.forEach(function(callback) {callback();});
                            running = false;
                        }

                        function addCallback(callback) {
                            if (callback) {
                                callbacks.push(callback);
                            }
                        }

                        return {
                            add: function(callback) {
                                if (!callbacks.length) {
                                    window.addEventListener('resize', resize);
                                }
                                addCallback(callback);
                            }
                        }
                    }());

                    //리사이즈 시 PC모드에서 모바일 모드 진입 시작 점 체크 후 init
                    optimizedResize.add(function() {
                        var isSwiper = null;
                        isSwiper = snkrs.VALUE.getSwiper();

                        function initSwiper(){
                            var swiper = new Swiper('.snkrs-gallery-swiper', {
                                scrollbar: {el: '.snkrs-gallery-swiper-scrollbar', hide: false,},
                            });
                            snkrs.VALUE.setSwiper(swiper);
                        }

                        if (snkrs.UTIL.isMobile()) { // 모바일일 때 init
                            if(typeof isSwiper == 'undefined'){
                                initSwiper();
                            }else{
                                if(snkrs.VALUE.getSwiper().destroyed){
                                    initSwiper();
                                }
                            }

                            if($('[data-scrollbar]').length === 1 && typeof Scrollbar == "function"){
                                if(Scrollbar.destroyAll() !== null)
                                    Scrollbar.destroyAll();
                            }
                        } else { // PC모드일 때 인스턴스 삭제
                            if ( typeof isSwiper !== 'undefined' ){
                                snkrs.VALUE.getSwiper().destroy();
                            }

                            if($('[data-scrollbar]').length === 1 && typeof Scrollbar == "function"){
                                if(Scrollbar.initAll() !== null)
                                    Scrollbar.initAll();
                            }
                        }
                    });

                    //초기 init
                    if(snkrs.UTIL.isMobile()){ // 모바일일 때 init
                        snkrsPDPGallerySwiper = new Swiper('.snkrs-gallery-swiper', {
                            scrollbar: {el: '.snkrs-gallery-swiper-scrollbar', hide: false,},
                        });
                    }
                    if(!snkrs.UTIL.isMobile()) { // PC일 때 init
                        if($('[data-scrollbar]').length === 1 && typeof Scrollbar == "function"){
                            if(Scrollbar.initAll() !== null)
                                Scrollbar.initAll();
                        }
                    }

                    if(snkrsPDPGalleryFullscreenImage !== null){
                        for(var i = 0; i < snkrsPDPGalleryFullscreenImage.length ; i++){

                            snkrsPDPGalleryFullscreenImage[i].addEventListener('click', function(){

                                var _self = this;
                                //PDP 갤러리 이미지 클릭 시 전체 화면 상품 슬라이더
                                if (!snkrs.UTIL.isMobile()) { //PC 일 때만 작동
                                    var snkrsPDPGalleryFullscreenImageSwiper = new Swiper('.snkrs-gallery-fullscreen-swiper', {
                                        centeredSlides: true,
                                        slidesPerView: 1,
                                        navigation: {nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev'},
                                        scrollbar: {el: '.swiper-scrollbar'},
                                        on: {
                                            init: function () {
                                                var images = querySelector('[data-ui-gallery-fullscreen-image]');
                                                var selectedImageIndex = 0;
                                                if(images !== null){
                                                    for(var i = 0 ; i < images.length ; i++){
                                                        if(images[i] === _self){
                                                            selectedImageIndex = i;
                                                        }
                                                    }
                                                }
                                                this.slideTo(selectedImageIndex);

                                                var fullscreenContainer = querySelector('.carousel.full-screen');
                                                if (fullscreenContainer !== null) {
                                                    fullscreenContainer.classList.add('show');
                                                }

                                                var closeButton = querySelector('.full-screen-close');
                                                if (closeButton !== null) {
                                                    closeButton.addEventListener('click', function () {
                                                        var fullscreenContainer = querySelector('.carousel.full-screen');
                                                        fullscreenContainer.classList.remove('show');
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            });

                        }
                    }

                }
            }
        }
    } catch (error) {
        console.error("SNKRS js Error" + error)
    }

    document.addEventListener("DOMContentLoaded", function () {
        if(document.body.classList.contains('snkrs')){  // body에 snkrs class 체크 후 init
             snkrs.init();
        }
    });
})();