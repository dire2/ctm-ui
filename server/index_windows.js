
var fs = require('fs');
var path = require('path');
var mysql = require('mysql');
var xml2js = require('xml2js');
var xmlBuild2 = require('xmlbuilder');

var xmlParser = new xml2js.Parser({explicitArray:false, mergeAttrs:true});
var xmlBuild = new xml2js.Builder({pretty:false});
var dt = require('date-utils');

/***
utils
***/
// 지정된 포맷으로 현재 일시 반환
function getDate(format) {
	var d = new Date();
	return d.toFormat(format);
}
function logTime() {
	return "["+getDate('HH24:MI:SS')+"] ";
}
function isString(v) { return typeof v === 'string'; }
function log(arg, arg2) {
	if ( arg2 === void 0 ){
		if( typeof arg === 'string' ) {
			console.log(logTime(), arg);
		} else {
			console.log(logTime(), arg);
		}
	} else {
		if( typeof arg === 'string' && typeof arg2 === 'string') {
			console.log(logTime(), arg, arg2);
		} else if (typeof arg === 'string') {
			console.log(logTime(), arg, arg2);
			console.log(arg2);
		}
	}
}
function getValue(arg) {
	if( arg !== void 0 && arg !== null) {
		return arg.value();
	}
	return "";
}



// 설정 정보
var serverConfiguration = require( "../server/server-config" );


// pool 생성
var connectionPool = mysql.createPool(serverConfiguration.gulp.db);
var metaXml = '';
var themeXml = '';
var metaMap = {};
var themeInfo = {};

function mergeMeta( deployPath, filePath, themeFilePath, type, themeName ){

	if( filePath != null ){

		try {
			fs.readFile(filePath, {encoding: 'utf-8'}, function(err,fdata){
				if( !err ){
					var metaInfo = {};
					metaXml = fdata;

					xmlParser.parseString(metaXml, function(err, result){
						metaInfo = result;
						metaInfo['theme']['name'] = themeName || metaInfo['theme']['name'];
					});

					if( themeFilePath != null ){
						try {
							var themeXml = fs.readFileSync(themeFilePath, 'utf-8' );
							xmlParser.parseString(themeXml, function(err, result){
								for(var key in result.theme){
									if(key === 'page' || key === 'template'){
										metaInfo['theme'][key] = metaInfo['theme'][key].concat(result['theme'][key]);
									}else{
										metaInfo['theme'][key] = result['theme'][key];
									}
								}
							});

							var root = xmlBuild2.create('theme');
							for(var key in metaInfo['theme']){
								if(key === 'page' || key === 'template'){
									for(var i=0; i < metaInfo['theme'][key].length; i++){
										var childrenNode = root.ele(key)
										for(var key2 in metaInfo['theme'][key][i]){
											childrenNode.att(key2, metaInfo['theme'][key][i][key2]);
										}
									}
								}else{
									root.att(key, metaInfo['theme'][key]);
								}
							}

							metaXml = root.end({pretty:true});

						}catch (err) {
							log( '테마 xml 없음' );
						}
					}else{
						log('themeFilePath 없음');
					}

					if( type === 'build'){
						//console.log(xmlBuild.buildObject(metaInfo));
						//console.log(deployPath+'metadata.xml');
						fs.writeFile(deployPath+'metadata.xml', metaXml, encoding='utf8');
					}

					metaMap = metaInfo;
				}else{
					console.log('read File error');
				}
			});
		} catch (err) {
			log(err);
		}
	}

}

function upload( changePath, basePath, fileType ) {
	changePath = changePath.split(String.fromCharCode(92)).join('/');
	basePath = basePath.split(String.fromCharCode(92)).join('/');
		
	var realPath = "/" + changePath.replace( basePath, "").replace("."+fileType, "");
	//realPath = realPath.split(String.fromCharCode(92)).join('/')
	log("changePath ", changePath);
	log("basePath ", basePath);
	log("realPath ", realPath);
	log("themeInfo ", themeInfo);

	fs.readFile(changePath, {encoding: 'utf-8'}, function(err,data){
		if (!err){
			var replaceData = data.split('href="/assets/').join('href="/cmsstatic/theme/' + metaMap.theme.name + '/assets/');
			replaceData = replaceData.split("href='/assets/").join("href='/cmsstatic/theme/" + metaMap.theme.name + "/assets/");

			replaceData = replaceData.split('src="/assets/').join('src="/cmsstatic/theme/' + metaMap.theme.name + '/assets/');
			replaceData = replaceData.split("src='/assets/").join("src='/cmsstatic/theme/" + metaMap.theme.name + "/assets/");

			replaceData = replaceData.split('url("/assets/').join('url("/cmsstatic/theme/' + metaMap.theme.name + '/assets/');
			replaceData = replaceData.split("url('/assets/").join("url('/cmsstatic/theme/" + metaMap.theme.name + "/assets/");
			replaceData = replaceData.split('url(/assets/').join('url(/cmsstatic/theme/' + metaMap.theme.name + '/assets/');

			//log('replaceData', replaceData )
			updateContent(realPath, replaceData);
		}else{
			log(err);
		}
	});
}

function updateContent(realPath, content) {
	// DB 커넥션 풀에서 커넥션 얻기
	try {
		connectionPool.getConnection(function(connectionError,connection){
			if(connectionError) {
				log(connectionError);
				return;
			}

			//var updateColumns = {tmplt_source : content, date_updated : 'now()'};

			// insert & update query
			var queryString =
				// ' select b.page_tmplt_id' +
				// ' from brzc_page_tmplt b' +
				// ' where b.page_tmplt_id = (' +
				// 	' select a.page_tmplt_id ' +
				// 	' from blc_page_tmplt a' +
				// 	' where a.tmplt_path = ?' +
				// 	' and a.page_tmplt_id > 1' +
				// 	' limit 1' +
				// ' )';
// 여러 테마가 있더라도 동일한 경로면 다 변경 처리
				' update brzc_page_tmplt' +
				'    set tmplt_source = ?' +
				'      , date_updated = now()' +
				'  where page_tmplt_id in (' +
					' select a.page_tmplt_id ' +
					' from blc_page_tmplt a' +
					' where a.tmplt_path = ?' +
					' and brzc_page_tmplt.theme_id > 1'+
					' and a.page_tmplt_id > -10000' +  // admin theme 충돌문제가 있어 임시로 특정 번호 이상만 변경 처리
//					' limit 1' +
				' )';

			// run query
			log("(---- inner query ==> ");
			//log("---- select a.page_tmplt_id from blc_page_tmplt a where a.tmplt_path = '" +realPath +  "' and a.page_tmplt_id > 0 limit 1");
			// 여러 테마가 있더라도 동일한 경로면 다 변경 처리
			log("---- select a.page_tmplt_id from blc_page_tmplt a where a.tmplt_path = '" +realPath +  "'");
			var query = connection.query(queryString, [content, realPath], function(queryError, result) {

				//log("mysql query", query.sql);

				// 에러 발생 시
				if(queryError){
					// connection 반환
					connection.release();
					log(queryError);
					return;
				}

				// connection 반환
				connection.release();

				log("result=====================> " + realPath);
				log(result);
				log("\n\n\n");
			});
		});
	} catch (err) {
		connection.release();
		log(err);
	}
}

function upload_html(args, basePath) {
	log(args);
	upload( args.path, basePath, "html" );
}
// js, css 변경 감지도 추가 할것.
// 단, theme upload 되야 함.
// 못찾는 정보의 경우 예외처리 필요..
function upload_js(args, basePath) {
	log(args);

	// mockData 예외처리
	if(args.path.indexOf("/mockData/") > -1 ) {
		console.log("====> mockData 처리 예외처리");
		return;
	}

	upload( args.path, basePath, "js" );
}
function upload_css(args, basePath) {
	log(args);
	upload( args.path, basePath, "css" );
}



module.exports = {
	upload_html : upload_html,
	upload_js : upload_js,
	upload_css : upload_css,
	mergeMeta : mergeMeta
}


// exports.start = function (args) {
// }
